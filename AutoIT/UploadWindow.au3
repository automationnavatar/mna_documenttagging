Sleep(5000)
If WinExists("Upload Documents - Mozilla Firefox") Then
   WinWait("Upload Documents - Mozilla Firefox","","10")
   WinActivate("Upload Documents - Mozilla Firefox")
EndIf
If WinExists("Upload Documents - Google Chrome") Then
   WinWait("Upload Documents - Google Chrome","","10")
   WinActivate("Upload Documents - Google Chrome")
EndIf
If WinExists("Upload Documents ‎- Microsoft Edge") Then
   WinWait("Upload Documents - Microsoft Edge","","10")
   WinActivate("Upload Documents ‎- Microsoft Edge")
EndIf