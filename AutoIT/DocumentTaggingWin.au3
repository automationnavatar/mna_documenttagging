Sleep(5000)
If WinExists("Tag Documents - Mozilla Firefox") Then
   WinWait("Tag Documents - Mozilla Firefox","","10")
   WinActivate("Tag Documents - Mozilla Firefox")
EndIf
If WinExists("Tag Documents - Google Chrome") Then
   WinWait("Tag Documents - Google Chrome","","10")
   WinActivate("Tag Documents - Google Chrome")
EndIf
If WinExists("Tag Documents - Microsoft Edge") Then
   WinWait("Tag Documents - Microsoft Edge","","10")
   WinActivate("Tag Documents - Microsoft Edge")
EndIf