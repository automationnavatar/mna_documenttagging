/**
 * 
 */
package com.navatar.generic;

import static com.navatar.generic.CommonLib.*;

import java.util.ArrayList;
import java.util.List;

import com.navatar.generic.EnumConstants.excelLabel;
import com.navatar.scripts.SmokeTestCases;

import static com.navatar.generic.BaseLib.*;



public class SmokeCommonVariables {
	
	public static String appName;
	public static String superAdminUserName,superAdminRegistered,superAdminFirstName,superAdminLastName,adminPassword,browserToLaunch,BoxUserEmail,BoxPassword;
	public static String crmUser1FirstName,crmUser1LastName,crmUser1EmailID,crmUserProfile,crmUserLience;
	
	public static String externalUserFirstName,externalUserLastName,externalUserEmailID, externalUserProfile,externalUserLience,externalUserRole;
	
	public static String gmailUserName,gmailUserName2,gmailPassword;
	
	public static String SmokeAcc1;
	public static String SmokeAcc2;
	
	public static String SmokeTarget1,SmokeTarget1_DealName,SmokeTarget1_AccName;
	
	public static String SmokeDeal1Name,SmokeDeal1_DealType,SmokeDeal1_Status,SmokeDeal1_Stage,Smoke_Deal1UpdatedName;
	public static String SmokeDeal2Name,SmokeDeal2_DealType,SmokeDeal2_Status,SmokeDeal2_Stage;
	public static String SmokeDeal3Name,SmokeDeal3_DealType,SmokeDeal3_Status,SmokeDeal3_Stage;
	
	
	public static String SmokeC1_FName,SmokeC1_LName,SmokeC1_AccName,SmokeC1_EmailId,smokeC1_Profile;
	
	
	
	public static String Smoke_Project1Name,SmokeProject1_DealName;
	public static String Smoke_Project2Name,SmokeProject2_DealName;
	
	public static String Smoke_Task1Name,Smoke_Task1ProjectName,Smoke_Task1Status,smoke_Task1EndDate;
	public static String Smoke_Task2Name,Smoke_Task2ProjectName,Smoke_Task2Status;
	public static String Smoke_Task3Name,Smoke_Task3ProjectName,Smoke_Task3Status;
	public static String Smoke_Task4Name,Smoke_Task4ProjectName,Smoke_Task4Status,Smoke_Task4EndDate;
	public static String Smoke_Task5Name,Smoke_Task5ProjectName,Smoke_Task5Status,Smoke_Task5EndDate;
	public static String Smoke_Task6Name,Smoke_Task6ProjectName,Smoke_Task6Status,Smoke_Task6EndDate;
	
	
	public static String NavatarStepPageURL,CommUnityUserURL;
	public static String EnvironmentVariable,ModeVariable;
	
	
	
	public SmokeCommonVariables(Object obj) {
		// TODO Auto-generated constructor stub
		long StartTime = System.currentTimeMillis();
		if(obj instanceof SmokeTestCases){
		System.err.println("smokeExcelPathCommonVariable : "+testCasesFilePath);
		
		//****************************************************************	SuperAdmin And CRM User **********************************************************//
		
		superAdminUserName=ExcelUtils.readDataFromPropertyFile("SuperAdminUsername");
		superAdminRegistered=ExcelUtils.readDataFromPropertyFile("SuperAdminRegistered");
		BoxUserEmail=ExcelUtils.readDataFromPropertyFile("BoxUserEmail");
		BoxPassword=ExcelUtils.readDataFromPropertyFile("boxPassword");
		adminPassword=ExcelUtils.readDataFromPropertyFile("password");
		gmailUserName=ExcelUtils.readDataFromPropertyFile("gmailUserName");
		gmailUserName2=ExcelUtils.readDataFromPropertyFile("gmailUserName2");
		gmailPassword=ExcelUtils.readDataFromPropertyFile("gmailPassword");
			NavatarStepPageURL=ExcelUtils.readDataFromPropertyFile("NavatarStepPageURL");
		superAdminFirstName= ExcelUtils.readData(testCasesFilePath,"Users",excelLabel.Variable_Name, "AdminUser", excelLabel.User_First_Name); 
		superAdminLastName= ExcelUtils.readData(testCasesFilePath,"Users",excelLabel.Variable_Name, "AdminUser", excelLabel.User_Last_Name); 
		crmUser1FirstName=ExcelUtils.readData(testCasesFilePath,"Users",excelLabel.Variable_Name, "User1", excelLabel.User_First_Name);
		crmUser1LastName=ExcelUtils.readData(testCasesFilePath,"Users",excelLabel.Variable_Name, "User1", excelLabel.User_Last_Name);
		crmUser1EmailID=ExcelUtils.readData(testCasesFilePath,"Users",excelLabel.Variable_Name, "User1", excelLabel.User_Email);
		crmUserProfile=ExcelUtils.readData(testCasesFilePath,"Users",excelLabel.Variable_Name, "User1", excelLabel.User_Profile);
		crmUserLience=ExcelUtils.readData(testCasesFilePath,"Users",excelLabel.Variable_Name, "User1", excelLabel.User_License);
		
		externalUserFirstName=ExcelUtils.readData(testCasesFilePath,"Users",excelLabel.Variable_Name, "User2", excelLabel.User_First_Name);
		externalUserLastName=ExcelUtils.readData(testCasesFilePath,"Users",excelLabel.Variable_Name, "User2", excelLabel.User_Last_Name);
		externalUserEmailID=ExcelUtils.readData(testCasesFilePath,"Users",excelLabel.Variable_Name, "User2", excelLabel.User_Email);
		externalUserProfile=ExcelUtils.readData(testCasesFilePath,"Users",excelLabel.Variable_Name, "User2", excelLabel.User_Profile);
		externalUserLience=ExcelUtils.readData(testCasesFilePath,"Users",excelLabel.Variable_Name, "User2", excelLabel.User_License);
		externalUserRole=ExcelUtils.readData(testCasesFilePath,"Users",excelLabel.Variable_Name, "User2", excelLabel.Role);
		
		browserToLaunch = ExcelUtils.readDataFromPropertyFile("Browser");
		appName=ExcelUtils.readDataFromPropertyFile("appName");
		EnvironmentVariable=ExcelUtils.readDataFromPropertyFile("Environment");
		ModeVariable=ExcelUtils.readDataFromPropertyFile("Mode");
		CommUnityUserURL=ExcelUtils.readDataFromPropertyFile("CommUnityUserURL");
		
		
		//INS1..............
		SmokeAcc1=ExcelUtils.readData(testCasesFilePath,"Account",excelLabel.Variable_Name, "SmokeAcc1", excelLabel.Account_Name);
		SmokeAcc2=ExcelUtils.readData(testCasesFilePath,"Account",excelLabel.Variable_Name, "SmokeAcc2", excelLabel.Account_Name);
		
	
		//***************************************** Deal *************************************************//
		

		SmokeDeal1Name=ExcelUtils.readData(testCasesFilePath,"Deal",excelLabel.Variable_Name, "SmokeD1", excelLabel.Deal_Name);
		SmokeDeal1_DealType=ExcelUtils.readData(testCasesFilePath,"Deal",excelLabel.Variable_Name, "SmokeD1", excelLabel.Deal_Type);
		SmokeDeal1_Status=ExcelUtils.readData(testCasesFilePath,"Deal",excelLabel.Variable_Name, "SmokeD1", excelLabel.Status);
		SmokeDeal1_Stage=ExcelUtils.readData(testCasesFilePath,"Deal",excelLabel.Variable_Name, "SmokeD1", excelLabel.Stage);
		Smoke_Deal1UpdatedName=ExcelUtils.readData(testCasesFilePath,"Deal",excelLabel.Variable_Name, "SmokeD1", excelLabel.UpdatedDeal_Name);
		
		SmokeDeal2Name=ExcelUtils.readData(testCasesFilePath,"Deal",excelLabel.Variable_Name, "SmokeD2", excelLabel.Deal_Name);
		SmokeDeal2_DealType=ExcelUtils.readData(testCasesFilePath,"Deal",excelLabel.Variable_Name, "SmokeD2", excelLabel.Deal_Type);
		SmokeDeal2_Status=ExcelUtils.readData(testCasesFilePath,"Deal",excelLabel.Variable_Name, "SmokeD2", excelLabel.Status);
		SmokeDeal2_Stage=ExcelUtils.readData(testCasesFilePath,"Deal",excelLabel.Variable_Name, "SmokeD2", excelLabel.Stage);
		
		
		SmokeDeal3Name=ExcelUtils.readData(testCasesFilePath,"Deal",excelLabel.Variable_Name, "SmokeD3", excelLabel.Deal_Name);
		SmokeDeal3_DealType=ExcelUtils.readData(testCasesFilePath,"Deal",excelLabel.Variable_Name, "SmokeD3", excelLabel.Deal_Type);
		SmokeDeal3_Status=ExcelUtils.readData(testCasesFilePath,"Deal",excelLabel.Variable_Name, "SmokeD3", excelLabel.Status);
		SmokeDeal3_Stage=ExcelUtils.readData(testCasesFilePath,"Deal",excelLabel.Variable_Name, "SmokeD3", excelLabel.Stage);
		
		
		//***************************************** Target *************************************************//
		
		
		SmokeTarget1 = ExcelUtils.readData(testCasesFilePath,"Target",excelLabel.Variable_Name, "SmokeT1", excelLabel.Target_Name);
		SmokeTarget1_DealName=SmokeDeal1Name;
		SmokeTarget1_AccName=SmokeAcc1;
		
		
		//***************************************** Contact *************************************************//
		
		
		SmokeC1_FName=ExcelUtils.readData(testCasesFilePath,"Contact",excelLabel.Variable_Name, "SmokeC1", excelLabel.Contact_FirstName);
		SmokeC1_LName=ExcelUtils.readData(testCasesFilePath,"Contact",excelLabel.Variable_Name, "SmokeC1", excelLabel.Contact_LastName);
		SmokeC1_AccName=SmokeAcc1;
		SmokeC1_EmailId=ExcelUtils.readData(testCasesFilePath,"Contact",excelLabel.Variable_Name, "SmokeC1", excelLabel.Contact_EmailId);
		smokeC1_Profile=ExcelUtils.readData(testCasesFilePath,"Contact",excelLabel.Variable_Name, "SmokeC1", excelLabel.Profile);
		
		//***************************************** Project *************************************************//
		
			
		Smoke_Project1Name = ExcelUtils.readData(testCasesFilePath,"Project",excelLabel.Variable_Name, "SmokePR1", excelLabel.Project_Name);
		SmokeProject1_DealName=SmokeDeal1Name;
		Smoke_Project2Name = ExcelUtils.readData(testCasesFilePath,"Project",excelLabel.Variable_Name, "SmokePR2", excelLabel.Project_Name);
		SmokeProject2_DealName=SmokeDeal3Name;
		//***************************************** Task *************************************************//
		
		Smoke_Task1Name = ExcelUtils.readData(testCasesFilePath,"Task",excelLabel.Variable_Name, "SmokeTask1", excelLabel.Task_Name);

		
		
		Smoke_Task1ProjectName = Smoke_Project1Name;
		Smoke_Task1Status = ExcelUtils.readData(testCasesFilePath,"Task",excelLabel.Variable_Name, "SmokeTask1", excelLabel.Status);
		smoke_Task1EndDate = ExcelUtils.readData(testCasesFilePath,"Task",excelLabel.Variable_Name, "SmokeTask1", excelLabel.End_Date);
		
		
		Smoke_Task2Name = ExcelUtils.readData(testCasesFilePath,"Task",excelLabel.Variable_Name, "SmokeTask2", excelLabel.Task_Name);
		Smoke_Task2ProjectName = Smoke_Project2Name;
		Smoke_Task2Status = ExcelUtils.readData(testCasesFilePath,"Task",excelLabel.Variable_Name, "SmokeTask2", excelLabel.Status);
		
		
		Smoke_Task3Name = ExcelUtils.readData(testCasesFilePath,"Task",excelLabel.Variable_Name, "SmokeTask3", excelLabel.Task_Name);
		Smoke_Task3ProjectName = Smoke_Project1Name;
		Smoke_Task3Status = ExcelUtils.readData(testCasesFilePath,"Task",excelLabel.Variable_Name, "SmokeTask3", excelLabel.Status);
		
		Smoke_Task4Name = ExcelUtils.readData(testCasesFilePath,"Task",excelLabel.Variable_Name, "SmokeTask4", excelLabel.Task_Name);
		Smoke_Task4ProjectName = Smoke_Project2Name;
		Smoke_Task4Status = ExcelUtils.readData(testCasesFilePath,"Task",excelLabel.Variable_Name, "SmokeTask4", excelLabel.Status);
		Smoke_Task4EndDate = ExcelUtils.readData(testCasesFilePath,"Task",excelLabel.Variable_Name, "SmokeTask4", excelLabel.End_Date);
		
		
		Smoke_Task5Name = ExcelUtils.readData(testCasesFilePath,"Task",excelLabel.Variable_Name, "SmokeTask5", excelLabel.Task_Name);
		Smoke_Task5ProjectName = Smoke_Project1Name;
		Smoke_Task5Status = ExcelUtils.readData(testCasesFilePath,"Task",excelLabel.Variable_Name, "SmokeTask5", excelLabel.Status);
		Smoke_Task5EndDate = ExcelUtils.readData(testCasesFilePath,"Task",excelLabel.Variable_Name, "SmokeTask5", excelLabel.End_Date);
		
		Smoke_Task6Name = ExcelUtils.readData(testCasesFilePath,"Task",excelLabel.Variable_Name, "SmokeTask6", excelLabel.Task_Name);
		Smoke_Task6ProjectName =Smoke_Project2Name;
		Smoke_Task6Status = ExcelUtils.readData(testCasesFilePath,"Task",excelLabel.Variable_Name, "SmokeTask6", excelLabel.Status);
		Smoke_Task6EndDate = ExcelUtils.readData(testCasesFilePath,"Task",excelLabel.Variable_Name, "SmokeTask6", excelLabel.End_Date);

		
		
		
		AppListeners.appLog.info("Done with intialization in Smoke Test Variable. Enjoy the show.\nTotal Time Taken: "+((System.currentTimeMillis()-StartTime)/1000)+" seconds.");
		}
			
	}
		

	
}
