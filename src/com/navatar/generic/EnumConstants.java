package com.navatar.generic;


public class EnumConstants {

	/**
	 * 
	 * @author Ankur Rana
	 * @description static enums
	 */
	public static enum action {
		ACCEPT, DECLINE, GETTEXT, THROWEXCEPTION, BOOLEAN, SCROLL, SCROLLANDTHROWEXCEPTION, SCROLLANDBOOLEAN;
	}

	public static enum excelLabel {
		Variable_Name,Institutions_Name,Contact_FirstName,Contact_LastName,Contact_EmailId, Contact_password_updated,Registered,Fund_Name,FundRaising_Name,
		Limited_Partner,PartnerShip_Name,Commitment_ID,User_First_Name,User_Last_Name,User_Email,User_Profile,User_License,Fund_Type,Firm_Name,UploadedFileInternal,UploadedFileShared,UploadedFileStandard,UploadedFileGlobal,UpdatedFileCommon,UpdatedFileInternal,UpdatedFileShared,UpdatedFileStandard,TestCases_Name ,Alert_Count,AllDocument_Count,
		Folder_Template_Name,Deal_Contact,Deal_Phone,Deal_Email,Firm_Description,ContactNew_fName, ContactNew_lName, Contact_Title, Contact_Phone, Facebook, Mailing_Street, Linkedin, Institution_Type_MyProfile, Industry_Dropdown_myprofile, FundType_myprofile, IndustrySelectBox_myprofile,Investment_Category, GeoFocus_myprofile, Firm_Description_Firmprofile, Billing_street_Firmprofile,
		Billing_city_firmprofile, Billing_state_firmprofile, Billing_country_firmprofile, Industries_selectbox, FundType_selectbox, Geofocus_selectbox, Industry_dropdown, InstType_dropdown, Min_investment, Max_investment, AUM,Contact_updated_firmname,
		Folder_Description,Fund_Size,Fund_VintageYear,Deal_Description,Disclaimer_Name,Disclaimer_Description,StandardPath,GlobalPath,SharedPath,InternalPath,MyProfile_FName,MyProfile_LName,Updated_FirmName,Updated_FirstName,Updated_LastName,Title,Business_Phone,Mailing_City,Mailing_State,Mailing_Zip,Mailing_Country,Firm_Contact,KeyWord_For_Search,AllFirms_Count,OnlineImportPath,AdvisorInvolvementID,FOLDER_NAME,INVALID_FOLDER_NAME,
		TargetRegistrationURL,TargetLoginURL,Watermarking,UpdateInstitution_NameFormManageInvestor,UpdatedLimitedPartner_NameFormManageInvestor,HomePageAlertCount,FundsPageALertCount,ContactPageALertCount,BillingZip,UpdateFund_NameFromUpdateInfoIWR,UpdateFund_NameFromUpdateInfoFR,ContactName,FundName,Module_Name,Execute,Statistics,FRW_Value,INV_Value,FRW_DocumentsName,
		INV_DocumentsName,Updated_InstitutionName_From_InvestorSide,Activity_Count,Viewed_Or_DownloadedAnyFile,Account_Name,Logo_Name,Contact_Access,ContactUpdatedEmailID, Path, DrawdownID, CapitalCalllID, CapitalAmount,ManagementFee, OtherFee, CallAmount, CallDate, DueDate, CallAmountReceived,ReceivedDate, AmountDue,CapitalReturn,Dividends,RealizedGain,OtherProceeds,TotalDistributions,FundDistributionID,InvestorDistributionID,Capital_Returned_Recallable,Capital_Returned_NonRecallable,Item_ID {
			 @Override
			    public String toString() {
			      return "Item Id";
			    }
		}, Item_Name{
			@Override
			public String toString(){
				return "Item Name";
			}
		},Record_Type,Street,City,State,Postal_Code,Country,Other_Street,Other_City,Other_State,Other_Zip,Other_Country,Report_Folder_Name,Report_Name,Select_Report_Type,Show,Range,Email_Template_Folder_Label,Public_Folder_Access,Type,Available_For_Use,Description,Subject,Email_Body,Email_Template_Name,Marketing_InitiativeName,Target_Commitments,Vintage_Year,Fax,
		Frist_Closing_Date{
			@Override
			public String toString() {
				return "1st Closing Date";
			}
		}
		,Start_Date,End_Date,uploadFileName,updateFileName,FolderStructure, Pipeline_Name, Company_Name, Stage, Source, Source_Firm, Source_Contact, Deal_Type, Employees, Website, Email, Legal_Name, Name, Investment_Size, Log_In_Date, Our_Role, Last_Name, Last_Stage_Change_Date, Highest_Stage_Reached, Age_of_Current_Stage, Date_Stage_Changed, Changed_Stage, Age, First_Stage_Changed, Second_Stage_Changed, Office_Location_Name, State_Province, ZIP, Organization_Name, Primary, Updated_Primary, Start, Related_To, Due_Date,Investment_Likely_Amount,Total_Fundraising_Contacts,Fundraising_Contact_ID,Fundraising,Role, Other_Address, Mailing_Address,Total_Commitments,Commitment_Amount,Partner_Type,Tax_Forms,Final_Commitment_Date,Company,Bank_Name,Placement_Fee,Fund_Investment_Category,Total_CoInvestment_Commitments,Total_Fund_Commitments, Institution_Type, Fund_Preferences, Industry_Preferences, Shipping_Street, Shipping_City, Shipping_State, Shipping_Zip, Shipping_Country, Mobile_Phone, Assistant, Asst_Phone, Phone,Total_Call_Amount_Received, Total_Amount_Called,Total_Amount_Received,Total_Uncalled_Amount,Total_Commitment_Due,Commitment_Called,Called_Due,Preferred_Mode_of_Contact,Percent, TotalCommitment, Priority, Project_Type, Project_Name, Task_Name, Status, Formula, Target_Name, Deal_Name, UpdatedDeal_Name, Deal, Profile;
};

	
	public static enum FolderType{
		Common,
		Shared,
		Internal,
		Standard,
		Global;
	}
	
	public static enum accessType {
		InternalUserAccess, AdminUserAccess;
	}

	public static enum userType {
		CRMUser, SuperAdmin;
	}

	public static enum WorkSpaceAction{
		CREATEFOLDERTEMPLATE,
		IMPORTFOLDERTEMPLATE,
		WITHOUTEMPLATE,
		WITHTARGET,
		WITHOUTTARGET,
		ACTIVE,
		INACTIVE,
		UPDATE,
		CHECKERRORMSG,
		UPLOAD;
	}
	
	public static enum OnlineImportFileAddTo{
		SingleInstitute, MultipleInstitute
	}
	
	
	public static enum SortOrder {
		Assecending,
		Decending;
	}
	

	public static enum TabName {
		InstituitonsTab, ContactTab, FundraisingsTab, FundsTab, NIMTab, CommitmentsTab, PartnershipsTab, 
		NavatarInvestorAddOns, CurrentInvesment, PotentialInvesment, RecentActivities, AllDocuments, HomeTab,BoxSettings, 
		FolderTemplate, FundDistributions, InvestorDistributions, MarketingInitiatives, MarketingProspects, 
		NavatarSetup, Pipelines, FundDrawdowns, CapitalCalls, FundraisingContacts, LimitedPartne, ReportsTab, LimitedPartner,CompaniesTab, TaskRayTab,TaskRay,MyTasks, Targets, DealTab, AccountsTab,DRMTab;
	}
	
	public static enum Mode{
		Lightning,Classic;
	}
	public static enum Environment{
		Sandbox,Testing,Dev;
	}
	
	public static enum Workspace{

		FundraisingWorkspace,InvestorWorkspace,CurrentInvestment,Other,PotentialInvestment;

	}
	
	public static enum sideMenu{
		InternalUsers,FolderTemplates,ManageApprovals,Watermarking,FileDistributorSettings,FileSplitterOptions,Profiles,MyFirmProfile;
		}
	
	
	public static enum customTabActionType{
		Add,Remove;
	}
	
	
	public static enum PageName{
		FundsPage,ContactsPage,InstitutionsPage,CommitmentsPage,HomePage,DealRoomManager,ManageFolderPopUp,ManageApprovalsPopUp,ProjectDetailsPoPUp,NavatarInvestorAddOnsPage,NewProjectPopUp
		,CompanyPage,CreateFundraisingPage,CreateCommitmentFundType,CreateCommitmentCoInvestmentType,FundraisingPage,PartnershipsPage, DealPage,TaskRayPage, LimitedPartnerPage, BuildStep2Of3,PipelinesPage,createNewTaskDetailsPopUpFrame,
		NavatarDocumentPopUpOnPipeLinePage,NavatarDocumentsPopUpFrameOnTaskRayPage,DocumentsPageFrameOnDealPage,DocumentsBoxViewFrameOnPipeLinePage,ExternalSideNavatarDocumentPage,FormulaFieldTagDocuments, BoxSettings, FolderTemplate, DealRoomManagerParentFarme,CommUnityUserNavatarDocumentPage;
	}
	
	
	
	public static enum UploadFileActions{
		BulkUploaderOrFileSplitter,Upload,Update,IgnoreAll;
	}
	
	
	public static enum YesNo {
		Yes,No,YesWinium;
	}
	public static enum EnableDisable {
		Enable,Disable;
	}
	public static enum ErrorMessageType{
		BlankErrorMsg,PrefixErrorMsg,SpiecalCharErrorMsg,FolderCreationRestrictionErrorMsg,DuplicateFolder;
	}
	public static enum EditViewMode {
		Edit,View;
	}
	public static enum ManageApprovalTabs {
		PendingDocuments, ApprovedDocuments;
	}
	
	public static enum ExpandCollapse{
		Expand,Collapse
	}
	
	public static enum CheckUncheck{
		Check, UnCheck;
	}
	public static enum UpdateIgnore {
		Update, Ignore;
	}
	public static enum Status {
		Pending, Approved, Both;
	}
	
	
	public static enum columnName{
		fundName,contactName,institutionName,AccountName,Fundraising_Name;
	}
	
	
	public static enum SelectDeselect{
		Select,Deselect
	};
	
	public static enum AllOr1By1{
		All,OneByOne
	};
	
	
	
	
	
	public static enum object{
		Account{
			@Override
			public String toString() {
//				if(ExcelUtils.readDataFromPropertyFile("Mode").equalsIgnoreCase(Mode.Lighting.toString())) {
//					return "Institution";
//				} else {
					return "Accounts";
			//	}
			}
		},Contact,Fund,Fundraising,Pipeline,Deal,Fundraising_Contact{
			@Override
			public String toString() {
				return "Fundraising Contact";
			}
		},Email,InstalledPackage{
			@Override
			public String toString() {
				return "Installed Packages";
		}
	}
	};
	

//*************************************************************** Pages Field Labels*********************************************//
	public static enum InstitutionPageFieldLabelText {
		Street,Referral_Source_Description,Legal_Name,Description,
		Shipping_State{
			@Override
			public String toString() {
				return "Shipping State/Province";
			};
		},Shipping_Zip{
			@Override
			public String toString() {
				return "Shipping Zip/Postal Code";
			};
		},Parent_Institution;
		
	}
	
	public static enum LimitedPartnerPageFieldLabelText {
		Street,Referral_Source_Description,Legal_Name,Description,
		Total_CoInvestment_Commitments{
			@Override
			public String toString() {
				return "Total Co-investment Commitments (mn)";
			};
		},
		Total_Fund_Commitments{
			@Override
			public String toString() {
				return "Total Fund Commitments (mn)";
			};
		}
		
	}
	
	public static enum ContactPageFieldLabelText {
		Legal_Name,Description,Mailing_Street,Other_Street,Candidate_Notes,First_Name,Last_Name,Contact_Referral_Source,Mobile,
		Mailing_State{
			@Override
			public String toString() {
				return "Mailing State/Province";
			};
		},Mailing_Zip{
			@Override
			public String toString() {
				return "Mailing Zip/Postal Code";
			};
		},Other_State{
			@Override
			public String toString() {
				return "Other State/Province";
			};
		},Other_Zip{
			@Override
			public String toString() {
				return "Other Zip/Postal Code";
			};
		}
		
	}
	
	
	public static enum CommitmentPageFieldLabelText {
		Commitment_ID,Partner_Type,Limited_Partner,Final_Commitment_Date,Tax_Forms,Commitment_Amount,Total_Amount_Called,Total_Amount_Received,
		Total_Uncalled_Amount,Total_Commitment_Due,Partnership,Commitment_Called{
			@Override
			public String toString() {
				return "% Commitment Called";
			}
		},Called_Due{
			@Override
			public String toString() {
				return "% Called Due";
			}
		}
		,Total_Distributions{
			@Override
			public String toString() {
				return "Total Distributions";
			}
		}
		,Capital_Returned_Recallable{
			@Override
			public String toString() {
				return "Capital Returned (Recallable)";
			}
		}
		,Capital_Returned_NonRecallable{
			@Override
			public String toString() {
				return "Capital Returned (Non-Recallable)";
			}
		};
	}
	public static enum FundPageFieldLabelText{
		Fund_Name,Fund_Type,Investment_Category,Vintage_Year,
		Frist_Closing_Date{
			@Override
			public String toString() {
					return "1st Closing Date";
		}
		},Second_Closing_Date{
			@Override
			public String toString() {
					return "2nd_Closing_Date";
		}
		}
		,Third_Closing_Date{
			@Override
			public String toString() {
					return "3rd_Closing_Date";
		}
		},Fourth_Closing_Date{
			@Override
			public String toString() {
					return "4th_Closing_Date";
		}
		},Fivth_Closing_Date{
			@Override
			public String toString() {
					return "5th_Closing_Date";
		}
		},Sixth_Closing_Date{
			@Override
			public String toString() {
					return "6th_Closing_Date";
		}
		},Final_Closing_Date,Termination_Date,Dissolution_Date,Step_Down_Date,Investment_Period_End_Date,Target_Commitments{
			@Override
			public String toString() {
					return "Target Commitments (mn)";
		}
		};
	}
	
	public static enum CreateCommitmentPageFieldLabelText{
		Legal_Name,Fundraising_Name, Investment_Likely_Amount{
			@Override
			public String toString() {
					return "Investment Likely Amount (mn)";
		}
		},Fund_Name,Company;
		
	}
	
	
	//*****************************************************************************************************************************************//
	
	
	public static enum CreationPage{
		InstitutionPage,ContactPage, AccountPage;
	}
	
	public static enum UserLicense{
		Salesforce_Platform{
			@Override
			public String toString() {
					return "Salesforce Platform";
		}
		}
	}
	
	public static enum UserProfile{
		PE_Standard_User{
			@Override
			public String toString() {
					return "PE Standard User";
		}
		}
		
	}
	
	
	
	

	public static enum NavatarQuickLink{
		CreateDeal{
			@Override
			public String toString() {
				return "Create New Deal";
			}
		},CreateFundRaising{
			@Override
			public String toString() {
				return "Create Fundraisings";
			}
		},CreateCommitment{
			@Override
			public String toString() {
				return "Create New Commitment";
			}
		},BulkEmail{
			@Override
			public String toString() {
				return "Send Bulk Email";
			}
		},CreateIndiviualInvestor{
			@Override
			public String toString() {
				return "Create New Individual Investor";
			}
		}
	};

	

	public static enum RecordType{
		Company,Institution,IndividualInvestor,Contact, PipeLine, Fund, Fundraising,Partnerships;
	}
	
	public static enum searchContactInEmailProspectGrid{
		Yes,No
	};
	
	public static enum Locator{
		Xpath,Name;
	}

	public static enum CheckBox{
		Checked,Unchecked
	};
	
	
	public static enum LookUpIcon{
		OfficeLocation{
			@Override
			public String toString() {
				return "Office Location Lookup (New Window)";
			}
		},
		selectFundFromCreateFundraising{
			@Override
			public String toString() {
				return "Fund Name Lookup (New Window)";
			}
		},newTaskProject{
			@Override
			public String toString() {
				return "Project Lookup (New Window)";
			}
		}
		};
		
	public static enum RelatedList {
		Fundraising_Contacts{
			@Override
			public String toString() {
				return "Fundraising Contacts";
			}
		},Office_Locations, Open_Activities,Affiliations,Contacts, Activities,Activity_History,Commitments,Partnerships,Fundraisings,FundDistribution {
			@Override
			public String toString() {
				return "Fund Distribution";
				
			}
			}, InvestorDistributions{
				@Override
				public String toString() {
					return "Investor Distributions";
				}
			}, FundDrawdown {
		@Override
		public String toString() {
			return "Fund Drawdown";
			
		}
		}, CapitalCalls{
			@Override
			public String toString() {
				return "Capital Calls";
			}
		},
			Deals_Sourced{
			@Override
			public String toString() {
				return "Deals Sourced";
			}
		},
			Pipeline_Stage_Logs{
			@Override
			public String toString() {
				return "Pipeline Stage Logs";
			}
		},Correspondence_Lists{
			@Override
			public String toString() {
				return "Correspondence Lists";
			}
		}
	};

	
	public static enum RevertToDefaultPopUpButton{
		YesButton,NoButton,CrossIcon
	};
			
	public static enum IndiviualInvestorFieldLabel{
		First_Name,Last_Name,Contact_Description,Business_Phone,Business_Fax,Mobile_Phone,Email,Mailing_Street,Mailing_City,
		Mailing_State{
			@Override
			public String toString() {
				return "Mailing State/Province";
			}
		},Mailing_Zip{
			@Override
			public String toString() {
				return "Mailing Zip/Postal Code";
			}
		}, Other_State{
			@Override
			public String toString() {
				return "Other State/Province";
			}
		},Other_Zip{
			@Override
			public String toString() {
				return "Other Zip/Postal Code";
			}
		},Assistant{
		@Override
		public String toString() {
			return "Assistant's Name";
		}
	},Asst_Phone{
		@Override
		public String toString() {
			return "Asst. Phone";
		}
	},Mailing_Country,Other_Street,Other_City,Other_Country,Fund_Preferences,Industry_Preferences,Website,Preferred_Mode_of_Contact
};	

	public static enum NotApplicable{
		NA;
	}
	
	public static enum ClickOrCheckEnableDisableCheckBox{
		Click,EnableOrDisable;
	}
	
	public static enum sideListOnLayout{
		Related_Lists{
			@Override
			public String toString() {
				return "Related Lists";
			}
		},other{
			@Override
			public String toString() {
				return null;
			}
		}
	}

	public static enum TaskRayProjectButtons {
		AddPlus, Search,Other;
	}

	public static enum AddPlusIconDropDownList {
		New_Project;
	};

	public static enum taskSubTab{
		Details,Chatter,Files,Checklist,Navatar_Documents{
			@Override
			public String toString() {
				return "Navatar Documents";
			}
		}
	}
		
	public static enum Buttons{
		SaveNext,SaveClose,cancel,Cross, move,copy,Okay,Cancel,Save;	  
	};
	
	public static enum ProjectLabel{
		Deal, Fund, Project_Name, Project_Type;	  
	};
	
	public static enum RelatedTab{
		Related,Details,Deal_Evaluation,Due_Diligence,Documents,Tasks,Box,Deal_Prep;	  
	};
	
	public static enum Header{
		Project,OpenTask{
			@Override
			public String toString() {
				return "Open";
			}
		}  ,
		CompletedTask{
			@Override
			public String toString() {
				return "Completed";
			}
		} 
	};
	
	public static enum BoardFilters{
		My_Tasks,My_Projects,Prower_Filter,Single_Project_Search;
	}
	
	public static enum TaskType{
		Pending,Under_Review,Completed	;
	}
	
	public static enum SelectOption{
		selectOrDeSelectAll,OneByOneSelect
	}
	
	public static enum HeaderSideButton{
		Open_SDG_record,Toggle_Filters,Reload;
	}
	
	
	public static enum BoxActions{
		Trash,MoveOrCopy{
			@Override
			public String toString() {
				return "Move or Copy";
			}
		},Rename;
	}
	
	public static enum DataType{
		Formula;
	}
	
	public static enum folderTypeforAll{
		GLOBAL,
		SHARED,
		INTERNAL,
		STD;
	}
	
	public static enum FolderTemplateOrderType {
		Alphabetical{
		@Override
		public String toString(){
			return "Alphabetical Sorting (A-Z)";
		}
	},ManaulSorting_WithoutIndexing{
		@Override
		public String toString(){
			return "Manual Sorting (Without Indexing)";
		}
	},ManaulSorting_WithIndexing{
		@Override
		public String toString(){
			return "Manual Sorting (With Indexing)";
		}
	}
	};
	
	public static enum DealRoomAction{
		CREATEFOLDERTEMPLATE,
		IMPORTFOLDERTEMPLATE,
		WITHOUTEMPLATE,
		WITHTARGET,
		WITHOUTTARGET,
		ACTIVE,
		INACTIVE,
		UPDATE,
		CHECKERRORMSG,
		UPLOAD, IGNORE;
	}
	
	public static enum externalAdminUploadType{
		externalAdminSimpleUpload,externalAdminOnlineImport;
	}
	
}
