/**
 * 
 */
package com.navatar.pageObjects;

import org.apache.poi.hssf.view.brush.PendingPaintings;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.sikuli.script.App;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Key;
import org.sikuli.script.Screen;
import org.testng.Assert;

import com.jcraft.jsch.ConfigRepository.Config;
import com.navatar.generic.BaseLib;
import com.navatar.generic.CommonLib;
import com.navatar.generic.ExcelUtils;
import com.navatar.generic.SoftAssert;
import com.navatar.generic.CommonLib.*;
import com.navatar.generic.EnumConstants.Mode;
import com.navatar.generic.EnumConstants.PageName;
import com.navatar.generic.EnumConstants.RecordType;
import com.navatar.generic.EnumConstants.RelatedList;
import com.navatar.generic.EnumConstants.YesNo;
import com.navatar.generic.EnumConstants.action;
import com.navatar.generic.EnumConstants.excelLabel;
import com.relevantcodes.extentreports.LogStatus;
import com.sun.org.apache.bcel.internal.generic.GOTO;
import com.navatar.generic.CommonVariables;

import static com.navatar.generic.AppListeners.*;
import static com.navatar.generic.BaseLib.sa;
import static com.navatar.generic.CommonLib.*;

import java.util.Random;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;
import java.util.regex.Pattern;
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.awt.image.TileObserver;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


public class BasePageBusinessLayer extends BasePage implements BasePageErrorMessage{

	/**
	 * @param driver
	 */
	public BasePageBusinessLayer(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	
	/**
	 * @author Ankit Jaiswal
	 * @description- This method is used to set new password for CRM Users
	 * @param userType TODO
	 */
	public boolean setNewPassword(String userType) {
		try {
			Assert.assertTrue(getChnageYourPassword(60).getText().trim().contains("Change Your Password"),
					"Change Your Password text is not verified");
		} catch (Exception e) {
			driver.navigate().refresh();
			e.printStackTrace();
		}
		appLog.info("Password To Be Entered: " + ExcelUtils.readDataFromPropertyFile("password"));
		if (sendKeys(driver, getNewPassword(60), ExcelUtils.readDataFromPropertyFile("password"),
				"New Password Text box", action.SCROLLANDBOOLEAN)) {
			appLog.info("Password Entered: " + getNewPassword(10).getAttribute("value"));
			appLog.info("Confirm Password To Be Entered: " + ExcelUtils.readDataFromPropertyFile("password"));
			ThreadSleep(5000);
			if (sendKeys(driver, getConfimpassword(60), ExcelUtils.readDataFromPropertyFile("password"),
					"Confirm Password text Box", action.SCROLLANDBOOLEAN)) {
				appLog.info("Confirm Password Entered: " + getConfimpassword(60).getAttribute("value"));
				if(userType.equalsIgnoreCase("CRM User")) {
					CommonLib.selectVisibleTextFromDropDown(driver, getQuestion(60), "In what city were you born?",
							"Question drop down list");
					sendKeys(driver, getAnswer(60), "New York", "Answer Text Box", action.SCROLLANDBOOLEAN);
					ThreadSleep(5000);
				}
				if (click(driver, getChangePassword(60), "Chnage Password Button", action.SCROLLANDBOOLEAN)) {
					appLog.info("clicked on change password button");
					appLog.info("CRM User Password is set successfully.");
					return true;
				} else {
					appLog.error("Not able to click on change password button so cannot set user password");
				}

			} else {
				appLog.error("Not able to exter confirm password in text box so cannot set user password");
			}
		} else {
			appLog.error("Not able to exter password in text box so cannot set user password");
		}
		return false;
	}

	/**
	 * @author Ankit Jaiswal
	 * @param addRemoveTabName
	 * @param customTabActionType
	 * @return list
	 */
	public List<String> addRemoveCustomTab(String addRemoveTabName, customTabActionType customTabActionType) {
		List<String> result = new ArrayList<String>();
		String[] splitedTabs = addRemoveTabName.split(",");
		if (click(driver, getAllTabBtn(60), "All Tab Button", action.SCROLLANDBOOLEAN)) {
			appLog.info("clicked on all tabs icon");
			if (click(driver, getAddTabLink(60), "Add a Tab Link", action.SCROLLANDBOOLEAN)) {
				appLog.info("clicked on add a tab link");
				if (customTabActionType.toString().equalsIgnoreCase("Add")) {
					System.err.println("inside Add");
					for (int i = 0; i < splitedTabs.length; i++) {
						if (selectVisibleTextFromDropDown(driver, getAvailableTabList(60), "Available Tab List",
								splitedTabs[i])) {
							appLog.info(splitedTabs[i] + " is selected successfully in available tabs");
							if (click(driver, getCustomTabAddBtn(60), "Custom Tab Add Button",
									action.SCROLLANDBOOLEAN)) {
								appLog.error("clicked on add button");
							} else {
								result.add("Not able to click on add button so cannot add custom tabs");
								appLog.error("Not able to click on add button so cannot add custom tabs");
							}
						} else {
							appLog.error(splitedTabs[i] + " custom tab name is not Available list Tab.");
							result.add(splitedTabs[i] + " custom tab name is not Available list Tab.");
						}
					}
				} else if (customTabActionType.toString().equalsIgnoreCase("Remove")) {
					System.err.println("inside remove");
					for (int i = 0; i < splitedTabs.length; i++) {
						if (selectVisibleTextFromDropDown(driver, getCustomTabSelectedList(60), "Selected Tab List",
								splitedTabs[i])) {
							appLog.info(splitedTabs[i] + " is selected successfully in Selected tabs");
							if (click(driver, getCustomTabRemoveBtn(60), "Remove Button", action.SCROLLANDBOOLEAN)) {
								appLog.error("clicked on remove button");
							} else {
								result.add("Not able to click on add button so cannot add custom tabs");
								appLog.error("Not able to click on add button so cannot add custom tabs");
							}
						} else {
							appLog.error(splitedTabs[i] + " custom tab name is not selected list Tab.");
							result.add(splitedTabs[i] + " custom tab name is not selected list Tab.");
						}
					}
				} else {
					result.add(
							"custom tab action type is not mtached so cannot add or remove custom tab please pass correct arrgument");
					appLog.error(
							"custom tab action type is not mtached so cannot add or remove custom tab please pass correct arrgument");
				}

				if (click(driver, getCustomTabSaveBtn(60), "Custom Tab Save Button", action.SCROLLANDBOOLEAN)) {
					appLog.info("clicked on save button");

				} else {
					result.add("Not able to click on save button so cannot save custom tabs");
					appLog.error("Not able to click on save button so cannot save custom tabs");
				}

			} else {
				result.add("Not able to click on add a tab link so cannot add custom tabs");
				appLog.error("Not able to click on add a tab link so cannot add custom tabs");
			}
		} else {
			result.add("Not able to click on all tabs icon so cannot add custom tabs");
			appLog.error("Not able to click on all tabs icon so cannot add custom tabs");
		}
		return result;
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @return string value or null
	 */
	public String generateRandomNumber() {
		Random randomGenerator = new Random();
		int randomInt = randomGenerator.nextInt(99999);
		String RandomNumber = String.valueOf(randomInt);
		return RandomNumber;
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param errorMessage
	 * @param errorMessageElement
	 * @param elementName
	 * @return true/false
	 */
	public boolean verifyErrorMessageOnPage(String errorMessage, WebElement errorMessageElement, String elementName) {
		System.err.println(errorMessageElement.getText());
		String actualResult = errorMessageElement.getText().trim();
		System.out.println(">>>>>>>>>>>>>>" + actualResult);
		if (actualResult.contains(errorMessage)) {
			appLog.info(elementName);
			return true;
		}
		return false;
	}
	
	

	/**
	 * @author ANKIT JAISWAL
	 * @return true/false
	 */
	public boolean removeUnusedTabs(){
		WebElement ele=null;
		List<String> lst=new ArrayList<String>();
		ele=FindElement(driver, "//a[contains(@title,'Reports')]", "Reports tab",
				action.SCROLLANDBOOLEAN, 10);
	 if(ele!=null){
		 lst=addRemoveCustomTab("Reports", customTabActionType.Remove);
		 if(!lst.isEmpty()){
			 for(int i=0; i<lst.size();i++){
				 BaseLib.sa.assertTrue(false, lst.get(i));
			 }
		 }
	 }
	 ThreadSleep(1000);
	 ele=FindElement(driver, "//a[contains(@title,'Dashboards')]", "Dashboards tab",
				action.SCROLLANDBOOLEAN, 10);
	 if(ele!=null){
		 lst= addRemoveCustomTab("Dashboards", customTabActionType.Remove);
		 lst.clear();
		 if(!lst.isEmpty()){
			 for(int i=0; i<lst.size();i++){
				 BaseLib.sa.assertTrue(false, lst.get(i));
			 }
		 }
	 }	
	 ThreadSleep(1000);
	 ele=FindElement(driver, "//a[contains(@title,'Marketing')]", "Marketing Initiatives tab",
				action.SCROLLANDBOOLEAN, 10);
	 if(ele!=null){
		 lst=addRemoveCustomTab("Marketing Initiatives", customTabActionType.Remove);
		 lst.clear();
		 if(!lst.isEmpty()){
			 for(int i=0; i<lst.size();i++){
				 BaseLib.sa.assertTrue(false, lst.get(i));
			 }
		 }
	 }
	 ThreadSleep(1000);
	 ele=FindElement(driver, "//a[contains(@title,'Navatar Setup')]", "Navatar setup tab",
				action.SCROLLANDBOOLEAN, 10);
	 if(ele!=null){
		 lst=addRemoveCustomTab("Navatar Setup", customTabActionType.Remove);
		 lst.clear();
		 if(!lst.isEmpty()){
			 for(int i=0; i<lst.size();i++){
				 BaseLib.sa.assertTrue(false, lst.get(i));
			 }
		 }
	 }
	 ThreadSleep(1000);
	 ele=FindElement(driver, "//a[contains(@title,'Navatar Deal')]", "Navatar Deal connect tab",
				action.SCROLLANDBOOLEAN, 10);
	 if(ele!=null){
		 lst=addRemoveCustomTab("Navatar Deal Connect", customTabActionType.Remove);
		 lst.clear();
		 if(!lst.isEmpty()){
			 for(int i=0; i<lst.size();i++){
				 BaseLib.sa.assertTrue(false, lst.get(i));
			 }
		 }
	 }	 
	 ThreadSleep(1000);
	 ele=FindElement(driver, "//a[contains(@title,'Pipelines')]", "Pipelines tab",
				action.SCROLLANDBOOLEAN, 10);
	 if(ele!=null){
		 lst=addRemoveCustomTab("Pipelines", customTabActionType.Remove);
		 lst.clear();
		 if(!lst.isEmpty()){
			 for(int i=0; i<lst.size();i++){
				 BaseLib.sa.assertTrue(false, lst.get(i));
			 }
		 }
	 }	 
	return true;	
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param date
	 * @param dateFormat
	 * @param typeOfDate
	 * @return true/false
	 */
	public boolean verifyDate(String date, String dateFormat, String typeOfDate){
		if(dateFormat==null) {
			if(date.contains(getDateAccToTimeZone("America/New_York", "M/dd/yyyy"))){
				appLog.info(typeOfDate+" date is verified : "+getDateAccToTimeZone("America/New_York", "M/dd/yyyy"));
				return true;
			} else if (date.contains(getDateAccToTimeZone("America/New_York", "MM/dd/yyyy"))) {
				appLog.info(typeOfDate+" date is verified : "+getDateAccToTimeZone("America/New_York", "MM/dd/yyyy"));
				return true;
			} else if (date.contains(getDateAccToTimeZone("America/New_York", "dd/M/yyyy"))) {
				appLog.info(typeOfDate+" date is verified : "+getDateAccToTimeZone("America/New_York", "dd/M/yyyy"));
				return true;
			} else if (date.contains(getDateAccToTimeZone("America/New_York", "dd/MM/yyyy"))) {
				appLog.info(typeOfDate+" date is verified : "+getDateAccToTimeZone("America/New_York", "dd/MM/yyyy"));
				return true;
			}else if (date.contains(getDateAccToTimeZone("America/New_York",  "M/d/yyyy"))) {
				appLog.info(typeOfDate+" date is verified : "+getDateAccToTimeZone("America/New_York", "M/d/yyyy"));
				return true;
			}else if (date.contains(getDateAccToTimeZone("America/New_York",  "d/M/yyyy"))) {
				appLog.info(typeOfDate+" date is verified : "+getDateAccToTimeZone("America/New_York", "d/M/yyyy"));
				return true;
			}else {
				appLog.info(typeOfDate+" date is not verified. found result : "+date);
				appLog.info("Expected Date is : "+getDateAccToTimeZone("America/New_York","M/dd/yyyy")+ " or "+getDateAccToTimeZone("America/New_York", "MM/dd/yyyy")+" or "+getDateAccToTimeZone("America/New_York", "dd/M/yyyy")+" or "+getDateAccToTimeZone("America/New_York", "dd/MM/yyyy")+" or "+getDateAccToTimeZone("America/New_York", "M/d/yyyy"));
				return false;
			}
		}else {
			if(date.contains(getDateAccToTimeZone("America/New_York", dateFormat))){
				appLog.info(typeOfDate+" date is verified : "+getDateAccToTimeZone("America/New_York", dateFormat));
				return true;
			}else {
				appLog.info(typeOfDate+" date is not verified. found result : "+date);
				appLog.info("Expected Date is : "+getDateAccToTimeZone("America/New_York", dateFormat)+ " or "+getDateAccToTimeZone("America/New_York", dateFormat)+" or "+getDateAccToTimeZone("America/New_York", dateFormat)+" or "+date.contains(getDateAccToTimeZone("America/New_York", dateFormat)));
				return false;
			}
			
		}
		
		

	}

	

	/**
	 * @author Ankit Jaiswal
	 * @param TabName
	 * @return true if click on Tab
	 */
	public boolean clickOnTab(String environment, String mode, TabName TabName) {
		String tabName = null;
		String suffix = " Tab";
		boolean flag = false;
		WebElement ele;
		switch (TabName) {
		case ContactTab:
			tabName = "Contacts";
			break;
		case TaskRayTab:
			tabName = "TaskRay";
			break;
		case AccountsTab:
			tabName = "Accounts";
			break;
		case FundraisingsTab:
			tabName = "Fundraisings";
			break;
		case DealTab:
			tabName = "Deals";
			break;
		case CommitmentsTab:
			tabName = "Commitments";
			break;
		case Targets:
			tabName = "Targets";
			break;
		case HomeTab:
			tabName = "Home";
			break;
		case NavatarSetup:
			tabName = "Navatar Setup";
			break;
		case MyTasks:
			tabName = "My Tasks";
			break;
		case BoxSettings:
			tabName = "Box Settings";
			break;
		case DRMTab:
			tabName = "Deal Room Manager";
			break;
		default:
			return flag;
		}
		System.err.println("Passed switch statement");
		if (mode.equalsIgnoreCase(Mode.Classic.toString())) {
			tabName = tabName + suffix;
			ele = isDisplayed(driver, FindElement(driver, "//a[contains(@title,'" + tabName + "')]", tabName,
					action.SCROLLANDBOOLEAN, 10), "visibility", 10, tabName);
			if (ele != null) {
				if (click(driver, ele, tabName, action.SCROLLANDBOOLEAN)) {
					CommonLib.log(LogStatus.PASS, "Tab found", YesNo.No);
					flag = true;
				} else {

				}
			} else {
				CommonLib.log(LogStatus.INFO, "Going to found tab after clicking on More Icon", YesNo.No);
				if (click(driver, getMoreTabIcon(environment, mode, 10), "More Icon", action.SCROLLANDBOOLEAN)) {
					if (click(driver,
							isDisplayed(driver,
									FindElement(driver, "//a[contains(@title,'" + tabName + "')]", tabName,
											action.SCROLLANDBOOLEAN, 10),
									"visibility", 10, tabName),
							tabName, action.SCROLLANDBOOLEAN)) {
						CommonLib.log(LogStatus.INFO, "Tab found on More Icon", YesNo.No);
						flag = true;
					}
				} else {

				}
			}
		} else {
			ele = isDisplayed(driver,
					FindElement(driver, "//a[contains(@href,'lightning') and contains(@title,'" + tabName + "')]/span/..",
							tabName, action.SCROLLANDBOOLEAN,30),
					"visibility", 30, tabName);
			if (ele != null) {
				appLog.info("Tab Found");
				ThreadSleep(5000);
				if (clickUsingJavaScript(driver, ele, tabName+" :Tab")) {
					CommonLib.log(LogStatus.INFO, "Tab found", YesNo.No);
					appLog.info("Clicked on Tab : "+tabName);
					flag = true;
				} else {
					appLog.error("Not Able to Click on Tab : "+tabName);
				}

			} else {
				CommonLib.log(LogStatus.INFO, "Going to found tab after clicking on More Icon", YesNo.No);
				if (click(driver, getMoreTabIcon(environment, mode, 10), "More Icon", action.SCROLLANDBOOLEAN)) {
					ele = isDisplayed(driver,
							FindElement(driver,
									"//a[contains(@href,'lightning')]/span[@class='slds-truncate']/span[contains(text(),'"
											+ tabName + "')]",
									tabName, action.SCROLLANDBOOLEAN, 10),
							"visibility", 10, tabName);
					if (ele!=null) {
						if (clickUsingJavaScript(driver, ele, tabName+" :Tab")) {
							appLog.info("Clicked on Tab on More Icon: "+tabName);
							CommonLib.log(LogStatus.INFO, "Tab found on More Icon", YesNo.No);
							flag = true;
						}	
					}
					
				} else {
					appLog.error("Not Able to Clicked on Tab on More Icon: "+tabName);
				}

			}
		}
		
		/*if (TabName.NavatarSetup.toString().equalsIgnoreCase(TabName.toString())) {
			NavatarSetupPageBusinessLayer np = new NavatarSetupPageBusinessLayer(driver);
			
			if (mode.equalsIgnoreCase(Mode.Lightning.toString())) {
			switchToFrame(driver, 10, np.getnavatarSetUpTabFrame_Lighting(environment, 10));
		}
			if(FindElement(driver, "(//p[contains(text(),'Deal Creation')])[1]", "Deal Creation", action.BOOLEAN, 60)!=null){
				
				appLog.info("Landing Page Verified : " + "Deal Creation");
				
				
				flag = true;
				
			}else{
				appLog.error("Landing Page Not Verified : Deal Creation");
				flag = false;
			}
			switchToDefaultContent(driver);
		}*/
		return flag;
	}

	/**
	 * @author Ankit Jaiswal
	 * @param environment
	 * @param mode
	 * @param tabName
	 * @param labelName
	 * @param labelValue
	 * @return true/false
	 */
	public boolean FieldValueVerificationOnAllPages(String environment, String mode, TabName tabName,
			String labelName,String labelValue) {
		String xpath = "";
		WebElement ele = null;
		if (mode.equalsIgnoreCase(Mode.Classic.toString())) {
			xpath = "//td[contains(text(),'"+ labelName +"')]/../td[2]/div";
		} else {
			xpath = "//span[@class='test-id__field-label'][contains(text(),'" + labelName
					+ "')]/../following-sibling::div/span/*//a";
		}
		ele = isDisplayed(driver,
				FindElement(driver, xpath, labelName + " label text in " + mode, action.SCROLLANDBOOLEAN, 60),
				"Visibility", 30, labelName + " label text in " + mode);
		if (ele != null) {
			String aa = ele.getText().trim();
			appLog.info("Lable Value is: "+aa);
			if(aa.contains(labelValue)) {
				appLog.info(labelValue + " Value is matched successfully.");
				return true;
				
			}else {
				appLog.info(labelValue + " Value is not matched. Expected: "+labelValue+" /t Actual : "+aa);
			}
		} else {
			appLog.error(labelName + " Value is not visible so cannot matched  label Value "+labelValue);
		}
		return false;

	}

	/**
	 * @author ANKIT JAISWAL
	 * @param environment
	 * @param mode
	 * @param tabName
	 * @param alreadyCreated
	 * @param timeout
	 * @return true/false
	 */
	public boolean clickOnAlreadyCreated_Lighting(String environment, String mode, TabName tabName,
			String alreadyCreated, int timeout) {

		String viewList = null;
		switch (tabName) {
		case ContactTab:
			viewList = "All Contacts";
			break;
		case InstituitonsTab:
			viewList = "All Institutions";
			break;
		case CompaniesTab:
			viewList = "All Companies";
			break;
		case LimitedPartner:
			viewList = "All Limited Partners";
			break;
		case FundraisingsTab:
			viewList = "All";
			break;
		case DealTab:
			viewList = "All";
			break;
		case Targets:
			viewList = "All";
			break;
		case PartnershipsTab:
			viewList = "All";
			break;
		case FundDistributions:
			viewList = "All";
			break;
		case InvestorDistributions:
			viewList = "All";
			break;
		case MarketingInitiatives:
			viewList = "All";
			break;
		case MarketingProspects:
			viewList = "Marketing Prospects";
			break;
		case Pipelines:
			viewList = "All";
			break;
		case CapitalCalls:
			viewList = "All";
			break;
		case FundDrawdowns:
			viewList = "All";
			break;
		case FundraisingContacts:
			viewList = "All";
			break;
		case NavatarSetup:
			viewList = "All";
			break;
		default:
			return false;
		}
		System.err.println("Passed switch statement");
		WebElement ele, selectListView;
		ele = null;
		if (click(driver, getSelectListIcon(60), "Select List Icon", action.SCROLLANDBOOLEAN)) {
			ThreadSleep(3000);
			selectListView = FindElement(driver, "//div[@class='listContent']//li/a/span[text()='" + viewList + "']",
					"Select List View", action.SCROLLANDBOOLEAN, 30);
			if (click(driver, selectListView, "select List View", action.SCROLLANDBOOLEAN)) {
				ThreadSleep(3000);
				if(mode.equalsIgnoreCase(Mode.Lightning.toString())) {
					refresh(driver);
					ThreadSleep(5000);
				}
				if (sendKeys(driver, getSearchIcon_Lighting(20), alreadyCreated+"\n", "Search Icon Text",
						action.SCROLLANDBOOLEAN)) {
					ThreadSleep(5000);
					ele = FindElement(driver,
							"//table[@data-aura-class='uiVirtualDataTable']//tbody//tr//th//span//a[text()='"
									+ alreadyCreated + "']",
							alreadyCreated, action.BOOLEAN, 30);
					ThreadSleep(2000);
					if (click(driver, ele, alreadyCreated, action.BOOLEAN)) {
						ThreadSleep(3000);
						return true;
					} else {
						appLog.error("Not able to Click on Already Created : " + alreadyCreated);
					}
				} else {
					appLog.error("Not able to enter value on Search Box");
				}
			} else {
				appLog.error("Not able to select on Select View List");
			}
		} else {
			appLog.error("Not able to click on Select List Icon");
		}
		return false;
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @return true/false
	 */
	public boolean switchToLighting() {
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.scrollTo(0,0)");
		ThreadSleep(1000);
		if (!getUserMenuTab_Lightning().isEmpty()) {
			appLog.info("Sales Force is Already open in Lighting mode.");
			return true;
		} else {
			ThreadSleep(2000);
			if (click(driver, getSwitchToLightingLink(60), "sales force lighting icon", action.SCROLLANDBOOLEAN)) {
				appLog.info("Sales Force is switched in Lighting mode successfully.");
				return true;
			} else {
				appLog.error("Not able to click on Lighting Link");
			}
		}
		return false;

	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @return true/false
	 */
	public boolean switchToClassic() {
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.scrollTo(0,0)");
		ThreadSleep(1000);
		if (getUserMenuTab(10) != null) {
			appLog.info("Sales Force is Already open in classic mode.");
			return true;
		} else {
			ThreadSleep(2000);
			if (click(driver, getSalesForceLightingIcon(30), "sales force lighting icon", action.SCROLLANDBOOLEAN)) {
				ThreadSleep(1000);
				if (click(driver, getSwitchToClassic(30), "sales force switch to classic link",action.SCROLLANDBOOLEAN)) {
					appLog.info("Sales Force is switched in classic mode successfully.");
					return true;
				} else {
					appLog.error("Not able to switch Classic.");
				}
			} else {
				appLog.error("Not able to click on Lighting Icon");
			}

		}
		return false;
	}
	
	/**
	 * @author Azhar
	 * @param onlymail
	 * @return string value or null
	 */
	public String generateRandomEmailId(String onlymail) {
		Random randomGenerator = new Random();
		int randomInt = randomGenerator.nextInt(99999);
		String contactEmail = onlymail;
		String[] EmailIDContact = contactEmail.split("@");
		String contactEmailID = EmailIDContact[0] + "+" + randomInt + "@gmail.com";
		return contactEmailID;
	}
	
	/**
	 * @author Azhar
	 * return webelement or null
	 */
	@FindBy(xpath="//span[text()='Related']")
	private WebElement relatedTab1_Lighting;
	
	/**
	 * @author Azhar
	 * return webelement or null
	 */
	@FindBy(xpath="(//span[text()='Related'])[2]")
	private WebElement relatedTab2_Lighting;
	
	
	/**@author Azhar
	 * @param environment
	 * @param timeOut
	 * @return
	 */
	public WebElement getRelatedTab_Lighting(String environment,RecordType RecordType,int timeOut) {
		
		List<WebElement> eleList = FindElements(driver, "//span[text()='Related']", "Related Tab");
		int i=0;
		for (WebElement ele : eleList) {
			i++;
			WebElement ele1 ;
			ele1=isDisplayed(driver, ele, "Visibility", timeOut, "Related Tab "+i);	
			if (ele1!=null	) {
				return ele1;	
			}
			
		
		}
			
		return null;
	
	}
	
	/**
	 * @author Azhar
	 * return webelement or null
	 */
	@FindBy(xpath="//span[text()='Details']")
	private WebElement detailsTab_Lighting;
	
	/**@author Azhar
	 * @param environment
	 * @param timeOut
	 * @return
	 */
	public WebElement getdetailsTab_Lighting(String environment,TabName TabName,int timeOut) {
		return isDisplayed(driver, detailsTab_Lighting, "Visibility", timeOut, "Details Tab");	
		}

	/**
	 * @author Ankit Jaiswal
	 * @param searchText
	 * @return true/false
	 */
	public boolean selectValueFromLookUpWindow(String searchText) {
		String parentWindow=null;
		WebElement ele=null;
		parentWindow=switchOnWindow(driver);
		if(parentWindow!=null) {
			switchToFrame(driver, 20, getLookUpSearchFrame(10));
			if(sendKeys(driver, getLookUpSearchTextBox(30), searchText, "search text box", action.SCROLLANDBOOLEAN)) {
				if(click(driver, getLookUpSearchGoBtn(20), "go button", action.SCROLLANDBOOLEAN)) {
					switchToDefaultContent(driver);
					switchToFrame(driver, 20, getLookUpResultFrame(10));
					ele=isDisplayed(driver, FindElement(driver, "//a[text()='"+searchText+"']",searchText+" text value", action.SCROLLANDBOOLEAN, 20),"visibility", 20,searchText+" text value");
					if(ele!=null) {
						if(!click(driver, ele, searchText+" text value", action.SCROLLANDBOOLEAN)) {
							appLog.info("clicked on "+searchText+" in lookup pop up");
						}
						driver.switchTo().window(parentWindow);
						return true;
					}else {
						appLog.error(searchText+" is not visible in look up popup so cannot select it");
						driver.close();
						driver.switchTo().window(parentWindow);
					}
				}else {
					appLog.error("Not able to click on go button so cannot select "+searchText);
					driver.close();
					driver.switchTo().window(parentWindow);
				}
			}else {
				appLog.error("Not able to pass value in search text box : "+searchText+" so cannot select value "+searchText+" from look up");
				driver.close();
				driver.switchTo().window(parentWindow);
			}
		}else {
			appLog.error("No new window is open so cannot select value "+searchText+" from look up");
		}
		return false;
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param environment
	 * @param mode
	 * @param lookUpIcon
	 * @param searchText
	 * @param lookUpValues
	 * @return true/false
	 */
	public boolean clickOnLookUpAndSelectValueFromLookUpWindow_Classic(String environment,String mode,LookUpIcon lookUpIcon,String searchText,String lookUpValues){
		String[] values = lookUpValues.split(",");
		WebElement ele=null;
		String xpath="";
		if(lookUpIcon.toString().equalsIgnoreCase(LookUpIcon.selectFundFromCreateFundraising.toString())) {
			xpath="(//img[@title='"+lookUpIcon+"'])[2]";
		}else {
			xpath="//img[@title='"+lookUpIcon+"']";
		}
		WebElement lookUpIconEle = FindElement(driver,xpath, lookUpIcon.toString(), action.SCROLLANDBOOLEAN, 10);
		if (click(driver, lookUpIconEle, "Look Up Icon", action.SCROLLANDBOOLEAN)) {
		
		String parentWindow=null;
		parentWindow=switchOnWindow(driver);
		if(parentWindow!=null) {
			switchToFrame(driver, 20, getLookUpSearchFrame(10));
			if(sendKeys(driver, getLookUpSearchTextBox(30), searchText, "search text box", action.SCROLLANDBOOLEAN)) {
				if(click(driver, getLookUpSearchGoBtn(20), "go button", action.SCROLLANDBOOLEAN)) {
					switchToDefaultContent(driver);
					switchToFrame(driver, 20, getLookUpResultFrame(10));
					for(int i=0;i<values.length;i++){
						ele=isDisplayed(driver, FindElement(driver, "//a[text()='"+values[i]+"']",values[i]+" text value", action.SCROLLANDBOOLEAN, 20),"visibility", 20,values[i]+" text value");
						if(ele!=null) {
							appLog.info(values[i]+" is visible in look up popup");	

							if(i==values.length-1){
								ele=isDisplayed(driver, FindElement(driver, "//a[text()='"+values[0]+"']",values[0]+" text value", action.SCROLLANDBOOLEAN, 20),"visibility", 20,values[0]+" text value");
								if(!click(driver, ele, values[0]+" text value", action.SCROLLANDBOOLEAN)) {
									appLog.info("clicked on "+values[0]+" in lookup pop up");
									driver.switchTo().window(parentWindow);
									return true;
								}
							}

						}else {
							appLog.error(values[i]+" is not visible in look up popup");
							driver.close();
							driver.switchTo().window(parentWindow);
							return false;
						}
					}
				}else {
					appLog.error("Not able to click on go button so cannot select "+searchText);
					driver.close();
					driver.switchTo().window(parentWindow);
				}
			}else {
				appLog.error("Not able to pass value in search text box : "+searchText+" so cannot select value "+searchText+" from look up");
				driver.close();
				driver.switchTo().window(parentWindow);
			}
		}else {
			appLog.error("No new window is open so cannot select value "+searchText+" from look up");
		}
	}else{
		appLog.error("Not Able to Click oN Look Up Icon");	
	}
		return false;
	}

	
	/**
	 * @author ANKIT JAISWAL
	 * @param environment
	 * @param RelatedList
	 * @return true/false
	 */
	public boolean clickOnRelatedList_Classic(String environment, RelatedList RelatedList) {
		String relatedList = null;
		WebElement ele;
		switch (RelatedList) {
		case Fundraising_Contacts:
			relatedList = "Fundraising Contacts";
			break;
		case Office_Locations:
			relatedList = "Office Locations";
			break;
		case Open_Activities:
			relatedList = "Open Activities";
			break;
		case Fundraisings:
			relatedList = "Fundraisings";
			break;
		case FundDrawdown:
			relatedList = "Fund Drawdown";
			break;
		case CapitalCalls:
			relatedList = "Capital Calls";
			break;
		case Affiliations:
			relatedList = "Affiliations";
			break;
		case Activities:
			relatedList = "Activities";
			break;
		case Activity_History:
			relatedList = "Activity History";
			break;
		case Commitments:
			relatedList = "Commitments";
			break;
		case Partnerships:
			relatedList = "Partnerships";
			break;
		case Deals_Sourced:
			relatedList = "Deals Sourced";
			break;
		case Pipeline_Stage_Logs:
			relatedList = "Pipeline Stage Logs";
			break;
			
		default:
			return false;
		}
		ThreadSleep(2000);
		System.err.println("Passed switch statement");
		
			ele = isDisplayed(driver, FindElement(driver, "//span[@class='listTitle'][text()='"+relatedList+"']", relatedList,
					action.SCROLLANDBOOLEAN, 10), "visibility", 10, relatedList);
			if (ele != null) {
				if (click(driver, ele, relatedList, action.SCROLLANDBOOLEAN)) {
					CommonLib.log(LogStatus.INFO, "Related List found : "+relatedList, YesNo.No);
					ThreadSleep(2000);
					return true;
				}
			}
		

		return false;
	}
	
	/**
	 * @author Azhar
	 * @param environment
	 * @param mode
	 * @param RelatedList
	 * @return true/false
	 */
	public boolean clickOnViewAllRelatedList_Lighting(String environment,String mode, RelatedList RelatedList) {
		if(mode.equalsIgnoreCase(Mode.Lightning.toString())) {
			String relatedList = null;
			WebElement ele;
			switch (RelatedList) {
			case Fundraising_Contacts:
				relatedList = "Fundraising Contacts";
				break;
			case Office_Locations:
				relatedList = "Office Locations";
				break;
			case FundDrawdown:
				relatedList = "Fund Drawdown";
				break;
			case FundDistribution:
				relatedList = "Fund Distribution";
				break;
			case CapitalCalls:
				relatedList = "Capital Calls";
				break;
			case Commitments:
				relatedList = "Commitments";
				break;
			default:
				return false;
			}
			ThreadSleep(2000);
			System.err.println("Passed switch statement");
			
			
			ele = isDisplayed(driver, FindElement(driver, "//span[text()='"+relatedList+"']/ancestor::article//span[text()='View All']", relatedList,
					action.SCROLLANDBOOLEAN, 10), "visibility", 10, relatedList);
			if (ele != null) {
				if (click(driver, ele, relatedList, action.SCROLLANDBOOLEAN)) {
					CommonLib.log(LogStatus.INFO, "Related List found : "+relatedList, YesNo.No);
					ThreadSleep(2000);
					return true;
				}
			}
			
		}else {
			return true;
		}
		return false;
	}
	
	
	
	/**
	 * @author Azhar
	 * @param number
	 * @param format
	 * @return string value or null
	 */
	public static String convertNumberAccordingToFormatWithCurrencySymbol(String number,String format){

		double d = Double.parseDouble(number);
		DecimalFormat myFormatter = new DecimalFormat(format);
		String output = new DecimalFormatSymbols(Locale.US).getCurrencySymbol()+myFormatter.format(d);
		System.err.println(" outpurttt >>>> "+output);
		return output;

	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param number
	 * @param format
	 * @return string value or null
	 */
	public static String convertNumberAccordingToFormatWithoutCurrencySymbol(String number,String format){

		double d = Double.parseDouble(number);
		DecimalFormat myFormatter = new DecimalFormat(format);
		String output = myFormatter.format(d);
		System.err.println(" outpurttt >>>> "+output);
		return output;

	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param number
	 * @return string value or null
	 */
	public static String convertNumberIntoMillions(String number){
		double d = Double.parseDouble(number);
		double aa = d/1000000;
		String output = new DecimalFormatSymbols(Locale.US).getCurrencySymbol()+aa;
		System.err.println("convertNumberIntoMillions  outpurttt >>>> "+output);
		return output;
	}
	
	

	/**
	 * @author Azhar
	 * @param mode
	 * @param tabToBeAdded
	 * @param timeOut
	 * @return true/false
	 */
	public boolean addTab_Lighting(String mode,String tabToBeAdded,int timeOut){

		String xpath;
		WebElement ele;
		boolean flag = true;
		if (click(driver, getPersonalizePencilIcon(mode, timeOut), "Personalize Pencil Icon", action.SCROLLANDBOOLEAN)) {
			ThreadSleep(2000);
			if (click(driver, getAddMoreItemsLink(mode, timeOut), "Add More items Link", action.SCROLLANDBOOLEAN)) {
				ThreadSleep(2000);
				if (click(driver, getAllAddLink(mode, timeOut), "All Link", action.SCROLLANDBOOLEAN)) {
					ThreadSleep(2000);
					click(driver, getAllAddLink(mode, timeOut), "All Link", action.SCROLLANDBOOLEAN);
					ThreadSleep(2000);
					String[] tabs = tabToBeAdded.split(",");
					for (int i = 0; i < tabs.length; i++) {
						xpath ="//h3[contains(text(),'"+tabs[i]+"')]/..//preceding-sibling::label/div";
						ele = FindElement(driver, xpath, "Tab to be add : "+tabs[i], action.SCROLLANDBOOLEAN, timeOut);
						
						ThreadSleep(1000);
						if (ele!=null) {
							scrollDownThroughWebelement(driver, ele, "TABS : "+tabs[i]);	
							if (click(driver, ele, "Tab to be add : "+tabs[i], action.SCROLLANDBOOLEAN)) {
								log(LogStatus.INFO, "Tab Added : "+tabs[i], YesNo.No);
							} else {
								flag = false;
								log(LogStatus.INFO, "Not Able to add Tab : "+tabs[i], YesNo.Yes);
							}
							
						} else {
							log(LogStatus.INFO, "Tab Already Added : "+tabs[i], YesNo.No);
						}
						
					}

					if (click(driver, getAddNavButton(mode, timeOut), "Add Nav Button", action.SCROLLANDBOOLEAN)) {
						if (click(driver, getTabSaveButton(mode, timeOut), "Save Button", action.SCROLLANDBOOLEAN)) {
						} else {
							log(LogStatus.FAIL, "Not Able to click on Save Button", YesNo.Yes);
							flag = false;
						}
					} else {
						log(LogStatus.FAIL, "Not Able to click on Add Nav Button", YesNo.Yes);
						flag = false;
					}
				} else {
					log(LogStatus.FAIL, "Not Able to click on All Link", YesNo.Yes);
					flag = false;
				}
			} else {
				log(LogStatus.FAIL, "Not Able to click on Add More items Link", YesNo.Yes);
				flag = false;
			}
		} else {
			log(LogStatus.FAIL, "Not Able to click on personalize Pencil Icon", YesNo.Yes);
			flag = false;
		}
		return flag;
	}

	/**
	 * @author ANKIT JAISWAL
	 * @param pageName
	 * @param button
	 * @param timeOut
	 * @return true/false
	 */
	public WebElement getButton(PageName pageName,Buttons button,int timeOut){
	WebElement ele=null;
	String xpath=null;
	if (Buttons.SaveNext.toString().equalsIgnoreCase(button.toString())) {
		xpath ="//a[text()='Save & Next']";
	}else if(Buttons.SaveClose.toString().equalsIgnoreCase(button.toString())) {
		xpath ="//a[text()='Save & Close']";	
	}else if(Buttons.cancel.toString().equalsIgnoreCase(button.toString())) {
		xpath ="(//a[text()='Cancel'])[1]";
	}else if (Buttons.Save.toString().equalsIgnoreCase(button.toString())) {
		xpath="(//div[@class='modal-footer']/a[text()='Save'])[1]";
	}
	ele = FindElement(driver, xpath, "Button : "+button, action.SCROLLANDBOOLEAN, timeOut);
	return ele;
	

	
}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param environment
	 * @param mode
	 * @param task
	 * @return true/false
	 */
	public boolean clickOnDocumentLinkInFrontOfTask(String environment, String mode, String task)
{
	WebElement ele=isDisplayed(driver, FindElement(driver, "//div//a[text()='"+task+"']"
			+ "/ancestor::td/following-sibling::td//button[text()='Documents']", "document link", action.BOOLEAN, 20), "visibility", 20, "document link");
	if (ele!=null) {
		if (click(driver, ele, "document name", action.SCROLLANDBOOLEAN)) {
			return true;
		}
		else {
			appLog.error("could not click on documents link");
		}
		
	}
	else {
		appLog.error("task is not found at this page");
	}
	return false;
}

	/**
	 * @author ANKIT JAISWAL
	 * @param environment
	 * @param mode
	 * @param pageName
	 * @param relatedTab
	 * @param timeOut
	 * @return true/false
	 */
	public WebElement getRelatedTab(String environment,String mode,PageName pageName,RelatedTab relatedTab,int timeOut){
	
	boolean flag=false;
	String xpath="";
	WebElement ele;
	String related = relatedTab.toString().replace("_", " ");
	xpath = "//li/a[text()='"+related+"' or @title='"+related+"']";
	ele = FindElement(driver, xpath, relatedTab.toString(), action.SCROLLANDBOOLEAN, timeOut);
	if (ele!=null) {
	appLog.info("Element Found : "+related);	
	flag=true;
	}else {
		appLog.info("Element Not Found : "+related);	
	}
	return ele;
	
}

	/**
	 * @author ANKIT JAISWAL
	 * @param environment
	 * @param mode
	 * @param header
	 * @param count
	 * @param timeOut
	 * @return true/false
	 */
	public Boolean verifyingHeaderonSubTab(String environment,String mode,Header header,int count,int timeOut){
	
	boolean flag=false;
	String xpath="";
	WebElement ele;
	String head = header.toString().replace("_", " ");
	xpath = "//h2//a[contains(text(),'"+head+"')]";
	ele = FindElement(driver, xpath, header.toString()+" header xpath", action.SCROLLANDBOOLEAN, timeOut);
	if (ele!=null) {
		if(ele.getText().trim().contains(String.valueOf(count))) {
			log(LogStatus.PASS, header+" Header Count is matched", YesNo.No);
			flag =true;
		}else {
			log(LogStatus.FAIL, header+" Header Count is not matched", YesNo.Yes);
		}
	}else {
		appLog.info("Element Not Found : "+head);	
	}
	return flag;
}

	/**
	 * @author ANKIT JAISWAL
	 * @param environment
	 * @param mode
	 * @param xpath
	 * @param lookUpIconFor
	 * @param itemName
	 * @param timeOut
	 * @return true/false
	 */
	public boolean selectItemFromLoookUpWindow(String environment, String mode, String xpath,String lookUpIconFor,String itemName,int timeOut) {
	boolean flag = false;
	WebElement ele=null;
	ele = FindElement(driver, xpath, lookUpIconFor, action.BOOLEAN, timeOut);
	if(click(driver, ele, lookUpIconFor+" look up icon", action.BOOLEAN)) {
		appLog.info("clicked on look up icon : "+lookUpIconFor);
		if(selectValueFromLookUpWindow(itemName)) {
			log(LogStatus.INFO, itemName+"  is select successfully", YesNo.No);
			appLog.info(itemName+" is select successfully");
			flag=true;
		}else {
			appLog.error("Not able to select  "+itemName+" from "+lookUpIconFor);
		}
	}else {
		appLog.error("Not able to click on : "+lookUpIconFor+"so can not"+itemName);
	}
	return flag;
}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param userName
	 * @param password
	 * @return true/false
	 */
	public String TagOrDocumentBoxLogin(String userName,String password) {
		String parentId=null;
		switchToFrame(driver,30,getFrame(PageName.NavatarDocumentPopUpOnPipeLinePage, 30));
		if(click(driver, getTagOrUploadDocumentsButton(30), "tag or document button", action.SCROLLANDBOOLEAN)) {
			log(LogStatus.PASS, "clicked on tag or document button", YesNo.No);
			ThreadSleep(5000);
			parentId=switchOnWindow(driver);
			if(parentId!=null) {
				log(LogStatus.PASS, "new window is open ", YesNo.No);
				if(matchTitle(driver, "Customer Log In",60)) {
					if(sendKeys(driver, getBoxUserName(60),userName,"Box username Text Box", action.BOOLEAN)) {
						log(LogStatus.PASS, "passed value in user name text box : "+userName, YesNo.No);
						if(sendKeys(driver, getBoxPasswordTextBox(60), password,"Box Password Text Box", action.BOOLEAN)) {
							log(LogStatus.PASS, "passed value in password text box : "+password, YesNo.No);
							if(click(driver, getBoxAuthorizeButton(60), "Authorize button", action.SCROLLANDBOOLEAN)) {
								log(LogStatus.PASS, "clicked on authorized button", YesNo.No);
								ThreadSleep(5000);
								if(click(driver, getGrantAccessToBoxButton(60), "Grant Access To Box Button", action.SCROLLANDBOOLEAN)) {
									log(LogStatus.PASS, "clicked on grant access to box button",YesNo.No);
									return parentId;
								}else {
									log(LogStatus.FAIL, "Not able to click on grant access to box button so cannot login in box ", YesNo.Yes);
								}
								
							}else {
								log(LogStatus.FAIL, "Not able to click on authorized button so cannot login in box ", YesNo.Yes);
							}
							
						}else {
							log(LogStatus.FAIL, "Not able to pass value in password text box so cannot login in box ", YesNo.Yes);
						}
					}else {
						log(LogStatus.FAIL, "Not able to pass value in username text box so cannot login in box ", YesNo.Yes);
					}
				}else {
					log(LogStatus.FAIL, "Box login window title is not matched Expected : \"Customer Log In\" Actual Result :"+getTitle(driver), YesNo.Yes);
				}
			}else {
				log(LogStatus.FAIL, "No new window is open so cannot login in Box", YesNo.Yes);
			}
		}else {
			log(LogStatus.FAIL, "Not able to click tag or document button so cannot login in box", YesNo.Yes);
		}
		return parentId;
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param driver
	 * @param pathToTraverse
	 * @return true/false
	 */
	public boolean verifyFolderStructure(WebDriver driver, String pathToTraverse) {
		String folderStruct[] = pathToTraverse.split("/");
		String xpath1 = "//span[text()='";
		String xpath2 = "/../preceding-sibling::span[2]";
		String xpath3 = "/../following-sibling::ul//span[text()='";
		String xpath4 = "']";
		String xpath5 = xpath1 + folderStruct[0] + xpath4 + xpath2;
		for (int i = 0; i < folderStruct.length; i++) {
			WebElement ele = FindElement(driver, xpath5, "+Sign In Front Of Folder: " + folderStruct[i], action.BOOLEAN,
					20);
			if (ele != null) {
				if (click(driver, ele, "Folder: " + folderStruct[i], action.SCROLLANDBOOLEAN)) {
					log(LogStatus.PASS, "Clicked on Folder Structure : "+folderStruct[i], YesNo.No);
					if(i==folderStruct.length-1){
						break;
					}
					xpath5 = xpath5 + xpath3 + folderStruct[i + 1] + xpath4 + xpath2;
				} else {
					log(LogStatus.FAIL, "Not able to click on Folder Name  "+folderStruct[i],YesNo.Yes);
					return false;
				}
			} else {
				log(LogStatus.FAIL, "Folder Name is not found : "+folderStruct[i], YesNo.No);
				return false;
			}
		}
		return true;
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param sheetName
	 * @param timeOut
	 * @return true/false
	 */
	public boolean createFolderStructureFromExcel(String sheetName, int timeOut){
		Map<String, String> s = folderStructureInExcel(sheetName);
		Set<String> paths = s.keySet();
		Iterator<String> i = paths.iterator();
		i = paths.iterator();
		FolderType folderType = null;
		boolean flag = true;
		while (i.hasNext()) {
			String string = i.next();
			if (string.isEmpty())
				continue;
			System.out.println("\n\n\nCreating folder template\n\n\n");
			if(s.get(string).equalsIgnoreCase("Shared")){
				folderType=FolderType.Shared;
			} else if (s.get(string).equalsIgnoreCase("Common")){
				folderType=FolderType.Common;
			} else if (s.get(string).equalsIgnoreCase("Internal")){
				folderType=FolderType.Internal;
			} else if (s.get(string).equalsIgnoreCase("Standard")){
				folderType=FolderType.Standard;
			}
			List<String> notCreatedFolders = createFolderStructure(string, folderType, Workspace.FundraisingWorkspace, PageName.DealRoomManager, timeOut);
			if(notCreatedFolders.isEmpty()){
				appLog.info("Successfully created: "+string);
			} else {
				String folderNames = createStringOutOfList(notCreatedFolders);
				BaseLib.sa.assertTrue(false,"Following folders are not created: "+folderNames);
				flag = false;
			}
		}
		return flag;
	}
	
	/**
	 * @author ANKUR RANA
	 * @param sheetName
	 * @return folder structure in map
	 */
	public Map<String, String> folderStructureInExcel(String sheetName) {
		int i = 2;
		Map<String, String> struct = new LinkedHashMap<String, String>();
		while (true) {
			String value = ExcelUtils.readData(sheetName, i, 0);
			if (value != null) {
				int totalValues = ExcelUtils.getLastColumn(sheetName, i);
				for (int j = 1; j < totalValues; j++) {
					String path = ExcelUtils.readData(sheetName, i, j);
					String[] paths = path.split(",");
					for (int k = 0; k < paths.length; k++) {
						struct.put(paths[k], ExcelUtils.readData(sheetName, i, 0));
					}
				}
			} else {
				break;
			}
			i++;
		}
		return struct;
	}
	
	/**
	 * @author ANKUR RANA
	 * @param folderPath
	 * @param folderType
	 * @param workspace
	 * @param pageName
	 * @param timeOut
	 * @return
	 */
	@SuppressWarnings("unused")
	public List<String> createFolderStructure(String folderPath, FolderType folderType, Workspace workspace, PageName pageName, int timeOut) {
		String folderStruct[] = folderPath.split("/");
		List<String> notCreatedFolders = new ArrayList<String>();
		String folderStructTemp=null;
		if(folderStruct[0].contains("(Common)")||folderStruct[0].contains("(Internal)")||folderStruct[0].contains("(Shared)")){
			folderStructTemp=(folderStruct[0].split("\\("))[0];
			folderStruct[0]=folderStructTemp;
		}
		int trying = 0;
		String xpath = "//span[contains(text(),'All Folders')]/span/../../../following-sibling::ul/li/div//label[contains(text(),'"
				+ folderStruct[0] + "')]";
		for (int i = 0; i < folderStruct.length; i++) {
			if (checkElementVisibility(driver, FindElement(driver, xpath, "folder", action.SCROLLANDBOOLEAN, 0), "", 2)) {
				// if (i != folderStruct.length - 1) {
				try {
					xpath = xpath + "/../../../following-sibling::ul/li/div//label[contains(text(),'"
							+ folderStruct[i + 1] + "')]";
					System.out.println("\n\n\nSkipping the iteration: " + i + "\n\n\n\n");
				} catch (Exception e) {
					appLog.info(folderPath + " Folder structure is already present.");
				}
				continue;
				// }
			}
			if (i == 0) {
				if(!createParentFolder(folderStruct[i], folderType, pageName, workspace, timeOut)){
					notCreatedFolders.add(folderStruct[i]);
				}
			} else {
				int len = xpath.length();
				String spitby = "/../../../following-sibling::ul/li/div//label[contains(text(),'" + folderStruct[i]
						+ "')]";
				int len1 = spitby.length();
				int lenReq = len - len1;
				String xp = xpath.substring(0, lenReq);
				ec:System.out.println("Value of i: " + i + "XPath after split: " + xp);
				String id = FindElement(driver, xp, "", action.BOOLEAN, 10).getAttribute("id");
				if (!clickOnAddFolderButton(id)) {
					System.err.println("Returning false.");
				}
				if(!createChildFolder(folderStruct[i], workspace, pageName, timeOut)){
					notCreatedFolders.add(folderStruct[i]);
					appLog.error(trying+" Count number when not matched.");
					System.err.println(trying+" Count number when not matched.");
					if(trying!=0){
						trying++;
						GOTO ec;
					}
				}
			}
			if (i != folderStruct.length - 1) {
				xpath = xpath + "/../../../following-sibling::ul/li/div//label[contains(text(),'" + folderStruct[i + 1]
						+ "')]";
				System.out.println("xpath before split " + i + " creation: " + xpath);
			}
		}
		return notCreatedFolders;
	}
	
	/**
	 * @author ANKUR RANA
	 * @param folderName
	 * @param folderType
	 * @param pageName
	 * @param workspace
	 * @param timeOut
	 * @return true/false
	 */
	public boolean createParentFolder(String folderName, FolderType folderType, PageName pageName, Workspace workspace,
			int timeOut) {
		if(workspace.toString().equalsIgnoreCase(Workspace.InvestorWorkspace.toString())) {
			scrollDownThroughWebelement(driver, FindElement(driver, "//a[@title='Alert History']", "alert history link",
					action.SCROLLANDBOOLEAN, 1), "alert history link");
		}
		for (int i = 0; i < 2; i++)
			if (click(driver, FindElement(driver, "//span[@id='add0000'][@title='Add a Folder']", "+Icon",
					action.SCROLLANDBOOLEAN, timeOut), "", action.SCROLLANDBOOLEAN)) {
				if (click(driver, getFolderTypeRadioButton(folderType, workspace, pageName, timeOut),
						folderType.toString() + " Folder Radio Button", action.BOOLEAN)) {
					if (sendKeys(driver, getParentFolderNameTextBox(workspace, pageName, timeOut), folderName,
							"folder name textbox", action.BOOLEAN)) {
						if (click(driver, getParentFolderSaveButton(workspace, pageName, timeOut),
								"Parent folder Save Button folder creation", action.BOOLEAN)) {
							appLog.info("Folder successfully created. folder Name '" + folderName + "' of type '"
									+ folderType.toString() + "'");
							return true;
						} else {
							appLog.error("Not able to click on save button to create folder '" + folderName
									+ "' of type '" + folderType.toString() + "'");
							BaseLib.sa.assertTrue(false, "Not able to click on save button to create folder '"
									+ folderName + "' of type '" + folderType.toString() + "'");
						}
					} else {
						appLog.error("Not able to pass value to folder text box for folder '" + folderName
								+ "' of type '" + folderType.toString() + "'");
						BaseLib.sa.assertTrue(false, "Not able to pass value to folder text box for folder '"
								+ folderName + "' of type '" + folderType.toString() + "'");
					}
				} else {
					appLog.error(
							"Not able to create folder '" + folderName + "' of type '" + folderType.toString() + "'");
					BaseLib.sa.assertTrue(false,
							"Not able to create folder '" + folderName + "' of type '" + folderType.toString() + "'");
				}
			} else {
				appLog.error("Add Icon cannot be clicked, So wont be able to continue with the template creation.");
				BaseLib.sa.assertTrue(false,
						"Add Icon cannot be clicked, So wont be able to continue with the template creation.");
			}
		return false;
	}
	
	/**
	 * @author ANKUR RANA
	 * @param folderName
	 * @param workspace
	 * @param pageName
	 * @param timeOut
	 * @return true/false
	 */
	public boolean createChildFolder(String folderName, Workspace workspace, PageName pageName, int timeOut) {

		for (int i = 0; i < 2; i++)
			if (sendKeys(driver, getChildFolderNameTextBox(workspace, pageName, timeOut), folderName,
					"Child Folder name text box", action.BOOLEAN)) {
				if (click(driver, getChildFolderSaveButton(workspace, pageName, timeOut), "Save Button",
						action.BOOLEAN)) {
					appLog.info(folderName + " Folder created successfully.");
					return true;
				} else {
					appLog.error("Not able to click on save button for folder '" + folderName + "'");
					BaseLib.sa.assertTrue(false, "Cannot enter folder name to create folder '" + folderName + "'");
				}
			} else {
				appLog.error("Cannot enter folder name to create folder '" + folderName + "'");
				BaseLib.sa.assertTrue(false, "Cannot enter folder name to create folder '" + folderName + "'");
			}
		return false;
	}
	
	/**
	 * @author ANKUR RANA
	 * @param id
	 * @return true/false
	 */
	public boolean clickOnAddFolderButton(String id) {
		((JavascriptExecutor) driver).executeScript("document.getElementById('" + "add" + id.substring(3, id.length())
				+ "').setAttribute('style', 'height: 8px; position: absolute; padding: 12px 22px 0px; margin-left: 22px; display: inline-block;');");
		scrollDownThroughWebelement(driver, FindElement(driver,
				"//label[@id='" + id + "']//span[@title='Add a Folder']", "Add folder Button", action.SCROLLANDBOOLEAN, 20),
				"Add Folder Button");
		return click(driver, FindElement(driver, "//label[@id='" + id + "']//span[@title='Add a Folder']",
				"Add folder Button", action.BOOLEAN, 20), "Add a folder", action.BOOLEAN);
	}
	
	/**
	 * @author Ankur Rana
	 * @param sheetName
	 * @return Map<String, String>
	 */
	public Map<String, String> folderStructureInExcel(String filePath, String sheetName) {
		int i = 2;
		Map<String, String> struct = new LinkedHashMap<String, String>();
		while (true) {
			String value = ExcelUtils.readData(filePath, sheetName, i, 0);
			if (value != null) {
				int totalValues = ExcelUtils.getLastColumn(filePath, sheetName, i);
				for (int j = 1; j < totalValues; j++) {
					String path = ExcelUtils.readData(filePath, sheetName, i, j);
					String[] paths = path.split(",");
					for (int k = 0; k < paths.length; k++) {
						struct.put(paths[k], ExcelUtils.readData(filePath, sheetName, i, 0));
					}
				}
			} else {
				break;
			}
			i++;
		}
		return struct;
	}
	
	
	/**
	 * @author ANKIT JAISWAL
	 * @param environment
	 * @param mode
	 * @param tabName
	 * @param timeout
	 * @return true/false
	 */
	public boolean clickOnCreatedNavatarStepId_Lighting(String environment, String mode, TabName tabName, int timeout) {

		String viewList = null;
		switch (tabName) {
		case NavatarSetup:
			viewList = "All";
			break;
		default:
			return false;
		}
		System.err.println("Passed switch statement");
		WebElement ele, selectListView;
		ele = null;
		if (click(driver, getSelectListIcon(60), "Select List Icon", action.SCROLLANDBOOLEAN)) {
			ThreadSleep(3000);
			selectListView = FindElement(driver, "//div[@class='listContent']//li/a/span[text()='" + viewList + "']",
					"Select List View", action.SCROLLANDBOOLEAN, 30);
			if (click(driver, selectListView, "select List View", action.SCROLLANDBOOLEAN)) {
				ThreadSleep(3000);
				if(mode.equalsIgnoreCase(Mode.Lightning.toString())) {
					refresh(driver);
					ThreadSleep(5000);
				}
				if(getNavatarStepNameAscendingDescendingOderXpath(40)!=null) {
					String aa =getNavatarStepNameAscendingDescendingOderXpath(20).getText().trim();
					if(aa.contains("Descending")) {
						if(click(driver, getNavatarStepNameSortingXpath(20), "sorting icon", action.SCROLLANDBOOLEAN)) {
							appLog.info("clicked on navatar step sorting icon");
						}else {
							appLog.error("Not able to click on navatar step sorting icon so cannot sort navatar step name in ascending order");
							return false;
						}
					}else {
						appLog.info("navatar step name is in ascending order");
					}
				}else {
					appLog.error("Navatar Step Name Sorting xpath is not found so cannot click on Navatar Step Id");
				}
				ThreadSleep(5000);
				ele = FindElement(driver,
						"(//table[@data-aura-class='uiVirtualDataTable']//tbody//tr//th//span//a)[1]","navatar step id", action.BOOLEAN, 30);
				ThreadSleep(2000);
				if (click(driver, ele, "navatar step id", action.BOOLEAN)) {
					ThreadSleep(3000);
					return true;
				} else {
					appLog.error("Not able to Click on Already Created Navtar Step Id");
				}
			} else {
				appLog.error("Not able to select on Select View List");
			}
		} else {
			appLog.error("Not able to click on Select List Icon");
		}
		return false;
	}
	
	/**
	 * @author AZHAR ALAM
	 * @param environment
	 * @param mode
	 * @param RecordType
	 * @return  true/false
	 */
	public boolean ClickonRelatedTab_Lighting(String environment,String mode,RecordType RecordType) {
		if(mode.toString().equalsIgnoreCase(Mode.Lightning.toString())) {
			for(int i=0;i<2; i++){
				refresh(driver);
				ThreadSleep(3000);
				List<WebElement> eleList = FindElements(driver, "//*[text()='Related']", "Related Tab");
				for (WebElement ele : eleList) {
					if(click(driver, ele, RecordType+" related tab", action.BOOLEAN)) {
						log(LogStatus.INFO, "clicked on "+RecordType+" related tab", YesNo.No);
						return true;
					}
				}
			}		
			log(LogStatus.ERROR,"Not able to click on related tab "+RecordType ,YesNo.Yes);
			
		}else {
			return true;
		}
		return false;
	}
	
	/**
	 * @author AZHAR ALAM
	 * @param environment
	 * @param mode
	 * @param gridSectionName
	 * @param timeOut
	 * @return true/false
	 */
	public boolean clickOnGridSection_Lightning(String environment,String mode,RelatedList gridSectionName ,int timeOut) {
		WebElement ele = null;
		boolean flag=false;
		String xpath1="//span[@title='"+gridSectionName+"']";
		ele = isDisplayed(driver, FindElement(driver,xpath1, gridSectionName.toString()+ " link", action.SCROLLANDBOOLEAN,timeOut),"visibility", timeOut, gridSectionName.toString()+ " link");
		if(click(driver, ele, gridSectionName.toString()+ " link", action.SCROLLANDBOOLEAN)) {
			log(LogStatus.INFO, "clicked on "+gridSectionName.toString()+" link", YesNo.No);
			flag=true;
		}else {
			log(LogStatus.ERROR, "Not able to click on "+gridSectionName.toString()+" link so cannot verify error message", YesNo.Yes);
		}
		return flag;
	}
}
