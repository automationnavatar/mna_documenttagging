package com.navatar.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

import com.navatar.generic.BaseLib;

import static com.navatar.generic.CommonLib.*;

import java.util.List;

public class DealRoomManagerPage extends BasePageBusinessLayer {

	public DealRoomManagerPage(WebDriver driver) {
		// TODO Auto-generated constructor stub
		super(driver);
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//a[@title='Admin Users']")
	private WebElement adminUserTab;

	/**
	 * @return the adminUserTab
	 */
	public WebElement getAdminUserTab(int timeOut) {
		return isDisplayed(driver, adminUserTab, "Visibility", timeOut, "Admin User Tab");
	}

	@FindBy(xpath = "//div[@id='left_section']//a[@title='Internal Users']")
	private WebElement internalUserTab;

	/**
	 * @return the internalUserTab
	 */
	public WebElement getInternalUserTab(int timeOut) {
		return isDisplayed(driver, internalUserTab, "Visibility", timeOut, "Internal User Tab");
	}

	@FindBy(xpath = "//a[@title='Folder Templates']")
	private WebElement folderTemplateTab;

	/**
	 * @return the folderTemplateTab
	 */
	public WebElement getFolderTemplateTab(int timeOut) {
		return isDisplayed(driver, folderTemplateTab, "Visibility", timeOut, "Folder Template Tab");
	}

	@FindBy(xpath = "//div[@id='left_section']//a[@title='Manage Approvals']")
	private WebElement manageApprovalsTab;

	/**
	 * @return the manageApprovalsTab
	 */
	public WebElement getManageApprovalsTab(int timeOut) {
		return isDisplayed(driver, manageApprovalsTab, "Visibility", timeOut, "Manage Approvals Tab");
	}

	@FindBy(xpath = "//div[@id='left_section']//a[@title='Watermarking']")
	private WebElement watermarkingTab;

	/**
	 * @return the watermarkingTab
	 */
	public WebElement getWatermarkingTab(int timeOut) {
		return isDisplayed(driver, watermarkingTab, "Visibility", timeOut, "Watermarking Tab");
	}

	@FindBy(xpath = "//a[@title='Contact Access']")
	private WebElement contactAccessTab;

	/**
	 * @return the contactAccessTab
	 */
	public WebElement getContactAccessTab(int timeOut) {
		return isDisplayed(driver, contactAccessTab, "Visibility", timeOut, "Contact Access Tab");
	}

	@FindBy(xpath = "//a[@title='Deal Room Import']")
	private WebElement dealRoomImporttab;

	/**
	 * @return the dealRoomImporttab
	 */
	public WebElement getDealRoomImporttab(int timeOut) {
		return isDisplayed(driver, dealRoomImporttab, "Visibility", timeOut, "Deal Room Import Tab");
	}

	@FindBy(xpath = "//a[@title='Profiles']")
	private WebElement profileTab;

	/**
	 * @return the profileTab
	 */
	public WebElement getProfileTab(int timeOut) {
		return isDisplayed(driver, profileTab, "Visibility", timeOut, "Profiles Tab");
	}

	@FindBy(xpath = "//a[@title='My Profile']")
	private WebElement myProfileTab;

	/**
	 * @return the myProfileTab
	 */
	public WebElement getMyProfileTab(int timeOut) {
		return isDisplayed(driver, myProfileTab, "Visibility", timeOut, "My ProFile Tab");
	}

	@FindBy(xpath = "//a[contains(@title,'My Firm')]")
	private WebElement myFirmsProfileTab;

	/**
	 * @return the myFirmsProfileTab
	 */
	public WebElement getMyFirmsProfileTab(int timeOut) {
		return isDisplayed(driver, myFirmsProfileTab, "Visibility", timeOut, "My Firm's Profile Tab");
	}

	@FindBy(xpath = "//span[@class='head']//a")
	private WebElement informationIconOnAdminUsersTab;

	/**
	 * @return the informationIconOnAdminUsersTab
	 */
	public WebElement getInformationIconOnAdminUsersTab(int timeOut) {
		return isDisplayed(driver, informationIconOnAdminUsersTab, "Visibility", timeOut,
				"Information Icon OnAdmin Users Tab");
	}

	@FindBy(id="errMsgTemplateImported")
	private WebElement errorMessageTemplateImport;
	/**
	 * @return the errorMessageTemplateImport
	 */
	public WebElement getErrorMessageTemplateImport(int timeOut) {
		return isDisplayed(driver, errorMessageTemplateImport, "Visibility", timeOut, "Error Message Template Import Error Msg");
	}

	@FindBy(xpath = "//input[@value='Start']")
	private WebElement startButton;

	/**
	 * @return the startButton
	 */
	public WebElement getStartButton(int timeOut) {
		return isDisplayed(driver, startButton, "Visibility", timeOut, "Start Button");
	}

	@FindBy(id = "pageid:RegFormId:firstName")
	private WebElement firstName;

	/**
	 * @return the firstName
	 */
	public WebElement getFirstName(int timeOut) {
		return isDisplayed(driver, firstName, "Visibility", timeOut, "First Name");
	}

	@FindBy(id = "pageid:RegFormId:lastName")
	private WebElement lastName;

	/**
	 * @return the lastName
	 */
	public WebElement getLastName(int timeOut) {
		return isDisplayed(driver, lastName, "Visibility", timeOut, "Last Name");
	}

	@FindBy(xpath = "//input[@title='Register']")
	private WebElement registerButton;

	/**
	 * @return the registerButton
	 */
	public WebElement getRegisterButton(int timeOut) {
		//return isDisplayed(driver, registerButton, "Visibility", timeOut, "Register Button");
		WebElement ele= BaseLib.edriver.findElement(By.cssSelector("input[title=Register]"));
		return ele;
	}

	@FindBy(id = "pageid:RegFormId:LNameError")
	private WebElement lastNameErrorMsg;

	/**
	 * @return the lastNameErrorMsg
	 */
	public WebElement getLastNameErrorMsg(int timeOut) {
		return isDisplayed(driver, lastNameErrorMsg, "Visibility", timeOut, "Last Name Error Message");
	}

	@FindBy(id = "pageid:RegFormId:FNameError")
	private WebElement firstNameErrorMsg;

	/**
	 * @return the firstNameErrorMsg
	 */
	public WebElement getFirstNameErrorMsg(int timeOut) {
		return isDisplayed(driver, firstNameErrorMsg, "Visibility", timeOut, "First Name Error Message");
	}

	@FindBy(xpath = "//a[@onclick='mailopen();']")
	private WebElement navatarSalesTeamLink;

	/**
	 * @return the navatarSalesTeamLink
	 */
	public WebElement getNavatarSalesTeamLink(int timeOut) {
		return isDisplayed(driver, navatarSalesTeamLink, "Visibility", timeOut, "Navatar Sales Team Link");
	}

	@FindBy(id = "theIframe")
	public WebElement dealRoomManagerFrame;

	/**
	 * @return the dealRoomManagerFrame
	 */
	public WebElement getDealRoomManagerFrame(int timeOut) {
		return isDisplayed(driver, dealRoomManagerFrame, "Visibility", timeOut, "Deal Room Manager IFrame");
	}

	@FindBy(xpath = "//div[contains(@class,'Information_fancybox')]//a[@title='Navatar Support']")
	private WebElement navatarSupportLink;

	/**
	 * @return the navatarSupportLink
	 */
	public WebElement getNavatarSupportLink(int timeOut) {
		return isDisplayed(driver, navatarSupportLink, "Visibility", timeOut, "Navatar Support Link");
	}

	@FindBy(xpath = "(//a[@title='Close'])[2]")
	private WebElement closeButtonInInformationPopup;

	/**
	 * @return the closeButtonInInformationPopup
	 */
	public WebElement getCloseButtonInInformationPopup(int timeOut) {
		return isDisplayed(driver, closeButtonInInformationPopup, "Visibility", timeOut,
				"Close Button In Information Icon Popup");
	}

	@FindBy(xpath = "(//a[@title='Search'])[1]")
	private WebElement searchIconOnDRMgrid;

	/**
	 * @return the searchIconOnDRMgri
	 */
	public WebElement getSearchIconOnDRMgrid(int timeOut) {
	//	return isDisplayed(driver, searchIconOnDRMgrid, "Visibility", timeOut, "Search Icon On DRM grid");
		WebElement ele= BaseLib.edriver.findElement(By.cssSelector("a[title=Search]"));
		return ele;
	}

	@FindBy(xpath = "//span[contains(text(),'No data to display')]")
	private WebElement errorMessageOnGlobalUserGrid;

	/**
	 * @return the errorMessageOnUserGrid
	 */
	public WebElement getErrorMessageOnGlobalUserGrid(int timeOut) {
		return isDisplayed(driver, errorMessageOnGlobalUserGrid, "Visibility", timeOut, "Error Message On User Grid");
	}
	
	
	@FindBy(xpath = "//span[contains(text(),'No Contacts to display.')]")
	private WebElement errorMessageOnContactAccess;

	/**
	 * @return the errorMessageOnContactAccess
	 */
	public WebElement getErrorMessageOnContactAccess(int timeOut) {
		return isDisplayed(driver, errorMessageOnContactAccess, "Visibility", timeOut, "Error Message On User Grid");
	}
	
	
	@FindBy(xpath = "//span[contains(text(),'No data to display.')]")
	private WebElement errorMessageOnManageApproval;

	/**
	 * @return the errorMessageOnUserGrid
	 */
	public WebElement getErrorMessageOnManageApproval(int timeOut) {
		return isDisplayed(driver, errorMessageOnManageApproval, "Visibility", timeOut, "Error Message On User Grid");
	}
	
	@FindBy(xpath = "//span[contains(text(),'No data to display')]")
	private WebElement errorMessageOnHomeAlert;

	/**
	 * @return the errorMessageOnHomeAlert
	 */
	public WebElement getErrorMessageOnHomeAlert(int timeOut) {
		return isDisplayed(driver, errorMessageOnHomeAlert, "Visibility", timeOut, "Error Message On User Grid");
	}
	
	@FindBy(xpath = "//span[contains(text(),'No data to display.')]")
	private WebElement errorMessageOnHomeRecentDocument;

	/**
	 * @return the errorMessageOnHomeRecentDocument
	 */
	public WebElement getErrorMessageOnHomeRecentDocument(int timeOut) {
		return isDisplayed(driver, errorMessageOnHomeRecentDocument, "Visibility", timeOut, "Error Message On User Grid");
	}

	@FindBy(xpath = "//span[@id='grid_AdminUsers2-view-box-middle']//span[text()='No data to display.']")
	private WebElement errorMessageOnByDealUserGrid;

	/**
	 * @return the errorMessageOnByDealUserGrid
	 */
	public WebElement getErrorMessageOnByDealUserGrid(int timeOut) {
		return isDisplayed(driver, errorMessageOnByDealUserGrid, "Visibility", timeOut,
				"Error Message On By Deal User Grid ");
	}

	@FindBy(xpath = "//a[contains(@title,'Edit')]//img")
	private WebElement editIconOnDRMTab;

	/**
	 * @return the editIconOnDRMTab
	 */
	public WebElement getEditIconOnDRMTab(int timeOut) {
		ThreadSleep(5000);
		if(isDisplayed(driver, editIconOnDRMTab, "visibility", timeOut, "Edit IconOn DRM Tab")!=null && isDisplayed(driver, editIconOnDRMTab, "Clickable", timeOut, "Edit IconOn DRM Tab")!=null)
			return isDisplayed(driver, editIconOnDRMTab, "visibility", timeOut, "Edit IconOn DRM Tab");
		else
			return isDisplayed(driver, editIconOnDRMTab, "visibility", timeOut, "Edit IconOn DRM Tab");
	}

	@FindBy(xpath = "//input[@id='bydealEditRadioBtn']")
	private WebElement byDealRadioButton;

	/**
	 * @return the byDealRadioButton
	 */
	public WebElement getByDealRadioButton(int timeOut) {
		return isDisplayed(driver, byDealRadioButton, "Visibility", timeOut, "By Deal Radio Button");
	}

	@FindBy(xpath = "//h2[text()='Allow Access?']")
	private WebElement allowAccessLabelText;

	/**
	 * @return the allowAccessLabelText
	 */
	public WebElement getAllowAccessLabelText(int timeOut) {
		return isDisplayed(driver, allowAccessLabelText, "Visibility", timeOut, "Allow Access? Text");
	}

	@FindBy(xpath = "//input[@title='Deny']")
	private WebElement denyButton;

	/**
	 * @return the denyButton
	 */
	public WebElement getDenyButton(int timeOut) {
		return isDisplayed(driver, denyButton, "Visibility", timeOut, "Deny Button");
	}

	@FindBy(xpath = "//p[text()='We can�t log you in because of an authentication error. For help, contact your Salesforce administrator.']")
	private WebElement deniedAccessLabeltext;

	/**
	 * @return the deniedAccessLabeltext
	 */
	public WebElement getDeniedAccessLabeltext(int timeOut) {
		return isDisplayed(driver, deniedAccessLabeltext, "Visibility", timeOut, "Remote denied text");
	}

	@FindBy(xpath = "//input[@title='Allow']")
	private WebElement allowButton;

	/**
	 * @return the allowButton
	 */
	public WebElement getAllowButton(int timeOut) {
		return isDisplayed(driver, allowButton, "Visibility", timeOut, "Allow Button");
	}

	@FindBy(xpath = "//div[@class='FancyboxExtraSmall']//div[2]/div")
	private WebElement registrationsuccessfulText;

	/**
	 * @return the registrationsuccessfulText
	 */
	public WebElement getRegistrationsuccessfulText(int timeOut) {
		return isDisplayed(driver, registrationsuccessfulText, "Visibility", timeOut, "Registration successful Text");
	}

	@FindBy(xpath = "//div[@class='FancyboxExtraSmall']/..//input[@title='Close']")
	private WebElement registrationSuccessfulPopUpCloseButton;

	/**
	 * @return the registrationSuccessfulPopUpCloseButton
	 */
	public WebElement getRegistrationSuccessfulPopUpCloseButton(int timeOut) {
		return isDisplayed(driver, registrationSuccessfulPopUpCloseButton, "Visibility", timeOut,
				"Registration Close Button");
	}

	@FindBy(xpath = "//div[contains(@class,'div_a')]//span[contains(text(),'Admin Users')]")
	private WebElement adminUserLabelText;

	/**
	 * @return the adminUserLabelText
	 */
	public WebElement getAdminUserLabelText(int timeOut) {
		return isDisplayed(driver, adminUserLabelText, "Visibility", timeOut, "Admin User Label Text");
	}

	@FindBy(xpath = "//div[@id='UserErrorMsg']//p")
	private WebElement CRMErrorMessagebBeforeAccess;

	/**
	 * @return the cRMErrorMessageOnDRMTabBeforeAccess
	 */
	public WebElement getCRMErrorMessageOnBeforeAccess(int timeOut) {
		return isDisplayed(driver, CRMErrorMessagebBeforeAccess, "Visibility", timeOut, "CRM User Error Message");
	}
	@FindBy(xpath="//span[@id='internalUsersGridSpan2']/../../..//a[@title='Save']")
	private WebElement internalUserSaveButton;

	/**
	 * @return the saveButton
	 */
	public WebElement getInternalUserSaveButton(int timeOut) {
		return isDisplayed(driver, internalUserSaveButton, "Visibility", timeOut, "Save Button");
	}

	@FindBy(xpath = "//form[@id='pageid:formId']/div[7]/div[3]/a")
	private WebElement internalUserCloseButton;

	/**
	 * @return the internalUserCloseButton
	 */
	public WebElement getInternalUserCloseButton(int timeOut) {
		return isDisplayed(driver, internalUserCloseButton, "Visibility", timeOut, "internal user close button");
	}

	@FindBy(xpath = "//span[text()='Folder Templates']")
	private WebElement folderTemplateLabelText;

	/**
	 * @return the folderTemplateLabelText
	 */
	public WebElement getFolderTemplateLabelText(int timeOut) {
		return isDisplayed(driver, folderTemplateLabelText, "Visibility", timeOut, "Folder Template Text Label");
	}

	@FindBy(xpath="//div[@id='content_divid']//a[@title='Save']")
	private WebElement adminUserTabSaveButton;

	/**
	 * @return the adminUserTabSaveButton
	 */
	public WebElement getAdminUserTabSaveButton(int timeOut) {
		return isDisplayed(driver, adminUserTabSaveButton, "Visibility", timeOut, "Admin User Save Button");
	}
	@FindBy(xpath="//div[contains(@class,'SuccessSmall_fancybox')]//a[contains(text(),'Close')]")
	private WebElement adminUserSuccessSmallCloseButton;

	/**
	 * @return the adminUserTabCloseButton
	 */
	public WebElement getAdminUserSuccessSmallCloseButton(int timeOut) {
		return isDisplayed(driver, adminUserSuccessSmallCloseButton, "Visibility", timeOut, "Admin User Success Close Button");
	}
	@FindBy(xpath="//div[contains(@class,'SuccessMedium_fancybox')]//a[contains(text(),'Close')]")
	private WebElement adminUserSuccessMediumCloseButton;

	/**
	 * @return the adminUserSuccessMediumCloseButton
	 */
	public WebElement getAdminUserSuccessMediumCloseButton(int timeOut) {
		return isDisplayed(driver, adminUserSuccessMediumCloseButton, "Visibility", timeOut, "Admin User Success Close Button.");
	}
	
	@FindBy(xpath="//div[contains(@class,'SuccessMedium_fancybox')]//div[2]")
	private WebElement adminUserSuccessMediumTextLabel;

	/**
	 * @return the adminUserSuccessMediumTextLabel
	 */
	public WebElement getAdminUserSuccessMediumTextLabel(int timeOut) {
		return isDisplayed(driver, adminUserSuccessMediumTextLabel, "Visibility", timeOut, "Admin User Success Text");
	}
	
	@FindBy(xpath="//div[contains(@class,'SuccessMedium_fancybox')]//a[@title='Internal Users']")
	private WebElement internalUserLinkInAdminUserSuccessMsg;

	/**
	 * @return the internalUserLinkInAdminUserSuccessMsg
	 */
	public WebElement getInternalUserLinkInAdminUserSuccessMsg(int timeOut) {
		return isDisplayed(driver, internalUserLinkInAdminUserSuccessMsg, "Visibility", timeOut, "Internal User Link");
	}
	
	@FindBy(xpath="//div[@id='div_a']//span[contains(text(),'Internal Users')]")
	private WebElement internalUserLabelText;

	/**
	 * @return the internalUserLabelText
	 */
	public WebElement getInternalUserLabelText(int timeOut) {
		return isDisplayed(driver, internalUserLabelText, "Visibility", timeOut, "Internal User Text label");
	}
	@FindBy(xpath="(//div[@class='heading_box'])[1]/span")
	private WebElement DRMTabsLandingTextMsg;

	/**
	 * @return the dRMTabsLandingTextMsg
	 */
	public WebElement getDRMTabsLandingTextMsg(int timeOut) {
		return isDisplayed(driver, DRMTabsLandingTextMsg, "Visibility", timeOut, "CRM User's Landing Page Text");
	}
	@FindBy(xpath = "//a[@title='Activate']")
	private WebElement activateButton;

	/**
	 * @return the manageApprovalActivateButton
	 */
	public WebElement getActivateButton(int timeOut) {
		return isDisplayed(driver, activateButton, "Visibility", timeOut, "Activate Button");
	}
	@FindBy(xpath="(//div[@class='content_div'])[1]//b[text()='Active']")
	private WebElement manageApprovalActiveStatus;

	/**
	 * @return the manageApprovalStatus
	 */
	public WebElement getManageApprovalStatus(int timeOut) {
		return isDisplayed(driver, manageApprovalActiveStatus, "Visibility", timeOut, "Manage Approval Status");
	}
	@FindBy(xpath="//input[@id='edit_byDR']")
	private WebElement byDealRoomRadioButton;

	/**
	 * @return the byDealRoomRadioButton
	 */
	public WebElement getByDealRoomRadioButton(int timeOut) {
		return isDisplayed(driver, byDealRoomRadioButton, "Visibility", timeOut, "By Deal Room Radio Button");
	}
	@FindBy(xpath="//select[@id='pgid:frmid:mgr3']")
	private WebElement firmNameWatermarkingLocation;

	/**
	 * @return the firmNameWatermarkingLocation
	 */
	public WebElement getFirmNameWatermarkingLocation(int timeOut) {
		return isDisplayed(driver, firmNameWatermarkingLocation, "Visibility", timeOut, "Firm NAme Watermarking Location");
	}
	
	@FindBy(xpath="//input[@title='Save']")
	private WebElement myFirmProfileSaveButton;

	/**
	 * @return the myFirmProfileSaveButton
	 */
	public WebElement getMyFirmProfileSaveButton(int timeOut) {
		return isDisplayed(driver, myFirmProfileSaveButton, "Visibility", timeOut, "My Firm Profile Save Button");
	}
	
	@FindBy(id="pageid:frm1:selectDealViewTextBox")
	private WebElement dealSelectionList;

	/**
	 * @return the dealSelectionList
	 */
	public WebElement getDealSelectionList(int timeOut) {
		return isDisplayed(driver, dealSelectionList, "Visibility", timeOut, "Deal Select Drop Down");
	}
	
	@FindBy(id="searchcon_grid")
	private WebElement userGridSearchBox;

	/**
	 * @return the userGridSearchBox
	 */
	public WebElement getuserGridSearchBox(int timeOut) {
		return isDisplayed(driver, userGridSearchBox, "Visibility", timeOut, "User Grid Search Box");
	}
	
	@FindBys(@FindBy(xpath="//a[@title='Search']"))
	private List<WebElement> searchIcon;

	/**
	 * @return the searchIcon
	 */
	public List<WebElement> getSearchIcon() {
			return searchIcon;
	}
	
	@FindBy(id="pageid:frmCA:IdsearchTxtmaingrid")
	private WebElement contactAccessSearchBox;

	/**
	 * @return the contactAccessSearchBox
	 */
	public WebElement getContactAccessSearchBox(int timeOut) {
		return isDisplayed(driver, contactAccessSearchBox, "Visibility", timeOut, "Search Contacts Field");
	}
	
	@FindBys(@FindBy(xpath="(//span[@id='grid_AdminUsers-rows-start']/following-sibling::span/span)[3]"))
	private List<WebElement> usersInUsersGrid;

	/**
	 * @return the usersInUsersGrid
	 */
	public List<WebElement> getUsersInUsersGrid(int timeOut) {
		if(checkElementsVisibility(driver, usersInUsersGrid, "Users in grid", timeOut)){
			return usersInUsersGrid;
		} else {
			return null;
		}
	}
	
	@FindBys(@FindBy(xpath="//b[contains(text(),'Select Deal Room')]/..//select"))
	private List<WebElement> selectDealRoom;

	/**
	 * @return the selectDealRoom
	 */
	public List<WebElement> getSelectDealRoom() {
		return selectDealRoom;
	} 
	
	@FindBy(xpath="//div[@id='noDealRoomExistMsg']")
	private WebElement noDealRoomErrorMessage;

	/**
	 * @return the noDealRoomErrorMessage
	 */
	public WebElement getNoDealRoomErrorMessage(int timeOut) {
		return isDisplayed(driver, noDealRoomErrorMessage, "Visibility", timeOut, "No deal room error message.");
	}
	
	@FindBy(xpath="//div[contains(@class,'ErrorMsgText ')]//p")
	private WebElement byDealSaveErrorMessagewithoutSelectData;

	/**
	 * @return the byDealSaveErrorMessagewithoutSelectData
	 */
	public WebElement getByDealSaveErrorMessagewithoutSelectData(int timeOut) {
		return isDisplayed(driver, byDealSaveErrorMessagewithoutSelectData, "Visibility", timeOut, "By Deal Save Error Message when Created Deal and User is not Selected.");
	}
	
	@FindBy(xpath="//div[contains(@class,'ErrorMsgText')]//a[contains(text(),'Close')]")
	private WebElement byDealSaveErrorMessageWithoutSelectDataCloseButton;

	/**
	 * @return the byDealSaveErrorMessageWithoutSelectDataCloseButton
	 */
	public WebElement getByDealSaveErrorMessageWithoutSelectDataCloseButton(int timeOut) {
		return isDisplayed(driver, byDealSaveErrorMessageWithoutSelectDataCloseButton, "Visibility", timeOut, "Close Button");
	}
	@FindBy(xpath="//div[contains(@class,'ErrorMsgText')]//p")
	private WebElement byDealSaveErrorMsgWithOutSelectUser;
	
	/**
	 * @return the byDealSaveErrorMsgWithOutSelectUser
	 */
	public WebElement getByDealSaveErrorMsgWithOutSelectUser(int timeOut) {
		return isDisplayed(driver, byDealSaveErrorMsgWithOutSelectUser, "Visibility", timeOut, "By Deal Error Message without select user.");
	}
	
	@FindBy(xpath="//div[contains(@class,'ErrorMsgText')]//a[contains(text(),'Close')]")
	private WebElement byDealSaveCloseBtnWithOutSelectUser;

	/**
	 * @return the byDealSaveCloseBtnWithOutSelectUser
	 */
	public WebElement getByDealSaveCloseBtnWithOutSelectUser(int timeOut) {
		return isDisplayed(driver, byDealSaveCloseBtnWithOutSelectUser, "Visibility", timeOut, "By Deal Close Button without select user");
	}

	@FindBy(xpath="//div[contains(@class,'select_deal_room')]//input[contains(@class,'autofieldEdit')]")
	private WebElement selectDealTextBox;

	/**
	 * @return the selectDealTextBox
	 */
	public WebElement getSelectDealTextBox(int timeOut) {
		return isDisplayed(driver, selectDealTextBox, "Visibility", timeOut, "Select Deal Auto Complete Text Box");
	}
	
	@FindBy(xpath="//div[contains(@class,'Confirmation2_fancybox')]//div[2]")
	private WebElement byDealChangesLostErrorMessage;

	/**
	 * @return the byDealChangesLostErrorMessage
	 */
	public WebElement getByDealChangesLostErrorMessage(int timeOut) {
		return isDisplayed(driver, byDealChangesLostErrorMessage, "Visibility", timeOut, "By Deal Page Changes Lost Error Message Text");
	}
	
	@FindBy(xpath="//div[contains(@class,'Confirmation2_fancybox')]//a[@title='Close']")
	private WebElement byDealchangesLostErrorMessageCloseIcon;

	/**
	 * @return the byDealchangesLostErrorMessageCloseIcon
	 */
	public WebElement getByDealchangesLostErrorMessageCloseIcon(int timeOut) {
		return isDisplayed(driver, byDealchangesLostErrorMessageCloseIcon, "Visibility", timeOut, "By Deal Changes Lost Error Message Close Icon");
	}
	
	@FindBy(xpath="//div[contains(@class,'Confirmation2_fancybox')]//a[@title='Yes']")
	private WebElement byDealChnageLostErrorMessageSaveBtn;

	/**
	 * @return the byDealChnageLostErrorMessageSaveBtn
	 */
	public WebElement getByDealChnageLostErrorMessageSaveBtn(int timeOut) {
		return isDisplayed(driver, byDealChnageLostErrorMessageSaveBtn, "Visibility", timeOut, "By Deal Chnages Lost Save Button");
	}
	@FindBy(xpath="//div[contains(@class,'Confirmation_fancybox')]/div[2]/div[2]")
	private WebElement byDealSaveErrorMessageText;

	/**
	 * @return the byDealSaveErrorMessageText
	 */
	public WebElement getByDealSaveErrorMessageText(int timeOut) {
		return isDisplayed(driver, byDealSaveErrorMessageText, "Visibility", timeOut, "By Deal Save Error Text.");
	}
	
	@FindBy(xpath="//div[contains(@class,'Confirmation_fancybox')]//a[@title='No']")
	private WebElement byDealSaveErrorMessageNoBtn;

	/**
	 * @return the byDealSaveErrorMessageNoBtn
	 */
	public WebElement getByDealSaveErrorMessageNoBtn(int timeOut) {
		return isDisplayed(driver, byDealSaveErrorMessageNoBtn, "Visibility", timeOut, "No Button");
	}
	@FindBy(xpath="//div[contains(@class,'Confirmation_fancybox')]//a[@title='Yes']")
	private WebElement byDealSaveErrorMessageYesBtn;

	/**
	 * @return the byDealSaveErrorMessageYesBtn
	 */
	public WebElement getByDealSaveErrorMessageYesBtn(int timeOut) {
		return isDisplayed(driver, byDealSaveErrorMessageYesBtn, "Visibility", timeOut, "Yes Button");
	}
	
	@FindBy(xpath="//div[contains(@class,'SuccessLarge_fancybox')]//a[contains(text(),'Close')]")
	private WebElement byDealCloseBtn;

	/**
	 * @return the byDealCloseBtn
	 */
	public WebElement getByDealCloseBtn(int timeOut) {
		return isDisplayed(driver, byDealCloseBtn, "Visibility", timeOut, "Close Button");
	}
	@FindBy(xpath="//div[@id='savecanceldiv']//a[@title='Save']")
	private WebElement watermarkingSaveButton;

	/**
	 * @return the watermarkingSaveButton
	 */
	public WebElement getWatermarkingSaveButton(int timeOut) {
		return isDisplayed(driver, watermarkingSaveButton, "Visibility", timeOut, "Watermarking save button");
	}
	
//	@FindBy(xpath="//center//a[@title='Save']")
//	private WebElement manageApprovalSaveButton;
//
//	/**
//	 * @return the manageApprovalSaveButton
//	 */
//	public WebElement getManageApprovalSaveButton(int timeOut) {
//		return isDisplayed(driver, manageApprovalSaveButton, "Visibility", timeOut, "Manage Approval Save Button");
//	}
	
	@FindBy(xpath="//a[@title='Deactivate']")
	private WebElement DeactivateButton;

	/**
	 * @return the deactivateButton
	 */
	public WebElement getDeactivateButton(int timeOut) {
		return isDisplayed(driver, DeactivateButton, "Visibility", timeOut, "Deactivate Button");
	}
	@FindBy(xpath="(//div[@class='content_div'])[1]//b[text()='Inactive']")
	private WebElement manageApprovalDeactivateStatus;

	/**
	 * @return the manageApprovalDeactivateStatus
	 */
	public WebElement getManageApprovalDeactivateStatus(int timeOut) {
		return isDisplayed(driver, manageApprovalDeactivateStatus, "Visibility", timeOut, "Deactivate Status");
	}
	
	@FindBy(xpath="//div[@id='recordsCount'] ")
	private WebElement userRecordCount;

	/**
	 * @return the userRecordCount
	 */
	public WebElement getUserRecordCount(int timeOut) {
		return isDisplayed(driver, userRecordCount, "Visibility", timeOut, "User record Count");
	}
	
	@FindBy(xpath="//input[@id='globalEditRadioBtn']")
	private WebElement globalRadioButton;

	/**
	 * @return the globalRadioButton
	 */
	public WebElement getGlobalRadioButton(int timeOut) {
		return isDisplayed(driver, globalRadioButton, "Visibility", timeOut, "Global Radio Button");
	}
	
	@FindBy(id="globalViewRadioBtn")
	private WebElement globalRadioBtnInViewMode;
	
	/**
	 * @return the globalRadioBtnInViewMode
	 */
	public WebElement getGlobalRadioBtnInViewMode(int timeOut) {
		return isDisplayed(driver, globalRadioBtnInViewMode, "Visibility", timeOut, "Global Radio Button in view mode");
	}

	@FindBy(id="noDealRoomExistMsg")
	private WebElement errorMessageOnInternalUserInByDeal;

	/**
	 * @return the errorMessageOnInternalUserInByDeal
	 */
	public WebElement getErrorMessageOnInternalUserInByDeal(int timeOut) {
		return isDisplayed(driver, errorMessageOnInternalUserInByDeal, "Visibility", timeOut, "Error Message on Internal User");
	}
	
	@FindBy(xpath="(//div[contains(text(),'Insufficient')]/following-sibling::*)[1]")
	private WebElement insufficientPermissionErrorMessage;

	/**
	 * @return the insufficientPermissionErrorMessage
	 */
	public WebElement getInsufficientPermissionErrorMessage(int timeOut) {
		return isDisplayed(driver, insufficientPermissionErrorMessage, "Visibility", timeOut, "Insufficient Permission Error Message Text");
	}
	
	@FindBy(xpath="((//div[contains(text(),'Insufficient')]/following-sibling::*)[2]//following-sibling::*)[1]")
	private WebElement insufficientPermissionErrorMessageCloseBtn;

	/**
	 * @return the insufficientPermissionErrorMessageCloseBtn
	 */
	public WebElement getInsufficientPermissionErrorMessageCloseBtn(int timeOut) {
		return isDisplayed(driver, insufficientPermissionErrorMessageCloseBtn, "Visibility", timeOut, "Insufficient Permission Error Message Close Button");
	}
	
	@FindBy(xpath="//input[@id='maGlobal']")
	private WebElement successPopupManageApprovalGlobalRadioButton;

	/**
	 * @return the successPopupManageApprovalGlobalRadioButton
	 */
	public WebElement getSuccessPopupManageApprovalGlobalRadioButton(int timeOut) {
		return isDisplayed(driver, successPopupManageApprovalGlobalRadioButton, "Visibility", timeOut, "Success popup Manage Approval Global Radio Button");
	}
	
	@FindBy(xpath="//input[@id='maByDeal']")
	private WebElement successPopupManageApprovalByDealRadioButton;

	/**
	 * @return the successPopupManageApprovalByDealRadioButton
	 */
	public WebElement getSuccessPopupManageApprovalByDealRadioButton(int timeOut) {
		return isDisplayed(driver, successPopupManageApprovalByDealRadioButton, "Visibility", timeOut, "Success popup Manage Approval By Deal Radio Button");
	}
	
	@FindBy(xpath="//input[@id='wmGlobal']")
	private WebElement successPopupWatermarkingGlobalRadioButton;

	/**
	 * @return the successPopupWatermarkingGlobalRadioButton
	 */
	public WebElement getSuccessPopupWatermarkingGlobalRadioButton(int timeOut) {
		return isDisplayed(driver, successPopupWatermarkingGlobalRadioButton, "Visibility", timeOut, "Success popup Watermarking Global Radio Button");
	}
	
	@FindBy(xpath="//input[@id='wmByDeal']")
	private WebElement successPopupWatermarkingByDealRadioButton;

	/**
	 * @return the successPopupWatermarkingByDealRadioButton
	 */
	public WebElement getSuccessPopupWatermarkingByDealRadioButton(int timeOut) {
		return isDisplayed(driver, successPopupWatermarkingByDealRadioButton, "Visibility", timeOut, "Success popup Watermarking By Deal Radio Button");
	}
	
	@FindBy(xpath="//div[contains(@class,'SuccessLarge_fancybox')]//a[@title='Save']")
	private WebElement successLargeSaveButton;

	/**
	 * @return the successLargeSaveButton
	 */
	public WebElement getSuccessLargeSaveButton(int timeOut) {
		return isDisplayed(driver, successLargeSaveButton, "Visibility", timeOut, "Success Large Popup Save Button");
	}
	
	@FindBy(id="pageid:frm1:selectDealEditTextBox")
	private WebElement dealSelectionListEditMode;

	/**
	 * @return the dealSelectionListEditMode
	 */
	public WebElement getDealSelectionListEditMode(int timeOut) {
		return isDisplayed(driver, dealSelectionListEditMode, "Visibility", timeOut, "Deal Selection List");
	}
	
	@FindBy(xpath="//input[@id='edit_glbR']")
	private WebElement manageApprovalEditGlobalRadioButton;

	/**
	 * @return the manageApprovalEditGlobalRadioButton
	 */
	public WebElement getManageApprovalEditGlobalRadioButton(int timeOut) {
		return isDisplayed(driver, manageApprovalEditGlobalRadioButton, "Visibility", timeOut, "Manage Approval Edit Global Radio Button");
	}
	
	@FindBy(xpath="//input[@id='edit_byDR']")
	private WebElement manageApprovalEditByDealRadioButton;

	/**
	 * @return the manageApprovalEditByDEalRadio
	 */
	public WebElement getManageApprovalEditByDealRadioButton(int timeOut) {
		return isDisplayed(driver, manageApprovalEditByDealRadioButton, "Visibility", timeOut, "Manage Approval Edit By Deal Radio Button");
	}
	@FindBy(xpath="//input[@id='rbtGlobal']")
	private WebElement watermarkingEditGlobalRadioButton;

	/**
	 * @return the watermarkingEditGlobalRadioButton
	 */
	public WebElement getWatermarkingEditGlobalRadioButton(int timeOut) {
		return isDisplayed(driver, watermarkingEditGlobalRadioButton, "Visibility", timeOut, "Watermarking Edit Global Radio Button");
	}
	@FindBy(xpath="//input[@id='rbtbyDeal']")
	private WebElement watermarkingEditByDealRadioButton;

	/**
	 * @return the watermarkingEditByDealRadioButton
	 */
	public WebElement getWatermarkingEditByDealRadioButton(int timeOut) {
		return isDisplayed(driver, watermarkingEditByDealRadioButton, "Visibility", timeOut, "Watermarking By Deal Radio Button");
	}

	@FindBy(xpath="//a[@title='Add Folder Template']")
	private WebElement addFolderTemplateButton;

	/**
	 * @return the addFolderTemplateButton
	 */
	public WebElement getAddFolderTemplateButton(int timeOut) {
		return isDisplayed(driver, addFolderTemplateButton, "Visibility", timeOut, "Add Folder Template Button");
	}
	
	@FindBy(id="newname")
	private WebElement folderTemplateNameTextBox;

	/**
	 * @return the folderTemplateNameTextBox
	 */
	public WebElement getFolderTemplateNameTextBox(int timeOut) {
		return isDisplayed(driver, folderTemplateNameTextBox, "Visibility", timeOut, "Folder template input text box.");
	}
	
	@FindBy(id="page:form:newdesc")
	private WebElement folderTemplateDescription;

	/**
	 * @return the folderTemplateDescription
	 */
	public WebElement getFolderTemplateDescription(int timeOut) {
		return isDisplayed(driver, folderTemplateDescription, "Visibility", timeOut, "Folder Template Description Text Box");
	}
	
	@FindBy(xpath="//a[@title='Add a Folder']")
	private WebElement addAFolderButton;

	/**
	 * @return the addAFolderButton
	 */
	public WebElement getAddAFolderButton(int timeOut) {
		return isDisplayed(driver, addAFolderButton, "Visibility", timeOut, "Add a folder button");
	}
	
	@FindBy(xpath="//input[@id='chkStandardFolder']")
	private WebElement standardFolderRadioButton;

	
	@FindBy(xpath="//input[@id='checkboxstandard_grid1']")
	private WebElement standardFolderRadioButtonOnManageFolder;
	
	/**
	 * @return the standardFolderRadioButton
	 */
	public WebElement getStandardFolderRadioButton(int timeOut,PageName pageName1) {
		WebElement ele;
		if(pageName1.toString().equalsIgnoreCase(PageName.ManageFolderPopUp.toString())) {
			ele=isDisplayed(driver, standardFolderRadioButtonOnManageFolder, "Visibility", timeOut, "Standard folder radio button");
		}else {
			ele=isDisplayed(driver, standardFolderRadioButton, "Visibility", timeOut, "Standard folder radio button");
		}
		
		return ele;
	}
	
	@FindBy(xpath="//input[@id='chkGlobalFolder']")
	private WebElement globalFolderRadioButton;
	
	@FindBy(xpath="//input[@id='checkboxglobal_grid1']")
	private WebElement globalFolderRadioButtonOnManageFolder;
	
	

	/**
	 * @return the globalFolderRadioButton
	 */
	public WebElement getGlobalFolderRadioButton(int timeOut,PageName pageName1) {
		
		WebElement ele;
		if(pageName1.toString().equalsIgnoreCase(PageName.ManageFolderPopUp.toString())) {
			ele=isDisplayed(driver, globalFolderRadioButtonOnManageFolder, "Visibility", timeOut, "Global folder radio Button");
		}else {
			ele=isDisplayed(driver, globalFolderRadioButton, "Visibility", timeOut, "Global folder radio Button");
		}
		
		return ele;
	}
	
	@FindBy(xpath="//input[@id='chkSharedFolder']")
	private WebElement sharedFolderRadioButton;
	
	@FindBy(xpath="//input[@id='checkboxshared_grid1']")
	private WebElement sharedFolderRadioButtonOnManageFolder;
	
	
	/**
	 * @return the sharedFolderRadioButton
	 */
	public WebElement getSharedFolderRadioButton(int timeOut,PageName pageName1) {
		
		WebElement ele;
		if(pageName1.toString().equalsIgnoreCase(PageName.ManageFolderPopUp.toString())) {
			ele=isDisplayed(driver, sharedFolderRadioButtonOnManageFolder, "Visibility", timeOut, "Shared Folder radio button");
		}else {
			ele=isDisplayed(driver, sharedFolderRadioButton, "Visibility", timeOut, "Shared Folder radio button");
		}
		
		return ele;
	}
	
	@FindBy(xpath="//input[@id='chkInternalFolder']")
	private WebElement internalFolderRadioButton;

	@FindBy(xpath="//input[@id='checkboxinternal_grid1']")
	private WebElement internalFolderRadioButtonOnManageFolder;
	
	/**
	 * @return the internalFolderRadioButton
	 */
	public WebElement getInternalFolderRadioButton(int timeOut,PageName pageName1) {
		
		WebElement ele;
		if(pageName1.toString().equalsIgnoreCase(PageName.ManageFolderPopUp.toString())) {
			ele=isDisplayed(driver, internalFolderRadioButtonOnManageFolder, "Visibility", timeOut, "Internal folder radio button");
		}else {
			ele=isDisplayed(driver, internalFolderRadioButton, "Visibility", timeOut, "Internal folder radio button");
		}
		return ele;
	}
	
	@FindBy(id="txtParentFolderName")
	private WebElement parentFolderNameTextBoxOnFolderTemplate;

	
	/**
	 * @return the parentFolderNameTextBox
	 */
	public WebElement getParentFolderNameTextBox(int timeOut) {
		return isDisplayed(driver, parentFolderNameTextBoxOnFolderTemplate, "Visibility", timeOut, "Parent Folder Name Text box");
	}
	
	@FindBy(id="txtparentFolderDescription")
	private WebElement parentFolderDescriptionTextBox;
	
	/**
	 * @return the parentFolderDescriptionTextBox
	 */
	public WebElement getParentFolderDescriptionTextBox(int timeOut,PageName pageName) {
		return isDisplayed(driver, parentFolderDescriptionTextBox, "Visibility", timeOut, "Parent folder description text box");
	}
	
	@FindBy(xpath="//a[contains(@onclick,'ParentFolder')]")
	private WebElement parentFolderSaveButton;

	
	/**
	 * @return the parentFolderSaveButton
	 */
	public WebElement getParentFolderSaveButton(int timeOut) {
		return isDisplayed(driver, parentFolderSaveButton, "Visibility", timeOut, "Parent Folder Save Button");
	}
	
	@FindBy(id="txtChildFolderName")
	private WebElement childFolderName;

	/**
	 * @return the childFolderName
	 */
	public WebElement getChildFolderName(int timeOut) {
		return isDisplayed(driver, childFolderName, "Visibility", timeOut, "Child Folder Text Box");
	}
	
	@FindBy(id="txtChildFolderDescription")
	private WebElement childFolderDescriptionTextBox;

	/**
	 * @return the childFolderDescriptionTextBox
	 */
	public WebElement getChildFolderDescriptionTextBox(int timeOut) {
		return isDisplayed(driver, childFolderDescriptionTextBox, "Visibility", timeOut, "Child Folder Description");
	}
	
	@FindBy(xpath="//a[contains(@onclick,'ChildFolder')]")
	private WebElement childFolderSaveButton;

	/**
	 * @return the parentFolderSaveButton
	 */
	public WebElement getChildFolderSaveButton(int timeOut) {
		return isDisplayed(driver, childFolderSaveButton, "Visibility", timeOut, "Child Folder Save Button");
	}
	
	@FindBy(xpath="(//a[@id='btnSaveTempActiveTop'])[1]")
	private WebElement folderTemplatePageSaveButton;

	/**
	 * @return the folderTemplatePageSaveButton
	 */
	public WebElement getFolderTemplatePageSaveButton(int timeOut) {
		return isDisplayed(driver, folderTemplatePageSaveButton, "Visibility", timeOut, "Folder Template Save Button");
	}
	
	@FindBy(xpath="//div[text()='Confirm Save']/following-sibling::div//a[text()='Yes']")
	private WebElement confirmSaveYesButton;

	/**
	 * @return the confirmSaveYesButton
	 */
	public WebElement getConfirmSaveYesButton(int timeOut) {
		return isDisplayed(driver, confirmSaveYesButton, "Visibility", timeOut, "Confirm Save Yes Button");
	}
	
	@FindBy(xpath="//a[@title='Go Back']")
	private WebElement goBackLink;

	/**
	 * @return the goBackLink
	 */
	public WebElement getGoBackLink(int timeOut) {
		return isDisplayed(driver, goBackLink, "Visibility", timeOut, "Go Back Link");
	}
	
	@FindBys(@FindBy(xpath="//span[contains(@id,'grid_AdminUsers2-cell-1')]"))
	private  List<WebElement> noOfUsersInGrid;
	
	public List<WebElement> getnoOfUsersInGrid(int timeOut) {
		if (checkElementsVisibility(driver, noOfUsersInGrid,"No.Of Users In Grid",timeOut)){
			return noOfUsersInGrid;
		}
		return null;
	}
	@FindBy(xpath="//table[@id='labelsTable']//input[@id='chkFirmName']")
	private WebElement watermarkingMyFirmNameCheckbox;

	/**
	 * @return the watermarkingMyFirmNameCheckbox
	 */
	public WebElement getWatermarkingMyFirmNameCheckbox(int timeOut) {
		return isDisplayed(driver, watermarkingMyFirmNameCheckbox, "Visibility", timeOut, "WaterMarking My Firm Name Checkbox");
	}
	
	@FindBy(xpath="//div[contains(@class,'Confirmation_fancybox')]//a[@title='Close']")
	private WebElement confirmationPopupCloseIconWhileSwitchingSetting;

	/**
	 * @return the confirmationPopupCloseIconWhileSwitchingSetting
	 */
	public WebElement getConfirmationPopupCloseIconWhileSwitchingSetting(int timeOut) {
		return isDisplayed(driver, confirmationPopupCloseIconWhileSwitchingSetting, "Visibility", timeOut, "Close Icon In Confirmation Popup while switching the setting");
	}
	
	@FindBy(xpath = ".//*[@id='deleteTemp']")
	private WebElement folderTemplateDeleteBtn;

	/**
	 * @return the folderTemplateDeleteBtn
	 */
	public WebElement getFolderTemplateDeleteBtn(int timeOut) {
		return isDisplayed(driver, folderTemplateDeleteBtn, "Visibility", timeOut, "Folder Template delete Button");
	}
	
	@FindBy(xpath = "//div[@id='confirmDeleteTemp']/div[3]/a[@title='Yes']")
	private WebElement folderTemplateDeleteYesBtn;

	/**
	 * @return the folderTemplateDeleteYesBtn
	 */
	public WebElement getFolderTemplateDeleteYesBtn(int timeOut) {
		return isDisplayed(driver, folderTemplateDeleteYesBtn, "Visibility", timeOut, "Folder Template Delete Yes Button");
	}
	@FindBys(@FindBy(xpath="//span[@ id='templateGrid-rows']/span//a"))
	private List<WebElement> folderTemplateGrid;

	/**
	 * @return the folderTemplateGrid
	 */
	public List<WebElement> getFolderTemplateGrid(int timeOut) {
		if(checkElementsVisibility(driver, folderTemplateGrid, "Folder Template Grid", timeOut)){
			return folderTemplateGrid;
		} else {
			return null;
		}
	}
	
	@FindBy(xpath="//select[@name='page:form:j_id14']")
	private WebElement folderTemplateDisplayDropDownList;

	/**
	 * @return the folderTemplateDisplayDropDownList
	 */
	public WebElement getFolderTemplateDisplayDropDownList(int timeOut) {
		return isDisplayed(driver, folderTemplateDisplayDropDownList, "Visibility", timeOut, "Folder Template Display Drop Down List");
	}
	
	@FindBy(id="page_myprofiletragetid:frm_myprofiletragetid:txtusertitle")
	private WebElement title;

	/**
	 * @return the title
	 */
	public WebElement getTitle(int timeOut) {
		return isDisplayed(driver, title, "Visibility", timeOut, "Title");
	}
	
	@FindBy(id="page_myprofiletragetid:frm_myprofiletragetid:txtuserphone")
	private WebElement phoneNumber;

	/**
	 * @return the phoneNumber
	 */
	public WebElement getPhoneNumber(int timeOut) {
		return isDisplayed(driver, phoneNumber, "Visibility", timeOut, "Phone Number");
	}
	
	@FindBy(id="page_myprofiletragetid:frm_myprofiletragetid:txtuserstreet")
	private WebElement mailingStreet;

	/**
	 * @return the mailingStreet
	 */
	public WebElement getMailingStreet(int timeOut) {
		return isDisplayed(driver, mailingStreet, "Visibility", timeOut, "Mailing Street");
	}
	
	@FindBy(id="page_myprofiletragetid:frm_myprofiletragetid:txtusercity")
	private WebElement mailingCity;

	/**
	 * @return the mailingCity
	 */
	public WebElement getMailingCity(int timeOut) {
		return isDisplayed(driver, mailingCity, "Visibility", timeOut, "Miling City");
	}
	
	@FindBy(id="page_myprofiletragetid:frm_myprofiletragetid:txtuserstate")
	private WebElement mailingState;

	/**
	 * @return the mailingState
	 */
	public WebElement getMailingState(int timeOut) {
		return isDisplayed(driver, mailingState, "Visibility", timeOut, "Mailing State");
	}
	
	@FindBy(id="page_myprofiletragetid:frm_myprofiletragetid:txtusercountry")
	private WebElement mailingCountry;

	/**
	 * @return the mailngCountry
	 */
	public WebElement getMailngCountry(int timeOut) {
		return isDisplayed(driver, mailingCountry, "Visibility", timeOut, "Mailing Country");
	}
	
	@FindBy(id="page_myprofiletragetid:frm_myprofiletragetid:txtuserzip")
	private WebElement mailingZipCode;

	/**
	 * @return the mailingZipCode
	 */
	public WebElement getMailingZipCode(int timeOut) {
		return isDisplayed(driver, mailingZipCode, "Visibility", timeOut, "Mailing Zip Code");
	}
	
	@FindBy(xpath="//input[contains(@name,'page_myprofiletragetid:frm_myprofiletragetid')][@value='Save']")
	private WebElement saveButton;

	/**
	 * @return the saveButton
	 */
	public WebElement getSaveButton(int timeOut) {
		return isDisplayed(driver, saveButton, "Visibility", timeOut, "Save Button");
	}
	
	@FindBy(xpath="//textarea[contains(@id,'fstreet')]")
	private WebElement firmMailingStreet;

	/**
	 * @return the mailingStreet
	 */
	public WebElement getFirmMailingStreet(int timeOut) {
		return isDisplayed(driver, firmMailingStreet, "Visibility", timeOut, "Firm Mailing Street");
	}
	
	@FindBy(id="pageid:j_id0:fcity")
	private WebElement firmMailingCity;

	/**
	 * @return the firmMailingCity
	 */
	public WebElement getFirmMailingCity(int timeOut) {
		return isDisplayed(driver, firmMailingCity, "Visibility", timeOut, "Firm Mialing City");
	}
	
	@FindBy(id="pageid:j_id0:fstate")
	private WebElement firmMailingState;

	/**
	 * @return the firmMailingState
	 */
	public WebElement getFirmMailingState(int timeOut) {
		return isDisplayed(driver, firmMailingState, "Visibility", timeOut, "Firm Mialing State");
	}
	
	@FindBy(id="pageid:j_id0:fcountry")
	private WebElement firmMailingCountry;

	/**
	 * @return the firmMailingCountry
	 */
	public WebElement getFirmMailingCountry(int timeOut) {
		return isDisplayed(driver, firmMailingCountry, "Visibility", timeOut, "Firm Mailing Country");
	}
	
	@FindBy(id="pageid:j_id0:fzip")
	private WebElement firmMailingZipCode;

	/**
	 * @return the firmMailingZipCode
	 */
	public WebElement getFirmMailingZipCode(int timeOut) {
		return isDisplayed(driver, firmMailingZipCode, "Visibility", timeOut, "Firm Mailing Zip Code");
	}
	
	@FindBy(id="pageid:j_id0:fwebsite")
	private WebElement firmWebsite;

	/**
	 * @return the firmWebsite
	 */
	public WebElement getFirmWebsite(int timeOut) {
		return isDisplayed(driver, firmWebsite, "Visibility", timeOut, "Firm Website Text Box");
	}
	

	/**
	 * @return the contactNameInContactAccessTabGrid
	 */
	public List<WebElement> getContactNameInContactAccessTabGrid() {
		return FindElements(driver, "//span[contains(@id,'grid_ContactAccess-cell-1')]//a", "Contact Name in Contact Access Tab Grid");

	}
		
	@FindBy(id="pageid:j_id0:fcon")
	private WebElement firmContact;

	/**
	 * @return the firmPhone
	 */
	public WebElement getfirmContact(int timeOut) {
		return isDisplayed(driver, firmContact, "Visibility", timeOut, "Firm Contact");
	}
	
	@FindBy(id="pageid:j_id0:femail")
	private WebElement firmEmail;

	/**
	 * @return the firmEmail
	 */
	public WebElement getFirmEmail(int timeOut) {
		return isDisplayed(driver, firmEmail, "Visibility", timeOut, "Firm Email");
	} 
	
	@FindBy(id="pageid:j_id0:fphone")
	private WebElement firmPhone;

	/**
	 * @return the firmPhone
	 */
	public WebElement getFirmPhone(int timeOut) {
		return isDisplayed(driver, firmPhone, "Visibility", timeOut, "Firm phone");
	}
	
	@FindBy(id="fdesc")
	private WebElement firmDescription;

	/**
	 * @return the firmDescription
	 */
	public WebElement getFirmDescription(int timeOut) {
		return isDisplayed(driver, firmDescription, "Visibility", timeOut, "Firm Description");
	}
	
	
	@FindBy(xpath="//a[text()='Change Logo']")
	private WebElement changeLogoLink;

	/**
	 * @return the changeLogoLink
	 */
	public WebElement getChangeLogoLink(int timeOut) {
		return isDisplayed(driver, changeLogoLink, "Visibility", timeOut, "Change Logo Link");
	}
	
	@FindBy(id="bckgrndcolor")
	private WebElement backgroundColorIcon;

	/**
	 * @return the backgroundColorIcon
	 */
	public WebElement getBackgroundColorIcon(int timeOut) {
		return isDisplayed(driver, backgroundColorIcon, "Visibility", timeOut, "Background Color Icon");
	}
	
	@FindBy(xpath="//div[@class='colorpicker_hex']/input")
	private WebElement colorPickerHexCode;

	/**
	 * @return the colorPickerHexCode
	 */
	public WebElement getColorPickerHexCode(int timeOut) {
		return isDisplayed(driver, colorPickerHexCode, "Visibility", timeOut, "Color Picker Hex Code");
	}
	
	@FindBy(xpath="//input[@title='Browse']")
	private WebElement browseButton;

	/**
	 * @return the browseButton
	 */
	public WebElement getBrowseButton(int timeOut) {
		return isDisplayed(driver, browseButton, "Visibility", timeOut, "Browser Button");
	}
	
	@FindBy(xpath="//a[@title='Save']")
	private WebElement saveButtonImageUpload;

	/**
	 * @return the saveButton
	 */
	public WebElement getSaveButtonImageUpload(int timeOut) {
		return isDisplayed(driver, saveButtonImageUpload, "Visibility", timeOut, "Save Button");
	}
	
	@FindBy(id="pg:frm1:errmessages")
	private WebElement OtherTypeFileUploadInLogoErrorMessage;
	
	/**
	 * @return the otherTypeFileUploadInLogoErrorMessage
	 */
	public WebElement getOtherTypeFileUploadInLogoErrorMessage(int timeOut) {
		return isDisplayed(driver, OtherTypeFileUploadInLogoErrorMessage, "Visibility", timeOut, "File Type not correct error message");
	}

	@FindBy(xpath="//a[@title='Cancel']")
	private WebElement cancelButton;
	
	/**
	 * @return the cancelButton
	 */
	public WebElement getCancelButton(int timeOut) {
		return isDisplayed(driver, cancelButton, "Visibility", timeOut, "Cancel Button");
	}

	@FindBy(id="ErrorDiv")
	private WebElement errorMsgOnLogoPopUp;

	/**
	 * @return the errorMsgOnLogoPopUp
	 */
	public WebElement getErrorMsgOnLogoPopUp(int timeOut) {
		return isDisplayed(driver, errorMsgOnLogoPopUp, "Visibility", timeOut, "Error Message on logo pop Up");
	}

	@FindBy(id="btnCrop")
	private WebElement cropButton;

	/**
	 * @return the cropButton
	 */
	public WebElement getCropButton(int timeOut) {
		return isDisplayed(driver, cropButton, "Visibility", timeOut, "Crop Button");
	}
	
	@FindBy(xpath="//input[@value='Save']")
	private WebElement saveButtonFirmProfile;

	
	/**
	 * @return the saveButtonFirmProfile
	 */
	public WebElement getSaveButtonFirmProfile(int timeOut) {
		return isDisplayed(driver, saveButtonFirmProfile, "Visibility", timeOut, "Save Button");
	}
	
	@FindBy(xpath="//td[text()='Header Background Color:']")
	private WebElement header;

	/**
	 * @return the header
	 */
	public WebElement getHeader(int timeOut) {
		return isDisplayed(driver, header, "Visibility", timeOut, "Header backGround Color");
	}
	
	@FindBy(xpath="//td[contains(text(),'Firm Name')]/../td[2]")
	private WebElement firmNameLabelTextInMyFirmProfile;

	/**
	 * @return the firmNameLabelTextInMyFirmProfile
	 */
	public WebElement getFirmNameLabelTextInMyFirmProfile(int timeOut) {
		return isDisplayed(driver, firmNameLabelTextInMyFirmProfile, "Visibility", timeOut, "MY Firm Profile Text");
	}
	
	@FindBy(xpath="//strong[contains(text(),'Firm Name')]/../../td[2]//input")
	private WebElement firmNameTextBoxInMyFirmName;
	
	/**
	 * @return the firmNameTextBoxInMyFirmName
	 */
	public WebElement getFirmNameTextBoxInMyFirmName(int timeOut) {
		return isDisplayed(driver, firmNameTextBoxInMyFirmName, "Visibility", timeOut, "My Firm Name Text Box");
	}

	@FindBys(@FindBy(xpath="//span[contains(@id,'grid_RemoveContactsAccess2-cell-1')]//label"))
	private List<WebElement> dealNameInRemoveContactAccessTabGrid;

	/**
	 * @return the contactNameInContactAccessTabGrid
	 */
	public List<WebElement> getdealNameInRemoveContactAccessTabGrid(int timeOut) {
		return dealNameInRemoveContactAccessTabGrid;
	}
	
	@FindBy(xpath="//div[contains(@class,'RemoveContactsAccess3')]//a[text()='Close']")
	private WebElement removeContactAccessCloseButton;

	/**
	 * @return the removeContactAccessCloseButton
	 */
	public WebElement getRemoveContactAccessCloseButton(int timeOut) {
		return isDisplayed(driver, removeContactAccessCloseButton, "Visibility", timeOut, "Remove Contact Access Close Button");
	}
	
	@FindBy(xpath="//span[@class='ViewFieldName' and contains(text(),'Name')]/../../td[2]/div")
	private WebElement myProfileSuperAdminName;

	/**
	 * @return the myProfileSuperAdminName
	 */
	public WebElement getMyProfileSuperAdminName(int timeOut) {
		return isDisplayed(driver, myProfileSuperAdminName, "Visibility", timeOut, "Super Admin Name in my profile");
	}

	/**
	 * @param myProfileSuperAdminName the myProfileSuperAdminName to set
	 */
	public void setMyProfileSuperAdminName(WebElement myProfileSuperAdminName) {
		this.myProfileSuperAdminName = myProfileSuperAdminName;
	}
	
	@FindBy(xpath="//td[contains(@id,'TitleView')]/div")
	private WebElement firmTitleLabelInMyProfile;

	/**
	 * @return the firmTitleLabelInMyProfile
	 */
	public WebElement getFirmTitleLabelInMyProfile(int timeOut) {
		return isDisplayed(driver, firmTitleLabelInMyProfile, "Visibility", timeOut, "Firm Title Label In My Profile");
	} 
	
	
	@FindBy(xpath="//td[contains(@id,'phoneView')]/div")
	private WebElement firmPhoneLabelInMyProfile;

	/**
	 * @return the firmPhoneLabelInMyProfile
	 */
	public WebElement getFirmPhoneLabelInMyProfile(int timeOut) {
		return isDisplayed(driver, firmPhoneLabelInMyProfile, "Visibility", timeOut, "Firm Phone Label In My Profile");
	} 
	
	
	@FindBy(xpath="//td[contains(text(),'Firm Contact')]/../td[2]")
	private WebElement firmContactLabelInMyFirmProfile;

	/**
	 * @return the firmContactInMyFirmProfile
	 * 
	 */
	public WebElement getFirmContactLabelInMyFirmProfile(int timeOut) {
		return isDisplayed(driver, firmContactLabelInMyFirmProfile, "Visibility", timeOut, "Firm Contact In My Firm Profile");
	} 
	
	@FindBy(xpath="//td[contains(text(),'Website')]/../td[2]/a")
	private WebElement firmWebsiteLabelInMyFirmProfile;

	/**
	 * @return the firmWebsiteInMyFirmProfile
	 */
	public WebElement getFirmWebsiteLabelInMyFirmProfile(int timeOut) {
		return isDisplayed(driver, firmWebsiteLabelInMyFirmProfile, "Visibility", timeOut, "Firm Website Label In My Firm Profile");
	}
	
	@FindBy(id="viewUserList")
	private WebElement internalUserTabViewDropDownList;

	/**
	 * @return the internalUserTabViewDropDownList
	 */
	public WebElement getInternalUserTabViewDropDownList(int timeOut) {
		return isDisplayed(driver, internalUserTabViewDropDownList, "Visibility", timeOut, "internal user tab view drop down list");
	}


	@FindBy(xpath="//span[@id='demo-basic']//img")
	private WebElement internalUserInfoIcon;

	/**
	 * @return the internalUserInfoIcon
	 */
	public WebElement getInternalUserInfoIcon(int timeOut) {
		return isDisplayed(driver, internalUserInfoIcon, "Visibility", timeOut, "Internal User Info Icon");
	}
	
	@FindBy(xpath="(//div[@class='tip-yellow']//td)[4]//div")
	private WebElement internalUserInfoText;

	/**
	 * @return the internalUserInfoText
	 */
	public WebElement getInternalUserInfoText(int timeOut) {
		return isDisplayed(driver, internalUserInfoText, "Visibility", timeOut, "Internal User Info text");
	}
	
	@FindBy(xpath="//div[contains(@class,'SuccessLarge_fancybox')]//a[@title='Manage Approvals']")
	private WebElement switchSettingPopupManageApprovalLink;

	/**
	 * @return the switchSettingPopupManageApprovalLink
	 */
	public WebElement getSwitchSettingPopupManageApprovalLink(int timeOut) {
		return isDisplayed(driver, switchSettingPopupManageApprovalLink, "Visibility", timeOut, "Switch Setting Popup Manage Approval Link");
	}
	
	@FindBy(xpath="//div[contains(@class,'SuccessLarge_fancybox')]//a[@title='Watermarking']")
	private WebElement switchSettingPopupWatermarkingLink;

	/**
	 * @return the switchSettingPopupWatermarkingLink
	 */
	public WebElement getSwitchSettingPopupWatermarkingLink(int timeOut) {
		return isDisplayed(driver, switchSettingPopupWatermarkingLink, "Visibility", timeOut, "SSwitch Setting Popup Watermarking Link");
	}

	
	@FindBy(xpath="//table[@id='labelsTable']//b/../..//input")
	private WebElement watermarkingCutomLabelCheckBox;

	/**
	 * @return the watermarkingCutomLabelCheckBox
	 */
	public WebElement getWatermarkingCutomLabelCheckBox(int timeOut) {
		return isDisplayed(driver, watermarkingCutomLabelCheckBox, "Visibility", timeOut, "Watermarking Custom Label CheckBox");
	}
	
	@FindBy(xpath="//table[@id='labelsTable']//div//a[@id='btnAddMore']")
	private WebElement watermarkingCustlabelAddRowLink;

	/**
	 * @return the watermarkingCustlabelAddRowLink
	 */
	public WebElement getWatermarkingCustlabelAddRowLink(int timeOut) {
		return isDisplayed(driver, watermarkingCustlabelAddRowLink, "Visibility", timeOut, "Watermarking Custom Label Add Row Link");
	}
	
	@FindBys(@FindBy(xpath="//table[@id='labelsTable']/tbody/tr/td[2]/input"))
	private List<WebElement> waterMarkingCustomLabelTextBox;

	
	public List<WebElement> getwaterMarkingCustomLabelTextBox(int timeOut) {
		if(checkElementsVisibility(driver, waterMarkingCustomLabelTextBox, "Watermarking Custome Label Text Box", timeOut)){
			return waterMarkingCustomLabelTextBox;
		} else {
			return null;
		}
	}
	
	@FindBys(@FindBy(xpath="//b[text()='Additional Custom Labels']/../..//following-sibling::tr//select"))
     private List<WebElement> waterMarkingCustomLabelDropDownList;

	
	public List<WebElement> getwaterMarkingCustomLabelDropDownList(int timeOut) {
		if(checkElementsVisibility(driver, waterMarkingCustomLabelDropDownList, "Watermarking Custome Label Drop down", timeOut)){
			return waterMarkingCustomLabelDropDownList;
		} else {
			return null;
		}
	}
	
	@FindBy(xpath="//input[@id='chkFirmName']")
	private WebElement MyFirmWatermarkingCheckbox;

	/**
	 * @return the myFirmWatermarkingCheckbox
	 */
	public WebElement getMyFirmWatermarkingCheckbox(int timeOut) {
		return isDisplayed(driver, MyFirmWatermarkingCheckbox, "Visibility", timeOut, "My Firm Watermarking Checkbox");
	}
	
	@FindBy(xpath="//a[@title='Preview']")
	private WebElement previewLinkInWatermarkingPage;

	/**
	 * @return the previewLinkInWatermarkingPage
	 */
	public WebElement getPreviewLinkInWatermarkingPage(int timeOut) {
		return isDisplayed(driver, previewLinkInWatermarkingPage, "Visibility", timeOut, "Preview Link In Watermarking Page");
	}
	
	@FindBy(xpath="//span[@id='grid_ManageApprovals-cell-0-0']/span")
	private WebElement errorMessageOnGridMA;

	/**
	 * @return the errorMessageOnGridMA
	 */
	public WebElement getErrorMessageOnGridMA(int timeOut) {
		return isDisplayed(driver, errorMessageOnGridMA, "Visibility", timeOut, "No data to display error message.");
	}
	
	
	@FindBy(id="rbtbyDeal")
	private WebElement watermarkingByDealRadioButton;

	/**
	 * @return the byDealRadioButton
	 */
	public WebElement getWatermarkingByDealRadioButton(int timeOut) {
		return isDisplayed(driver, watermarkingByDealRadioButton, "Visibility", timeOut, "watermarking by deal radio button.");
	}
	
	@FindBy(id="edit_byDR")
	private WebElement manageApprovalByDealRadioButton;

	/**
	 * @return the byDealRadioButton
	 */
	public WebElement getManageApprovalByDealRadioButton(int timeOut) {
		return isDisplayed(driver, manageApprovalByDealRadioButton, "Visibility", timeOut, "manage approval by deal radio button.");
	}
	
	@FindBy(id="redMsg2")
	private WebElement errorMessageManageApprovalNoDealRoom;

	/**
	 * @return the errorMessageManageApprovalNoDealRoom
	 */
	public WebElement getErrorMessageManageApprovalNoDealRoom(int timeOut) {
		return isDisplayed(driver, errorMessageManageApprovalNoDealRoom, "Visibility", timeOut, "No Deal room error message");
	}
	
	@FindBy(id="msgNoDealRoom")
	private WebElement watermarkingNoDealRoomMsg;
	
	/**
	 * @return the watermarkingNoDealRoomMsg
	 */
	public WebElement getWatermarkingNoDealRoomMsg(int timeOut) {
		return isDisplayed(driver, watermarkingNoDealRoomMsg, "Visibility", timeOut, "No Deal Room Error Message");
	}

	@FindBy(xpath="//a[@id='saveMAs']/following-sibling::a")
	private WebElement manageApprovalcancelButton;

	/**
	 * @return the manageApprovalcancelButton
	 */
	public WebElement getManageApprovalcancelButton(int timeOut) {
		return isDisplayed(driver, manageApprovalcancelButton, "Visibility", timeOut, "Manage Approval Cancel Button.");
	}
	
	@FindBy(id="saveMAs")
	private WebElement manageApprovalSaveButton;

	/**
	 * @return the manageApprovalSaveButton
	 */
	public WebElement getManageApprovalSaveButton(int timeOut) {
		return isDisplayed(driver, manageApprovalSaveButton, "Visibility", timeOut, "Manage Approval Save Button");
	}
	
	@FindBy(id="edit_glbR")
	private WebElement manageApprovalGlobalRadioButton;

	/**
	 * @return the manageApprovalGlobalRadioButton
	 */
	public WebElement getManageApprovalGlobalRadioButton(int timeOut) {
		return isDisplayed(driver, manageApprovalGlobalRadioButton, "Visibility", timeOut, "Manage Approval Global Radio Button");
	}
	
	@FindBy(xpath="//div[contains(text(),'switched approvals')]/../following-sibling::div/a[@title='Yes']")
	private WebElement manageApprovalSwitchToGlobalYesButton;

	/**
	 * @return the manageApprovalSwitchToGlobalYesButton
	 */
	public WebElement getManageApprovalSwitchToGlobalYesButton(int timeOut) {
		return isDisplayed(driver, manageApprovalSwitchToGlobalYesButton, "Visibility", timeOut, "Yes Button");
	}
	
	@FindBy(xpath="//p[contains(text(),'switched watermarking')]/../../../following-sibling::div/a[@title='Yes']")
	private WebElement watermarkingSwitchToGlobalYesButton;

	/**
	 * @return the watermarkingSwitchToGlobalYesButton
	 */
	public WebElement getWatermarkingSwitchToGlobalYesButton(int timeOut) {
		return isDisplayed(driver, watermarkingSwitchToGlobalYesButton, "Visibility", timeOut, "Yes Button");
	}
	
//	@FindBy(id="//a[@id='savebtn']")
//	private WebElement watermarkingSaveButton;
//
//	/**
//	 * @return the watermarkingSaveButton
//	 */
//	public WebElement getWatermarkingSaveButton(int timeOut) {
//		return isDisplayed(driver, watermarkingSaveButton, "Visibility", timeOut, "");
//	}
	
	@FindBy(id="cancelbtn")
	private WebElement watermarkingCancelButton;

	/**
	 * @return the watermarkingCancelButton
	 */
	public WebElement getWatermarkingCancelButton(int timeOut) {
		return isDisplayed(driver, watermarkingCancelButton, "Visibility", timeOut, "watermarking Cancel Button");
	}
	
	@FindBy(id="rbtGlobal")
	private WebElement watermarkingGlobalRadioButton;

	/**
	 * @return the watermarkingGlobalRadioButton
	 */
	public WebElement getWatermarkingGlobalRadioButton(int timeOut) {
		return isDisplayed(driver, watermarkingGlobalRadioButton, "Visibility", timeOut, "watermarking global radio button");
	}
	
	@FindBy(xpath="//span[@id='templateGrid-cell-0-0']/span")
	private WebElement folderTemplateErrorMessage;

	/**
	 * @return the folderTemplateErrorMessage
	 */
	public WebElement getFolderTemplateErrorMessage(int timeOut) {
		return isDisplayed(driver, folderTemplateErrorMessage, "Visibility", timeOut, "Folder Template Error Message.");
	}
	
	@FindBy(xpath="//div[@class='tip-inner tip-bg-image']")
	private WebElement toolTipPopUp;

	/**
	 * @return the toolTipPopUp
	 */
	public WebElement getToolTipPopUp(int timeOut) {
		return isDisplayed(driver, toolTipPopUp, "Visibility", timeOut, "Tool Tip Pop Up");
	}
	
	public WebElement getTitleAndSettingsType(String mode, int timeOut){
		if(mode.equalsIgnoreCase("Edit")){
			return isDisplayed(driver, FindElement(driver, "(//div[@id='content_divid'])[2]/div[@class='contacts_n_name_div']/div[2]", "Titles and settings", action.BOOLEAN, 30), "Visibility", 30, "Titles and settings");
		} else {
			return isDisplayed(driver, FindElement(driver, "(//div[@id='content_divid'])[1]/div[@class='contacts_n_name_div']/div[2]", "Titles and settings", action.BOOLEAN, 30), "Visibility", 30, "Titles and settings");
		}
	}
	
	@FindBy(xpath="//input[@id='byDealRDetail']")
	private WebElement byDealRoomInActiveRadioButton;

	/**
	 * @return the byDealRoomInActiveRadioButton
	 */
	public WebElement getByDealRoomInActiveRadioButton(int timeOut) {
		return isDisplayed(driver, byDealRoomInActiveRadioButton, "Visibility", timeOut, "By Deal Room Inactive Radio Button");
	}
	
	@FindBy(id="globRDetail")
	private WebElement globalInactiveRadioButtong;

	/**
	 * @return the globalInactiveRadioButtong
	 */
	public WebElement getGlobalInactiveRadioButtong(int timeOut) {
		return isDisplayed(driver, globalInactiveRadioButtong, "Visibility", timeOut, "Global InActive Radio Button");
	}
	
	@FindBy(id="searchcon_grid")
	private WebElement manageApprovalSearchbox;

	/**
	 * @return the manageApprovalSearchbox
	 */
	public WebElement getManageApprovalSearchbox(int timeOut) {
		return isDisplayed(driver, manageApprovalSearchbox, "Visibility", timeOut, "Manage Approval Search Box");
	}
	
	@FindBy(id="searchcon_grid2")
	private WebElement manageApprovalSearchboxEdit;

	/**
	 * @return the manageApprovalSearchboxEdit
	 */
	public WebElement getManageApprovalSearchboxEdit(int timeOut) {
		return isDisplayed(driver, manageApprovalSearchboxEdit, "Visibility", timeOut, "Manage Approval Search Box");
	}
	
	@FindBy(xpath="//div[@class='ConfirmManageApprovalsActivation_fancybox FancyboxContainer ui-draggable']//div[@class='formbox']")
	private WebElement manageApprovalActivationConfirmationMessage;

	/**
	 * @return the manageApprovalActivationConfirmationMessage
	 */
	public WebElement getManageApprovalActivationConfirmationMessage(int timeOut) {
		return isDisplayed(driver, manageApprovalActivationConfirmationMessage, "Visibility", timeOut, "Manage Approval Activation Confirmation msg");
	}
	
	@FindBy(xpath="//a[@id='upMASetting']")
	private WebElement manageApprovalActivationConfirmationYesBubtton;

	/**
	 * @return the manageApprovalActivationConfirmationYesBubtton
	 */
	public WebElement getManageApprovalActivationConfirmationYesBubtton(int timeOut) {
		return isDisplayed(driver, manageApprovalActivationConfirmationYesBubtton, "Visibility", timeOut, "manage Approval activation confirmation pop up Yes Button.");
	}
	
	@FindBy(xpath="//a[@id='upMASetting']/following-sibling::a")
	private WebElement manageApprovalActivationNoButton;

	/**
	 * @return the manageApprovalActivationNoButton
	 */
	public WebElement getManageApprovalActivationNoButton(int timeOut) {
		return isDisplayed(driver, manageApprovalActivationNoButton, "Visibility", timeOut, "manage Approval activation confirmation pop up No Button.");
	}
	
	@FindBy(xpath="//div[text()='Confirm Manage Approvals Activation']/a")
	private WebElement manageApprovalConfirmationCrossIcon;

	/**
	 * @return the manageApprovalConfirmationCrossIcon
	 */
	public WebElement getManageApprovalConfirmationCrossIcon(int timeOut) {
		return isDisplayed(driver, manageApprovalConfirmationCrossIcon, "Visibility", timeOut, "manage approval confirmation pop up Cross Icon");
	}
	
	@FindBy(xpath="//a[@id='upMASetting_de']")
	private WebElement manageApprovalDeactivatePopUpYesButton;

	/**
	 * @return the manageApprovalDeactivatePopUpYesButton
	 */
	public WebElement getManageApprovalDeactivatePopUpYesButton(int timeOut) {
		return isDisplayed(driver, manageApprovalDeactivatePopUpYesButton, "Visibility", timeOut, "Manage Approval Deactivate Pop Up Yes Button.");
	}
	
	@FindBy(xpath="//a[@id='upMASetting_de']/following-sibling::a")
	private WebElement manageApprovalDeactivatePopUpNoButton;

	/**
	 * @return the manageApprovalDeactivatePopUpNoButton
	 */
	public WebElement getManageApprovalDeactivatePopUpNoButton(int timeOut) {
		return isDisplayed(driver, manageApprovalDeactivatePopUpNoButton, "Visibility", timeOut, "Manage Approval Deactivate Pop Up No Button.");
	}
	
	@FindBy(xpath="//div[text()='Confirm Manage Approvals Deactivation']/a")
	private WebElement manageApprovalDeactivatePopUpCrossIcon;

	/**
	 * @return the manageApprovalDeactivatePopUpCrossIcon
	 */
	public WebElement getManageApprovalDeactivatePopUpCrossIcon(int timeOut) {
		return isDisplayed(driver, manageApprovalDeactivatePopUpCrossIcon, "Visibility", timeOut, "Manage Approval Deactivate Pop Up Cross Icon.");
	}
	
	@FindBy(id="tdLabelID")
	private WebElement watermarkingStatusText;

	/**
	 * @return the watermarkingStatusText
	 */
	public WebElement getWatermarkingStatusText(int timeOut) {
		return isDisplayed(driver, watermarkingStatusText, "Visibility", timeOut, "watermarking Status text");
	}
	
	@FindBy(xpath="//div[@class='Error_fancybox FancyboxContainer ui-draggable']/div[@class='formbox']")
	private WebElement watermarkingActivationErrorMsg;

	/**
	 * @return the watermarkingActivationErrorMsg
	 */
	public WebElement getWatermarkingActivationErrorMsg(int timeOut) {
		return isDisplayed(driver, watermarkingActivationErrorMsg, "Visibility", timeOut, "Watermarking activation error message.");
	}
	
	@FindBy(xpath="//div[@class='Error_fancybox FancyboxContainer ui-draggable']/div[@class='paginationstyle']/a")
	private WebElement watermarkingActivationErrorMsgCloseButton;

	/**
	 * @return the watermarkingActivationErrorMsgCloseBUtton
	 */
	public WebElement getWatermarkingActivationErrorMsgCloseButton(int timeOut) {
		return isDisplayed(driver, watermarkingActivationErrorMsgCloseButton, "Visibility", timeOut, "Watermarking Activation Close Button");
	}
	
	@FindBy(xpath="//div[@class='Error_fancybox FancyboxContainer ui-draggable']/div[@class='head_popup']/a")
	private WebElement watermarkingActivationErrorMsgCrossIcon;

	/**
	 * @return the watermarkingActivationErrorMsgCrossIcon
	 */
	public WebElement getWatermarkingActivationErrorMsgCrossIcon(int timeOut) {
		return isDisplayed(driver, watermarkingActivationErrorMsgCrossIcon, "Visibility", timeOut, "watermarking activation cross icon");
	}
	
	@FindBy(xpath="//a[@class='YellowToolTip']")
	private WebElement targetAccountNameInfoIcon;

	/**
	 * @return the targetAccountNameInfoIcon
	 */
	public WebElement getTargetAccountNameInfoIcon(int timeOut) {
		return isDisplayed(driver, targetAccountNameInfoIcon, "Visibility", timeOut, "Target Account Name Info Icon");
	}
	
	@FindBy(id="chkCustom")
	private WebElement customLabelsCheckBox;

	/**
	 * @return the customLabelsCheckBox
	 */
	public WebElement getCustomLabelsCheckBox(int timeOut) {
		return isDisplayed(driver, customLabelsCheckBox, "Visibility", timeOut, "Custom label check Box");
	}
	
	@FindBy(id="btnAddMore")
	private WebElement watermakingAddRowLink;

	/**
	 * @return the watermakingAddRowLink
	 */
	public WebElement getWatermakingAddRowLink(int timeOut) {
		return isDisplayed(driver, watermakingAddRowLink, "Visibility", timeOut, "Add Row Link");
	}
	
	@FindBy(id="chkTargetAccName")
	private WebElement targetAccountNameCheckBox;

	/**
	 * @return the targetAccountNameCheckBox
	 */
	public WebElement getTargetAccountNameCheckBox(int timeOut) {
		return isDisplayed(driver, targetAccountNameCheckBox, "Visibility", timeOut, "Target Account Name");
	}
	
//	@FindBy(xpath="//a[@onclick='saveBtnClick();']")
//	private WebElement internalUserSaveButton;
	
	@FindBy(xpath="//div[text()='Success']/following-sibling::div[@class='paginationstyle']/a")
	private WebElement successPopUpCloseButton;

	/**
	 * @return the successPopUpCloseButton
	 */
	public WebElement getSuccessPopUpCloseButton(int timeOut) {
		return isDisplayed(driver, successPopUpCloseButton, "Visibility", timeOut, "Success Pop Close Button");
	}
	
	@FindBy(xpath="//div[contains(text(),'Insufficient Permissions')]/a")
	private WebElement insufficientPermissionErrorMessageCrossIcon;

	/**
	 * @return the insufficientPermissionErrorMessageCrossIcon
	 */
	public WebElement getInsufficientPermissionErrorMessageCrossIcon(int timeOut) {
		return isDisplayed(driver, insufficientPermissionErrorMessageCrossIcon, "Visibility", timeOut, "Insufficient Error Message cross Icon");
	}
		
	@FindBy(xpath="//div[contains(@class,'Insufficient')]//a[text()='Close']")
	private WebElement insufficientPermissionCloseButton;

	/**
	 * @return the insufficientPermissionCloseButton
	 */
	public WebElement getInsufficientPermissionCloseButton(int timeOut) {
		return isDisplayed(driver, insufficientPermissionCloseButton, "Visibility", timeOut, "Insufficient permission Close Button");
	}
	
	@FindBy(xpath="//div[contains(@class,'Confirmation_fancybox')]//div[2]//div[2]")
	private WebElement manageApprovalAndWatermarkingSwitchSettingErrorMessage;

	/**
	 * @return the manageApprovalErrorMessage
	 */
	public WebElement getManageApprovalAndWatermarkingSwitchSettingErrorMessage(int timeOut) {
		return isDisplayed(driver, manageApprovalAndWatermarkingSwitchSettingErrorMessage, "Visibility", timeOut, "Manage Approval Error Message");
	}
	
	@FindBy(xpath="//div[contains(@class,'Confirmation_fancybox')]//a[text()='Yes']")
	private WebElement manageApprovalAndWatermarkingSwitchSettingPopupYesButton;

	/**
	 * @return the manageApprovalSwitchSettingPopupYesButton
	 */
	public WebElement getManageApprovalAndWatermarkingSwitchSettingPopupYesButton(int timeOut) {
		return isDisplayed(driver, manageApprovalAndWatermarkingSwitchSettingPopupYesButton, "Visibility", timeOut, "Manage Approval Switch Setting popup Yes button");
	} 
	
	@FindBy(xpath="//div[contains(@class,'Confirmation_fancybox')]//a[text()='No']")
	private WebElement manageApprovalAndWatermarkingSwitchSettingPopupNoButton;

	/**
	 * @return the manageApprovalSwitchSettingPopupNoButton
	 */
	public WebElement getManageApprovalAndWatermarkingSwitchSettingPopupNoButton(int timeOut) {
		return isDisplayed(driver, manageApprovalAndWatermarkingSwitchSettingPopupNoButton, "Visibility", timeOut, "Manage Approval Switch Setting popup No Button");
	}
	
	@FindBy(xpath="//div[contains(@class,'Confirmation_fancybox')]//a[@title='Close']")
	private WebElement manageApprovalAndWatermarkingSwitchSettingpopupcloseIcon;

	/**
	 * @return the manageApprovalSwitchSettingpopupcloseIcon
	 */
	public WebElement getManageApprovalAndWatermarkingSwitchSettingpopupcloseIcon(int timeOut) {
		return isDisplayed(driver, manageApprovalAndWatermarkingSwitchSettingpopupcloseIcon, "Visibility", timeOut, "Manage Approval Switch Setting popup close Icon");
	}
	
	@FindBy(xpath="//span[text()='User']")
	private WebElement manageApprovalUserLabel;

	/**
	 * @return the manageApprovalUserLabel
	 */
	public WebElement getManageApprovalUserLabel(int timeOut) {
		return isDisplayed(driver, manageApprovalUserLabel, "Visibility", timeOut, "Manager Approval User Label");
	}
	
	@FindBys(@FindBy(xpath="//span[contains(@id,'grid_ManageApprovals-cell-0-')]//span"))
	private List<WebElement> manageApprovalUsersList;

	
	public List<WebElement> getmanageApprovalUsersList(int timeOut) {
		if(checkElementsVisibility(driver, manageApprovalUsersList, "Manage Approval users List", timeOut)){
			return manageApprovalUsersList;
		} else {
			return null;
		}
	}
	
	@FindBy(xpath="//span[text()='Manage Approvals']")
	private WebElement manageApprovalManageApprovalLabel;
		
	/**
	 * @return the manageApprovalManageApprovalLabel
	 */
	public WebElement getManageApprovalManageApprovalLabel(int timeOut) {
		return isDisplayed(driver, manageApprovalManageApprovalLabel, "Visibility", timeOut, "Manage Approval Label");
	}

	@FindBys(@FindBy(xpath="//span[contains(@id,'grid_ManageApprovals-cell-1-')]//input"))
	private List<WebElement> manageApprovalCheckboxList;

	
	public List<WebElement> getmanageApprovalCheckboxList(int timeOut) {
		if(checkElementsVisibility(driver, manageApprovalCheckboxList, "Manage Approval Checkbox List", timeOut)){
			return manageApprovalCheckboxList;
		} else {
			return null;
		}
	}
	
	
	@FindBy(xpath="//div[@id='clearsearchenb']//a[@title='Clear Search']")
	private WebElement manageApprovalClearSearchIcon;

	/**
	 * @return the manageApprovalClearSearchIcon
	 */
	public WebElement getManageApprovalClearSearchIcon(int timeOut) {
		return isDisplayed(driver, manageApprovalClearSearchIcon, "Visibility", timeOut, "Manage Approval Clear Search icon");
	}
	
	@FindBy(id="chkFirmName")
	private WebElement watermarkingFirmNameLabel;

	/**
	 * @return the watermarkingFirmNameLabel
	 */
	public WebElement getWatermarkingFirmNameLabel(int timeOut) {
		return isDisplayed(driver, watermarkingFirmNameLabel, "Visibility", timeOut, "Firm Name Label");
	}
	
	@FindBy(id="chkTargetAccName")
	private WebElement targetAccountNameLabel;

	/**
	 * @return the targetAccountNameLabel
	 */
	public WebElement getTargetAccountNameLabel(int timeOut) {
		return isDisplayed(driver, targetAccountNameLabel, "Visibility", timeOut, "Target Account Name Label");
	}
	
	@FindBy(id="chkDealRoomName")
	private WebElement dealRoomNameLabel;

	/**
	 * @return the dealRoomNameLabel
	 */
	public WebElement getDealRoomNameLabel(int timeOut) {
		return isDisplayed(driver, dealRoomNameLabel, "Visibility", timeOut, "Deal Room Name Label");
	}
	
	@FindBy(id="chkDownloadDate")
	private WebElement downloadDateLabel;

	/**
	 * @return the downloadDateLabel
	 */
	public WebElement getDownloadDateLabel(int timeOut) {
		return isDisplayed(driver, downloadDateLabel, "Visibility", timeOut, "Download Date Label");
	}
	
	@FindBy(id="chkIpAddress")
	private WebElement IPAddressLabel;

	/**
	 * @return the iPAddressLabel
	 */
	public WebElement getIPAddressLabel(int timeOut) {
		return isDisplayed(driver, IPAddressLabel, "Visibility", timeOut, "IP Address Label");
	}
	
	@FindBy(id="chkEmailAddress")
	private WebElement emailAddressLabel;

	/**
	 * @return the emailAddressLabel
	 */
	public WebElement getEmailAddressLabel(int timeOut) {
		return isDisplayed(driver, emailAddressLabel, "Visibility", timeOut, "E-mail Address Label");
	}
	
	@FindBy(xpath="//td[contains(text(),'My')][contains(text(),'Firm')][contains(text(),'Name')]/following-sibling::td//select")
	private WebElement myFirmNameDropDown;

	/**
	 * @return the myFirmNameDropDown
	 */
	public WebElement getMyFirmNameDropDown(int timeOut) {
		return isDisplayed(driver, myFirmNameDropDown, "Visibility", timeOut, "My Firm Name Drop Down");
	}
	
	@FindBy(xpath="//td[contains(text(),'Deal')][contains(text(),'Room')][contains(text(),'Name')]/following-sibling::td//select")
	private WebElement dealRoomNameLabelDropDown;

	/**
	 * @return the dealRoomNameLabelDropDown
	 */
	public WebElement getDealRoomNameLabelDropDown(int timeOut) {
		return isDisplayed(driver, dealRoomNameLabelDropDown, "Visibility", timeOut, "Deal Room Name Label Drop Down");
	}
	
	@FindBy(xpath="//td[contains(text(),'Date')][contains(text(),'Time')][contains(text(),'EST')]/following-sibling::td//select")
	private WebElement downloadDateLabelDropDown;

	/**
	 * @return the downloadDateLabelDropDown
	 */
	public WebElement getDownloadDateLabelDropDown(int timeOut) {
		return isDisplayed(driver, downloadDateLabelDropDown, "Visibility", timeOut, "DownLoad Date Drop Down");
	}
	
	@FindBy(xpath="//td[contains(text(),'IP')][contains(text(),'Address')]/following-sibling::td//select")
	private WebElement IPAddressLabelDropDown;

	/**
	 * @return the iPAddressLabelDropDown
	 */
	public WebElement getIPAddressLabelDropDown(int timeOut) {
		return isDisplayed(driver, IPAddressLabelDropDown, "Visibility", timeOut, "IP Address Drop Down");
	}
	
	@FindBy(xpath="//td[contains(text(),'Email')][contains(text(),'Address')]/following-sibling::td//select")
	private WebElement emailAddressLabelDropDown;

	/**
	 * @return the emailAddressLabelDropDown
	 */
	public WebElement getEmailAddressLabelDropDown(int timeOut) {
		return isDisplayed(driver, emailAddressLabelDropDown, "Visibility", timeOut, "email Address Label Drop Down");
	}
	
	@FindBy(xpath="//span[contains(text(),'Target')][contains(text(),'Account')][contains(text(),'Name')]/../following-sibling::td//select")
	private WebElement targetAccountNameLabelDropDown;

	/**
	 * @return the targetAccountNameLabelDropDown
	 */
	public WebElement getTargetAccountNameLabelDropDown(int timeOut) {
		return isDisplayed(driver, targetAccountNameLabelDropDown, "Visibility", timeOut, "Target Account Name Drop Down");
	}
	
	@FindBy(id="chkCustom")
	private WebElement customLabelCheckBox;

	/**
	 * @return the customLabelCheckBox
	 */
	public WebElement getCustomLabelCheckBox(int timeOut) {
		return isDisplayed(driver, customLabelCheckBox, "Visibility", timeOut, "Custom Label CheckBox");
	}
	
	@FindBy(id="btnAddMore")
	private WebElement customLabelAddRowLink;

	/**
	 * @return the customLabelAddRowLink
	 */
	public WebElement getCustomLabelAddRowLink(int timeOut) {
		return isDisplayed(driver, customLabelAddRowLink, "Visibility", timeOut, "Cutom Label Add Row Link");
	}


	@FindBy(xpath="//input[@name='page_myprofiletragetid:frm_myprofiletragetid:txtuserfirstname']")
	private WebElement myProfileFirstNameTextBox;

	/**
	 * @return the myProfileFirstNameTextBox
	 */
	public WebElement getMyProfileFirstNameTextBox(int timeOut) {
		return isDisplayed(driver, myProfileFirstNameTextBox, "Visibility", timeOut, "My Profile First Name Text Box");
	} 
	
	@FindBy(xpath="//input[@name='page_myprofiletragetid:frm_myprofiletragetid:txtuserlastname']")
	private WebElement myProfileLastNameTextBox;

	/**
	 * @return the myProfileLastNameTextBox
	 */
	public WebElement getMyProfileLastNameTextBox(int timeOut) {
		return isDisplayed(driver, myProfileLastNameTextBox, "Visibility", timeOut, "My Profile Last Name Text Box");
	}
	
	@FindBy(xpath="//input[@id='pageid:j_id0:fname']")
	private WebElement MyFirmProfileFirmName;

	/**
	 * @return the myFiemProfileFirmName
	 */
	public WebElement getMyFirmProfileFirmName(int timeOut) {
		return isDisplayed(driver, MyFirmProfileFirmName, "Visibility", timeOut, "My Firm Profile Firm Name");
	}

	public List<WebElement> folderTemplatesTableHeader(){
		return FindElements(driver, "//span[@id='templateGrid-view-box-top']//span[@class='aw-item-text ']", "Folder Template Header List");
	}
	
	@FindBy(id="btnCancelTempActiveTop")
	private WebElement folderTemplateCancelButton;

	/**
	 * @return the folderTemplateCancelButton
	 */
	public WebElement getFolderTemplateCancelButton(int timeOut) {
		return isDisplayed(driver, folderTemplateCancelButton, "Visibility", timeOut, "Folder Template Cancel Button");
	}
	
	@FindBy(xpath="(//td[@id='tdTempDetails']//div[@class='head_outer']//span)[2]")
	private WebElement folderTemplateTemplateInfoLabel;

	/**
	 * @return the folderTemplateTemplateInfoLabel
	 */
	public WebElement getFolderTemplateTemplateInfoLabel(int timeOut) {
		return isDisplayed(driver, folderTemplateTemplateInfoLabel, "Visibility", timeOut, "Folder Template Template Info Label ");
	}
	
	@FindBy(xpath="//div[@id='editModeOrder']//span[1]")
	private WebElement folderTemplateFoldersHeader;

	/**
	 * @return the folderTemplateFoldersHeader
	 */
	public WebElement getFolderTemplateFoldersHeader(int timeOut) {
		return isDisplayed(driver, folderTemplateFoldersHeader, "Visibility", timeOut, "Folder Template Folder Header");
	}
	
	@FindBy(xpath="(//div[@class='tree_row_div']//a[@class='x-tree-node-anchor'])[1]")
	private WebElement folderTemplateAddFolderLink;

	/**
	 * @return the folderTemplateAddFolderLink
	 */
	public WebElement getFolderTemplateAddFolderLink(int timeOut) {
		return isDisplayed(driver, folderTemplateAddFolderLink, "Visibility", timeOut, "Folder Template aDd Folder Link");
	} 
	 
	@FindBy(id="btnSaveTempActive")
	private WebElement folderTemplateBottomSaveButton;

	/**
	 * @return the folderTemplateBottomSaveButton
	 */
	public WebElement getFolderTemplateBottomSaveButton(int timeOut) {
		return isDisplayed(driver, folderTemplateBottomSaveButton, "Visibility", timeOut, "Folder Template Bottom Save Button");
	}
	
	@FindBy(id="btnCancelTempActive")
	private WebElement folderTemplateBottomCancelButton;

	/**
	 * @return the folderTemplateBottomCancelButton
	 */
	public WebElement getFolderTemplateBottomCancelButton(int timeOut) {
		return isDisplayed(driver, folderTemplateBottomCancelButton, "Visibility", timeOut, "Folder Template Bottom Cancel Button");
	}
	

	@FindBy(xpath="(//div[@id='home']//span)[1]")
	private WebElement dealRoomImportLabel;

	/**
	 * @return the dealRoomImportLabel
	 */
	public WebElement getDealRoomImportLabel(int timeOut) {
		return isDisplayed(driver, dealRoomImportLabel, "Visibility", timeOut, "Deal Room Import Label");
	}
	
	@FindBy(xpath="//div[@id='home']//div[@id='content_divid']/div[1]/div[2]")
	private WebElement dealRoomImportMsg1;

	/**
	 * @return the dealRoomImportMsg1
	 */
	public WebElement getDealRoomImportMsg1(int timeOut) {
		return isDisplayed(driver, dealRoomImportMsg1, "Visibility", timeOut, "Deal Room Import Msg1");
	}
	
	@FindBy(xpath="//div[@id='home']//div[@id='content_divid']/div[1]/div[2]/p")
	private WebElement dealRoomImportMsg2;

	/**
	 * @return the dealRoomImportMsg2
	 */
	public WebElement getDealRoomImportMsg2(int timeOut) {
		return isDisplayed(driver, dealRoomImportMsg2, "Visibility", timeOut, "Deal Room Import Msg2");
	}
	
	@FindBy(xpath = "//div[@id='home']//div[@id='content_divid']//a[@title='Import']")
	private WebElement importButtonInDealRoomImportTab;

	/**
	 * @return the importButtonInDealRoomImportTab
	 */
	public WebElement getImportButtonInDealRoomImportTab(int timeOut) {
		return isDisplayed(driver, importButtonInDealRoomImportTab, "Visibility", timeOut, "Import Button");
	}
	
	@FindBy(xpath="//div[@id='home']//div[@id='content_divid']//b")
	private WebElement lastImportDateLabelInDealRoomImportTab;

	/**
	 * @return the lastImportDateLabelInDealRoomImportTab
	 */
	public WebElement getLastImportDateLabelInDealRoomImportTab(int timeOut) {
		return isDisplayed(driver, lastImportDateLabelInDealRoomImportTab, "Visibility", timeOut, "Last Import Date Label");
	}
	
	@FindBy(xpath="//div[@id='home']//div[@id='content_divid']//span")
	private WebElement lastImportDateValueInDealRoomImportTab;

	/**
	 * @return the lastImportDateValueInDealRoomImportTab
	 */
	public WebElement getLastImportDateValueInDealRoomImportTab(int timeOut) {
		return isDisplayed(driver, lastImportDateValueInDealRoomImportTab, "Visibility", timeOut, "Last Import Date Value");
	}
	
	@FindBy(xpath="//div[contains(@class,'InsufficientPermissions')]/div[1]")
	private WebElement insuffieceintPermissionPopUp;

	/**
	 * @return the insuffieceintPermissionPopUp
	 */
	public WebElement getInsuffieceintPermissionPopUp(int timeOut) {
		return isDisplayed(driver, insuffieceintPermissionPopUp, "Visibility", timeOut, "Insuffieceint Permission Pop Up");
	}
	
	@FindBy(xpath="//div[contains(@class,'InsufficientPermissions')]/div[2]")
	private WebElement insuffieceintPermissionPopUpMsg;

	/**
	 * @return the insuffieceintPermissionPopUpMsg
	 */
	public WebElement getInsuffieceintPermissionPopUpMsg(int timeOut) {
		return isDisplayed(driver, insuffieceintPermissionPopUpMsg, "Visibility", timeOut, "Insuffieceint Permission Pop Up Msg");
	}
	
	@FindBy(xpath="//div[contains(@class,'InsufficientPermissions')]/div[3]")
	private WebElement insuffieceintPermissionPopUpCloseButton;

	/**
	 * @return the insuffieceintPermissionPopUpCloseButton
	 */
	public WebElement getInsuffieceintPermissionPopUpCloseButton(int timeOut) {
		return isDisplayed(driver, insuffieceintPermissionPopUpCloseButton, "Visibility", timeOut, "Insuffieceint Permission Pop Up Close Button");
	}
	
	@FindBy(xpath="//div[contains(@class,'InsufficientPermissions')]/div[3]")
	private WebElement insuffieceintPermissionPopUpCrossIcon;

	/**
	 * @return the insuffieceintPermissionPopUpCrossIcon
	 */
	public WebElement getInsuffieceintPermissionPopUpCrossIcon(int timeOut) {
		return isDisplayed(driver, insuffieceintPermissionPopUpCrossIcon, "Visibility", timeOut, "Insuffieceint Permission Pop Up Cross Icon");
	}
	
	@FindBy(xpath="//div[@id='blankTextErr']")
	private WebElement folderTemplateNameErrorMessage;

	/**
	 * @return the folderTemplateNameErrorMessage
	 */
	public WebElement getFolderTemplateNameErrorMessage(int timeOut) {
		return isDisplayed(driver, folderTemplateNameErrorMessage, "Visibility", timeOut, "Folder Template Error Message");
	}
	
	@FindBy(xpath="//div[contains(@class,'cancelTemplate_fancybox2 ')]//div[@class='formbox']/div")
	private WebElement folderTemplateCancelErrorMessage;

	/**
	 * @return the folderTemplateCancelErrorMessage
	 */
	public WebElement getFolderTemplateCancelErrorMessage(int timeOut) {
		return isDisplayed(driver, folderTemplateCancelErrorMessage, "Visibility", timeOut, "Folder Template Cancel Error Message");
	}
	
	@FindBy(xpath="//div[contains(@class,'cancelTemplate_fancybox2 ')]//a[@title='No']")
	private WebElement cancelFolderTemplateNoButton;

	/**
	 * @return the cancelFolderTemplateNoButton
	 */
	public WebElement getCancelFolderTemplateNoButton(int timeOut) {
		return isDisplayed(driver, cancelFolderTemplateNoButton, "Visibility", timeOut, "Cancel Folder Template No Button");
	}
	
	@FindBy(xpath="//div[contains(@class,'cancelTemplate_fancybox2 ')]//a[@class='close_facnybox']")
	private WebElement folderTemplateCancelCrossIcon;

	/**
	 * @return the folderTemplateCancelCrossIcon
	 */
	public WebElement getFolderTemplateCancelCrossIcon(int timeOut) {
		return isDisplayed(driver, folderTemplateCancelCrossIcon, "Visibility", timeOut, "Folder Template Cancel Cross Icon");
	}
	
	@FindBy(xpath="//div[contains(@class,'cancelTemplate_fancybox2 ')]//a[@title='Yes']")
	private WebElement folderTemplateCancelYesButon;

	/**
	 * @return the folderTemplateCancelYesButon
	 */
	public WebElement getFolderTemplateCancelYesButon(int timeOut) {
		return isDisplayed(driver, folderTemplateCancelYesButon, "Visibility", timeOut, "Folder Template Cancel Yes Button");
	}
	
	@FindBy(xpath="//div[@id='idDup']//div[@class='formbox']")
	private WebElement folderTemplateAlreadyExistErrorMessage;

	/**
	 * @return the folderTemplateAlreadyExistErrorMessage
	 */
	public WebElement getFolderTemplateAlreadyExistErrorMessage(int timeOut) {
		return isDisplayed(driver, folderTemplateAlreadyExistErrorMessage, "Visibility", timeOut, "Folder Template Already Exist Error Message");
	}
	
	@FindBy(xpath="//div[@id='idDup']//a[@title='Close']")
	private WebElement folderTemplateExistPopupcloseButton;

	/**
	 * @return the folderTemplateExistPopupcloseButton
	 */
	public WebElement getFolderTemplateExistPopupcloseButton(int timeOut) {
		return isDisplayed(driver, folderTemplateExistPopupcloseButton, "Visibility", timeOut, "Folder Template Already Exist popup close button");
	}
	
	@FindBy(xpath="//div[contains(@class,'saveTemplate_fancybox')]//div[@class='formbox']/div")
	private WebElement folderTemplateCreateErrorMessage;

	/**
	 * @return the folderTemplateCreateErrorMessage
	 */
	public WebElement getFolderTemplateCreateErrorMessage(int timeOut) {
		return isDisplayed(driver, folderTemplateCreateErrorMessage, "Visibility", timeOut, "Folder Temlate Create Error Message");
	}
	
	@FindBy(xpath="//div[contains(@class,'saveTemplate_fancybox')]//a[@title='No']")
	private WebElement folderTemplateSaveTemplatePopupNoButton;

	/**
	 * @return the folderTemplateSaveTemplatePopupNoButton
	 */
	public WebElement getFolderTemplateSaveTemplatePopupNoButton(int timeOut) {
		return isDisplayed(driver, folderTemplateSaveTemplatePopupNoButton, "Visibility", timeOut, "Folder Template Save Popup no Button");
	}
	
	@FindBy(xpath="//div[contains(@class,'saveTemplate_fancybox')]//a[@title='Yes']")
	private WebElement folderTemplateSaveTemplatePopupYesButton;

	/**
	 * @return the folderTemplateSaveTemplatePopupYesButton
	 */
	public WebElement getFolderTemplateSaveTemplatePopupYesButton(int timeOut) {
		return isDisplayed(driver, folderTemplateSaveTemplatePopupYesButton, "Visibility", timeOut, "Folder Template Save Popup Yes Button");
	}
	
	@FindBy(xpath="//div[contains(@class,'saveTemplate_fancybox')]//a[@title='Close']")
	private WebElement folderTemplateSavePopupCrossIcon;

	/**
	 * @return the folderTemplateSavePopupCrossIcon
	 */
	public WebElement getFolderTemplateSavePopupCrossIcon(int timeOut) {
		return isDisplayed(driver, folderTemplateSavePopupCrossIcon, "Visibility", timeOut, "Folder Template Save Popup Cross Icon");
	}
	
	@FindBy(xpath="//span[@id='idTemplateName']")
	private WebElement folderTemplatename;

	/**
	 * @return the folderTemplatename
	 */
	public WebElement getFolderTemplatename(int timeOut) {
		return isDisplayed(driver, folderTemplatename, "Visibility", timeOut, "folder Template name");
	}
	
	@FindBy(xpath="//div[@id='AddFolderPopup']/div[@class='head_popup']")
	private WebElement addAFolderHeader;
	
	@FindBy(xpath="//div[@id='addfirstlevelpop1']/div[@class='head_popup']")
	private WebElement addAFolderHeaderOnManageFolder;

	/**
	 * @return the addAFolderHeader
	 */
	public WebElement getAddAFolderHeader(int timeOut, PageName pageName) {
		WebElement ele=null;
		if(pageName.toString().equalsIgnoreCase(pageName.ManageFolderPopUp.toString())) {
			
			ele= isDisplayed(driver, addAFolderHeaderOnManageFolder, "Visibility", timeOut, "Add A Folder Header");
			
		}else {
			ele= isDisplayed(driver, addAFolderHeader, "Visibility", timeOut, "Add A Folder Header");
			
		}
		return ele;
	} 
	
	@FindBy(xpath="(//table[@class='table padding_none']//span[@class='txt_bold float_r'])[1]")
	private WebElement addAfolderFolderTypeLabel;
	
	@FindBy(xpath="(//table[@class='table padding_none']//span[@class='txt_bold'])[4]")
	private WebElement addAfolderFolderTypeLabelOnManageFolder;
	
	

	/**
	 * @return the addAfolderFolderTypeLabel
	 */
	public WebElement getAddAfolderFolderTypeLabel(int timeOut,PageName pageName) {
		WebElement ele=null;
		if(pageName.toString().equalsIgnoreCase(PageName.ManageFolderPopUp.toString())) {
			ele=isDisplayed(driver, addAfolderFolderTypeLabelOnManageFolder, "Visibility", timeOut, "Add A folder folder Type Label");
		}else {
			ele=isDisplayed(driver, addAfolderFolderTypeLabel, "Visibility", timeOut, "Add A folder folder Type Label");
		}
		return ele;
	}
	
	@FindBy(xpath="//a[@id='demo-basicDF']//img")
	private WebElement addAFolderInfoIcon;
	
	@FindBy(xpath="//a[@id='AddFolder_tolltip']//img")
	private WebElement addAFolderInfoIconOnManageFolder;
	
	/**
	 * @return the addAFolderInfoIcon
	 */
	public WebElement getAddAFolderInfoIcon(int timeOut,PageName pageName) {
		WebElement ele=null;
		if(pageName.toString().equalsIgnoreCase(PageName.ManageFolderPopUp.toString())) {
			ele=isDisplayed(driver, addAFolderInfoIconOnManageFolder, "Visibility", timeOut, "Add a folder Info Icon");
		}else {
			ele=isDisplayed(driver, addAFolderInfoIcon, "Visibility", timeOut, "Add a folder Info Icon");
		}
		return ele;
	}
	
	
	
	
	@FindBy(xpath="//a[contains(@onclick,'ParentFolder')]/..//a[@title='Cancel']")
	private WebElement addAFolderPopupCancelButton;
	
	@FindBy(xpath="//a[contains(@onclick,'CreateGlobal_pop1')]/..//a[@title='Cancel']")
	private WebElement addAFolderPopupCancelButtonOnManageFolder;
	
	
	
	/**
	 * @return the addAFolderPopupCancelButton
	 */
	public WebElement getAddAFolderPopupCancelButton(int timeOut,PageName pageName) {
		WebElement ele=null;
		if(pageName.toString().equalsIgnoreCase(PageName.ManageFolderPopUp.toString())) {
			ele=isDisplayed(driver, addAFolderPopupCancelButtonOnManageFolder, "Visibility", timeOut, "Add A Folder Popup cancel Button");
		}else {
			ele=isDisplayed(driver, addAFolderPopupCancelButton, "Visibility", timeOut, "Add A Folder Popup cancel Button");
		}
		return ele;
	}
	 
	@FindBy(xpath="//div[@id='AddFolderPopup']/div[@class='head_popup']//a")
	private WebElement addAFolderPopupCrossIcon;

	@FindBy(xpath="//div[@id='addfirstlevelpop1']/div[@class='head_popup']//span")
	private WebElement addAFolderPopupCrossIconOnManageFolder;
	
	/**
	 * @return the addAFolderPopupCrossIcon
	 */
	public WebElement getAddAFolderPopupCrossIcon(int timeOut,PageName pageName) {
		WebElement ele=null;
		if(pageName.toString().equalsIgnoreCase(PageName.ManageFolderPopUp.toString())) {
			ele=isDisplayed(driver, addAFolderPopupCrossIconOnManageFolder, "Visibility", timeOut, "Add a Folder popup Cross Icon");
		}else {
			ele=isDisplayed(driver, addAFolderPopupCrossIcon, "Visibility", timeOut, "Add a Folder popup Cross Icon");
		}
		return ele;
	}
	
	@FindBy(xpath="//input[@id='txtParentFolderName']/../..//span")
	private WebElement addAFolderMandatoryLine;
	
	@FindBy(xpath="//input[@id='foldeglobaladdfund']/../..//span")
	private WebElement addAFolderMandatoryLineOnManageFolder;

	/**
	 * @return the addAFolderMandatoryLine
	 */
	public WebElement getAddAFolderMandatoryLine(int timeOut, PageName pageName) {
		WebElement ele=null;
		if(pageName.toString().equalsIgnoreCase(PageName.ManageFolderPopUp.toString())) {
			ele=isDisplayed(driver, addAFolderMandatoryLineOnManageFolder, "Visibility", timeOut, "Add a Folder Manadatory line");
		}else {
			ele=isDisplayed(driver, addAFolderMandatoryLine, "Visibility", timeOut, "Add a Folder Manadatory line");
		}
		return ele;
	}
	
	@FindBy(xpath="//span[@id='err1stLvlFolder']")
	private WebElement addAFolderErrorMessage;

	/**
	 * @return the addAFolderErrorMessage
	 */
	
	public WebElement getAddAFolderErrorMessage(int timeOut) {
		return isDisplayed(driver, addAFolderErrorMessage, "Visibility", timeOut, "Add a Folder Error Message");
	}

	
	@FindBy(xpath="//span[@id='errforInvalidCharFirst' and contains(@style,'display: block;')]")
	private WebElement addAFolderInvalidFolderSpecialCharacterErrorMessage;

	/**
	 * @return the addAFolderInvalidFolderErrorMessage
	 */
	public WebElement getAddAFolderInvalidFolderSpecialCharacterErrorMessage(int timeOut) {
		return isDisplayed(driver, addAFolderInvalidFolderSpecialCharacterErrorMessage, "Visibility", timeOut, "Invalif folder error message");
	}
	 
	@FindBy(xpath="//span[@id='errRenFolderNameForGlobalCreate' and contains(@style,'display: block;')]")
	private WebElement addaFolderInvalidFolderNameErroMessage;

	/**
	 * @return the addaFolderInvalidFolderNameErroMessage
	 */
	public WebElement getAddaFolderInvalidFolderNameErroMessage(int timeOut) {
		return isDisplayed(driver, addaFolderInvalidFolderNameErroMessage, "Visibility", timeOut, "Invalid Name Error Message");
	}
	
	@FindBy(xpath="//div[@id='errforInvalidCharFirstDesc']")
	private WebElement addAFolderInvalidDescriptionErrorMessage;

	/**
	 * @return the addAFolderInvalidDescriptionErrorMessage
	 */
	public WebElement getAddAFolderInvalidDescriptionErrorMessage(int timeOut) {
		return isDisplayed(driver, addAFolderInvalidDescriptionErrorMessage, "Visibility", timeOut, "Invalid Description Error Message");
	}

	@FindBy(xpath="//div[contains(@class,'RenameFolder')]//div[@class='head_popup']")
	private WebElement folderTemplateRenameFolderPopupLabel;

	/**
	 * @return the folderTemplateRenameFolderPopupLabel
	 */
	public WebElement getFolderTemplateRenameFolderPopupLabel(int timeOut) {
		return isDisplayed(driver, folderTemplateRenameFolderPopupLabel, "Visibility", timeOut, "Folder Template Rename folder Popup");
	}
	
	@FindBy(xpath="//div[contains(@class,'RenameFolder')]//a[@title='Close']")
	private WebElement folderTemplateRenameFolderPopupCloseIcon;

	/**
	 * @return the folderTemplateRenameFolderPopupCloseIcon
	 */
	public WebElement getFolderTemplateRenameFolderPopupCloseIcon(int timeOut) {
		return isDisplayed(driver, folderTemplateRenameFolderPopupCloseIcon, "Visibility", timeOut, "Folder Template Rename Folder Popup Close Icon");
	}
	
	@FindBy(xpath="//div[contains(@class,'RenameFolder')]//a[@title='Save']")
	private WebElement folderTemplateRenameFolderPopupSaveButton;

	/**
	 * @return the folderTemplateRenameFolderPopupSaveButton
	 */
	public WebElement getFolderTemplateRenameFolderPopupSaveButton(int timeOut) {
		return isDisplayed(driver, folderTemplateRenameFolderPopupSaveButton, "Visibility", timeOut, "Folder Template Rename Folder Save Button");
	}	 
	
	@FindBy(xpath="//div[contains(@class,'RenameFolder')]//a[@title='Cancel']")
	private WebElement	folderTemplateRenameFolderPopupCancelButton;

	/**
	 * @return the folderTemplateRenameFolderPopupCancelButton
	 */
	public WebElement getFolderTemplateRenameFolderPopupCancelButton(int timeOut) {
		return isDisplayed(driver, folderTemplateRenameFolderPopupCancelButton, "Visibility", timeOut, "Folder Template Rename Folder Cancel Button");
	}
	
	@FindBy(id="txtRenameFolder")
	private WebElement folderTemplateRenameFolderPopupNameTextBox;

	/**
	 * @return the folderTemplateRenameFolderPopupNameTextBox
	 */
	public WebElement getFolderTemplateRenameFolderPopupNameTextBox(int timeOut) {
		return isDisplayed(driver, folderTemplateRenameFolderPopupNameTextBox, "Visibility", timeOut, "Folder Template Rename Folder Popup Name Text Box");
	}
	
	@FindBy(xpath="//div[@id='errRenFolderName']")
	private WebElement folderTemplateRenameFolderErrorMessage;

	/**
	 * @return the folderTemplateRenameFolderErrorMessage
	 */
	public WebElement getFolderTemplateRenameFolderErrorMessage(int timeOut) {
		return isDisplayed(driver, folderTemplateRenameFolderErrorMessage, "Visibility", timeOut, "Folder Template Rename Folder Error Messgae");
	} 
	
	@FindBy(id="txtRenameFolderDescription")
	private WebElement folderTemplateRenameFolderDescriptionTextBox;

	/**
	 * @return the folderTemplateRenameFolderDescriptionTextBox
	 */
	public WebElement getFolderTemplateRenameFolderDescriptionTextBox(int timeOut) {
		return isDisplayed(driver, folderTemplateRenameFolderDescriptionTextBox, "Visibility", timeOut, "Folder Template Rename Folder Popup Description Text Box");
	}
	
	@FindBy(xpath="//div[@id='subFolderParent']")
	private WebElement folderTemplateChildLevelParentFolderName;

	/**
	 * @return the folderTemplateChildLevelParentFolderName
	 */
	public WebElement getFolderTemplateChildLevelParentFolderName(int timeOut) {
		return isDisplayed(driver, folderTemplateChildLevelParentFolderName, "Visibility", timeOut, "Folder Template Child Lebvel Parent folder name");
	}
	
	@FindBy(id="txtChildFolderName")
	private WebElement folderTemplateChildLevelFolderNameTextBox;

	/**
	 * @return the folderTemplateChildLevelFolderNameTextBox
	 */
	public WebElement getFolderTemplateChildLevelFolderNameTextBox(int timeOut) {
		return isDisplayed(driver, folderTemplateChildLevelFolderNameTextBox, "Visibility", timeOut, "Folder Template Child Level Folder Name Text Box");
	}
	
	@FindBy(id="txtChildFolderDescription")
	private WebElement folderTemplateChildLevelFolderDescriptionTextBox;

	/**
	 * @return the folderTemplateChildLevelFolderDescriptionTextBox
	 */
	public WebElement getFolderTemplateChildLevelFolderDescriptionTextBox(int timeOut) {
		return isDisplayed(driver, folderTemplateChildLevelFolderDescriptionTextBox, "Visibility", timeOut, "Folder Template Child Level Folder Description Text Box");
	}
	
	@FindBy(xpath="//div[contains(@class,'AddFirstLevelFolder_a')]//a[@title='Save']")
	private WebElement folderTemplateChildLevelSaveButton;

	/**
	 * @return the folderTemplateChildLevelSaveButton
	 */
	public WebElement getFolderTemplateChildLevelSaveButton(int timeOut) {
		return isDisplayed(driver, folderTemplateChildLevelSaveButton, "Visibility", timeOut, "Folder Template Child level Save button");
	}
	
	@FindBy(xpath="//div[contains(@class,'AddFirstLevelFolder_a')]//a[@title='Cancel']")
	private WebElement folderTemplateChildLevelCancelButton;

	/**
	 * @return the folderTemplateChildLevelCancelButton
	 */
	public WebElement getFolderTemplateChildLevelCancelButton(int timeOut) {
		return isDisplayed(driver, folderTemplateChildLevelCancelButton, "Visibility", timeOut, "Folder Template Child Level Cancel Buttton");
	}
	
	@FindBy(xpath="//div[contains(@class,'AddFirstLevelFolder_a')]//a[@title='Close']")
	private WebElement folderTemplateChildLevelCloseIcon;

	/**
	 * @return the folderTemplateChildLevelCloseIcon
	 */
	public WebElement getFolderTemplateChildLevelCloseIcon(int timeOut) {
		return isDisplayed(driver, folderTemplateChildLevelCloseIcon, "Visibility", timeOut, "Folder Template Child Level Close Icon");
	}
	
	@FindBy(id="errSubFolderName")
	private WebElement folderTemplateChildLevelErroMessage;

	/**
	 * @return the folderTemplateChildLevelErroMessage
	 */
	public WebElement getFolderTemplateChildLevelErroMessage(int timeOut) {
		return isDisplayed(driver, folderTemplateChildLevelErroMessage, "Visibility", timeOut, "Folder Template Child Level Error Message");
	}
	
	@FindBy(xpath="//div[@id='idConfirmDeletion']//div[@class='head_popup']")
	private WebElement folderTemplateDeletefoderPopupLabel;

	/**
	 * @return the folderTemplateDeletefoderPopupLabel
	 */
	public WebElement getFolderTemplateDeletefoderPopupLabel(int timeOut) {
		return isDisplayed(driver, folderTemplateDeletefoderPopupLabel, "Visibility", timeOut, "Folder Template Delete Folder Popup Label");
	}
	
	@FindBy(xpath="//div[@id='idConfirmDeletion']//a[@title='Yes']")
	private WebElement folderTemplateDeleteFolderPopupYesButton;

	/**
	 * @return the folderTemplateDeleteFolderPopupYesButton
	 */
	public WebElement getFolderTemplateDeleteFolderPopupYesButton(int timeOut) {
		return isDisplayed(driver, folderTemplateDeleteFolderPopupYesButton, "Visibility", timeOut, "Folder Template Delete Folder popup Yes button");
	}
	
	@FindBy(xpath="//div[@id='idConfirmDeletion']//a[@title='No']")
	private WebElement folderTemplateDeleteFolderPopupNoButton;

	/**
	 * @return the folderTemplateDeleteFolderPopupNoButton
	 */
	public WebElement getFolderTemplateDeleteFolderPopupNoButton(int timeOut) {
		return isDisplayed(driver, folderTemplateDeleteFolderPopupNoButton, "Visibility", timeOut, "Folder Template Delete Folder popup No button");
	} 
	
	@FindBy(xpath="//div[@id='idConfirmDeletion']//a[@title='Close']")
	private WebElement folderTemplateDeleteFolderPopupCloseIcon;

	/**
	 * @return the folderTemplateDeleteFolderPopupCloseIcon
	 */
	public WebElement getFolderTemplateDeleteFolderPopupCloseIcon(int timeOut) {
		return isDisplayed(driver, folderTemplateDeleteFolderPopupCloseIcon, "Visibility", timeOut, "Folder Template Delete Folder Popup Close Icon");
	} 
	
	@FindBy(xpath="//div[@id='errMsgDupFolder']")
	private WebElement folderTemplateFolderAlreadyExistErrorMssage;

	/**
	 * @return the folderTemplateFolderAlreadyExistErrorMssage
	 */
	public WebElement getFolderTemplateFolderAlreadyExistErrorMssage(int timeOut) {
		return isDisplayed(driver, folderTemplateFolderAlreadyExistErrorMssage, "Visibility", timeOut, "Folder Already Exist Error Message");
	}
	
	@FindBy(xpath="//div[contains(@id,'idCharError')]//a[text()='Close']")
	private WebElement folderTemplateFolderAlreadyExistPopupCloseButton;

	/**
	 * @return the folderTemplateFolderAlreadyExistPopupCloseButton
	 */
	public WebElement getFolderTemplateFolderAlreadyExistPopupCloseButton(int timeOut) {
		return isDisplayed(driver, folderTemplateFolderAlreadyExistPopupCloseButton, "Visibility", timeOut, "Folder Already Exist Error Message Close Button");
	}
	
	@FindBy(xpath="//div[@id='errMsgSecondGlobalFolder']")
	private WebElement folderTemplateSecondGlobalFolderErrorMessage;

	/**
	 * @return the folderTemplateSecondGlobalFolderErrorMessage
	 */
	public WebElement getFolderTemplateSecondGlobalFolderErrorMessage(int timeOut) {
		return isDisplayed(driver, folderTemplateSecondGlobalFolderErrorMessage, "Visibility", timeOut, "Global folder Error Message");
	}
	@FindBy(id="errMsgSecondInternalFolder")
	private WebElement folderTemplateSecondInternalFolderErrorMessage;

	/**
	 * @return the folderTemplateSecondInternalFolderErrorMessage
	 */
	public WebElement getFolderTemplateSecondInternalFolderErrorMessage(int timeOut) {
		return isDisplayed(driver, folderTemplateSecondInternalFolderErrorMessage, "Visibility", timeOut, "Internal folder Error Message");
	}
	
	@FindBy(xpath="//div[@id='editpermit']//p")
	private WebElement folderTemplateInsufficientPermissionErrorMessage;

	/**
	 * @return the folderTemplateInsufficientPermissionErrorMessage
	 */
	public WebElement getFolderTemplateInsufficientPermissionErrorMessage(int timeOut) {
		return isDisplayed(driver, folderTemplateInsufficientPermissionErrorMessage, "Visibility", timeOut, "Folder Template Insufficient Permission Error Message");
	}
	
	@FindBy(xpath="//div[@id='editpermit']//a[text()='Close']")
	private WebElement folderTemplateInsufficientPermissionPopupCloseutton;

	/**
	 * @return the folderTemplateInsufficientPermissionPopupCloseutton
	 */
	public WebElement getFolderTemplateInsufficientPermissionPopupCloseutton(int timeOut) {
		return isDisplayed(driver, folderTemplateInsufficientPermissionPopupCloseutton, "Visibility", timeOut, "Insufficient permission popup close Button");
	} 
	
	@FindBy(xpath="//div[@id='deletetpermit']//p")
	private WebElement folderTemplateDeleteTemplateInsufficientPermissionMessage;

	/**
	 * @return the folderTemplateDeleteTemplateInsufficientPermissionMessage
	 */
	public WebElement getFolderTemplateDeleteTemplateInsufficientPermissionMessage(int timeOut) {
		return isDisplayed(driver, folderTemplateDeleteTemplateInsufficientPermissionMessage, "Visibility", timeOut, "Delete Folder Template Insufficient permission message");
	}
	
	@FindBy(xpath="//div[@id='deletetpermit']//a[text()='Close']")
	private WebElement folderTemplateDeleteTemplatePopupCloseButton;

	/**
	 * @return the folderTemplateDeleteTemplatePopupCloseButton
	 */
	public WebElement getFolderTemplateDeleteTemplatePopupCloseButton(int timeOut) {
		return isDisplayed(driver, folderTemplateDeleteTemplatePopupCloseButton, "Visibility", timeOut, "Folder Template Delete Template Close Button");
	}
	
	@FindBy(xpath="//div[@id='RecordsCountId']//span[text()='Records: ']/span")
	private WebElement folderTemplateRecordCount;

	/**
	 * @return the folderTemplateRecordCount
	 */
	public WebElement getFolderTemplateRecordCount(int timeOut) {
		return isDisplayed(driver, folderTemplateRecordCount, "Visibility", timeOut, "Folder Template Record Count");
	}
	
	@FindBy(xpath="//span[contains(@id,'templateGrid-header-0')]//span[@class='aw-item-text ']")
	private WebElement folderTemplateTemplateNameLabel;

	/**
	 * @return the folderTemplateTemplateNameLabel
	 */
	public WebElement getFolderTemplateTemplateNameLabel(int timeOut) {
		return isDisplayed(driver, folderTemplateTemplateNameLabel, "Visibility", timeOut, "Folder Template Template Name Label");
	}
	
	@FindBy(xpath="//span[contains(@id,'templateGrid-header-2')]//span[@class='aw-item-text ']")
	private WebElement folderTemplateCreatedByLabel;

	/**
	 * @return the folderTemplateCreatedByLabel
	 */
	public WebElement getFolderTemplateCreatedByLabel(int timeOut) {
		return isDisplayed(driver, folderTemplateCreatedByLabel, "Visibility", timeOut, "Folder Template Created By Label");
	}

	@FindBy(xpath="//span[contains(@id,'templateGrid-header-3')]//span[@class='aw-item-text ']")
	private WebElement folderTemplateCreatedOnLabel;

	/**
	 * @return the folderTemplateCreatedOnLabel
	 */
	public WebElement getFolderTemplateCreatedOnLabel(int timeOut) {
		return isDisplayed(driver, folderTemplateCreatedOnLabel, "Visibility", timeOut, "Folder Templated Created On Label");
	}
	
	@FindBys(@FindBy(xpath="//span[contains(@id,'templateGrid-cell-0-')]//a"))
	private List<WebElement> folderTemplateTemplateList;

	public List<WebElement> getfolderTemplateTemplateList(int timeOut) {
		return FindElements(driver, "//span[contains(@id,'templateGrid-cell-0-')]//a"," Folder Template List");
		
	}
	
	
	@FindBys(@FindBy(xpath="//span[contains(@id,'templateGrid-cell-2-')]"))
	private List<WebElement> folderTemplateCreatedByList;

	public List<WebElement> getfolderTemplateCreatedByList(int timeOut) {
		return FindElements(driver, "//span[contains(@id,'templateGrid-cell-2-')]"," Folder Template Created by List");	
			}
	
	@FindBys(@FindBy(xpath="//span[contains(@id,'templateGrid-cell-3-')]"))
	private List<WebElement> folderTemplateCreatedOnList;

	public List<WebElement> getfolderTemplateCreatedOnList(int timeOut) {
		return FindElements(driver, "//span[contains(@id,'templateGrid-cell-3-')]"," Folder Template Created on List");	
	}
	
	@FindBy(xpath="//div[@id='confirmDeleteTemp']//a[@title='Yes']")
	private WebElement folderTemplateDeleteFolderTemplatePopupYesButton;

	/**
	 * @return the folderTemplateDeleteFolderTemplatePopupYesButton
	 */
	public WebElement getFolderTemplateDeleteFolderTemplatePopupYesButton(int timeOut) {
		return isDisplayed(driver, folderTemplateDeleteFolderTemplatePopupYesButton, "Visibility", timeOut, "Delete Folder Template Popup Yes button");
	}
	

	@FindBy(xpath="(//div[@id='home']//span)[1]")
	private WebElement landingPageHeadingOnDRMTabs;

	/**
	 * @return the landingPageHeadingOnDRMTabs
	 */
	public WebElement getLandingPageHeadingOnDRMTabs(int timeOut) {
		return isDisplayed(driver, landingPageHeadingOnDRMTabs, "Visibility", timeOut, "Landing Page Heading on DRM Tab");
	}
	
	@FindBy(id="pageid:fm:sl1")
	private WebElement manageApprovalByDealRoomDropDownViewMode;

	/**
	 * @return the manageApprovalByDealRoomDropDownViewMode
	 */
	public WebElement getManageApprovalByDealRoomDropDownViewMode(int timeOut) {
		return isDisplayed(driver, manageApprovalByDealRoomDropDownViewMode, "Visibility", timeOut, "Deal Room Drop Down");
	}
	
	public List<WebElement> getProfilesTabList(){
		 return FindElements(driver, "//ul[@id='profile_sidebar']/li/a", "proifle Tab lists");
	}
	
	@FindBy(xpath="(//span[contains(text(),'Profiles')])[2]")
	private WebElement profileTabLabelText;

	/**
	 * @return the profileTabLabelText
	 */
	public WebElement getProfileTabLabelText(int timeOut) {
		return isDisplayed(driver, profileTabLabelText, "Visibility", timeOut, "Profile Tab label text");
	}
	
	@FindBy(xpath="(//span[contains(text(),'My Profile')])[1]")
	private WebElement myProfileHeaderText;

	/**
	 * @return the myProfileHeaderText
	 */
	public WebElement getMyProfileHeaderText(int timeOut) {
		return isDisplayed(driver, myProfileHeaderText, "Visibility", timeOut, "My Profile Header text");
	}
	
	@FindBy(xpath="(//div[@class='BasicInformation_box'])[1]/div")
	private WebElement myprofilebasicInfoText;

	/**
	 * @return the myprofilebasicInfoText
	 */
	public WebElement getMyprofilebasicInfoText(int timeOut) {
		return isDisplayed(driver, myprofilebasicInfoText, "Visibility", timeOut, "my profile basic info text");
	}
	
	public List<WebElement> getMyProfileBaicInfoList(){
		return FindElements(driver, "(//div[@class='BasicInformation_box'])[1]//td", "basic info text list");
	}
	
	@FindBy(xpath="(//div[@class='InvestmentPreferences_box topspace'])[1]/div")
	private WebElement myProfileLoginInfoTex;

	/**
	 * @return the myProfileLoginInfoTex
	 */
	public WebElement getMyProfileLoginInfoTex(int timeOut) {
		return isDisplayed(driver, myProfileLoginInfoTex, "Visibility", timeOut, "my profile login info text");
	}
	
	public List<WebElement> getMyProfileLoginInfoList(){
		return FindElements(driver, "(//div[@class='InvestmentPreferences_box topspace'])[1]//tr/td/span", "my profile login information text list");
	}
	
	@FindBy(xpath="(//div[@class='PrivacyPreferences_box topspace'])[1]/div[1]")
	private WebElement myProfileNotificationPreferenceText;

	/**
	 * @return the myProfileNotificationPreferenceText
	 */
	public WebElement getMyProfileNotificationPreferenceText(int timeOut) {
		return isDisplayed(driver, myProfileNotificationPreferenceText, "Visibility", timeOut, "my profile notification preference text");
	}
	
	@FindBy(xpath="(//div[@class='PrivacyPreferences_box topspace'])[1]/div[3]//strong")
	private WebElement myProfileNotificationPreferenceInnertext;

	/**
	 * @return the myProfileNotificationPreferenceInnertext
	 */
	public WebElement getMyProfileNotificationPreferenceInnertext(int timeOut) {
		return isDisplayed(driver, myProfileNotificationPreferenceInnertext, "Visibility", timeOut, "my Profile Notification Preference Inner text");
	}
	
	public List<WebElement> getMyProfileNotificationPreferenceRadioBtntextList(){
		return FindElements(driver, "(//div[@class='PrivacyPreferences_box topspace'])[1]/div[3]//li/div", "My Profile Notification Preference Radio Btn text List");
	}
	
	public List<WebElement> getMyProfileNotificationPreferenceRadioBtntextListInEdit(){
		return FindElements(driver, "//div[@id='notifaction']/span", "My Profile Notification Preference Radio Btn text List in edit");		
	}
	
	
	@FindBy(xpath="//div[@id='EmailDiv']/../input")
	private WebElement myProfileNotificationPreferncesDailyDigestEmailRadioBtn;

	/**
	 * @return the myProfileNotificationPreferncesDailyDigestEmailRadioBtn
	 */
	public WebElement getMyProfileNotificationPreferncesDailyDigestEmailRadioBtn(int timeOut) {
		return isDisplayed(driver, myProfileNotificationPreferncesDailyDigestEmailRadioBtn, "Visibility", timeOut, "My Profile Notification Prefernces Daily Digest Email Radio Btn");
	}
	
	@FindBy(xpath="//div[contains(text(),'E-mail')]/../../td[2]/div")
	private WebElement myProfileEmailText;

	/**
	 * @return the myProfileEmailText
	 */
	public WebElement getMyProfileEmailText(int timeOut) {
		return isDisplayed(driver, myProfileEmailText, "Visibility", timeOut, "My profile Email text box");
	}
	
	@FindBy(xpath="//div[contains(text(),'E-mail')]/..//a")
	private WebElement myProfileEmailToolTip;

	/**
	 * @return the myProfileEmailToolTip
	 */
	public WebElement getMyProfileEmailToolTip(int timeOut) {
		return isDisplayed(driver, myProfileEmailToolTip, "Visibility", timeOut, "My profile email tootl tip");
	}
	
	@FindBy(id="page_myprofiletragetid:frm_myprofiletragetid:txtuserlinkedin")
	private WebElement myProfileLinkedinTextBox;

	/**
	 * @return the myProfileLinkedinTextBox
	 */
	public WebElement getMyProfileLinkedinTextBox(int timeOut) {
		return isDisplayed(driver, myProfileLinkedinTextBox, "Visibility", timeOut, "My profile Linkedin text box");
	}
	
	@FindBys(@FindBy(xpath="//div[@class='error_txt']"))
	private WebElement myProfileFirstAndLastNameErrorMsg;

	/**
	 * @return the myProfileFirstAndLastNameErrorMsg
	 */
	public WebElement getMyProfileFirstAndLastNameErrorMsg(int timeOut) {
		return isDisplayed(driver, myProfileFirstAndLastNameErrorMsg, "Visibility", timeOut, "My Profile First Name and last Name Error Message");
	}
	
	@FindBy(xpath="(//div[@class=\"EditFieldName\"]/../../td[2]/div)[2]")
	private WebElement myProfileUserNametextInEditMode;

	/**
	 * @return the myProfileUserNametextInEditMode
	 */
	public WebElement getMyProfileUserNametextInEditMode(int timeOut) {
		return isDisplayed(driver, myProfileUserNametextInEditMode, "Visibility", timeOut, "My Profile User Name text In Edit Mode");
	}
	
	@FindBy(id="idonlyEmail")
	private WebElement myProfileNotificationPreferncesDailyDigestEmailRadioBtnInEdit;

	/**
	 * @return the myProfileNotificationPreferncesDailyDigestEmailRadioBtnInEdit
	 */
	public WebElement getMyProfileNotificationPreferncesDailyDigestEmailRadioBtnInEdit(int timeOut) {
		return isDisplayed(driver, myProfileNotificationPreferncesDailyDigestEmailRadioBtnInEdit, "Visibility", timeOut,"My Profile Notification Prefernces Daily Digest Email Radio Btn In Edit");
	}
	
	@FindBy(name="page_myprofiletragetid:frm_myprofiletragetid:j_id36")
	private WebElement myProfileCancelBtn;

	/**
	 * @return the myProfileCancelBtn
	 */
	public WebElement getMyProfileCancelBtn(int timeOut) {
		return isDisplayed(driver, myProfileCancelBtn, "Visibility", timeOut, "My profile cancel button");
	}
	
	@FindBy(id="MailView")
	private WebElement myprofileEmailLink;

	/**
	 * @return the myprofileEmailLink
	 */
	public WebElement getMyprofileEmailLink(int timeOut) {
		return isDisplayed(driver, myprofileEmailLink, "Visibility", timeOut, "My Profile Email Link");
	}
	
	@FindBy(id="linkedinView")
	private WebElement myProfileLinkedInLink;

	/**
	 * @return the myProfileLinkedInLink
	 */
	public WebElement getMyProfileLinkedInLink(int timeOut) {
		return isDisplayed(driver, myProfileLinkedInLink, "Visibility", timeOut, "My Profile LinkedIn Link");
	}
	
	public List<WebElement> getUserListInAdminUserTab(){
		return FindElements(driver, "//span[@id='grid_AdminUsers-view']//span[contains(@id,'grid_AdminUsers-cell-1')]/span", "User List in Admin User Tab");
	}
	
	public List<WebElement> getUserListInInternalUserTab(){
		return FindElements(driver, "//span[@id='grid_InternalUsers-view']//span[contains(@id,'grid_InternalUsers-cell-1')]/span", "User List in Internal User Tab");
	}
	
	public List<WebElement> getUserListInManageApprovals(){
		return FindElements(driver, "//span[@id='grid_ManageApprovals-view']//span[contains(@id,'grid_ManageApprovals-cell-0')]/span","User List in Manage Approvals Tab");
	}
	
	@FindBy(xpath="(//div[@class='heading_box'])[2]/span")
	private WebElement firmProfileHeaderText;

	/**
	 * @return the firmProfileHeaderText
	 */
	public WebElement getFirmProfileHeaderText(int timeOut) {
		return isDisplayed(driver, firmProfileHeaderText, "Visibility", timeOut, "firm profile header text");
	}
	
	public List<WebElement> getFirmProfileLeftSideLableTextList(){
		return FindElements(driver, "(//div[@class='firm_profile_container_left'])[1]//tr/td[1]", "Firm Profile label text list");
	}
	
	public List<WebElement> getFirmProfileRighttSideLableTextList(){
		return FindElements(driver, "(//div[@class='firm_profile_container_left'])[1]//tr/td[2]", "Firm Profile label text list");
	}
	
	public List<WebElement> getFirmProfileLinksList(){
		return FindElements(driver, "(//div[@class='firm_profile_container_left'])[1]//tr/td[2]/a", "firm profile links");
	}
	
	@FindBy(xpath="((//div[@class='firm_profile_container_left'])[1]//tr/td[2]//div)[3]")
	private WebElement firmProfileBackGroundColour;
	
	/**
	 * @return the firmProfileBackGroundColour
	 */
	public WebElement getFirmProfileBackGroundColour(int timeOut) {
		return isDisplayed(driver, firmProfileBackGroundColour, "Visibility", timeOut, "firm profile back ground colour in view mode");
	}

	@FindBy(xpath="(//div[@class='firm_profile_container_left'])[1]//tr/td[1]/a")
	private WebElement firmProfileToolTip;

	/**
	 * @return the firmProfileToolTip
	 */
	public WebElement getFirmProfileToolTip(int timeOut) {
		return isDisplayed(driver, firmProfileToolTip, "Visibility", timeOut, "firm profile tool tip");
	}
	
	@FindBy(xpath="(//div[@class='heading_box'])[3]/span")
	private WebElement firmProfileHeadertextInEditMode;

	/**
	 * @return the firmProfileHeadertextInEditMode
	 */
	public WebElement getFirmProfileHeadertextInEditMode(int timeOut) {
		return isDisplayed(driver, firmProfileHeadertextInEditMode, "Visibility", timeOut, "firm profile header text in edit");
	}
	
	public List<WebElement> getFirmProfileLeftSidelabelInEdit(){
		return FindElements(driver, "(//div[@class='firm_profile_container_left'])[2]//tr/td[1]", "firm profile left side label in edit");
	}
	
	
	@FindBy(xpath="(//div[@class='firm_profile_container_left'])[2]//tr/td[1]//a")
	private WebElement firmProfileToolTipInEdit;

	/**
	 * @return the firmProfileToolTipInEdit
	 */
	public WebElement getFirmProfileToolTipInEdit(int timeOut) {
		return isDisplayed(driver, firmProfileToolTipInEdit, "Visibility", timeOut, "firm profile tool tip in edit");
	}
	
	@FindBy(id="IdRequriedFirmName")
	private WebElement firmProfileErroMsg;

	/**
	 * @return the firmProfileErroMsg
	 */
	public WebElement getFirmProfileErroMsg(int timeOut) {
		return isDisplayed(driver, firmProfileErroMsg, "Visibility", timeOut, "firm profile error message");
	}
	
	@FindBy(id="IdCheckEmail")
	private WebElement firmProfileEmailErrorMsg;

	/**
	 * @return the firmProfileEmailErrorMsg
	 */
	public WebElement getFirmProfileEmailErrorMsg(int timeOut) {
		return isDisplayed(driver, firmProfileEmailErrorMsg, "Visibility", timeOut, "firm profile email error message");
	}
	
	@FindBy(name="pageid:j_id0:j_id47")
	private WebElement firmProfileCancelBtn;

	/**
	 * @return the firmProfileCancelBtn
	 */
	public WebElement getFirmProfileCancelBtn(int timeOut) {
		return isDisplayed(driver, firmProfileCancelBtn, "Visibility", timeOut, "firm profile cancel button");
	}
	
	@FindBy(xpath="((//div[@class='firm_profile_container_left'])[1]//tr/td[2])[3]")
	private WebElement firmProfilePublicLoginLink;

	/**
	 * @return the firmProfilePublicLoginLink
	 */
	public WebElement getFirmProfilePublicLoginLink(int timeOut) {
		return isDisplayed(driver, firmProfilePublicLoginLink, "Visibility", timeOut, "firm profile public login link");
	}
	
	@FindBy(id="customWidget3")
	private WebElement firmProfileColourPickerInViewMode;

	/**
	 * @return the firmProfileColourPickerInViewMode
	 */
	public WebElement getFirmProfileColourPickerInViewMode(int timeOut) {
		return isDisplayed(driver, firmProfileColourPickerInViewMode, "Visibility", timeOut, "firm profile colour picker in view mode");
	}

	@FindBy(xpath="//span[@id='spanlince']")
	private WebElement contactAccessTabHeading;

	/**
	 * @return the contactAccessTabHeading
	 */
	public WebElement getContactAccessTabHeading(int timeOut) {
		return isDisplayed(driver, contactAccessTabHeading, "Visibility", timeOut, "Contact Access Tab Heading");
	}
	
	@FindBy(id="pageid:frmCA:idchkExtAdmin")
	private WebElement contactAccessExternalAdminCheckbox;

	/**
	 * @return the contactAccessExternalAdminCheckbox
	 */
	public WebElement getContactAccessExternalAdminCheckbox(int timeOut) {
		return isDisplayed(driver, contactAccessExternalAdminCheckbox, "Visibility", timeOut, "Include External Admin Checkbox");
	}
	
	// Version 2 to be deleted
/*	@FindBys(@FindBy(xpath="//span[contains(@id,'grid_ContactAccess-cell-3')]"))
	private List<WebElement> firmNameInContactAccessTabGrid;

	*//**
	 * @return the firmNameInContactAccessTabGrid
	 *//*
	public List<WebElement> getFirmNameInContactAccessTabGrid(int timeOut) {
		if(checkElementsVisibility(driver, firmNameInContactAccessTabGrid, "Firm Name in Contact Access Tab Grid", timeOut)){
			return firmNameInContactAccessTabGrid;
		} else {
			return null;
		}
	}*/
	
	@FindBys(@FindBy(xpath="//span[contains(@id,'grid_ContactAccess-headers')]//span[@class='aw-item-text ']"))
	private List<WebElement> HeadersNameInContactAccessTabGrid;

	
	public List<WebElement> getHeadersNameInContactAccessTabGrid(int timeOut) {
		if(checkElementsVisibility(driver, HeadersNameInContactAccessTabGrid, "Headers Name in Contact Access Tab Grid", timeOut)){
			return HeadersNameInContactAccessTabGrid;
		} else {
			return null;
		}
	}
	
	@FindBy(xpath="//span[text()='Contact Name']/span")
	private WebElement contactAccessSortIconOnContactNameColoumn;

	/**
	 * @return the contactAccessSortIconOnContactNameColoumn
	 */
	public WebElement getContactAccessSortIconOnContactNameColoumn(int timeOut) {
		return isDisplayed(driver, contactAccessSortIconOnContactNameColoumn, "Visibility", timeOut, "Sort Icon On Contact Name Coloumn");
	}
	
	@FindBy(xpath="//span[text()='Contact Name']")
	private WebElement contactAccessContactNameLabel;

	/**
	 * @return the contactAccessContactNameLabel
	 */
	public WebElement getContactAccessContactNameLabel(int timeOut) {
		return isDisplayed(driver, contactAccessContactNameLabel, "Visibility", timeOut, "Contact Name Label");
	}
	// Version 2 Changes
	public List<WebElement> getcontactAccessRemoveLink() {
	//	return FindElements(driver, "//span[contains(@id,'grid_ContactAccess-rows')]/span//span[2]//a", "Contact Access Remove Link");
		return FindElements(driver, "//span[contains(@id,'grid_ContactAccess-rows')]//a[@title='Remove']", "Contact Access Remove Link");
	}
	
	// Version 2 to be deleted
	/*public List<WebElement> getcontactAccessFirmName() {
		return FindElements(driver, "//span[contains(@id,'grid_ContactAccess-rows')]/span//span[5]//label", "Firm Name in in Contact Access Tab Grid");
		
	}*/
	
	@FindBy(xpath="(//div[contains(@class,'RemoveContactsAccess3')]//a[@title='Close'])[1]")
	private WebElement contactAccessRemovePoppupCrossIcon;

	/**
	 * @return the contactAccessRemovePoppupCrossIcon
	 */
	public WebElement getContactAccessRemovePoppupCrossIcon(int timeOut) {
		return isDisplayed(driver, contactAccessRemovePoppupCrossIcon, "Visibility", timeOut, "Cross icon on remove contact access popup");
	}
	
	@FindBy(xpath="//div[contains(@class,'RemoveContactsAccess_fancybox')]//div[@class='formbox']//span")
	private WebElement removeContactAccessExternalAdminErrorMessage;

	/**
	 * @return the removeContactAccessExternalAdminErrorMessage
	 */
	public WebElement getRemoveContactAccessExternalAdminErrorMessage(int timeOut) {
		return isDisplayed(driver, removeContactAccessExternalAdminErrorMessage, "Visibility", timeOut, "External Admin error Message");
	}
	
	@FindBy(xpath="//div[contains(@class,'RemoveContactsAccess_fancybox')]//a[text()='Close']")
	private WebElement  contactAccessExternalAdminErrorMessageCloseButton;

	/**
	 * @return the contactAccessExternalAdminErrorMessageCloseButton
	 */
	public WebElement getContactAccessExternalAdminErrorMessageCloseButton(int timeOut) {
		return isDisplayed(driver, contactAccessExternalAdminErrorMessageCloseButton, "Visibility", timeOut, "Close Icon On Error MessagePopup");
	}
	
	@FindBy(xpath="(//div[contains(@class,'RemoveContactsAccess_fancybox')]//a[@title='Close'])[1]")
	private WebElement contactAccessExternalAdminErrorMessageCrossIcon;

	/**
	 * @return the contactAccessExternalAdminErrorMessageCrossIcon
	 */
	public WebElement getContactAccessExternalAdminErrorMessageCrossIcon(int timeOut) {
		return isDisplayed(driver, contactAccessExternalAdminErrorMessageCrossIcon, "Visibility", timeOut, "Cross Icon On External Admin Error Message Popup");
	}
	
	@FindBy(xpath="//input[@id='pageid:frmCA:idchkExtAdmin']")
	private WebElement contactAccessIncludExternalAdminCheckbox;

	/**
	 * @return the contactAccessIncludExternalAdminCheckbox
	 */
	public WebElement getContactAccessIncludExternalAdminCheckbox(int timeOut) {
		return isDisplayed(driver, contactAccessIncludExternalAdminCheckbox, "Visibility", timeOut, "Contact Access Include External Admin Checkbox");
	}
	
	@FindBy(xpath="//div[contains(@class,'RemoveContactsAccess3_fancybox')]//p")
	private WebElement removeContactAccessText;

	/**
	 * @return the removeContactAccessText
	 */
	public WebElement getRemoveContactAccessText(int timeOut) {
		return isDisplayed(driver, removeContactAccessText, "Visibility", timeOut, "Remove contact Access Text");
	}
	
	@FindBy(xpath="//div[contains(@class,'ContactsExsit_fancybox')]//span")
	private WebElement contactAccessContactNotFindErrorMessage;

	/**
	 * @return the contactAccessContactNotFindErrorMessage
	 */
	public WebElement getContactAccessContactNotFindErrorMessage(int timeOut) {
		return isDisplayed(driver, contactAccessContactNotFindErrorMessage, "Visibility", timeOut, "Contact access Contact Not Found Erro Message");
	}
	
	@FindBy(xpath="//div[contains(@class,'ontactsExsit_fancybox')]//div//a[text()='Close']")
	private WebElement contactAccessContactNotFoundPopUpCloseButton;

	/**
	 * @return the contactAccessContactNotFoundPopUpCloseButton
	 */
	public WebElement getContactAccessContactNotFoundPopUpCloseButton(int timeOut) {
		return isDisplayed(driver, contactAccessContactNotFoundPopUpCloseButton, "Visibility", timeOut, "Contact Access Contact Not Found Popup Close Button");
	}
	
	@FindBy(xpath="//span[text()='Firm']")
	private WebElement contactAccessFirmLabel;

	/**
	 * @return the contactAccessFirmLabel
	 */
	public WebElement getContactAccessFirmLabel(int timeOut) {
		return isDisplayed(driver, contactAccessFirmLabel, "Visibility", timeOut, "Contact access Firm Label");
	}
	
	@FindBy(xpath="//span[text()='External Admin']")
	private WebElement contactAccessExternalAdminLabel;

	/**
	 * @return the contactAccessExternalAdminLabel
	 */
	public WebElement getContactAccessExternalAdminLabel(int timeOut) {
		return isDisplayed(driver, contactAccessExternalAdminLabel, "Visibility", timeOut, "Contact Access Firm Label");
	}
	
	@FindBys(@FindBy(xpath="//span[contains(@id,'grid_ContactAccess-rows')]/span//span[4]//input"))
	private List<WebElement> contactAccessExternalAdminCheckboxes;

	
	public List<WebElement> getcontactAccessExternalAdminCheckboxes(int timeOut) {
		return FindElements(driver, "//span[contains(@id,'grid_ContactAccess-rows')]/span//span[4]//input", "External Admin Checkboxes in Contact Access Tab Grid");
		
	}
	
	@FindBy(xpath="//span[text()='Deal Room Name']")
	private WebElement contactAccessRemoveContactAccessPopDealRoomNameLabel;

	/**
	 * @return the contactAccessRemoveContactAccessPopDealRoomNameLabel
	 */
	public WebElement getContactAccessRemoveContactAccessPopDealRoomNameLabel(int timeOut) {
		return isDisplayed(driver, contactAccessRemoveContactAccessPopDealRoomNameLabel, "Visibility", timeOut, "Deal Room name Label On Remove Contact access popup");
	} 
	 
	@FindBy(xpath="//a[@title='refresh']")
	private WebElement refreshLinkAfterRemovingContactAccess;

	/**
	 * @return the refreshLinkAfterRemovingContactAccess
	 */
	public WebElement getRefreshLinkAfterRemovingContactAccess(int timeOut) {
		return isDisplayed(driver, refreshLinkAfterRemovingContactAccess, "Visibility", timeOut, "Refresh Link After Removing contact access");
	}
	
	@FindBy(xpath="(//div[contains(@class,'RemoveContactsAccess2')]//div[@class='formbox']/span)[1]")
	private WebElement contactAccessByDealRemoveContactPopupErrorMessage;

	/**
	 * @return the contactAccessByDealRemoveContactPopupErrorMessage
	 */
	public WebElement getContactAccessByDealRemoveContactPopupErrorMessage(int timeOut) {
		return isDisplayed(driver, contactAccessByDealRemoveContactPopupErrorMessage, "Visibility", timeOut, "Error Message on ByDeal Setting in contact access");
	}
	
	@FindBy(xpath="//div[contains(@class,'RemoveContactsAccess1_fancybox')]//span")
	private WebElement contactAccessByDealErrorMessage;

	/**
	 * @return the contactAccessByDealErrorMessage
	 */
	public WebElement getContactAccessByDealErrorMessage(int timeOut) {
		return isDisplayed(driver, contactAccessByDealErrorMessage, "Visibility", timeOut, "Contact Access By Deal Error Message");
	}
	
	@FindBy(xpath="//div[contains(@class,'RemoveContactsAccess1_fancybox')]//a[text()='Close']")
	private WebElement contactAccessByDealErrorMessagePopupCloseButton;

	/**
	 * @return the contactAccessByDealErrorMessagePopupCloseButton
	 */
	public WebElement getContactAccessByDealErrorMessagePopupCloseButton(int timeOut) {
		return isDisplayed(driver, contactAccessByDealErrorMessagePopupCloseButton, "Visibility", timeOut, "Close button on Erro Message Popup in Contact Access");
	}
	
	@FindBys(@FindBy(xpath="//span[contains(@id,'templateGrid-cell-1-')]"))
	private List<WebElement> folderTemplateDescriptionText;

	
	public List<WebElement> getfolderTemplateDescriptionText(int timeOut) {
		if(checkElementsVisibility(driver, folderTemplateDescriptionText, "Folder Template Description Text", timeOut)){
			return folderTemplateDescriptionText;
		} else {
			return null;
		}
	}

	public List<WebElement> dealList() {
		return FindElements(driver, "//ul[contains(@class,'ui-autocomplete')]//a", "Deal List from by deal");
		
	}
	
	
	// Version 2 Changes
	@FindBy(xpath="//span[text()='Firm']")
	private WebElement firmLabelContactAccessTab;

	/**
	 * @return the firmLabelContactAccessTab
	 */
	public WebElement getFirmLabelContactAccessTab(int timeOut) {
		return isDisplayed(driver, firmLabelContactAccessTab, "Visibility", timeOut, "Firm Label Contact Access Tab");
	}
	
	@FindBy(xpath="//span[text()='External Admin']")
	private WebElement externalAdminLabelContactAccessTab;

	/**
	 * @return the externalAdminLabelContactAccessTab
	 */
	public WebElement getExternalAdminLabelContactAccessTab(int timeOut) {
		return isDisplayed(driver, externalAdminLabelContactAccessTab, "Visibility", timeOut, "External Admin Label Contact Access Tab");
	}
	
	@FindBy(xpath="//div[contains(@class,'ResetPassword_fancybox FancyboxContainer ui-draggable')]/div[text()='Reset Password']")
	private WebElement resetPasswordpopupHeader;

	/**
	 * @return the resetPasswordpopupHeader
	 */
	public WebElement getResetPasswordpopupHeader(int timeOut) {
		return isDisplayed(driver, resetPasswordpopupHeader, "Visibility", timeOut, "Reset password popup header");
	}
	
	@FindBy(xpath="//div[@class='OneDiv']/div")
	private WebElement resetPasswordPopupMessage;

	/**
	 * @return the resetPasswordPopupMessage
	 */
	public WebElement getResetPasswordPopupMessage(int timeOut) {
		return isDisplayed(driver, resetPasswordPopupMessage, "Visibility", timeOut, "Reset Passowrd popup message");
	}
	
	@FindBy(xpath="//div[@class='OneDiv']/div/b")
	private WebElement resetPasswordPopupMessageContactName;

	/**
	 * @return the resetPasswordPopupMessage
	 */
	public WebElement getResetPasswordPopupMessageContactName(int timeOut) {
		return isDisplayed(driver, resetPasswordPopupMessageContactName, "Visibility", timeOut, "Reset Passowrd popup message Contact name");
	}
	
	@FindBy(xpath="//input[@id='sendEmailRd']")
	private WebElement resetPasswordPopupsendMailRadioButton;

	/**
	 * @return the resetPasswordPopupsendMailRadioButton
	 */
	public WebElement getResetPasswordPopupsendMailRadioButton(int timeOut) {
		return isDisplayed(driver, resetPasswordPopupsendMailRadioButton, "Visibility", timeOut, "reset password popup send email radio button");
	}
	
	@FindBy(xpath="(//input[@id='sendEmailRd']/..//following-sibling::div/div)[1]")
	private WebElement resetPasswordPopupSendMailText1;

	/**
	 * @return the resetPasswordPopupSendMailText
	 */
	public WebElement getResetPasswordPopupSendMailText1(int timeOut) {
		return isDisplayed(driver, resetPasswordPopupSendMailText1, "Visibility", timeOut, "reset Passowrd popup  send email text 1");
	}
	
	@FindBy(xpath="(//input[@id='sendEmailRd']/..//following-sibling::div/div)[2]")
	private WebElement resetPasswordPopupSendMailText2;

	/**
	 * @return the resetPasswordPopupSendMailText
	 */
	public WebElement getResetPasswordPopupSendMailText2(int timeOut) {
		return isDisplayed(driver, resetPasswordPopupSendMailText2, "Visibility", timeOut, "reset Passowrd popup  send email text 2");
	}
	
	@FindBy(xpath="//input[@id='cpLinkRd']")
	private WebElement resetPasswordPopupcopyLinkRadioButton;

	/**
	 * @return the resetPasswordPopupcopyLinkRadioButton
	 */
	public WebElement getResetPasswordPopupcopyLinkRadioButton(int timeOut) {
		return isDisplayed(driver, resetPasswordPopupcopyLinkRadioButton, "Visibility", timeOut, "Reset Pasword Popup Copy link radio button");
	}
	
	@FindBy(xpath="//a[@title='Send Email']")
	private WebElement resetPasswordPopupSendEMailButton;

	/**
	 * @return the resetPasswordPopupSendEMailButton
	 */
	public WebElement getResetPasswordPopupSendEMailButton(int timeOut) {
		return isDisplayed(driver, resetPasswordPopupSendEMailButton, "Visibility", timeOut, "Reset Password Popup Send Email Button");
	}
	
	@FindBy(xpath="//a[@title='Copy Link']")
	private WebElement resetPasswordPopupCopyLinkButton;

	/**
	 * @return the resetPasswordPopupSendEMailButton
	 */
	public WebElement getResetPasswordPopupCopyEmailButton(int timeOut) {
		return isDisplayed(driver, resetPasswordPopupCopyLinkButton, "Visibility", timeOut, "Reset Password Popup Copy Email Button");
	}
	
	@FindBy(xpath="//a[@title='Cancel']")
	private WebElement resetPasswordPopupCancelButton;

	/**
	 * @return the resetPasswordPopupCancelButton
	 */
	public WebElement getResetPasswordPopupCancelButton(int timeOut) {
		return isDisplayed(driver, resetPasswordPopupCancelButton, "Visibility", timeOut, "reset Password Popup cancel button");
	}
	
	@FindBy(xpath="//div[contains(@class,'ResetPassword_fancybox FancyboxContainer ui-draggable')]/div[text()='Reset Password']/a[@title='Close']")
	private WebElement resetPasswordPopupCloseIcon;

	/**
	 * @return the resetPasswordPopupCloseIcon
	 */
	public WebElement getResetPasswordPopupCloseIcon(int timeOut) {
		return isDisplayed(driver, resetPasswordPopupCloseIcon, "Visibility", timeOut, "Reset Password Popup Close Icon");
	}
	
	@FindBy(xpath="//input[@id='cpLinkRd']/..")
	private WebElement resetPasswordCopyLinkText;

	/**
	 * @return the resetPasswordCopyLinkText
	 */
	public WebElement getResetPasswordCopyLinkText(int timeOut) {
		return isDisplayed(driver, resetPasswordCopyLinkText, "Visibility", timeOut, "reset Password Copy Link Text");
	}

	@FindBy(xpath = "(//span[contains(text(),'No document(s) to display.')])[1]")
	private WebElement errorMessageOnGlobalUserGrid1;

	/**
	 * @return the errorMessageOnUserGrid1
	 */
	public WebElement getErrorMessageOnGlobalUserGrid1(int timeOut) {
		return isDisplayed(driver, errorMessageOnGlobalUserGrid1, "Visibility", timeOut, "No document(s) to display.");
	}
	
	@FindBy(xpath = "//span[contains(text(),'No Contacts to Display.') or contains(text(),'No Contacts to display.')]")
	private WebElement errorMessageOnGlobalUserGrid2;

	/**
	 * @return the errorMessageOnUserGrid2
	 */
	public WebElement getErrorMessageOnGlobalUserGrid2(int timeOut) {
		return isDisplayed(driver, errorMessageOnGlobalUserGrid2, "Visibility", timeOut, "No Contacts to Display");

	}
	
	@FindBy(xpath="//a[@title='OK']")
	private WebElement resetPasswordOKButton;

	/**
	 * @return the resetPasswordOKButton
	 */
	public WebElement getResetPasswordOKButton(int timeOut) {
		return isDisplayed(driver, resetPasswordOKButton, "Visibility", timeOut, "Reset Password OK Button");
	}
	
	@FindBy(xpath="//div[contains(@class,'RemoveResetPassword_fancybox')]/div[@class='head_popup']")
	private WebElement resetPasswordErrorMessagePopupHeaderForByDealSetting;

	/**
	 * @return the resetPasswordErrorMessagePopupHeaderForByDealSetting
	 */
	public WebElement getResetPasswordErrorMessagePopupHeaderForByDealSetting(int timeOut) {
		return isDisplayed(driver, resetPasswordErrorMessagePopupHeaderForByDealSetting, "Visibility", timeOut, "Reset Password error message popupfor by deal setting");
	}
	
	@FindBy(xpath="//div[contains(@class,'RemoveResetPassword_fancybox')]//a[text()='Close']")
	private WebElement resetPasswordByDealSettingErrorMessagePopupCloseButton;

	/**
	 * @return the resetPasswordErrorMessagePopupCloseButton
	 */
	public WebElement getResetPasswordByDealSettingErrorMessagePopupCloseButton(int timeOut) {
		return isDisplayed(driver, resetPasswordByDealSettingErrorMessagePopupCloseButton, "Visibility", timeOut, "Reset Password By Deal Setting Error message Popup Close button ");
	}
	
	@FindBy(xpath="//div[contains(@class,'RemoveResetPassword_fancybox')]/div[@class='head_popup']/a[@title='Close']")
	private WebElement resetPasswordByDealSettingErrorMessagePopupCloseIcon;

	/**
	 * @return the resetPasswordErrorMessagePopupCloseButton
	 */
	public WebElement getResetPasswordByDealSettingErrorMessagePopupCloseIcon(int timeOut) {
		return isDisplayed(driver, resetPasswordByDealSettingErrorMessagePopupCloseIcon, "Visibility", timeOut, "Reset Password By Deal Setting Error message Popup Close Icon ");
	}
	
	@FindBy(xpath="//div[contains(@class,'RemoveResetPassword_fancybox')]/div[@class='formbox']/span")
	private WebElement resetPasswordByDealSettingErrorMessage;

	/**
	 * @return the resetPasswordErrorMessagePopupCloseButton
	 */
	public WebElement getResetPasswordByDealSettingErrorMessage(int timeOut) {
		return isDisplayed(driver, resetPasswordByDealSettingErrorMessage, "Visibility", timeOut, "Reset Password By Deal Setting Error message");
	}


	/*************************************************************************************************************/
	
	@FindBy(xpath = "//div[@id='left_section']//a[@title='Content Ordering']")
	private WebElement contentOrderingTab;

	/**
	 * @return the contentOrderingTab
	 */
	public WebElement getContentOrderingTab(int timeOut) {
		return isDisplayed(driver, contentOrderingTab, "Visibility", timeOut, "Content Ordering Tab");
	}
	
	@FindBy(xpath = "//div[@id='editModeOrder']//select")
	private WebElement folderTemplateOrderType;

	/**
	 * @return the folderTemplateOrderType
	 */
	public WebElement getFolderTemplateOrderType(int timeOut) {
		return isDisplayed(driver, folderTemplateOrderType, "Visibility", timeOut, "Folder Template Order Type");
	}

//	@FindBy(xpath="//a[@title='OK']")
//	private WebElement resetPasswordOKButton;
//
//	/**
//	 * @return the resetPasswordOKButton
//	 */
//	public WebElement getResetPasswordOKButton(int timeOut) {
//		return isDisplayed(driver, resetPasswordOKButton, "Visibility", timeOut, "Reset Password OK Button");
//	}
//	
//	@FindBy(xpath="//div[contains(@class,'RemoveResetPassword_fancybox')]/div[@class='head_popup']")
//	private WebElement resetPasswordErrorMessagePopupHeaderForByDealSetting;
//
//	/**
//	 * @return the resetPasswordErrorMessagePopupHeaderForByDealSetting
//	 */
//	public WebElement getResetPasswordErrorMessagePopupHeaderForByDealSetting(int timeOut) {
//		return isDisplayed(driver, resetPasswordErrorMessagePopupHeaderForByDealSetting, "Visibility", timeOut, "Reset Password error message popupfor by deal setting");
//	}
	
	@FindBy(xpath="//div[@id='coMaindivID']//a[@title='Save']")
	private WebElement contentOrderingSaveButton;

	/**
	 * @return the contentOrderingSaveButton
	 */
	public WebElement getContentOrderingSaveButton(int timeOut) {
		return isDisplayed(driver, contentOrderingSaveButton, "Visibility", timeOut, "Content Ordering save button");
	}
	
	@FindBy(xpath = "//div[@id='coMaindivID']//div/span")
	private WebElement contentOrderingHeading;
	
	
	/**
	 * @return the contentOrderingHeading
	 */
	public WebElement getContentOrderingHeading(int timeOut) {
		return isDisplayed(driver, contentOrderingHeading, "Visibility", timeOut, "Content Ordering heading");
	}

	@FindBy(xpath = "//span[contains(text(),'Content Ordering')]/span/img")
	private WebElement imgIconContentOrdering;
	
	
	/**
	 * @return the imgIconContentOrdering
	 */
	public WebElement getImgIconContentOrdering(int timeOut) {
		return isDisplayed(driver, imgIconContentOrdering, "Visibility", timeOut, "img icon content ordering");
	}

	@FindBy(xpath = "//div[contains(@style,'DR_CRMFinal')]")
	private WebElement getImgTextContentOrdering;
	
	
	/**
	 * @return the getImgTextContentOrdering
	 */
	public WebElement getGetImgTextContentOrdering(int timeOut) {
		return isDisplayed(driver, getImgTextContentOrdering, "Visibility", timeOut, "img icon text content ordering");
	}
	
	@FindBy(xpath = "//span[@id='disableOT']")
	private WebElement defaultOrderTypeCOEdit;
	

	@FindBy(xpath = "//span[@id='page:form:dotype']")
	private WebElement defaultOrderTypeContentOrdering;
	
	
	/**
	 * @return the defaultOrderTypeContentOrdering
	 */
	public WebElement getDefaultOrderTypeContentOrdering(int timeOut) {
		return isDisplayed(driver, defaultOrderTypeContentOrdering, "Visibility", timeOut, "default order type");
	}
	
	public WebElement getDefaultOrderTypeContentOrdering(EditViewMode ev,int timeOut) {
		if (ev == EditViewMode.View)
		return isDisplayed(driver, defaultOrderTypeContentOrdering, "Visibility", timeOut, "default order type");
		else
			return isDisplayed(driver, defaultOrderTypeCOEdit, "Visibility", timeOut, "default order type");
			
	}
	
	@FindBy(xpath = "//a[@id='finalCancel']")
	private WebElement contentOrderingCancel;
	
	/**
	 * @return the contentOrderingCancel
	 */
	public WebElement getContentOrderingCancel(int timeOut) {
		return isDisplayed(driver, contentOrderingCancel, "Visibility", timeOut, "content Ordering Cancel button");
	}

	/**
	 * @return the noDealRoomMessageContentOrdering
	 */
	public WebElement getNoDealRoomMessageContentOrdering(EditViewMode ev,int timeOut) {
		if (ev == EditViewMode.View)
			return isDisplayed(driver, FindElement(driver, "//p[@id='nodealdiv']", "view mode", action.SCROLLANDBOOLEAN, timeOut/2), "visibility", timeOut/2, "view mode error message");
		else
			return isDisplayed(driver, FindElement(driver, "//p[@id='nodealdiv2']", "edit mode", action.SCROLLANDBOOLEAN, timeOut/2), "Visibility", timeOut, "edit no Deal Room Message");
	}

	public List<WebElement> listOfRadioButtonsUIContentOrdering(EditViewMode ev) {
		if (ev == EditViewMode.View)
			return FindElements(driver, "//div[@id='pageBlur_rgt']/following-sibling::p//input[@type='radio']", "radio buttons");
		else
			return FindElements(driver, "//div[@id='pageBlur_rgt_edit']/following-sibling::p//input[@type='radio']", "radio buttons");
		
	
	}
	@FindBy(xpath = "//select[@id='SelectedDealId']")
	private WebElement dealRoomSelectContentOrdering;
	
	
	/**
	 * @return the dealRoomSelectContentOrdering
	 */
	public WebElement getDealRoomSelectContentOrdering(int timeOut) {
		return isDisplayed(driver, dealRoomSelectContentOrdering, "Visibility", timeOut, "deal Room Select Content Ordering");
	}

	@FindBy(xpath = "(//span[contains(text(),'Lock Index')]/following-sibling::span//img)[1]")
	private WebElement lockIndexInfoIcon;
	
	
	/**
	 * @return the lockIndexInfoIcon
	 */
	public WebElement getLockIndexInfoIcon(EditViewMode ev,int timeOut) {
		if (ev == EditViewMode.View)
		return isDisplayed(driver, FindElement(driver, "(//span[contains(text(),'Lock Index')]/following-sibling::span//img)[1]", "lock index info icon", action.SCROLLANDBOOLEAN, timeOut/2), "Visibility", timeOut, "lock Index Info view mode");
		else
			return isDisplayed(driver, FindElement(driver, "(//span[contains(text(),'Lock Index')]/following-sibling::span//img)[2]", "lock index info icon", action.SCROLLANDBOOLEAN, timeOut/2), "Visibility", timeOut, "lock Index Info edit mode");
		
	}

	@FindBy(xpath = "//div[contains(@class,'tip-inner tip-bg-image intro1') and contains(text(),'Users')]")
	private WebElement lockIndexInfoIconText;
	
	
	/**
	 * @return the lockIndexInfoIconText
	 */
	public WebElement getLockIndexInfoIconText(int timeOut) {
		return isDisplayed(driver, lockIndexInfoIconText, "Visibility", timeOut, "lock Index Info Icon");
	}

	@FindBy(xpath = "//input[@value='Lock Index']")
	private WebElement lockIndexCheckbox;
	
	
	/**
	 * @return the lockIndexCheckbox
	 */
	public WebElement getLockIndexCheckbox(int timeOut) {
		return isDisplayed(driver, lockIndexCheckbox, "Visibility", timeOut, "lock Index Checkbox");
	}

	@FindBy(xpath = "//select[@id='select_ordertype']")
	private WebElement orderTypeDropdown;
	
	
	/**
	 * @return the orderTypeDropdown
	 */
	public WebElement getOrderTypeDropdown(int timeOut) {
		return isDisplayed(driver, orderTypeDropdown, "Visibility", timeOut, "orderTypeDropdown");
	}

//	@FindBy(xpath="//div[contains(@class,'confirmation2_popup')]//a[@title='Yes']")
//	private WebElement contentOrderingConfirmationPopUpYesBtn;
//
//	/**
//	 * @return the contentOrderingConfirmationPopUpYesBtn
//	 */
//	public WebElement getContentOrderingConfirmationPopUpYesBtn(int timeOut) {
//		return isDisplayed(driver, contentOrderingConfirmationPopUpYesBtn, "Visibility", timeOut, "Content Ordering Confirmation PopUp YesBtn");
//	}
//	@FindBy(xpath="//div[contains(@class,'RemoveResetPassword_fancybox')]//a[text()='Close']")
//	private WebElement resetPasswordByDealSettingErrorMessagePopupCloseButton;
//
//	/**
//	 * @return the resetPasswordErrorMessagePopupCloseButton
//	 */
//	public WebElement getResetPasswordByDealSettingErrorMessagePopupCloseButton(int timeOut) {
//		return isDisplayed(driver, resetPasswordByDealSettingErrorMessagePopupCloseButton, "Visibility", timeOut, "Reset Password By Deal Setting Error message Popup Close button ");
//	}
//	
//	@FindBy(xpath="//div[contains(@class,'RemoveResetPassword_fancybox')]/div[@class='head_popup']/a[@title='Close']")
//	private WebElement resetPasswordByDealSettingErrorMessagePopupCloseIcon;
//
//	/**
//	 * @return the resetPasswordErrorMessagePopupCloseButton
//	 */
//	public WebElement getResetPasswordByDealSettingErrorMessagePopupCloseIcon(int timeOut) {
//		return isDisplayed(driver, resetPasswordByDealSettingErrorMessagePopupCloseIcon, "Visibility", timeOut, "Reset Password By Deal Setting Error message Popup Close Icon ");
//	}
//	
//	@FindBy(xpath="//div[contains(@class,'RemoveResetPassword_fancybox')]/div[@class='formbox']/span")
//	private WebElement resetPasswordByDealSettingErrorMessage;
//
//	/**
//	 * @return the resetPasswordErrorMessagePopupCloseButton
//	 */
//	public WebElement getResetPasswordByDealSettingErrorMessage(int timeOut) {
//		return isDisplayed(driver, resetPasswordByDealSettingErrorMessage, "Visibility", timeOut, "Reset Password By Deal Setting Error message");
//	}	
	
	@FindBy(xpath="//span[@id='showFolderMessage']")
	private WebElement foldertemplateMessageof100Folders;

	/**
	 * @return the foldertemplateMessageof100Folders
	 */
	public WebElement getFoldertemplateMessageof100Folders(int timeOut) {
		return isDisplayed(driver, foldertemplateMessageof100Folders, "Visibility", timeOut, "Folder template message of 100 folders");
	}
	
	@FindBy(xpath="//div[@id='scrollUpAreaId_FT']/span[@class='head']")
	private WebElement folderTemplateOrderTypeText;

	/**
	 * @return the folderTemplateOrderTypeText
	 */
	public WebElement getFolderTemplateOrderTypeText(int timeOut) {
		return isDisplayed(driver, folderTemplateOrderTypeText, "Visibility", timeOut, "Folde template order type text");
	}
	
	@FindBy(xpath="//div[@id='scrollUpAreaId_FT']/select")
	private WebElement folderTemplateOrderTypeDropdown;

	/**
	 * @return the folderTemplateOrderTypeDropdown
	 */
	public WebElement getFolderTemplateOrderTypeDropdown(int timeOut) {
		return isDisplayed(driver, folderTemplateOrderTypeDropdown, "Visibility", timeOut, "Folder template order type dropdown");
	}
	
	@FindBy(xpath="//tr[@id='positionRowParent']//select[@class='folderPosition']")
	private WebElement folderTemplateAddFolderFolderPositionDropdown;
	
	@FindBy(xpath="//tr[@id='positionRowParentMF']//select[contains(@class,'folderPosition')]")
	private WebElement folderTemplateAddFolderFolderPositionDropdownOnManageFolder;
	
	
	/**
	 * @return the folderTemplateAddFolderFolderPositionDropdown
	 */
	public WebElement getFolderTemplateAddFolderFolderPositionDropdown(int timeOut,PageName pageName) {
		WebElement ele;
		if(pageName.toString().equalsIgnoreCase(PageName.ManageFolderPopUp.toString())) {
			ele=isDisplayed(driver, folderTemplateAddFolderFolderPositionDropdownOnManageFolder, "Visibility", timeOut, "Foldr position dropdown");
		}else {
			ele=isDisplayed(driver, folderTemplateAddFolderFolderPositionDropdown, "Visibility", timeOut, "Foldr position dropdown");
		}
		return ele;
	}
	
	@FindBy(xpath="//div[@class='ErrorPopup_fancybox FancyboxContainer ui-draggable']//div[@class='contacts_n_name_div border_none']/p")
	private WebElement folderTemplateDragAndDropErrorMessage;

	/**
	 * @return the folderTemplateDragAndDropErrorMessage
	 */
	public WebElement getFolderTemplateDragAndDropErrorMessage(int timeOut) {
		return isDisplayed(driver, folderTemplateDragAndDropErrorMessage, "Visibility", timeOut, "Drag and drop error message");
	}
	
	@FindBy(xpath="//div[@class='ErrorPopup_fancybox FancyboxContainer ui-draggable']//img")
	private WebElement folderTemplateDragAndDropErrorInfoIcon;

	/**
	 * @return the folderTemplateDragAndDropErrorInfoIcon
	 */
	public WebElement getFolderTemplateDragAndDropErrorInfoIcon(int timeOut) {
		return isDisplayed(driver, folderTemplateDragAndDropErrorInfoIcon, "Visibility", timeOut, "Info icon");
	}
	
	@FindBy(xpath="//input[@title='OK']")
	private WebElement folderTemplateDragAndDropOKButton;

	/**
	 * @return the folderTemplateDragAndDropOKButton
	 */
	public WebElement getFolderTemplateDragAndDropOKButton(int timeOut) {
		return isDisplayed(driver, folderTemplateDragAndDropOKButton, "Visibility", timeOut, "OK Button");
	}
	
	@FindBy(xpath="//div[@class='ErrorPopup_fancybox FancyboxContainer ui-draggable']/div[@class='head_popup']/a[@title='Close']")
	private WebElement folderTemplateDragAndDropCloseIcon;

	/**
	 * @return the folderTemplateDragAndDropCloseIcon
	 */
	public WebElement getFolderTemplateDragAndDropCloseIcon(int timeOut) {
		return isDisplayed(driver, folderTemplateDragAndDropCloseIcon, "Visibility", timeOut, "Drag and Drop Close icon");
	}
	
	@FindBy(xpath="(//div[@class='tip-inner tip-bg-image'])[2]")
	private WebElement folderTemplateDragAndDropinfoiconMessages;

	/**
	 * @return the folderTemplateDragAndDropinfoiconMessages
	 */
	public WebElement getFolderTemplateDragAndDropinfoiconMessages(int timeOut) {
		return isDisplayed(driver, folderTemplateDragAndDropinfoiconMessages, "Visibility", timeOut, "Info icon message");
	}
	
	@FindBy(xpath="//div[@id='ConfirmationPopupLastNodeFT_Id']")
	private WebElement folderTemplateDragDropConfirmationPopup;

	/**
	 * @return the folderTemplateDragDropConfirmationPopup
	 */
	public WebElement getFolderTemplateDragDropConfirmationPopup(int timeOut) {
		return isDisplayed(driver, folderTemplateDragDropConfirmationPopup, "Visibility", timeOut, "Confirmation popup");
	}
	
	@FindBy(xpath="//div[@id='ConfirmationPopupLastNodeFT_Id']//a[@title='Yes']")
	private WebElement folderTemplateDragDropConfirmationYEsButton;

	/**
	 * @return the folderTemplateDragDropConfirmationYEsButton
	 */
	public WebElement getFolderTemplateDragDropConfirmationYEsButton(int timeOut) {
		return isDisplayed(driver, folderTemplateDragDropConfirmationYEsButton, "Visibility", timeOut, "Yes button");
	}
	
	@FindBy(xpath="//div[@id='ConfirmationPopupLastNodeFT_Id']//a[@title='No']")
	private WebElement folderTemplateDragDropConfirmationNoButton;

	/**
	 * @return the folderTemplateDragDropConfirmationYEsButton
	 */
	public WebElement getFolderTemplateDragDropConfirmationNoButton(int timeOut) {
		return isDisplayed(driver, folderTemplateDragDropConfirmationNoButton, "Visibility", timeOut, "No button");
	}
	
	@FindBy(xpath="//div[@id='ConfirmationPopupLastNodeFT_Id']//div[@class='contacts_n_name_div']")
	private WebElement folderTemplateDragDropConfirmationErrorMessage;

	/**
	 * @return the folderTemplateDragDropConfirmationYEsButton
	 */
	public WebElement getFolderTemplateDragDropConfirmationErrorMessage(int timeOut) {
		return isDisplayed(driver, folderTemplateDragDropConfirmationErrorMessage, "Visibility", timeOut, "Error Message");
	}
	

	@FindBy(xpath="//span[text()='Manual Sorting (With Indexing)']/preceding-sibling::input[@class='myradio_safari messageCheckbox']")
	private WebElement manualSortingWithIndexingRadioButton;

	/**
	 * @return the manualSortingWithIndexingRadioButton
	 */
	public WebElement getManualSortingWithIndexingRadioButton(int timeOut) {
		return isDisplayed(driver, manualSortingWithIndexingRadioButton, "Visibility", timeOut, "Manual Sorting with indexing radio button");
	}

	@FindBy(xpath="//span[text()='Manual Sorting (Without Indexing)']/preceding-sibling::input[@class='myradio_safari messageCheckbox']")
	private WebElement manualSortingWithOutIndexingRadioButton;

	/**
	 * @return the manualSortingWithIndexingRadioButton
	 */
	public WebElement getManualSortingWithOutIndexingRadioButton(int timeOut) {
		return isDisplayed(driver, manualSortingWithOutIndexingRadioButton, "Visibility", timeOut, "Manual Sorting without indexing radio button");
	}
	
	@FindBy(xpath="//input[@class='mycheck_safari messageCheckbox2']")
	private WebElement lockIndexCheckBox;

	/**
	 * @return the lockIndexCheckBox
	 */
	public WebElement getLockIndexCheckBox(int timeOut) {
		return isDisplayed(driver, lockIndexCheckBox, "Visibility", timeOut, "Lock Index Check Box");
	}
	
	@FindBy(xpath="(//b[text()='Preview:'])[2]")
	private WebElement previewImage;

	/**
	 * @return the previewImage
	 */
	public WebElement getPreviewImage(int timeOut) {
		return isDisplayed(driver, previewImage, "Visibility", timeOut, "Preview Image");
	}
	
	@FindBy(xpath="//span[text()='Manual Sorting (With Indexing)']/preceding-sibling::input[@class='myradio_safari']")
	private WebElement manualSortingWithIndexingRadioButtonDisabled;

	/**
	 * @return the manualSortingWithIndexingRadioButton
	 */
	public WebElement getManualSortingWithIndexingRadioButtonDisabled(int timeOut) {
		return isDisplayed(driver, manualSortingWithIndexingRadioButtonDisabled, "Visibility", timeOut, "Manual Sorting with indexing radio button");
	}
	
	
	@FindBy(xpath="//input[@id='myRadio1']")
	private WebElement manualSortingWithoutIndexingRadioButtonDisabled;

	/**
	 * @return the manualSortingWithIndexingRadioButton
	 */
	public WebElement getManualSortingWithoutIndexingRadioButtonDisabled(int timeOut) {
		return isDisplayed(driver, manualSortingWithoutIndexingRadioButtonDisabled, "Visibility", timeOut, "Manual Sorting without indexing radio button");
	}
	
	@FindBy(xpath="//b[text()='Order Type:']/following-sibling::select")
	private WebElement orderTypeDropDown;

	/**
	 * @return the orderTypeDropDown
	 */
	public WebElement getOrderTypeDropDown(int timeOut) {
		return isDisplayed(driver, orderTypeDropDown, "Visibility", timeOut, "Order type drop down");
	}
	
	@FindBy(xpath="//b[text()='Order Type:']/following-sibling::span[@id='page:form:dotype']")
	private WebElement orderTypeAfterSave;

	/**
	 * @return the orderTypeAfterSave
	 */
	public WebElement getOrderTypeAfterSave(int timeOut) {
		return isDisplayed(driver, orderTypeAfterSave, "Visibility", timeOut, "Order type after save");
	}

	@FindBy(xpath="//input[@id='myRadio4']")
	private WebElement AlphabeticalSortingRadioButton;
	
	
	
	/**
	 * @return the alphabeticalSortingRadioButton
	 */
	public WebElement getAlphabeticalSortingRadioButton(int timeOut) {
		return isDisplayed(driver, AlphabeticalSortingRadioButton, "Visibility", timeOut, "Alphabetical Sorting Radio Button");
	}
	
	
	@FindBy(xpath="//select[@id='EditDealId']")
	private WebElement selectDealRoomContentOrderingEditMode;

	/**
	 * @return the selectDealRoomContentOrderingEditMode
	 */
	public WebElement getSelectDealRoomContentOrderingEditMode(int timeOut) {
		return isDisplayed(driver, selectDealRoomContentOrderingEditMode, "Visibility", timeOut, "Select deal room edit mode");
	}
	
	@FindBy(xpath="//div[@id='dealchangepopup']//div[2]")
	private WebElement dealChangewithoutSavePopUpMessage;

	/**
	 * @return the dealChangewithoutSavePopUpMessage
	 */
	public WebElement getDealChangewithoutSavePopUpMessage(int timeOut) {
		return isDisplayed(driver, dealChangewithoutSavePopUpMessage, "Visibility", timeOut, "deal change message");
	}
	
	@FindBy(xpath="//div[@id='dealchangepopup']//a")
	private WebElement dealChangewithoutSavePopUpCrossIcon;

	/**
	 * @return the dealChangewithoutSavePopUpCrossIcon
	 */
	public WebElement getDealChangewithoutSavePopUpCrossIcon(int timeOut) {
		return isDisplayed(driver, dealChangewithoutSavePopUpCrossIcon, "Visibility", timeOut, "Deal change pop cross icon");
	}
	
	@FindBy(xpath="//div[@id='dealchangepopup']//input[@title='No']")
	private WebElement dealChangePopUpNoButton;

	/**
	 * @return the dealChangePopUpNoButton
	 */
	public WebElement getDealChangePopUpNoButton(int timeOut) {
		return isDisplayed(driver, dealChangePopUpNoButton, "Visibility", timeOut, "Deal Change pop up NO Button");
	}
	
	@FindBy(xpath="//div[@id='dealchangepopup']//input[@title='Yes']")
	private WebElement dealChangePopUpYesButton;

	/**
	 * @return the dealChangePopUpYesButton
	 */
	public WebElement getDealChangePopUpYesButton(int timeOut) {
		return isDisplayed(driver, dealChangePopUpYesButton, "Visibility", timeOut, "Yes button");
	}
	
	@FindBy(xpath="(//div[contains(text(),'Insufficient')]/following-sibling::*)[2]//input[@value='Close']")
	private WebElement insufficientErrorMessageCancelButton;

	/**
	 * @return the insufficientErrorMessageCancelButton
	 */
	public WebElement getInsufficientErrorMessageCancelButton(int timeOut) {
		return isDisplayed(driver, insufficientErrorMessageCancelButton, "Visibility", timeOut, "insufficient cancel button");
	}
	
	@FindBy(xpath="//div[text()='Insufficient Permissions ']/following-sibling::div//input[@title='Close']")
	private WebElement insufficientPermissionCloseButtonCO;

	/**
	 * @return the insufficientPermissionCloseButtonCO
	 */
	public WebElement getInsufficientPermissionCloseButtonCO(int timeOut) {
		return isDisplayed(driver, insufficientPermissionCloseButtonCO, "Visibility", timeOut, "Close button");
	}

	@FindBy(xpath="//div[contains(@class,'confirmation2_popup')]//a[@title='No']")
	private WebElement contentOrderingConfirmationPopUpNoBtn;

	/**
	 * @return the contentOrderingConfirmationPopUpYesBtn
	 */
	public WebElement getContentOrderingConfirmationPopUpNoBtn(int timeOut) {
		return isDisplayed(driver, contentOrderingConfirmationPopUpNoBtn, "Visibility", timeOut, "Content Ordering Confirmation PopUp No Btn");
	}
		
	@FindBy(xpath="//a[@title='Cancel']")
	private WebElement internalUsertabCancelBtn;

	/**
	 * @return the internalUsertabCancelBtn
	 */
	public WebElement getInternalUsertabCancelBtn(int timeOut) {
		return isDisplayed(driver, internalUsertabCancelBtn, "Visibility", timeOut, "Internal User tab Cancel Btn");
	}
	
	@FindBy(id="previewImg")
	private WebElement waterMarkingPreviewLink;

	/**
	 * @return the waterMarkingPreviewLink
	 */
	public WebElement getWaterMarkingPreviewLink(int timeOut) {
		return isDisplayed(driver, waterMarkingPreviewLink, "Visibility", timeOut, "watermarking preview link");
	}
	
	@FindBy(id="grid2Search")
	private WebElement manageApprovalsSearchBtn;

	/**
	 * @return the manageApprovalsSearchBtn
	 */
	public WebElement getManageApprovalsSearchBtn(int timeOut) {
		return isDisplayed(driver, manageApprovalsSearchBtn, "Visibility", timeOut, "search button");
	}
	
	@FindBy(id="idonlyAlert")
	private WebElement profilePrefrenceNoEmailRadioBtn;

	/**
	 * @return the profilePrefrenceNoEmailRadioBtn
	 */
	public WebElement getProfilePrefrenceNoEmailRadioBtn(int timeOut) {
		return isDisplayed(driver, profilePrefrenceNoEmailRadioBtn, "Visibility", timeOut, "profile Prefrence No Email Radio Btn");
	}
	

	@FindBy(xpath="//tr[@id='folderPostionDivFT']//select[@class='folderPosition']")
	private WebElement folderTemplateAddFolderPositionDropodown;

	/**
	 * @return the folderTemplateAddFolderPositionDropodown
	 */
	public WebElement getFolderTemplateAddFolderPositionDropodown(int timeOut) {
		return isDisplayed(driver, folderTemplateAddFolderPositionDropodown, "Visibility", timeOut, "Folder position dropodwn");
	}
	
	@FindBy(xpath="//div[@id='idConfirmDeletion']//div[@class='formbox']")
	private WebElement folderTemplateDeleteFolderErrorMessage;

	/**
	 * @return the folderTemplateDeleteFolderErrorMessage
	 */
	public WebElement getFolderTemplateDeleteFolderErrorMessage(int timeOut) {
		return isDisplayed(driver, folderTemplateDeleteFolderErrorMessage, "Visibility", timeOut, "Folder delete error message");
	}
	

	@FindBy(xpath = "//select[@id='EditDealId']")
	private WebElement dealRoomSelectContentOrderingEdit;
	
	
	/**
	 * @return the dealRoomSelectContentOrdering
	 */
	public WebElement getDealRoomSelectContentOrderingEdit(int timeOut) {
		return isDisplayed(driver, dealRoomSelectContentOrderingEdit, "Visibility", timeOut, "deal Room Select Content Ordering");
	}

	@FindBy(xpath="//div[contains(@class,'confirmation2_popup')]//a[@title='Yes']")
	private WebElement contentOrderingConfirmationPopUpYesBtn;

	/**
	 * @return the contentOrderingConfirmationPopUpYesBtn
	 */
	public WebElement getContentOrderingConfirmationPopUpYesBtn(int timeOut) {
		return isDisplayed(driver, contentOrderingConfirmationPopUpYesBtn, "Visibility", timeOut, "Content Ordering Confirmation PopUp YesBtn");
	}

	@FindBy(xpath = "//div[@id='confirmDeleteTemp']/div[3]/a[@title='No']")
	private WebElement folderTemplateDeleteNoBtn;

	/**
	 * @return the folderTemplateDeleteNoBtn
	 */
	public WebElement getFolderTemplateDeleteNoBtn(int timeOut) {
		return isDisplayed(driver, folderTemplateDeleteNoBtn, "Visibility", timeOut, "Folder Template Delete No Button");
	}

	
	@FindBy(xpath="//a[contains(@onclick,'ChildFolder')]//following-sibling::a")
	private WebElement childFolderCancelButton;
	
	/**
	 * @return the childFolderCancelButton
	 */
	public WebElement getChildFolderCancelButton(int timeOut) {
		return isDisplayed(driver, childFolderCancelButton, "Visibility", timeOut, "Cancel button");
	}
	
	@FindBy(xpath="//div[contains(@class,'AddFirstLevelFolder_a')]//a[@title='Close']")
	private WebElement childFolderCrossIcon;
		
	/**
	 * @return the childFolderCrossIcon
	 */
	public WebElement getChildFolderCrossIcon(int timeOut) {
		return isDisplayed(driver, childFolderCrossIcon, "Visibility", timeOut, "Cross icon");
	}
	
	@FindBy(xpath="//span[contains(@id,'templateGrid-header-2')]//span[@class='aw-item-text ']")
	private WebElement folderTemplateOrderTypeLabel;

	/**
	 * @return the folderTemplateCreatedByLabel
	 */
	public WebElement getFolderTemplateOrderTypeLabel(int timeOut) {
		return isDisplayed(driver, folderTemplateOrderTypeLabel, "Visibility", timeOut, "Folder Template order type Label");
	}

	@FindBys(@FindBy(xpath="//span[contains(@id,'templateGrid-cell-2-')]"))
	private List<WebElement> folderTemplateOrderTypeList;

	public List<WebElement> getfolderTemplateOrderTypeList(int timeOut) {
		return FindElements(driver, "//span[contains(@id,'templateGrid-cell-2-')]"," Folder Template order type List");	
			}

	@FindBy(xpath="//td[@id='NameView']//div[@class='ViewFieldValue']")
	private WebElement myProfileNameInviewMode;

	/**
	 * @return the myProfileNameInviewMode
	 */
	public WebElement getMyProfileNameInviewMode(int timeOut) {
		return isDisplayed(driver, myProfileNameInviewMode, "Visibility", timeOut, "My profile name in view mode");
	}

	@FindBy(xpath="//div[contains(@id,'idCharError')]//div[@class='head_popup']//a")
	private WebElement folderTemplateFolderAlreadyExistPopupCrossIcon;

	/**
	 * @return the folderTemplateFolderAlreadyExistPopupCloseButton
	 */
	public WebElement getFolderTemplateFolderAlreadyExistPopupCrossIcon(int timeOut) {
		return isDisplayed(driver, folderTemplateFolderAlreadyExistPopupCrossIcon, "Visibility", timeOut, "Folder Already Exist Error Message Cross Icon");
	}
	
	@FindBy(xpath="//a[@title='Manage Groups']")
	private WebElement manageGroupTab;

	/**
	 * @return the manageGroupTab
	 */
	public WebElement getManageGroupTab(int timeOut) {
		return isDisplayed(driver, manageGroupTab, "Visibility", timeOut, "Manage Group Tab");
	}
	
	@FindBy(xpath="//select[@id='page:j_id0:slDeal']")
	private WebElement selectDealRoomDropDownMG;

	/**
	 * @return the selectDealRoomDropDownMG
	 */
	public WebElement getSelectDealRoomDropDownMG(int timeOut) {
		return isDisplayed(driver, selectDealRoomDropDownMG, "Visibility", timeOut, "Select deal room Drop DOwn");
	}
	
	@FindBy(xpath="//a[@id='newGrpBtn']")
	private WebElement newGroupButton;

	/**
	 * @return the newGroupButton
	 */
	public WebElement getNewGroupButton(int timeOut) {
		return isDisplayed(driver, newGroupButton, "Visibility", timeOut, "New Group button");
	}
	
	@FindBy(xpath="//a[@onclick='cancelRedirect()']")
	private WebElement newGroupCancelButton;

	/**
	 * @return the newGroupCancelButton
	 */
	public WebElement getNewGroupCancelButton(int timeOut) {
		return isDisplayed(driver, newGroupCancelButton, "Visibility", timeOut, "Cancel Button");
	}
	
	@FindBy(xpath="//input[@id='AFCheckIdBWFR']")
	private WebElement showCheckBox;

	/**
	 * @return the showCheckBox
	 */
	public WebElement getShowCheckBox(int timeOut) {
		return isDisplayed(driver, showCheckBox, "Visibility", timeOut, "Show CheckBox");
	}
	
	@FindBy(xpath="//div[@id='hideFilterText12BWFR']//a[@class='AdvancedFilterLink HideShowSearchIcon']")
	private WebElement showFilterMG;

	/**
	 * @return the showFilterMG
	 */
	public WebElement getShowFilterMG(int timeOut) {
		return isDisplayed(driver, showFilterMG, "Visibility", timeOut, "Show Filter");
	}
	
	@FindBy(xpath="//span[@title='Add Row']")
	private WebElement addRowMG;

	/**
	 * @return the addRowMG
	 */
	public WebElement getAddRowMG(int timeOut) {
		return isDisplayed(driver, addRowMG, "Visibility", timeOut, "add row");
	}
	
	@FindBy(xpath="//img[@title='Remove Row']")
	private WebElement removeRowMG;

	/**
	 * @return the removeRowMG
	 */
	public WebElement getRemoveRowMG(int timeOut) {
		return isDisplayed(driver, removeRowMG, "Visibility", timeOut, "Remove Row");
	}
	
	@FindBy(xpath="//a[@title='Clear']")
	private WebElement clearButtonMG;

	/**
	 * @return the clearButton
	 */
	public WebElement getClearButtonMG(int timeOut) {
		return isDisplayed(driver, clearButtonMG, "Visibility", timeOut, "Clear button");
	}
	
	@FindBy(xpath="(//a[@title='Apply'])[2]")
	private WebElement filterApplyButtonMG;

	/**
	 * @return the filterApplyButtonMG
	 */
	public WebElement getFilterApplyButtonMG(int timeOut) {
		return isDisplayed(driver, filterApplyButtonMG, "Visibility", timeOut, "filter apply button");
	}
	
	@FindBy(xpath="//input[@id='searchcon_gridContactSearchTopGridBWFR']")
	private WebElement contactGridSearchBoxMG;

	/**
	 * @return the contactGridSearchBoxMG
	 */
	public WebElement getContactGridSearchBoxMG(int timeOut) {
		return isDisplayed(driver, contactGridSearchBoxMG, "Visibility", timeOut, "Search box");
	}
	
	@FindBy(xpath="//a[@id='btn-searchTopGridBWFR']")
	private WebElement contactGridSearchIconMG;

	/**
	 * @return the contactGridSearchBoxMG
	 */
	public WebElement getContactGridSearchIconMG(int timeOut) {
		return isDisplayed(driver, contactGridSearchIconMG, "Visibility", timeOut, "Search Icon");
	}

	@FindBy(xpath="//a[@title='Add']")
	private WebElement addButtonMG;

	/**
	 * @return the addButtonMG
	 */
	public WebElement getAddButtonMG(int timeOut) {
		return isDisplayed(driver, addButtonMG, "Visibility", timeOut, "Add button");
	}
	
	@FindBy(xpath="//input[@id='searchcon_gridContactSearchBottomGridBWFR']")
	private WebElement contactAccessGridSearchBoxMG;

	/**
	 * @return the contactAccessGridSearchBoxMG
	 */
	public WebElement getContactAccessGridSearchBoxMG(int timeOut) {
		return isDisplayed(driver, contactAccessGridSearchBoxMG, "Visibility", timeOut, "Search box");
	}
	
	@FindBy(xpath="//a[@id='btn-searchBottomGridBWFR']")
	private WebElement contactAccessGridSearchIconMG;

	/**
	 * @return the contactAccessGridSearchBoxMG
	 */
	public WebElement getContactAccessGridSearchIconMG(int timeOut) {
		return isDisplayed(driver, contactAccessGridSearchIconMG, "Visibility", timeOut, "Search Icon");
	}
	
	@FindBy(xpath="//a[@id='contactlinkIdBottomGridBWFR']")
	private WebElement serchForContactMG;

	/**
	 * @return the serchForContactMG
	 */
	public WebElement getSerchForContactMG(int timeOut) {
		return isDisplayed(driver, serchForContactMG, "Visibility", timeOut, "Search For contact*");
	}
	
	@FindBy(xpath="(//a[@title='Clear Search'])[1]")
	private WebElement contactGridSearchCrossIconMG;

	/**
	 * @return the contactGridSearchCrossIconMG
	 */
	public WebElement getContactGridSearchCrossIconMG(int timeOut) {
		return isDisplayed(driver, contactGridSearchCrossIconMG, "Visibility", timeOut, "Cross icon");
	}
	
	@FindBy(xpath="(//a[@title='Clear Search'])[2]")
	private WebElement contactAccessGridSearchCrossIconMG;

	/**
	 * @return the contactAccessGridSearchCrossIconMG
	 */
	public WebElement getContactAccessGridSearchCrossIconMG(int timeOut) {
		return isDisplayed(driver, contactAccessGridSearchCrossIconMG, "Visibility", timeOut, "Cross icon");
	}
	
	@FindBy(xpath="//input[@id='grpName']")
	private WebElement groupNameTextBox;

	/**
	 * @return the groupNameTextBox
	 */
	public WebElement getGroupNameTextBox(int timeOut) {
		return isDisplayed(driver, groupNameTextBox, "Visibility", timeOut, "Group Name text box");
	}
	
	@FindBy(xpath="//textarea[@id='grpDesc']")
	private WebElement groupDescTextBox;

	/**
	 * @return the groupNameTextBox
	 */
	public WebElement getGroupDescTextBox(int timeOut) {
		return isDisplayed(driver, groupDescTextBox, "Visibility", timeOut, "Group Desc text box");
	}
	
	@FindBy(xpath="//a[@id='activeSavebtn']")
	private WebElement saveButtonMG;

	/**
	 * @return the saveButtonMG
	 */
	public WebElement getSaveButtonMG(int timeOut) {
		return isDisplayed(driver, saveButtonMG, "Visibility", timeOut, "Save button");
	}
	
	@FindBy(xpath="//a[@onclick='saveRedirect();return false;'][text()='Close']")
	private WebElement groupSaveSuccessMsgCloseButtonMG;

	/**
	 * @return the groupSaveSuccessMsgCloseButtonMG
	 */
	public WebElement getGroupSaveSuccessMsgCloseButtonMG(int timeOut) {
		return isDisplayed(driver, groupSaveSuccessMsgCloseButtonMG, "Visibility", timeOut, "CLose button");
	}

	@FindBy(xpath="//a[@id='goBack']")
	private WebElement goBackLinkMG;

	/**
	 * @return the goBackLinkMG
	 */
	public WebElement getGoBackLinkMG(int timeOut) {
		return isDisplayed(driver, goBackLinkMG, "Visibility", timeOut, "Go back link");
	}
	
	@FindBy(xpath="//a[@id='btnFldrPrmsn']")
	private WebElement manageFolderPermissionButton;

	/**
	 * @return the manageFolderPermissionButton
	 */
	public WebElement getManageFolderPermissionButton(int timeOut) {
		return isDisplayed(driver, manageFolderPermissionButton, "Visibility", timeOut, "Manage Folder Permission");
	}
	
	@FindBy(xpath="//a[@class='btn_active close success-popup']")
	private WebElement activateGroupErrorMessageCloseButton;

	/**
	 * @return the activateGroupErrorMessage
	 */
	public WebElement getActivateGroupErrorMessageCloseButton(int timeOut) {
		return isDisplayed(driver, activateGroupErrorMessageCloseButton, "Visibility", timeOut, "Activate Group Error Message Close Button");
	}
	
	@FindBy(xpath="//a[text()='Clone Folder Permissions']")
	private WebElement cloneFolderPermissionButton;

	/**
	 * @return the cloneFolderPermissionButton
	 */
	public WebElement getCloneFolderPermissionButton(int timeOut) {
		return isDisplayed(driver, cloneFolderPermissionButton, "Visibility", timeOut, "Clone folder permission button");
	}
	
	@FindBy(xpath="//a[@onclick='cloneGroup(); return false;']")
	private WebElement cloneGroupSaveButton;

	/**
	 * @return the cloneGroupSaveButton
	 */
	public WebElement getCloneGroupSaveButton(int timeOut) {
		return isDisplayed(driver, cloneGroupSaveButton, "Visibility", timeOut, "Clone Group Save button");
	}
	
	@FindBy(xpath="//a[@onclick=\"closepopup('fancybox_background','FancyboxContainer'); return false;\"]")
	private WebElement cloneGroupCancelButton;

	/**
	 * @return the cloneGroupSaveButton
	 */
	public WebElement getCloneGroupCancelButton(int timeOut) {
		return isDisplayed(driver, cloneGroupCancelButton, "Visibility", timeOut, "Clone Group Cancel button");
	}
	
	@FindBy(xpath="//div[text()='Manage Folder Permissions']/following-sibling::div//a[@title='Cancel']")
	private WebElement manageFolderPermissionCancelButton;

	/**
	 * @return the manageFolderPermissionCancelButton
	 */
	public WebElement getManageFolderPermissionCancelButton(int timeOut) {
		return isDisplayed(driver, manageFolderPermissionCancelButton, "Visibility", timeOut, "Manage Folder Permission Cancel Button");
	}
	
	@FindBy(xpath="//a[@title='Add Folder Permissions']")
	private WebElement addFolderPermissionButton;

	/**
	 * @return the addFolderPermissionButton
	 */
	public WebElement getAddFolderPermissionButton(int timeOut) {
		return isDisplayed(driver, addFolderPermissionButton, "Visibility", timeOut, "Add Folder Permission button");
	}
	
	@FindBy(xpath="//input[@id='searchcon_gridEmail_MFP']")
	private WebElement searchBoxManageFolderPermission;

	/**
	 * @return the searchBoxManageFolderPermission
	 */
	public WebElement getSearchBoxManageFolderPermission(int timeOut) {
		return isDisplayed(driver, searchBoxManageFolderPermission, "Visibility", timeOut, "Search box");
	}
	
	@FindBy(xpath="(//a[@title='Clear Search'])[2]")
	private WebElement clearSearchIconMFP;

	/**
	 * @return the clearSearchIcon
	 */
	public WebElement getClearSearchIconMFP(int timeOut) {
		return isDisplayed(driver, clearSearchIconMFP, "Visibility", timeOut, "Clear search button");
	}


	@FindBy(xpath="//a[@id='saveSecond']")
	private WebElement manageFolderPermissionSaveBtton;

	/**
	 * @return the manageFolderPermissionSaveBtton
	 */
	public WebElement getManageFolderPermissionSaveBtton(int timeOut) {
		return isDisplayed(driver, manageFolderPermissionSaveBtton, "Visibility", timeOut, "Save button");
	}
	
	@FindBy(xpath="//div[contains(@class,'ManageFolderPermission')]//div[text()='Confirmation']/following-sibling::div[2]/a")
	private WebElement manageFolderPermissionConfirmationCloseButton;

	/**
	 * @return the manageFolderPermissionConfirmationCloseButton
	 */
	public WebElement getManageFolderPermissionConfirmationCloseButton(int timeOut) {
		return isDisplayed(driver, manageFolderPermissionConfirmationCloseButton, "Visibility", timeOut, "Manage Folder Permission Confirmation close button");
	}
	
	@FindBy(xpath="//a[@id='invitationpreviewID1']")
	private WebElement invitationPreviewLinkMG;

	/**
	 * @return the invitationPreviewLinkMG
	 */
	public WebElement getInvitationPreviewLinkMG(int timeOut) {
		return isDisplayed(driver, invitationPreviewLinkMG, "Visibility", timeOut, "Invitation preview link");
	}
	
	@FindBy(xpath="(//a[@onclick='cancelpopupppreviewstdtemp_ME();return false;'])[2]")
	private WebElement previewPopUpCloseButtonMG;

	/**
	 * @return the previewPopUpCloseButtonMG
	 */
	public WebElement getPreviewPopUpCloseButtonMG(int timeOut) {
		return isDisplayed(driver, previewPopUpCloseButtonMG, "Visibility", timeOut, "Close button");
	}
	
	@FindBy(xpath="(//a[@id='invitationEditID1'])[1]")
	private WebElement invitationMailEditLinkMG;

	/**
	 * @return the invitationMailEditLinkMG
	 */
	public WebElement getInvitationMailEditLinkMG(int timeOut) {
		return isDisplayed(driver, invitationMailEditLinkMG, "Visibility", timeOut, "Edit Link");
	}
	
	@FindBy(xpath="(//a[@onclick='cancelpopuppeditcusttemp_ME();mergerFun_close();return false;'])[2]")
	private WebElement invitationMailEditPopUpCloseButtonMG;

	/**
	 * @return the previewPopUpCloseButtonMG
	 */
	public WebElement getInvitationMailEditPopUpCloseButtonMG(int timeOut) {
		return isDisplayed(driver, invitationMailEditPopUpCloseButtonMG, "Visibility", timeOut, "Close button");
	}
	
	@FindBy(xpath="//input[@id='myRadio2_ME']")
	private WebElement customEmailRadioButtonMG;

	/**
	 * @return the customEmailRadioButtonMG
	 */
	public WebElement getCustomEmailRadioButtonMG(int timeOut) {
		return isDisplayed(driver, customEmailRadioButtonMG, "Visibility", timeOut, "Custom Email Radio Button");
	}
	
	@FindBy(xpath="(//a[@onclick='cancelpopuppeditcusttemp_ME();mergerFun_close();return false;'])[2]")
	private WebElement customEmailPopUpCloseButtonMG;

	/**
	 * @return the previewPopUpCloseButtonMG
	 */
	public WebElement getCustomEmailPopUpCloseButtonMG(int timeOut) {
		return isDisplayed(driver, customEmailPopUpCloseButtonMG, "Visibility", timeOut, "Close button");
	}
	
	@FindBy(xpath="(//a[@id='customEditID2'])[1]")
	private WebElement customMailEditLinkMG;

	/**
	 * @return the invitationMailEditLinkMG
	 */
	public WebElement getCustomMailEditLinkMG(int timeOut) {
		return isDisplayed(driver, customMailEditLinkMG, "Visibility", timeOut, "Edit Link");
	}
	
	@FindBy(xpath="//input[@id='page:j_id0:cust_subject_tempid']")
	private WebElement customEmailPopUpSubjectTextboxMG;

	/**
	 * @return the customEmailPopUpSubjectTextboxMG
	 */
	public WebElement getCustomEmailPopUpSubjectTextboxMG(int timeOut) {
		return isDisplayed(driver, customEmailPopUpSubjectTextboxMG, "Visibility", timeOut, "Subject text box");
	}

	@FindBy(xpath="(//a[@onclick=\"applyforcustomtemplate_ME('page:j_id0:cust_subject_tempid','page:j_id0:custtempid');mergerFun_close();return false;\"])[1]")
	private WebElement customEmailPopUpApplyButtonMG;

	/**
	 * @return the previewPopUpCloseButtonMG
	 */
	public WebElement getCustomEmailPopUpApplyButtonMG(int timeOut) {
		return isDisplayed(driver, customEmailPopUpApplyButtonMG, "Visibility", timeOut, "Apply button");
	}
	
	@FindBy(xpath="(//a[@onclick='cancelpopuppeditcusttemp_ME();return false;'])[2]")
	private WebElement changeAppliedNoButtonMG;

	/**
	 * @return the changeAppliedNoButtonMG
	 */
	public WebElement getChangeAppliedNoButtonMG(int timeOut) {
		return isDisplayed(driver, changeAppliedNoButtonMG, "Visibility", timeOut, "No button");
	}
	
	@FindBy(xpath="(//a[@onclick='applyfortemplateSave_ME();return false;'])[1]")
	private WebElement changeAppliedYesButtonMG;

	/**
	 * @return the changeAppliedNoButtonMG
	 */
	public WebElement getChangeAppliedYesButtonMG(int timeOut) {
		return isDisplayed(driver, changeAppliedYesButtonMG, "Visibility", timeOut, "Yes button");
	}
	
	@FindBy(xpath="//a[@id='customPreviewID2']")
	private WebElement customEmailPreviewLinkMG;

	/**
	 * @return the invitationPreviewLinkMG
	 */
	public WebElement getCustomEmailPreviewLinkMG(int timeOut) {
		return isDisplayed(driver, customEmailPreviewLinkMG, "Visibility", timeOut, "Custom Mail preview link");
	}
	
	@FindBy(xpath="(//a[@onclick='cancelpopupppreviewcusttemp_ME();return false;'])[2]")
	private WebElement customMailPreviewPopUpCloseButton;

	/**
	 * @return the customMailPreviewPopUpCancelButton
	 */
	public WebElement getCustomMailPreviewPopUpCloseButton(int timeOut) {
		return isDisplayed(driver, customMailPreviewPopUpCloseButton, "Visibility", timeOut, "cancel button");
	}
	
	@FindBy(xpath="//input[@id='myRadio1_ME']")
	private WebElement invitationEmailRadioButtonMG;

	/**
	 * @return the customEmailRadioButtonMG
	 */
	public WebElement getInvitationEmailRadioButtonMG(int timeOut) {
		return isDisplayed(driver, invitationEmailRadioButtonMG, "Visibility", timeOut, "Custom Email Radio Button");
	}
	
	@FindBy(xpath="//a[@id='sendActive']")
	private WebElement sendEmailButtonMG;

	/**
	 * @return the sendEmailButtonMG
	 */
	public WebElement getSendEmailButtonMG(int timeOut) {
		return isDisplayed(driver, sendEmailButtonMG, "Visibility", timeOut, "Send Email Button");
	}
	
	@FindBy(xpath="//a[@onclick='sendEmailData();return false;']")
	private WebElement sendEmailConfirmationYesButton;

	/**
	 * @return the sendEmailConfirmationYesButton
	 */
	public WebElement getSendEmailConfirmationYesButton(int timeOut) {
		return isDisplayed(driver, sendEmailConfirmationYesButton, "Visibility", timeOut, "Yes button");
	}
	
	@FindBy(xpath="//a[@id='deact_activ_btn']")
	private WebElement moveButtonMG;

	/**
	 * @return the moveButtonMG
	 */
	public WebElement getMoveButtonMG(int timeOut) {
		return isDisplayed(driver, moveButtonMG, "Visibility", timeOut, "Move button");
	}
	
	@FindBy(xpath="//select[@id='selOthrGrp']")
	private WebElement selectGroupDropDownMG;

	/**
	 * @return the selectGroupDropDownMG
	 */
	public WebElement getSelectGroupDropDownMG(int timeOut) {
		return isDisplayed(driver, selectGroupDropDownMG, "Visibility", timeOut, "Select Group Drop Down");
	}
	
	@FindBy(xpath="//a[@id='moveActive']/following-sibling::a[@title='Cancel']")
	private WebElement movePopUpCancelButtonMG;

	/**
	 * @return the movePopUpCancelButtonMG
	 */
	public WebElement getMovePopUpCancelButtonMG(int timeOut) {
		return isDisplayed(driver, movePopUpCancelButtonMG, "Visibility", timeOut, "Cancel Button");
	}
	
	@FindBy(xpath="//a[@id='moveActive']")
	private WebElement movePopUpMoveButtonMG;

	/**
	 * @return the movePopUpCancelButtonMG
	 */
	public WebElement getMovePopUpMoveButtonMG(int timeOut) {
		return isDisplayed(driver, movePopUpMoveButtonMG, "Visibility", timeOut, "Move Pop Up Move button");
	}
	
	@FindBy(xpath="//div[text()='Success']/following-sibling::div/a")
	private WebElement successPopUpCloseButtonMG;

	/**
	 * @return the successPopUpCloseButtonMG
	 */
	public WebElement getSuccessPopUpCloseButtonMG(int timeOut) {
		return isDisplayed(driver, successPopUpCloseButtonMG, "Visibility", timeOut, "Close button");
	}
	
	@FindBy(xpath = "//span[@id='idMmbrCount']")
	private WebElement memberCountDRMGrouping;
	
	public WebElement getMemberCountDRMGrouping(int timeOut) {
		return isDisplayed(driver, memberCountDRMGrouping, "Visibility", timeOut, "Close button");
	}
	
	@FindBy(xpath = "//span[@id='idFldrCount']")
	private WebElement DocCountDRMGrouping;
	
	public WebElement getDocCountDRMGrouping(int timeOut) {
		return isDisplayed(driver, DocCountDRMGrouping, "Visibility", timeOut, "Close button");
	}
	@FindBy(xpath="//a[@title='Delete Group']")
	private WebElement deleteGroupButtonMG;

	/**
	 * @return the deleteGroupButtonMG
	 */
	public WebElement getDeleteGroupButtonMG(int timeOut) {
		return isDisplayed(driver, deleteGroupButtonMG, "Visibility", timeOut, "Delete Group Button");
	}
	 
	@FindBy(xpath="//div[text()='Confirm deletion of Group ']/following-sibling::div[2]//a[@title='No']")
	private WebElement groupDeleteConfirmationNoButtonMG;

	/**
	 * @return the groupDeleteConfirmationNoButtonMG
	 */
	public WebElement getGroupDeleteConfirmationNoButtonMG(int timeOut) {
		return isDisplayed(driver, groupDeleteConfirmationNoButtonMG, "Visibility", timeOut, "No button");
	}
	
	@FindBy(xpath="//div[text()='Confirm deletion of Group ']/following-sibling::div[2]//a[@title='Yes']")
	private WebElement groupDeleteConfirmationYesButtonMG;

	/**
	 * @return the groupDeleteConfirmationYesButtonMG
	 */
	public WebElement getGroupDeleteConfirmationYesButtonMG(int timeOut) {
		return isDisplayed(driver, groupDeleteConfirmationYesButtonMG, "Visibility", timeOut, "Yes button");
	}
	
	@FindBy(xpath="//div[text()='Error']/following-sibling::div[2]/a[@id='Deactive']")
	private WebElement manageGroupDeactivateErrorPopUpCloseButton;

	/**
	 * @return the manageGroupDeactivateErrorPopUpCloseButton
	 */
	public WebElement getManageGroupDeactivateErrorPopUpCloseButton(int timeOut) {
		return isDisplayed(driver, manageGroupDeactivateErrorPopUpCloseButton, "Visibility", timeOut, "Close button");
	}
	
	@FindBy(xpath="//input[@id='foldeglobaladdfund']")
	private WebElement manageFolderStandardFolderNameText;

	/**
	 * @return the manageFolderStandardFolderNameText
	 */
	public WebElement getManageFolderStandardFolderNameText(int timeOut) {
		return isDisplayed(driver, manageFolderStandardFolderNameText, "Visibility", timeOut, "Manage Folder Standard Folder Name Text Box");
	}
	
	public List<WebElement> getdrmPageSideMenus() {
		return FindElements(driver, "//div[@id='home_main']//ul//li/a", "DRM Page Side Menus");
	}
	
}
