package com.navatar.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import com.navatar.generic.BaseLib;
import com.navatar.generic.EnumConstants.Mode;
import com.navatar.generic.EnumConstants.action;
import com.navatar.generic.EnumConstants.excelLabel;

import static com.navatar.generic.AppListeners.appLog;
import static com.navatar.generic.CommonLib.*;
import java.util.List;


public class InstitutionsPageBusinessLayer extends InstitutionsPage {

	public InstitutionsPageBusinessLayer(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param environment
	 * @param mode
	 * @param inst_name
	 * @return true/false
	 */
	public boolean clickOnCreatedInstitution(String environment,String mode,String inst_name) {
		
		if(mode.equalsIgnoreCase(Mode.Classic.toString())){
			
			List<WebElement> optionsInDropDown = FindElements(driver, "//select[@id='fcf']/option[text()='All Institutions']", "");
			String[] options = {};
			if(optionsInDropDown.size()>1){
				String[] o = {optionsInDropDown.get(0).getAttribute("value"), optionsInDropDown.get(1).getAttribute("value")};
				options = o;
			} else {
				String[] o = {optionsInDropDown.get(0).getAttribute("value")};
				options = o;
			}
			
			int i =1;
			if (getSelectedOptionOfDropDown(driver, getViewDropdown(60), "View dropdown", "text")
					.equalsIgnoreCase("All Institutions")) {
				if (click(driver, getGoButton(60), "Go button", action.BOOLEAN)) {

				} else {
					appLog.error("Go button not found");
				}
			} else {
				if (selectVisibleTextFromDropDown(driver, getViewDropdown(60), "View dropdown", options[0])) {
				} else {
					appLog.error("All institutions not found in dropdown");
				}

			}
			WebElement ele = isDisplayed(driver,
					FindElement(driver, "//div[@class='x-panel-bwrap']//span[text()='" + inst_name + "']/..",
							"Institution link", action.SCROLLANDBOOLEAN, 20),
					"visibility", 20, "");
			if(ele==null){
				if(options.length>1){
					if (selectVisibleTextFromDropDown(driver, getViewDropdown(60), "View dropdown", options[1])) {
						ele = isDisplayed(driver,
								FindElement(driver, "//div[@class='x-panel-bwrap']//span[text()='" + inst_name + "']/..",
										"Institution link", action.SCROLLANDBOOLEAN, 20),
								"visibility", 20, "");
					} else {
						appLog.error("All institutions not found in dropdown");
					}
				} else {
					appLog.error("All institutions not found in dropdown");
				}
			}
			if (ele != null) {
				scrollDownThroughWebelement(driver, ele, "");
				if (click(driver, ele, inst_name + " name text", action.SCROLLANDBOOLEAN)) {
					appLog.info("Clicked on institution link");
					return true;
				} else {
					appLog.error("Not able to click on " + inst_name);
				}
			} else {
				while (true) {
					appLog.error("Institutions is not Displaying on "+i+ " Page: " + inst_name);
					if (click(driver, getNextImageonPage(10), "Institutions Page Next Button",
							action.SCROLLANDBOOLEAN)) {
						ThreadSleep(2000);
						appLog.info("Clicked on Next Button");
						ele = FindElement(driver, "//div[@class='x-panel-bwrap']//span[text()='" + inst_name + "']/..",
								"Institution link", action.SCROLLANDBOOLEAN, 20);
						if (ele != null) {
							if (click(driver, ele, inst_name, action.SCROLLANDBOOLEAN)) {
								appLog.info("Clicked on Institutions name : " + inst_name);
								return true;

							} else {
								appLog.error("Not able to click on " + inst_name);
							}
						}
					} else {
						appLog.error("Institutions Not Available : " + inst_name);
						return false;
					}
					i++;
				}
			}
		}else{
			if(clickOnAlreadyCreated_Lighting(environment, mode, TabName.InstituitonsTab, inst_name, 30)){
				appLog.info("Clicked on Institutions name : " + inst_name);
				return true;
			}else{
				appLog.error("Institutions Not Available : " + inst_name);
			}	
		}
		return false;
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param environment
	 * @param mode
	 * @param lp_name
	 * @return true/false
	 */
	public boolean clickOnCreatedLP(String environment,String mode,String lp_name) {
		if(mode.equalsIgnoreCase(Mode.Classic.toString())){
		int i =1;
		if (getSelectedOptionOfDropDown(driver, getViewDropdown(60), "View dropdown", "text")
				.equalsIgnoreCase("All Limited Partners")) {
			if (click(driver, getGoButton(60), "Go button", action.BOOLEAN)) {
				appLog.info("Clicked on Go button");
			} else {
				appLog.error("Go button not found");
			}
		} else {
			if (selectVisibleTextFromDropDown(driver, getViewDropdown(60), "View dropdown", "All Limited Partners")) {
				appLog.info("Select Limited Partners in View Dropdown");

			}

		}
		WebElement ele = isDisplayed(driver,
				FindElement(driver,
						"//*[@id='ext-gen12']/div/table/tbody/tr/td[4]/div/a/span[text()='" + lp_name + "']", "LP link",
						action.SCROLLANDBOOLEAN, 10),
				"visibility", 10, "");
		if (ele != null) {
			scrollDownThroughWebelement(driver, ele, "");
			if (click(driver, ele, lp_name + " name text", action.SCROLLANDBOOLEAN)) {
				appLog.info("Clicked on limited partner link");
				return true;
			} else {
				appLog.error("Not able to click on " + lp_name);
			}
		} else {
			while (true) {
				appLog.error("limited partner is not Displaying on "+i+ " Page: " + lp_name);
				if (click(driver, getNextImageonPage(10), "limited partner Page Next Button",
						action.SCROLLANDBOOLEAN)) {
					ThreadSleep(2000);
					appLog.info("Clicked on Next Button");
					ele = FindElement(driver,
							"//*[@id='ext-gen12']/div/table/tbody/tr/td[4]/div/a/span[text()='" + lp_name + "']", "LP link",
							action.SCROLLANDBOOLEAN, 10);
					if (ele != null) {
						if (click(driver, ele, lp_name, action.SCROLLANDBOOLEAN)) {
							appLog.info("Clicked on limited partner name : " + lp_name);
							return true;
							
						}
					}

					

				} else {
					appLog.error("limited partner Not Available : " + lp_name);
					return false;
				}
				i++;
			}
		}
		}else{
			if(clickOnAlreadyCreated_Lighting(environment, mode, TabName.LimitedPartner, lp_name, 30)){
				appLog.info("Clicked on limited partner name : " + lp_name);
				return true;
			}else{
				appLog.error("limited partner Not Available : " + lp_name);
			}	
		}
		return false;
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param environment
	 * @param mode
	 * @param tabName
	 * @param labelName
	 * @param labelValue
	 * @return true/false
	 */
	public boolean fieldValueVerificationOnInstitutionPage(String environment, String mode, TabName tabName,
			String labelName,String labelValue) {
		String finalLabelName;

		if(labelName.contains(excelLabel.Total_CoInvestment_Commitments.toString())) {
			labelName=LimitedPartnerPageFieldLabelText.Total_CoInvestment_Commitments.toString();
			labelValue=convertNumberIntoMillions(labelValue);

		}else if (labelName.contains(excelLabel.Total_Fund_Commitments.toString())) {
			labelName=LimitedPartnerPageFieldLabelText.Total_Fund_Commitments.toString();
			labelValue=convertNumberIntoMillions(labelValue);
		}/*else if (labelName.equalsIgnoreCase(excelLabel.Phone.toString()) || labelName.equalsIgnoreCase(excelLabel.Fax.toString())) {
			labelValue=changeNumberIntoUSFormat(labelValue);
		}*/

		if (labelName.contains("_")) {
			finalLabelName = labelName.replace("_", " ");
		} else {
			finalLabelName = labelName;
		}
		String xpath = "";
		WebElement ele = null;
		if (mode.equalsIgnoreCase(Mode.Classic.toString())) {
			if(finalLabelName.equalsIgnoreCase(excelLabel.Institution_Type.toString().replace("_", " "))) {

				xpath = "(//span[text()='" + finalLabelName + "']/../following-sibling::td/div)[1]";
			}else {
				xpath = "//td[text()='"+finalLabelName+"']/following-sibling::td/div";
			}

		} else {

			if (finalLabelName.contains("Record Type")) {
				xpath = "//span[@class='test-id__field-label'][text()='"+finalLabelName+"']/../following-sibling::div/span";
			} else {
				xpath = "//span[@class='test-id__field-label'][text()='" + finalLabelName
						+ "']/../following-sibling::div/span/span";
			}


		}

		if(finalLabelName.contains("Street") || finalLabelName.contains("City") || finalLabelName.contains("State") || finalLabelName.contains("Postal") || finalLabelName.contains("Zip") || finalLabelName.contains("Country")) {

			if(mode.equalsIgnoreCase(Mode.Lightning.toString())) {
				//	xpath="//span[text()='Address Information']/../../following-sibling::div";
				if(finalLabelName.contains("Legal Name")){
					xpath="("+xpath+")[2]";
				}else if(finalLabelName.contains("Other Street") || finalLabelName.contains("Other City") || finalLabelName.contains("Other State") || finalLabelName.contains("Other Zip") || finalLabelName.contains("Other Country") ||
						finalLabelName.contains("Shipping")) {
					xpath="(//span[text()='Address Information']/../../following-sibling::div/div/div/div/div)[2]";	
				}else{
					xpath="(//span[text()='Address Information']/../../following-sibling::div/div/div/div/div)[1]";
				}
			}else {
				if(finalLabelName.contains("Other Street") || finalLabelName.contains("Other City") || 
						finalLabelName.contains("Other State") || finalLabelName.contains("Other Zip") || finalLabelName.contains("Other Country") || 
						finalLabelName.contains("Shipping Street") || finalLabelName.contains("Shipping City") || finalLabelName.contains("Shipping State") || 
						finalLabelName.contains("Shipping Zip") || finalLabelName.contains("Shipping Country")) {
					xpath="(//h3[text()='Address Information']/../following-sibling::div[1]//td//tbody/tr[1]/td)[2]";	
				}else{
					xpath="(//h3[text()='Address Information']/../following-sibling::div[1]//td//tbody/tr[1]/td)[1]";
				}
			}
		}
		ele = isDisplayed(driver,
				FindElement(driver, xpath, finalLabelName + " label text in " + mode, action.SCROLLANDBOOLEAN, 60),
				"Visibility", 30, finalLabelName + " label text in " + mode);
		if (ele != null) {
			String aa = ele.getText().trim();
			appLog.info("Lable Value is: "+aa);

			if (labelName.equalsIgnoreCase(excelLabel.Phone.toString()) || labelName.equalsIgnoreCase(excelLabel.Fax.toString())) {
				if(aa.contains(labelValue) || aa.contains(changeNumberIntoUSFormat(labelValue)) ) {
					appLog.info(labelValue + " Value is matched successfully.");
					return true;
				}
			}else if(aa.contains(labelValue)) {
				appLog.info(labelValue + " Value is matched successfully.");
				return true;

			}else {
				appLog.info(labelValue + " Value is not matched. Expected: "+labelValue+" /t Actual : "+aa);
			}
		} else {
			appLog.error(finalLabelName + " Value is not visible so cannot matched  label Value "+labelValue);
		}
		return false;

	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param environment
	 * @param mode
	 * @param company_name
	 * @return true/false
	 */
	public boolean clickOnCreatedCompany(String environment,String mode,String company_name) {
		if(mode.equalsIgnoreCase(Mode.Classic.toString())){
		int i =1;
		if (getSelectedOptionOfDropDown(driver, getViewDropdown(60), "View dropdown", "text")
				.equalsIgnoreCase("All Companies")) {
			if (click(driver, getGoButton(60), "Go button", action.BOOLEAN)) {

			} else {
				appLog.error("Go button not found");
			}
		} else {
			if (selectVisibleTextFromDropDown(driver, getViewDropdown(60), "View dropdown", "All Companies")) {
			} else {
				appLog.error("All Companies not found in dropdown");
			}

		}
		WebElement ele = isDisplayed(driver,
				FindElement(driver, "//div[@class='x-panel-bwrap']//span[text()='" + company_name + "']/..",
						"Company link", action.SCROLLANDBOOLEAN, 20),
				"visibility", 20, "");
		if (ele != null) {
			scrollDownThroughWebelement(driver, ele, "");
			if (click(driver, ele, company_name + " name text", action.SCROLLANDBOOLEAN)) {
				appLog.info("Clicked on company link");
				return true;
			} else {
				appLog.error("Not able to click on " + company_name);
			}
		} else {
			while (true) {
				appLog.error("Company is not Displaying on "+i+ " Page: " + company_name);
				if (click(driver, getNextImageonPage(10), "Company Page Next Button",
						action.SCROLLANDBOOLEAN)) {
					ThreadSleep(2000);
					appLog.info("Clicked on Next Button");
					ele = FindElement(driver, "//div[@class='x-panel-bwrap']//span[text()='" + company_name + "']/..",
							"Institution link", action.SCROLLANDBOOLEAN, 20);
					if (ele != null) {
						if (click(driver, ele, company_name, action.SCROLLANDBOOLEAN)) {
							appLog.info("Clicked on Company name : " + company_name);
							return true;
							
						}
					}

					

				} else {
					appLog.error("Company Not Available : " + company_name);
					return false;
				}
				i++;
			}
	}
		}else{
			if(clickOnAlreadyCreated_Lighting(environment, mode, TabName.CompaniesTab, company_name, 30)){
				appLog.info("Clicked on Company name : " + company_name);
				return true;
			}else{
				appLog.error("Company Not Available : " + company_name);
			}	
		}
		return false;
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param environment
	 * @param mode
	 * @param tabName
	 * @param labelName
	 * @param labelValue
	 * @return true/false
	 */
	public boolean fieldValueVerificationOnCompanyPage(String environment, String mode, TabName tabName,
			String labelName,String labelValue) {
		String finalLabelName;
		if (labelName.contains("_")) {
			finalLabelName = labelName.replace("_", " ");
		} else {
			finalLabelName = labelName;
		}
		String xpath = "";
		WebElement ele = null;
		if (mode.equalsIgnoreCase(Mode.Classic.toString())) {
				xpath = "//td[text()='" + finalLabelName + "']/../td[2]/div";
				if (finalLabelName.contains("Record Type")) {
					xpath = "//td[text()='"+finalLabelName+"']/following-sibling::td/div";
				} else {
					xpath = "//td[text()='" + finalLabelName + "']/../td[2]/div";
				}
				
		} else {
			
			if (finalLabelName.contains("Record Type")) {
				xpath = "//span[@class='test-id__field-label'][text()='"+finalLabelName+"']/../following-sibling::div/span";
			} else {
				xpath = "//span[@class='test-id__field-label'][text()='" + finalLabelName
						+ "']/../following-sibling::div/span/span";
			}
		
			
		}
		ele = isDisplayed(driver,
				FindElement(driver, xpath, finalLabelName + " label text in " + mode, action.SCROLLANDBOOLEAN, 60),
				"Visibility", 30, finalLabelName + " label text in " + mode);
		if (ele != null) {
			String aa = ele.getText().trim();
			appLog.info("Lable Value is: "+aa);
			if(aa.contains(labelValue)) {
				appLog.info(labelValue + " Value is matched successfully.");
				return true;
				
			}else {
				appLog.info(labelValue + " Value is not matched. Expected: "+labelValue+" /t Actual : "+aa);
			}
		} else {
			appLog.error(finalLabelName + " Value is not visible so cannot matched  label Value "+labelValue);
		}
		return false;

	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param environment
	 * @param mode
	 * @param institutionName
	 * @param otherLabelFields
	 * @param otherLabelValues
	 * @return true/false
	 */
	public boolean createAccounts(String environment,String mode,String institutionName, String otherLabelFields,String otherLabelValues) {
		String labelNames[]=null;
		String labelValue[]=null;
		if(otherLabelFields!=null && otherLabelValues !=null) {
			labelNames= otherLabelFields.split(",");
			labelValue=otherLabelValues.split(",");
		}
		refresh(driver);
		ThreadSleep(3000);
		if(mode.equalsIgnoreCase(Mode.Lightning.toString())) {
			ThreadSleep(10000);
			if(clickUsingJavaScript(driver, getNewButton(environment, mode, 60), "new button")) {
				appLog.info("clicked on new button");
			}else {
				appLog.error("Not able to click on New Button so cannot create institution: " + institutionName);
				return false;
			}
		}else {
			if (click(driver, getNewButton(environment,mode,60), "New Button", action.SCROLLANDBOOLEAN)) {
				appLog.info("clicked on new button");
			} else {
				appLog.error("Not able to click on New Button so cannot create institution: " + institutionName);
				return false;
			}
		}
		if (sendKeys(driver, getAccountNameTextBox(environment,mode,30), institutionName, "leagl name text box",action.SCROLLANDBOOLEAN)) {
			appLog.info("passed data in text box: " + institutionName);
			if(labelNames!=null && labelValue!=null) {
				for(int i=0; i<labelNames.length; i++) {
					WebElement ele = getInstitutionPageTextBoxOrRichTextBoxWebElement(environment, mode, labelNames[i].trim(), 30);
					if(sendKeys(driver, ele, labelValue[i], labelNames[i]+" text box", action.SCROLLANDBOOLEAN)) {
						appLog.info("passed value "+labelValue[i]+" in "+labelNames[i]+" field");
					}else {
						appLog.error("Not able to pass value "+labelValue[i]+" in "+labelNames[i]+" field");
						BaseLib.sa.assertTrue(false, "Not able to pass value "+labelValue[i]+" in "+labelNames[i]+" field");
					}
				}

			}
			if (click(driver, getSaveButton(environment,mode,30), "save button", action.SCROLLANDBOOLEAN)) {
				appLog.info("clicked on save button");
				String str = getText(driver, getLegalNameLabelTextbox(environment,mode,institutionName,30), "legal Name Label Text",
						action.SCROLLANDBOOLEAN);
				if (str != null) {
					if (str.contains(institutionName)) {
						appLog.info(
								"created institution " + institutionName + " is verified successfully.");
						appLog.info(institutionName + " is created successfully.");

						if(labelNames!=null && labelValue!=null ) {
							for(int i=0; i<labelNames.length; i++) {

								if(labelNames[i].contains("Street") || labelNames[i].contains("City") || labelNames[i].contains("State") || labelNames[i].contains("Postal") || labelNames[i].contains("Country") || labelNames[i].contains("Zip")) {
									WebElement ele= null;
									String xpath="";
									if(mode.equalsIgnoreCase(Mode.Lightning.toString())) {
										xpath="//span[text()='Address Information']/../../following-sibling::div";
									}else {
										xpath="//h3[text()='Address Information']/../following-sibling::div[1]//td//tbody/tr[1]/td";
									}
									ele = isDisplayed(driver,
											FindElement(driver,xpath,"address label text in " + mode, action.SCROLLANDBOOLEAN, 10),
											"Visibility", 10,"address label text in " + mode);
									if (ele != null) {
										String aa = ele.getText().trim();
										appLog.info("Lable Value is: "+aa);
										if(aa.contains(labelValue[i])) {
											appLog.info(labelValue[i] + " Value is matched successfully.");

										}else {
											appLog.info("Address Label Value is not matched. Expected: "+labelValue[i]+" /t Actual : "+aa);
											BaseLib.sa.assertTrue(false, "Address Label Value is not matched. Expected: "+labelValue[i]+" /t Actual : "+aa);
										}
									} else {
										appLog.error("Address Label Value is not visible so cannot matched label Value "+labelValue[i]);
									}
								}else {
									if(!labelNames[i].contains(excelLabel.Phone.toString())) {
										if(FieldValueVerificationOnAllPages(environment, mode, null, labelNames[i].replace("_", " ").trim(),labelValue[i])){
											appLog.info(labelNames[i]+" label value "+labelValue[i]+" is matched successfully.");
										}else {
											appLog.info(labelNames[i]+" label value "+labelValue[i]+" is not matched successfully.");
											BaseLib.sa.assertTrue(false, labelNames[i]+" label value "+labelValue[i]+" is not matched.");
										}
									}
								}
							}
						}
						return true;
					} else {
						appLog.error(
								"Created institution " + institutionName + " is not matched with " + str);
					}
				} else {
					appLog.error("Created institution " + institutionName + " is not visible");
				}
			} else {
				appLog.error("Not able to click on save button so cannot create institution: "
						+ institutionName);
			}
		} else {
			appLog.error("Not able to pass data in legal name text box so cannot create institution: "
					+ institutionName);
		}
		
		return false;
	}
	
	
}
