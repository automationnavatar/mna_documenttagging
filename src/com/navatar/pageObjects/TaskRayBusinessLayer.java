package com.navatar.pageObjects;

import static com.navatar.generic.CommonLib.*;
import static com.navatar.generic.SmokeCommonVariables.Smoke_Project1Name;
import static com.navatar.generic.SmokeCommonVariables.Smoke_Task1Name;
import static com.navatar.generic.SmokeCommonVariables.Smoke_Task5Name;

import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.navatar.generic.BaseLib;
import com.navatar.generic.ExcelUtils;
import com.navatar.generic.EnumConstants.BoardFilters;
import com.navatar.generic.EnumConstants.Buttons;
import com.navatar.generic.EnumConstants.Mode;
import com.navatar.generic.EnumConstants.PageName;
import com.navatar.generic.EnumConstants.TaskRayProjectButtons;
import com.navatar.generic.EnumConstants.TaskType;
import com.navatar.generic.EnumConstants.YesNo;
import com.navatar.generic.EnumConstants.action;
import com.navatar.generic.EnumConstants.excelLabel;
import com.navatar.generic.EnumConstants.taskSubTab;
import com.relevantcodes.extentreports.LogStatus;

import com.relevantcodes.extentreports.LogStatus;

import static com.navatar.generic.CommonLib.*;
public class TaskRayBusinessLayer extends TaskRayPage {

	public TaskRayBusinessLayer(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	

	/**
	 * @author ANKIT JAISWAL
	 * @param environment
	 * @param mode
	 * @param timeOut
	 * @return true/false
	 */
	public boolean clickOnNewTaskButton(String environment,String mode,int timeOut) {
		if(click(driver, getNewTaskButton(timeOut), "new task button", action.SCROLLANDBOOLEAN)) {
			log(LogStatus.PASS,"clicked on new task button", YesNo.No);
			return true;
		}else {
			log(LogStatus.FAIL,"Not able to click on new task button",YesNo.Yes);
		}
		return false;

	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param environment
	 * @param mode
	 * @param taskSubTab
	 * @param timeOut
	 * @return true/false
	 */
	public boolean clickOnNewTaskTabs(String environment,String mode,taskSubTab taskSubTab,int timeOut) {
		if(click(driver, getNewTaskSubTab(taskSubTab, timeOut), "task sub tab "+taskSubTab.toString(), action.SCROLLANDBOOLEAN)) {
			log(LogStatus.INFO, "clicked on task sub tab", YesNo.No);
			return true;
		}else {
			log(LogStatus.ERROR, "Not able to click on task sub tab "+taskSubTab.toString(), YesNo.Yes);
		}
		return false;
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param environment
	 * @param mode
	 * @param taskName
	 * @param projectName
	 * @param status
	 * @param labelNameAndValue
	 * @param timeOut
	 * @return true/false
	 */
	public boolean createNewTask(String environment,String mode,String taskName,String projectName,String status,String startDate,String endDate,String variableName,Buttons saveButton,int timeOut) {
		boolean flag = false;
		if(clickOnNewTaskButton(environment, mode, timeOut)) {
			log(LogStatus.INFO, "clicked on new task button", YesNo.No);
			ThreadSleep(5000);
			switchToFrame(driver, 60,  getFrame(PageName.createNewTaskDetailsPopUpFrame, 60));
			if(sendKeys(driver, getTaskNameTextBox(timeOut), taskName, "task name text box", action.SCROLLANDBOOLEAN)) {
				log(LogStatus.INFO, "passed value in task text box : "+taskName, YesNo.No);
				if(clickOnLookUpAndSelectValueFromLookUpWindow_Classic(environment, mode, LookUpIcon.newTaskProject, projectName,projectName)) {
					log(LogStatus.INFO, "project name is selcted from look up window "+projectName, YesNo.No);
					if(switchToFrame(driver, timeOut, getFrame(PageName.TaskRayPage, timeOut))){
						switchToFrame(driver, 60,  getFrame(PageName.createNewTaskDetailsPopUpFrame, 60));
						log(LogStatus.INFO, "switch on task ray page frame ",YesNo.No);
						if(selectVisibleTextFromDropDown(driver, getStatusDropDownList(timeOut),"status drop down list",status)) {
							log(LogStatus.INFO, "select status "+status, YesNo.No);
							if(startDate!=null) {
								if(sendKeys(driver, getStartOrEndDateTextBox(excelLabel.Start_Date,60),startDate,"start date text box ", action.SCROLLANDBOOLEAN)) {
									log(LogStatus.INFO, "passed value in start date text box : "+startDate, YesNo.No);
									
								}else {
									log(LogStatus.ERROR, "Not able to pass value in start date text box "+startDate,YesNo.Yes);
									switchToDefaultContent(driver);
									return false;
								}
							}else if (endDate!=null) {
								if(sendKeys(driver, getStartOrEndDateTextBox(excelLabel.End_Date,60),endDate,"end date text box ", action.SCROLLANDBOOLEAN)) {
									log(LogStatus.INFO, "passed value in end date text box : "+startDate, YesNo.No);
								}else {
									log(LogStatus.ERROR, "Not able to pass value in end date text box "+startDate,YesNo.Yes);
									switchToDefaultContent(driver);
									return false;
								}
							}
							WebElement ele =getButton(PageName.NewProjectPopUp, saveButton, 20);
							if(click(driver, ele, saveButton.toString()+" button", action.SCROLLANDBOOLEAN)) {
								log(LogStatus.PASS, "clicked on "+saveButton+" button and task is created "+taskName, YesNo.No);
								switchToDefaultContent(driver);
								if(saveButton.toString().equalsIgnoreCase(Buttons.Save.toString())) {
									if(startDate!=null) {
										ThreadSleep(5000);
										switchToFrame(driver, 60, getFrame(PageName.TaskRayPage, 60));
										switchToFrame(driver, 60, getFrame(PageName.createNewTaskDetailsPopUpFrame, 60));
										if(getCreateTaskStartDateInViewMode(20)!=null) {
											String aa = getCreateTaskStartDateInViewMode(20).getText().trim();
											ExcelUtils.writeData(testCasesFilePath, aa,"Task", excelLabel.Variable_Name, variableName,excelLabel.Start_Date);
										}
										switchToDefaultContent(driver);
									}else if (endDate!=null) {
										ThreadSleep(5000);
										switchToFrame(driver, 60, getFrame(PageName.TaskRayPage, 60));
										switchToFrame(driver, 60, getFrame(PageName.createNewTaskDetailsPopUpFrame, 60));
										if(getCreateTaskEndDateInViewMode(20)!=null) {
											String aa = getCreateTaskEndDateInViewMode(20).getText().trim();
											ExcelUtils.writeData(testCasesFilePath, aa,"Task", excelLabel.Variable_Name, variableName,excelLabel.End_Date);
										}
										switchToDefaultContent(driver);
									}
									clickOnCreateTaskCrossIcon();
								}
								flag= true;
							}else {
								log(LogStatus.ERROR, "Not able to click on "+saveButton+" button so cannot create task "+taskName, YesNo.Yes);
							}
							switchToDefaultContent(driver);
						}else {
							log(LogStatus.ERROR, "Not able to select status from drop down list "+status, YesNo.No);
						}
					}else {
						log(LogStatus.ERROR, "Not able to switch in frame in task ray page after select project name so cannot create task "+taskName,YesNo.Yes);
					}
				}else {
					log(LogStatus.ERROR, "Not able to select project name from look up window so cannot select project name "+projectName+" so cannot create task "+taskName, YesNo.Yes);
				}
			}else {
				log(LogStatus.ERROR, "Not able to pass value in task text box "+taskName+" so cannot create task",YesNo.Yes);
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on new task button so cannot create new task", YesNo.Yes);
		}
		switchToDefaultContent(driver);
		return flag;
		
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param environment
	 * @param mode
	 * @param taskRayProjectButtons
	 * @param timeOut
	 * @return webelement or null
	 */
	public WebElement getTaskRayProjectsButton(String environment,String mode,TaskRayProjectButtons taskRayProjectButtons,int timeOut) {
		String xpath="";
		WebElement ele=null;
		if(TaskRayProjectButtons.AddPlus.toString().equalsIgnoreCase(taskRayProjectButtons.toString())) {
			xpath = "(//span[text()='PROJECTS']/..//button)[1]";
		}else if(TaskRayProjectButtons.Search.toString().equalsIgnoreCase(taskRayProjectButtons.toString())) {
			xpath = "(//span[text()='PROJECTS']/..//button)[2]";
		}else {
			xpath = "(//span[text()='PROJECTS']/..//button)[3]";	
		}
		ele = FindElement(driver, xpath, taskRayProjectButtons.toString(), action.SCROLLANDBOOLEAN, timeOut);
		ele =isDisplayed(driver, ele, "Visibility", timeOut, taskRayProjectButtons.toString());	
		if (ele!=null) {
			appLog.info("Element  Found "+taskRayProjectButtons.toString());
		} else {
			appLog.error("Element not Found "+taskRayProjectButtons.toString());
		}
		
		return ele;
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param environment
	 * @param mode
	 * @param addPlusIconDropDownList
	 * @param timeOut
	 * @return webelement or null
	 */
	public WebElement getDropDownListOnAddPlusIconForProjects(String environment,String mode,AddPlusIconDropDownList addPlusIconDropDownList,int timeOut) {
		String xpath="";
		WebElement ele=null;
		String plusOption = addPlusIconDropDownList.toString().replace("_", " ");
		appLog.info("Going for  "+plusOption);
		if(AddPlusIconDropDownList.New_Project.toString().equalsIgnoreCase(addPlusIconDropDownList.toString())) {
			xpath = "//ul//span[text()='"+plusOption+"']";
		}
		ele = FindElement(driver, xpath, addPlusIconDropDownList.toString(), action.SCROLLANDBOOLEAN, timeOut);
		ele =isDisplayed(driver, ele, "Visibility", timeOut, addPlusIconDropDownList.toString());	
		if (ele!=null) {
			appLog.info("Element  Found "+addPlusIconDropDownList.toString());
			
		} else {
			appLog.error("Element not Found "+addPlusIconDropDownList.toString());
		}
		
		return ele;
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param environment
	 * @param mode
	 * @param labelFieldTextBox
	 * @param timeOut
	 * @return webelement or null
	 */
	public WebElement getLabelTextBox(String environment,String mode,String labelFieldTextBox,int timeOut) {
	
		WebElement ele=null;
		String labelTextBox = labelFieldTextBox.replace("_", " ");
		String xpath="(//label[text()='"+labelTextBox+"']/../following-sibling::td//input)[1]";
		ele = FindElement(driver, xpath, labelTextBox, action.SCROLLANDBOOLEAN, timeOut);
		ele =isDisplayed(driver, ele, "Visibility", timeOut, labelTextBox);	
		return ele;
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param environment
	 * @param mode
	 * @param labelFieldTextBox
	 * @param timeOut
	 * @return webelement or null
	 */
	public WebElement getLabelTextBoxForLookUpWindow(String environment,String mode,String labelFieldTextBox,int timeOut) {
		
		WebElement ele=null;
		String labelTextBox = labelFieldTextBox.replace("_", " ");
		String xpath="//label[text()='"+labelFieldTextBox+"']/../following-sibling::td//span[@class='lookupInput']/input";
		ele = FindElement(driver, xpath, labelTextBox, action.SCROLLANDBOOLEAN, timeOut);
		ele =isDisplayed(driver, ele, "Visibility", timeOut, labelTextBox);	
		return ele;
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param environment
	 * @param mode
	 * @param labelFieldSelect
	 * @param timeOut
	 * @return webelement or null
	 */
	public WebElement getLabelSelectElement(String environment,String mode,String labelFieldSelect,int timeOut) {
		
		WebElement ele=null;
		String selectLabel = labelFieldSelect.replace("_", " ");
		String xpath="//label[text()='"+selectLabel+"']/../following-sibling::td//select";
		ele = FindElement(driver, xpath, selectLabel, action.SCROLLANDBOOLEAN, timeOut);
		ele =isDisplayed(driver, ele, "Visibility", timeOut, selectLabel);	
		return ele;
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param environment
	 * @param mode
	 * @param fundName
	 * @return true/false
	 */
	public boolean selectFundNameonNewProjectPopUp(String environment, String mode, String fundName) {
		boolean flag = false;
		if(click(driver, getFundLookUpIconOnNewProjectPopUp(60), "fund look up icon", action.BOOLEAN)) {
			log(LogStatus.INFO, "clicked on fund name look up icon", YesNo.No);
			appLog.info("clicked on fund look up icon");
			if(selectValueFromLookUpWindow(fundName)) {
				log(LogStatus.INFO, fundName+" fund Name is select successfully", YesNo.No);
				appLog.info(fundName+" fund Name is select successfully");
				flag=true;
				switchToDefaultContent(driver);
				switchToFrame(driver, 10, getFrame(PageName.TaskRayPage, 10));
				switchToFrame(driver, 10, getFrame(PageName.ProjectDetailsPoPUp, 10));
			}else {
				log(LogStatus.ERROR, "Not able to select fund Name "+fundName+" from look up pop up", YesNo.Yes);
				appLog.error("Not able to select fund Name "+fundName+" from look up pop up");
			}
		}else {
			log(LogStatus.ERROR, "Not able to click on fund look up icon so cannot select fund name: "+fundName, YesNo.Yes);
			appLog.error("Not able to click on fund look up icon so cannot select fund name: "+fundName);
		}
		return flag;
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param environment
	 * @param mode
	 * @param projectName
	 * @param projectType
	 * @param owner
	 * @param pipeLineName
	 * @param fieldLabelwithValues
	 * @param button
	 * @param timeOut
	 * @return true/false
	 */
	public boolean createProject(String environment,String mode,String projectName,String owner,String dealName,String[][] fieldLabelwithValues,Buttons button,int timeOut) {
		boolean flag = false;
		String xpath="";
		WebElement ele=null;
		ele = getTaskRayProjectsButton(environment, mode, TaskRayProjectButtons.AddPlus, timeOut);
		if(click(driver, ele, TaskRayProjectButtons.AddPlus.toString(), action.SCROLLANDBOOLEAN)) {
			appLog.info("Clicked on "+TaskRayProjectButtons.AddPlus.toString());
			ThreadSleep(2000);
			ele = getDropDownListOnAddPlusIconForProjects(environment, mode, AddPlusIconDropDownList.New_Project, timeOut);
			if(click(driver, ele, AddPlusIconDropDownList.New_Project.toString(),action.SCROLLANDBOOLEAN)) {
				appLog.info("Clicked on "+AddPlusIconDropDownList.New_Project.toString());
				ThreadSleep(2000);
				//	switchToDefaultContent(driver);
				switchToFrame(driver, timeOut, getFrame(PageName.ProjectDetailsPoPUp, timeOut));
				ele = getLabelTextBox(environment,mode,ProjectLabel.Project_Name.toString(),timeOut) ;
				if (sendKeys(driver, ele, projectName, "projectName : "+projectName, action.BOOLEAN)) {
					appLog.error("Enter value in Project Name text box : "+projectName);
					ThreadSleep(1000);	
					ele = getLabelTextBoxForLookUpWindow(environment,mode,ProjectLabel.Deal.toString(),timeOut) ;
					if (sendKeys(driver, ele, dealName, ProjectLabel.Deal.toString(), action.BOOLEAN)) {
						appLog.info("Enter value in "+ProjectLabel.Deal.toString()  +" : "+dealName);
						flag=true;
					} else {
							appLog.error("Not Able to Enter value in "+ProjectLabel.Deal.toString()  +" : "+dealName);
					}
				} else {
					appLog.error("Not able to enter vale to Project Name in text box : "+projectName);
				}


			}else {
				appLog.error("Not Able to click on "+AddPlusIconDropDownList.New_Project.toString());
			}

		}else {
			appLog.error("Not Able to click on "+TaskRayProjectButtons.AddPlus.toString());
		}
		if (flag) {
			flag=false;
			if (fieldLabelwithValues!=null) {
				otherProjectLabel(environment, mode, fieldLabelwithValues, timeOut);
			}
			ele =getButton(PageName.NewProjectPopUp, button, timeOut);
			ThreadSleep(3000);
			if(click(driver, ele, button.toString(),action.SCROLLANDBOOLEAN)) {
				appLog.info("Clicked on "+button.toString());
				flag=true;
				ThreadSleep(2000);
			}else {
				appLog.error("Not Able to click on "+button.toString());
			}
		}
		return flag;
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param environment
	 * @param mode
	 * @param labelWithValues
	 * @param timeOut
	 */
	public void otherProjectLabel(String environment,String mode,String[][] labelWithValues,int timeOut) {
		String label="";
		String value="";
		WebElement ele;
		for (String[] labelwithvalue : labelWithValues) {
			ThreadSleep(500);
			label=labelwithvalue[0].replace("_", " ");
			value=labelwithvalue[1];
			if (label.equalsIgnoreCase(ProjectLabel.Fund.toString())) {
			
				if (selectItemFromLoookUpWindow(environment, mode, XpathForFundLookUpIconOnNewProjectPopUp, label, value, timeOut)) {
					appLog.info("selected "+label+" From Look Up Icon: "+ value);
					switchToDefaultContent(driver);
					switchToFrame(driver, 10, getFrame(PageName.TaskRayPage, 10));
					switchToFrame(driver, 10, getFrame(PageName.ProjectDetailsPoPUp, 10));
				} else {
					appLog.error("Not Able to select "+label+" From Look Up Icon: "+ value);
					BaseLib.sa.assertTrue(false, "Not Able to select "+label+" From Look Up Icon: "+ value);
				}
				
			}
		}
		
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param boardFilters
	 * @return true/false
	 */
	public boolean clickOnBoardFilters(BoardFilters boardFilters) {
		if(click(driver, getBoardFiltersOptions(boardFilters, 60),"board filters name ", action.SCROLLANDBOOLEAN)) {
			log(LogStatus.PASS, "clicked on board filters name :"+boardFilters.toString(), YesNo.No);
			return true;
		}else {
			log(LogStatus.FAIL, "Not able to click on board filter name "+boardFilters.toString(), YesNo.Yes);
		}
		return false;
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param projectName
	 * @return true/false
	 */
	public boolean clickOnCreatedProjectInTaskRay(String projectName) {
		if(click(driver, getProjects(projectName, 60),"project name "+projectName, action.SCROLLANDBOOLEAN)) {
			log(LogStatus.PASS, "clicked on project name :"+projectName, YesNo.No);
			return true;
		}else {
			log(LogStatus.FAIL, "Not able to click on project name "+projectName, YesNo.Yes);
		}
		return false;
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param projectName
	 * @return true/false
	 */
	public boolean searchSingleProject(String projectName) {
		if(sendKeys(driver, getSearchAProjectTextBox(20), projectName, "search project text box", action.SCROLLANDBOOLEAN)) {
			log(LogStatus.PASS, "passed value in search a project text box", YesNo.No);
			ThreadSleep(1000);
			if(click(driver, getSelectProjectFromDropDownInSearchAProject(projectName, 20), "project name "+projectName+" xpath", action.SCROLLANDBOOLEAN)) {
				log(LogStatus.PASS, "clicked on search result project name : "+projectName, YesNo.No);
				return true;
			}else {
				log(LogStatus.FAIL, "Not able to select search result project name from serach a project "+projectName,YesNo.Yes);
			}
		}else {
			log(LogStatus.FAIL,"Not able to pass value in search a project text box "+projectName, YesNo.Yes);
		}
		return false;
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param taskType
	 * @param taskName
	 * @param projectName
	 * @return true/false
	 */
	public boolean docubleClickOnCreatedTask(TaskType taskType,String taskName,String projectName) {
		WebElement ele=null;
		String xpath1="",xpath2="",xpath3="";
		String tasktypeName=taskType.toString().replace("_", " ");
		xpath1="//h5/span[text()='"+tasktypeName+"']/ancestor::div[@class='columnboard-header']/following-sibling::div//span[text()='"+projectName+"']/../../../..";
		xpath2="/preceding-sibling::div//span[text()='"+taskName+"']/../../..";
		xpath3=xpath1+xpath2;
		ele=FindElement(driver, xpath3,"task Name "+taskName+" xpath", action.SCROLLANDBOOLEAN,30);
		if(ele!=null) {
			log(LogStatus.PASS, "Task Name is found "+taskName, YesNo.No);
			if(doubleClick(driver, ele)) {
				log(LogStatus.PASS, "CLicked on created task Name "+taskName+" under "+tasktypeName, YesNo.No);
				return true;
			}else {
				log(LogStatus.FAIL, "Not able to click on created task name "+taskName+" under "+tasktypeName, YesNo.Yes);
			}
		}else {
			log(LogStatus.FAIL, "Task Name "+taskName+" is not found in task ray under "+tasktypeName, YesNo.Yes);
		}
		return false;
		
	}
	
	
	/**
	 * @author ANKIT JAISWAL
	 * @param taskType
	 * @param taskName
	 * @param Documentcount
	 * @return true/false
	 */
	public boolean verifyDocumentCountOnCreatedTask(TaskType taskType,String taskName,String Documentcount) {
		switchToFrame(driver, 60, getFrame(PageName.TaskRayPage, 60));
		WebElement ele= getTaggedDocumentCountOnCreatedTask(taskType,taskName,30);
		if(ele!=null) {
			String actualCount=ele.getText().trim();
			if(Documentcount.equalsIgnoreCase(actualCount)) {
				log(LogStatus.PASS, "Document Count "+Documentcount+" is matched on "+taskType.toString(), YesNo.No);
				switchToDefaultContent(driver);
				return true;

			}else {
				log(LogStatus.FAIL, "Document Count "+Documentcount+" is not matched on "+taskType.toString(), YesNo.Yes);
			}
		}else {
			log(LogStatus.FAIL, "Not able to found "+taskType.toString()+" document count", YesNo.Yes);
		}
		switchToDefaultContent(driver);
		return false;
		
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @return true/false
	 */
	public boolean clickOnCreateTaskCrossIcon() {
		switchToFrame(driver, 10, getFrame(PageName.TaskRayPage, 60));
		List<WebElement> ele = getCreateTaskPopUpCrossIcon();
		if(!ele.isEmpty()) {
			for (int i = 0; i < ele.size(); i++) {
				if(click(driver, ele.get(i), "created task pop up cross icon", action.SCROLLANDBOOLEAN)) {
					log(LogStatus.PASS, "clicked on created task cross icon", YesNo.No);
					switchToDefaultContent(driver);
					return true;
				}else {
					if(i==ele.size()-1) {
						log(LogStatus.FAIL, "Not able to click on task pop up cross icon so cannnot close created task popup", YesNo.Yes);
					}
				}
			}
		}else {
			log(LogStatus.FAIL, "created task cross icon is not found so cannot click on it", YesNo.Yes);
		}
		switchToDefaultContent(driver);
		return false;
		
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param projectName
	 * @param taskType
	 * @param taskName
	 * @param ownerName
	 * @return true/false
	 */
	public boolean changeOwnerOfCreatedTask(String projectName,TaskType taskType, String taskName, String ownerName) {
		boolean flag = false;
		if(switchToFrame(driver, 60, getFrame(PageName.TaskRayPage, 60))) {
			log(LogStatus.PASS, "switched in taskRay page frame ", YesNo.No);
			if(docubleClickOnCreatedTask(TaskType.Completed, taskName,projectName)) {
				log(LogStatus.PASS, "clicked on created task "+Smoke_Task5Name, YesNo.No);
				ThreadSleep(5000);
				if(switchToFrame(driver, 60, getFrame(PageName.createNewTaskDetailsPopUpFrame, 60))) {
					log(LogStatus.PASS, "switched in created task details popup frame ", YesNo.No);
					if(sendKeys(driver, getCreatedTaskOwnerNameTextBox(10), ownerName, "owner name text box", action.SCROLLANDBOOLEAN)) {
						log(LogStatus.PASS, "Passed value in owner name text box : "+ownerName, YesNo.No);
						WebElement ele =getButton(PageName.NewProjectPopUp, Buttons.SaveClose, 20);
						ThreadSleep(3000);
						if(click(driver, ele, Buttons.SaveClose.toString(),action.SCROLLANDBOOLEAN)) {
							appLog.info("Clicked on "+Buttons.SaveClose.toString());
							flag=true;
							ThreadSleep(2000);
						}else {
							appLog.error("Not Able to click on "+Buttons.SaveClose.toString());
						}
					}else {
						log(LogStatus.FAIL, "Not able to pass value in owner name text box "+ownerName, YesNo.Yes);
					}
				}else {
					log(LogStatus.FAIL, "Not able to switch task details popup frame so cannot change owner in "+taskName, YesNo.Yes);
				}
			}else {
				log(LogStatus.FAIL, "Not able to click on created task "+Smoke_Task5Name, YesNo.Yes);
				sa.assertTrue(false, "Not able to click on created task "+Smoke_Task5Name);
			}
		}else {
			log(LogStatus.FAIL, "Not able to switch frame in taskRay page so cannot change owner in : "+taskName, YesNo.Yes);
		}
		switchToDefaultContent(driver);
		return flag;
		
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param projectName
	 * @param taskType
	 * @param taskName
	 * @param updatedTaskName
	 * @param updateOwnerName
	 * @param startDate
	 * @param endDate
	 * @return true/false
	 */
	public boolean updatecreatedTaskDeatils(String projectName,TaskType taskType, String taskName,String updatedTaskName,String updateOwnerName,String status,String startDate,String endDate,Buttons saveButton) {
		if(switchToFrame(driver, 60, getFrame(PageName.TaskRayPage, 60))) {
			log(LogStatus.PASS, "switched in taskRay page frame ", YesNo.No);
			if(docubleClickOnCreatedTask(taskType, taskName,projectName)) {
				log(LogStatus.PASS, "clicked on created task "+Smoke_Task5Name, YesNo.No);
				ThreadSleep(5000);
				if(switchToFrame(driver, 60, getFrame(PageName.createNewTaskDetailsPopUpFrame, 60))) {
					log(LogStatus.PASS, "switched in created task details popup frame ", YesNo.No);
					if(click(driver, getCreatedTaskEditButton(60), "edit button", action.SCROLLANDBOOLEAN)) {
						log(LogStatus.PASS, "clicked on button of task "+taskName, YesNo.No);
						
						if(updatedTaskName!=null) {
							if(sendKeys(driver, getTaskNameTextBox(60), updatedTaskName, "task name text box", action.SCROLLANDBOOLEAN)) {
								log(LogStatus.INFO, "passed value in task text box : "+updatedTaskName, YesNo.No);
							}else {
								log(LogStatus.ERROR, "Not able to pass value in task text box "+updatedTaskName+" so cannot update task",YesNo.Yes);
								switchToDefaultContent(driver);
								return false;
							}
						}
						if(updateOwnerName!=null) {
							if(sendKeys(driver, getCreatedTaskOwnerNameTextBox(10), updateOwnerName, "owner name text box", action.SCROLLANDBOOLEAN)) {
								log(LogStatus.PASS, "Passed value in owner name text box : "+updateOwnerName, YesNo.No);
							
							}else {
								log(LogStatus.FAIL, "Not able to pass value in owner name text box "+updateOwnerName, YesNo.Yes);
								switchToDefaultContent(driver);
								return false;
							}
						}
						if(startDate!=null) {
							if(sendKeys(driver, getStartOrEndDateTextBox(excelLabel.Start_Date,60),startDate,"start date text box ", action.SCROLLANDBOOLEAN)) {
								log(LogStatus.INFO, "passed value in start date text box : "+startDate, YesNo.No);
							}else {
								log(LogStatus.ERROR, "Not able to pass value in start date text box "+startDate,YesNo.Yes);
								switchToDefaultContent(driver);
								return false;
							}
						}
						if(status!=null) {
							if(selectVisibleTextFromDropDown(driver, getStatusDropDownList(30), "status drop down list", status)) {
								log(LogStatus.INFO, "Select value from status drop downlist : "+status, YesNo.No);
							}else {
								log(LogStatus.ERROR, "Not able to select value in status dropdown list"+status,YesNo.Yes);
								switchToDefaultContent(driver);
								return false;
							}
						}
						if(endDate!=null) {
							if(sendKeys(driver, getStartOrEndDateTextBox(excelLabel.End_Date,60),endDate,"end date text box ", action.SCROLLANDBOOLEAN)) {
								log(LogStatus.INFO, "passed value in end date text box : "+startDate, YesNo.No);
							}else {
								log(LogStatus.ERROR, "Not able to pass value in end date text box "+startDate,YesNo.Yes);
								switchToDefaultContent(driver);
								return false;
							}
						}
						WebElement ele =getButton(PageName.NewProjectPopUp, saveButton, 20);
						ThreadSleep(3000);
						if(click(driver, ele, saveButton.toString(),action.SCROLLANDBOOLEAN)) {
							appLog.info("Clicked on "+saveButton.toString());
							switchToDefaultContent(driver);
							return true;
						}else {
							appLog.error("Not Able to click on "+saveButton.toString());
						}
						
					}else {
						log(LogStatus.FAIL, "Not able to click on created task "+taskName+" edit button so cannot update details", YesNo.Yes);
						switchToDefaultContent(driver);
					}
				}else {
					log(LogStatus.FAIL, "Not able to switch task details popup frame so cannot change owner in "+taskName, YesNo.Yes);
					switchToDefaultContent(driver);
				}
			}else {
				log(LogStatus.FAIL, "Not able to click on created task "+taskName, YesNo.Yes);
				switchToDefaultContent(driver);
			}
		}else {
			log(LogStatus.FAIL, "Not able to switch frame in taskRay page so cannot change owner in : "+taskName, YesNo.Yes);
		}
		return false;
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param environment
	 * @param mode
	 * @param projectName
	 * @param updateProjectName
	 * @return true/false
	 */
	public boolean updateCreatedProjectName(String environment,String mode,String projectName, String updateProjectName) {
		boolean flag = false;
		if(switchToFrame(driver, 60, getFrame(PageName.TaskRayPage, 60))) {
			log(LogStatus.PASS, "switched in taskRay page frame ", YesNo.No);
			if(clickOnCreatedProjectInTaskRay(projectName)) {
				log(LogStatus.PASS, "clicked on project : "+projectName, YesNo.No);
				ThreadSleep(500);
				if(click(driver, getCreatedProjectMoreActions(20), "more actions link", action.SCROLLANDBOOLEAN)) {
					log(LogStatus.PASS, "Clicked on project More Actions : "+projectName, YesNo.No);
					ThreadSleep(1000);
					WebElement ele = FindElement(driver, "//div[@id='tether-destination']//ul/li//span[text()='Edit Project']", "edit project", action.SCROLLANDBOOLEAN,20);
					if(ele!=null) {
						if(click(driver, ele, "edit project link", action.BOOLEAN)) {
							log(LogStatus.PASS, "Clicked on edit project link", YesNo.No);
							ThreadSleep(5000);
							if(switchToFrame(driver, 30, getFrame(PageName.ProjectDetailsPoPUp, 60))) {
								if(click(driver, getCreatedProjectPopUpEditBtn(30), "edit button", action.SCROLLANDBOOLEAN)) {
									log(LogStatus.PASS, "clicked on edit button", YesNo.No);
									ThreadSleep(3000);
									ele = getLabelTextBox(environment,mode,ProjectLabel.Project_Name.toString(),60);
									if(sendKeys(driver, ele, updateProjectName, "project name text box ", action.SCROLLANDBOOLEAN)) {
										log(LogStatus.PASS, "passed value project text box : "+updateProjectName, YesNo.No);
										ele =getButton(PageName.NewProjectPopUp, Buttons.SaveClose,30);
										ThreadSleep(3000);
										if(click(driver, ele,"button save close button",action.SCROLLANDBOOLEAN)) {
											log(LogStatus.PASS, "Clicked on save close button", YesNo.No);
											flag=true;
										}else {
											log(LogStatus.FAIL, "Not able to click on save close button so cannot update project : "+updateProjectName, YesNo.Yes);
										}
									}else {
										log(LogStatus.FAIL, "Not able to enter value in project text box : "+updateProjectName, YesNo.Yes);
									}
								}else {
									log(LogStatus.FAIL, "Not able to click on edit button of project  : "+projectName, YesNo.Yes);
								}
							}else {
								log(LogStatus.FAIL, "Not able to switch project details popUp frame so cannot update project name "+updateProjectName, YesNo.Yes);
							}
						}else {
							log(LogStatus.FAIL, "Not able to click on edit project so cannot update project name : "+updateProjectName, YesNo.Yes);
						}
					}else {
						log(LogStatus.FAIL, "Not able to click on edit project so cannot update project name : "+updateProjectName, YesNo.Yes);
					}
					
				}else {
					log(LogStatus.FAIL, "Not able to click on more action of project : "+projectName+" so cannot update project name "+updateProjectName, YesNo.Yes);
				}
			}else {
				log(LogStatus.FAIL, "Not able to click on created project : "+projectName+" so cannot click on more actions", YesNo.Yes);
			}
			
		}else {
			log(LogStatus.FAIL, "Not able to switch task Ray frame so cannot update project Name :"+projectName, YesNo.Yes);
		}
		switchToDefaultContent(driver);
		return flag;
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param projectName
	 * @param pipeLineName
	 * @param fundName
	 * @return true/false
	 */
	public boolean updatePipeLineNameOrFundName(String projectName,String pipeLineName,String fundName) {
		switchToFrame(driver, 60, getFrame(PageName.TaskRayPage, 60));
		if(clickOnBoardFilters(BoardFilters.My_Projects)) {
			log(LogStatus.PASS, "CLicked on my project side menu", YesNo.No);
			if(clickOnCreatedProjectInTaskRay(projectName)) {
				log(LogStatus.PASS, "clicked on created project name "+projectName, YesNo.No);
				if(click(driver,getCreatedProjectMoreActions(20), "more actions link", action.SCROLLANDBOOLEAN)) {
					log(LogStatus.PASS, "Clicked on project More Actions : "+projectName, YesNo.No);
					ThreadSleep(1000);
					if(click(driver, getCreatedProjectEditButton(30), "edit project link", action.BOOLEAN)) {
						log(LogStatus.PASS, "Clicked on edit project link", YesNo.No);
						ThreadSleep(5000);
						if(switchToFrame(driver, 30, getFrame(PageName.ProjectDetailsPoPUp, 60))) {
							if(click(driver, getCreatedProjectPopUpEditBtn(30), "edit button", action.SCROLLANDBOOLEAN)) {
								log(LogStatus.PASS, "clicked on edit button", YesNo.No);
								ThreadSleep(3000);
								if(sendKeys(driver, getPipeLineTextBox(30),pipeLineName, "pipeline text box ", action.SCROLLANDBOOLEAN)) {
									log(LogStatus.PASS, "pipeline text box is : "+pipeLineName, YesNo.No);
								}else {
									log(LogStatus.FAIL, "Not able to enter value in pipeline text box", YesNo.Yes);
									switchToDefaultContent(driver);
									return false;
								}
								if(sendKeys(driver,getFundTextBox(30),fundName, "fund text box ", action.SCROLLANDBOOLEAN)) {
									log(LogStatus.PASS, "passed value in fund text box: "+fundName, YesNo.No);
								}else {
									log(LogStatus.FAIL, "Not able to enter value in fund name text box : "+fundName, YesNo.Yes);
									switchToDefaultContent(driver);
									return false;

								}
								if(click(driver,getButton(PageName.NewProjectPopUp, Buttons.SaveClose,30),"button save close button",action.SCROLLANDBOOLEAN)) {
									log(LogStatus.PASS, "Clicked on save close button", YesNo.No);
									switchToDefaultContent(driver);
									return true;
									
								}else {
									log(LogStatus.FAIL, "Not able to click on save close button so cannot update project : "+projectName, YesNo.Yes);

								}
							}else {
								log(LogStatus.FAIL, "Not able to click on edit button of project  : "+projectName, YesNo.Yes);

							}
						}else {
							log(LogStatus.FAIL, "Not able to switch project details popUp frame so cannot edit project name "+projectName, YesNo.Yes);

						}
					}else {
						log(LogStatus.FAIL, "Not able to click on edit project : "+projectName, YesNo.Yes);

					}
					
				}else {
					log(LogStatus.FAIL, "Not able to click on more action of project : "+projectName, YesNo.Yes);

				}
			}else {
				log(LogStatus.FAIL, "Not able to click on created project "+projectName, YesNo.Yes);

			}
		}else {
			log(LogStatus.FAIL, "Not able to click on my projects side Tab so cannot update task details and project name ", YesNo.Yes);

		}
		switchToDefaultContent(driver);
		return false;
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param ownerName
	 * @param tasktypeName
	 * @param projectName
	 * @param taskName
	 * @return true/false
	 */
	public boolean dragNDropOwnerNameInCreateTask(String ownerName,TaskType tasktypeName,String projectName, String taskName) {
		Boolean flag = false;
		switchToFrame(driver, 30, getFrame(PageName.TaskRayPage, 60));
		String xpath="//span[text()='"+projectName+"']/../../../../preceding-sibling::div//span[text()='"+taskName+"']/../../../../div[1]/div/div";
		String desXpath=xpath;
		if(getOwnerName(ownerName, 30)!=null) {
			WebElement ele=FindElement(driver, desXpath,"task Name "+taskName+" xpath", action.SCROLLANDBOOLEAN,30);
			if(ele!=null) {
				//HTML 5 Drag and Drop.....
				final String java_script =
						"var src=arguments[0],tgt=arguments[1];var dataTransfer={dropEffe" +
								"ct:'',effectAllowed:'all',files:[],items:{},types:[],setData:fun" +
								"ction(format,data){this.items[format]=data;this.types.append(for" +
								"mat);},getData:function(format){return this.items[format];},clea" +
								"rData:function(format){}};var emit=function(event,target){var ev" +
								"t=document.createEvent('Event');evt.initEvent(event,true,false);" +
								"evt.dataTransfer=dataTransfer;target.dispatchEvent(evt);};emit('" +
								"dragstart',src);emit('dragenter',tgt);emit('dragover',tgt);emit(" +
								"'drop',tgt);emit('dragend',src);";
				((JavascriptExecutor)driver).executeScript(java_script, getOwnerName(ownerName, 30), ele);
				log(LogStatus.PASS, "Successfully Drag and drop owner name "+ownerName+" on task "+taskName, YesNo.No);
				System.err.println("Drag Success");
				flag= true;
			}else {
				log(LogStatus.FAIL, "Task Name is not present on created task panel so cannot drag and drop owner name in task "+taskName, YesNo.Yes);
			}
		}else {
			log(LogStatus.FAIL, "Owner Name is not present on right side panel so cannot drag and drop in task "+taskName, YesNo.Yes);
		}
		switchToDefaultContent(driver);
		return flag;
	}
}
