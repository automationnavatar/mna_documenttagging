package com.navatar.pageObjects;

public class CssPath {
		
	public static String cssPathForSearchIconContentGridCssPath ="a[class='icon_btn_search'][onclick='searchTextOnBox();']";
	public static String cssPathForSearchBtnOnSearchResultPopU ="div.formbox.contacts_n_name_div a[title=Search]";
	public static String cssPathForSearchBtnOnSearchResultPopUp1="div.formbox.contacts_n_name_div a.icon_btn_search";
	
	public static String cssPathForDealRoomSearchBtnOnContactPage ="a[title=Search]";
	public static String cssPathForNextButton ="#btnNext";
	public static String cssPathForBulkDownloadConfirmationYesButton ="input[title=Yes]";
	
	//  Module 3
	
	public static String cssPathForSearchBtnOnRecentDocumentAtDealPage ="a[title=Search]";
	public static String cssPathForSearchCloseIconOnRecentDocumentAtHomePage ="div.contacts_n_name_div a[title=Search]";
	public static String cssPathForSearchIcon ="a#btn-searchBWFR";
	public static String cssPathForManageEmailsConfirmationYesButton ="div[class*=Activity] a[title=Yes]";
	public static String cssPathForContactsInGridInManageEmails ="span[id^=grid][id*=cell-1]>a";
	public static String cssPathForFirmsInGridInManageEmails ="span[id^=grid][id*=cell-0]>input";
	public static String cssPathForDeleteButton ="td#topButtonRow input[title=Delete]";
	public static String cssPathForSaveButton ="a[title=Save]";
	public static String cssPathForTargetLoginButton ="input[title=Login]";
	public static String cssPathForManageEmailsSendButton ="a#active_sendbutton";
	
	// Modue 4
	
	public static String cssPathForAddMoreButton ="#btnAddMore";
	public static String cssPathForTargetAccountCheckBox ="#chkTargetAccName";
	public static String cssPathForClearDealRoomYesButton ="div#create_savebtn a[title=Yes]";
	public static String cssPathForUpdaeAllButton ="a[title='Update All']";
	public static String cssPathForManageApprovalIcon ="#manageApprovals";
	public static String cssPathForApproveButtonOnManageApproval ="#ApproveId";
	public static String cssPathForDeleteButtononManageApproval ="#DeleteId";
	public static String cssPathForSaveButtonAtWaterMarking ="div#savecanceldiv a[title=Save]";
	
	// Module 5
	
	public static String cssPathForYesButton ="a[title=Yes]";
	public static String cssPathForDeleteButtonForContact ="input[name=del]";
	public static String cssPathForDeleteButtonForAccount ="input[title=Delete]";
	public static String cssPathForDeleteButtonForParticularRecordOnDRD ="input[name=del]";
	
	//Module 6
	public static String cssPathForMyFirmProfileSaveButton ="input[title=Save]";
	public static String cssPathForImageCropButton ="#btnCrop";
	public static String cssPathForSearchIconOnContactAccessTab ="a[title=Search]";
	
	// NewSmokeTestCases
	
	public static String cssPathForDownLoadButton ="#downloadLink";
	public static String cssPathForSearchIconDRM ="a[title=Search]";
	public static String cssPathForRegisterButton ="input[title=Register]";
	
	
	public static String cssPathForSignUpButton ="span[title='Sign up']";
	
	
	public static String userSaveBtn="td[id='topButtonRow'] > input[title='Save']";
		
}
