package com.navatar.pageObjects;


import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import org.apache.poi.ss.formula.ptg.LessEqualPtg;
import org.bridj.cpp.com.OLEAutomationLibrary.UDATE;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Key;
import org.sikuli.script.Screen;

import com.navatar.generic.BaseLib;
import com.navatar.generic.CommonLib;
import com.navatar.generic.ExcelUtils;
import com.navatar.generic.SmokeCommonVariables;
import com.navatar.generic.EnumConstants.BoxActions;
import com.navatar.generic.EnumConstants.Mode;
import com.navatar.generic.EnumConstants.PageName;
import com.navatar.generic.EnumConstants.RecordType;
import com.navatar.generic.EnumConstants.RelatedTab;
import com.navatar.generic.EnumConstants.TabName;
import com.navatar.generic.EnumConstants.YesNo;
import com.navatar.generic.EnumConstants.action;
import com.navatar.generic.EnumConstants.excelLabel;
import com.navatar.generic.SoftAssert;
import com.relevantcodes.extentreports.LogStatus;
import static com.navatar.generic.AppListeners.appLog;
import static com.navatar.generic.AppListeners.currentlyExecutingTC;
import static com.navatar.generic.BaseLib.edriver;
import static com.navatar.generic.CommonLib.*;
public class DealPageBusinessLayer extends DealPage implements DealPageErrorMessage {

	public DealPageBusinessLayer(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	
	/**
	 * @author ANKIT JAISWAL
	 * @param environment
	 * @param mode
	 * @param pipeLineName
	 * @param companyName
	 * @param Stage
	 * @param otherFieldAndValues
	 * @return true/false
	 */
	public boolean createPipeLine(String environment,String mode,String pipeLineName,String companyName, String Stage,String[][] otherFieldAndValues,String recordType ) {
//		if(otherFieldAndValues!=null) {
//			labelNames= otherLabelFields.split(",");
//			labelValue=otherLabelValues.split(",");
//		}
		refresh(driver);
		ThreadSleep(3000);
		if(mode.equalsIgnoreCase(Mode.Lightning.toString())) {
			ThreadSleep(10000);
			if(clickUsingJavaScript(driver, getNewButton(environment, mode, 60), "new button")) {
				appLog.info("clicked on new button");
				ThreadSleep(5000);
				if (!recordType.equals("") || !recordType.isEmpty()) {
                    ThreadSleep(2000);
                    if(click(driver, getRadioButtonforRecordType(recordType, 5), "Radio Button for : "+recordType, action.SCROLLANDBOOLEAN)){
                        appLog.info("Clicked on radio Button  for record type : "+recordType);
                        if (click(driver, getContinueOrNextButton(5), "Continue Button", action.BOOLEAN)) {
                            appLog.info("Clicked on Continue or Nxt Button");   
                            ThreadSleep(1000);
                        }else{
                            appLog.error("Not Able to Clicked on Next Button");
                            return false;   
                        }
                    }else{
                        appLog.error("Not Able to Clicked on radio Button for record type : "+recordType);
                        return false;
                    }
                   
                }
				if(sendKeys(driver, getpipeLInePageTextBoxAllWebElement(environment, mode, excelLabel.Pipeline_Name.toString(),30), pipeLineName, "pipeline name text box ", action.SCROLLANDBOOLEAN)) {
					log(LogStatus.INFO, "Passed value in pipeline name text box : "+pipeLineName,YesNo.No);
					if(sendKeys(driver, getpipeLInePageTextBoxAllWebElement(environment, mode, excelLabel.Company_Name.toString(),30), companyName, "company name text box ", action.SCROLLANDBOOLEAN)) {
						log(LogStatus.INFO, "Passed value in company name text box : "+companyName,YesNo.No);
						ThreadSleep(1000);
						WebElement ele=FindElement(driver,
								"//div[contains(@class,'uiAutocomplete')]//a//div[@title='" + companyName+ "']","Legal Name List", action.SCROLLANDBOOLEAN, 30);
						if (click(driver,ele,companyName+" text ", action.SCROLLANDBOOLEAN)) {
							log(LogStatus.INFO, "clicked on company Name suggest Text : "+companyName, YesNo.No);
							
							if (click(driver, getpipeLInePageTextBoxAllWebElement(environment, mode, excelLabel.Stage.toString(),30), "Stage drop down ", action.SCROLLANDBOOLEAN)) {
								log(LogStatus.INFO, "clicked on Stage Drop Down ", YesNo.No);
								ele = FindElement(driver,
										"//div[@class='select-options']//a[@title='"+Stage+"']","Stage drop down list",action.SCROLLANDBOOLEAN, 10);
								ThreadSleep(500);
								if (click(driver, ele, "stage drop down list link "+Stage, action.SCROLLANDBOOLEAN)) {
									log(LogStatus.INFO, "clicked on stage drop down list value "+Stage, YesNo.No);

									if (click(driver, getSaveButton(environment, mode, 60), "Save Button", action.BOOLEAN)) {
										ThreadSleep(3000);
										if(fieldValueVerificationOnPipeLinePage1(environment, mode,excelLabel.Pipeline_Name.toString(),pipeLineName)) {
											log(LogStatus.PASS, excelLabel.Pipeline_Name.toString()+" is verified "+pipeLineName, YesNo.No);
											return true;
										}else {
											log(LogStatus.ERROR, excelLabel.Pipeline_Name.toString()+" is not verified "+pipeLineName, YesNo.Yes);
										}
									} else {
										log(LogStatus.ERROR, "Not able to click on pipeLine Save button so cannot create PipeLine "+pipeLineName, YesNo.Yes);
									}
								} else {
									log(LogStatus.ERROR, "Not able to select value from stage drop down "+Stage+", cannot create pipeLIne "+pipeLineName, YesNo.Yes);
								}
							} else {
								log(LogStatus.ERROR, "Not able to click on Stage drop down list so cannot select value from stage drop down "+Stage+", cannot create pipeLIne "+pipeLineName, YesNo.Yes);
							}
						} else {
							log(LogStatus.ERROR, "Not able to click on Company Name from suggestion drop down "+companyName,YesNo.Yes);
						}
						
					}else {
						log(LogStatus.ERROR, "Not able to pass value in company text box so cannot create PipeLine: " + companyName, YesNo.Yes);
					}
					
				}else {
					log(LogStatus.ERROR, "Not able to pass value in pipeline text box so cannot create PipeLine: " + pipeLineName, YesNo.Yes);
				}
				
			}else {
				log(LogStatus.ERROR, "Not able to click on New Button so cannot create PipeLine: " + pipeLineName, YesNo.Yes);
			}
		}
		return false;
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param environment
	 * @param mode
	 * @param labelName
	 * @param labelValue
	 * @return true/false
	 */
	public boolean fieldValueVerificationOnPipeLinePage1(String environment, String mode,String labelName,String labelValue) {
		String finalLabelName;
		if (labelName.contains("_")) {
			finalLabelName = labelName.replace("_", " ");
		} else {
			finalLabelName = labelName;
		}
		String xpath = "";
		WebElement ele = null;
		xpath="//span[text()='"+finalLabelName+"']/../following-sibling::div//*[text()='"+labelValue+"']";
		scrollDownThroughWebelement(driver, ele, finalLabelName);
		ele = isDisplayed(driver,
				FindElement(driver, xpath, finalLabelName + " label text in " + mode, action.SCROLLANDBOOLEAN, 10),
				"Visibility", 10, finalLabelName + " label text in " + mode);
		if (ele != null) {
			log(LogStatus.PASS,labelValue+" is verified on pipeLine Page",YesNo.No);
			return true;
		} else {
			log(LogStatus.ERROR,labelValue+" is not verified on pipeLine Page",YesNo.Yes);
		}
		return false;

		
	
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param environment
	 * @param mode
	 * @param tabName
	 * @param labelName
	 * @param labelValue
	 * @return true/false
	 */
	public boolean fieldValueVerificationOnPipelinePage(String environment, String mode, TabName tabName,
			String labelName,String labelValue) {
		String finalLabelName;
		if (labelName.contains("_")) {
			finalLabelName = labelName.replace("_", " ");
		} else {
			finalLabelName = labelName;
		}
		String xpath = "";
		WebElement ele = null;
		if (mode.equalsIgnoreCase(Mode.Classic.toString())) {
			
			if (finalLabelName.contains("Stage") || finalLabelName.contains("Source Firm") || finalLabelName.contains("Source Contact")) {
				xpath = "//td[text()='" + finalLabelName + "']/following-sibling::td/div";
			} else if (finalLabelName.contains("Source") || finalLabelName.contains("Transaction Type")) {
				xpath = "(//span[text()='" + finalLabelName + "']/../following-sibling::td/div)[1]";
			} else {
				xpath = "//td[text()='" + finalLabelName + "']/../td[2]/div";
			}
			
			
			
		} else {
			
			
			if (finalLabelName.contains("Source Firm") || finalLabelName.contains("Source Contact") || finalLabelName.contains("Company Name")) {
				xpath = "//span[@class='test-id__field-label'][text()='"+finalLabelName+"']/../following-sibling::div/span//a";
			}else if (finalLabelName.contains("Age of Current Stage") || finalLabelName.contains("Pipeline Name") || finalLabelName.contains("Last Stage Change Date")
					|| finalLabelName.contains("Highest Stage Reached") || finalLabelName.contains("Stage") || finalLabelName.contains("Source") || finalLabelName.contains("Deal Type") ) {
				xpath = "//span[@class='test-id__field-label'][text()='" + finalLabelName
						+ "']/../following-sibling::div//span//*";
			} else {
				xpath = "//span[@class='test-id__field-label'][text()='" + finalLabelName
						+ "']/../following-sibling::div//span//*";
			}
			
			
		}
		scrollDownThroughWebelement(driver, ele, finalLabelName);
		ele = isDisplayed(driver,
				FindElement(driver, xpath, finalLabelName + " label text in " + mode, action.SCROLLANDBOOLEAN, 10),
				"Visibility", 10, finalLabelName + " label text in " + mode);
		if (ele != null) {
			String aa = ele.getText().trim();
			appLog.info("Lable Value is: "+aa);
			if(aa.contains(labelValue)) {
				appLog.info(labelValue + " Value is matched successfully.");
				return true;
				
			}else {
				appLog.info(labelValue + " Value is not matched. Expected: "+labelValue+" /t Actual : "+aa);
			}
		} else {
			appLog.error(finalLabelName + " Value is not visible so cannot matched  label Value "+labelValue);
		}
		return false;

	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param environment
	 * @param mode
	 * @param pipeLineName
	 * @return true/false
	 */
	public boolean clickOnCreatedPipeLine(String environment,String mode,String pipeLineName) {
		if(mode.equalsIgnoreCase(Mode.Classic.toString())){
		int i =1;
		if (getSelectedOptionOfDropDown(driver, getViewDropdown(60), "View dropdown", "text")
				.equalsIgnoreCase("All")) {
			if (click(driver, getGoButton(60), "Go button", action.BOOLEAN)) {

			} else {
				appLog.error("Go button not found");
			}
		} else {
			if (selectVisibleTextFromDropDown(driver, getViewDropdown(60), "View dropdown", "All")) {
			} else {
				appLog.error("All  not found in dropdown");
			}

		}
		WebElement ele = isDisplayed(driver,
				FindElement(driver, "//div[@class='x-panel-bwrap']//span[text()='" + pipeLineName + "']/..",
						"PipeLine link", action.SCROLLANDBOOLEAN, 20),
				"visibility", 20, "");
		if (ele != null) {
			scrollDownThroughWebelement(driver, ele, "");
			if (click(driver, ele, pipeLineName + " name text", action.SCROLLANDBOOLEAN)) {
				appLog.info("Clicked on PipeLine link");
				return true;
			} else {
				appLog.error("Not able to click on " + pipeLineName);
			}
		} else {
			while (true) {
				appLog.error("PipeLine is not Displaying on "+i+ " Page: " + pipeLineName);
				if (click(driver, getNextImageonPage(10), "PipeLine Page Next Button",
						action.SCROLLANDBOOLEAN)) {
					ThreadSleep(2000);
					appLog.info("Clicked on Next Button");
					ele = FindElement(driver, "//div[@class='x-panel-bwrap']//span[text()='" + pipeLineName + "']/..",
							"Institution link", action.SCROLLANDBOOLEAN, 20);
					if (ele != null) {
						if (click(driver, ele, pipeLineName, action.SCROLLANDBOOLEAN)) {
							appLog.info("Clicked on PipeLine name : " + pipeLineName);
							return true;
							
						}
					}

					

				} else {
					appLog.error("PipeLine Not Available : " + pipeLineName);
					return false;
				}
				i++;
			}
	}
		}else{
			if(clickOnAlreadyCreated_Lighting(environment, mode, TabName.Pipelines, pipeLineName, 30)){
				appLog.info("Clicked on PipeLine name : " + pipeLineName);
				return true;
			}else{
				appLog.error("PipeLine Not Available : " + pipeLineName);
			}	
		}
		return false;
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param environment
	 * @param mode
	 * @param RecordType
	 * @param headersWithValues
	 * @return true/false
	 */
	public boolean verifyPipeLineStageLog(String environment,String mode,RecordType RecordType,String[][] headersWithValues){
		boolean flag=true;
		List<WebElement> header = new ArrayList<WebElement>();
		List<WebElement> values = new ArrayList<WebElement>();
		if(mode.equalsIgnoreCase(Mode.Classic.toString())){
		
		FindElement(driver, "//div[@class='bRelatedList']//h3[text()='//div[@class='bRelatedList']//h3[text()='Pipeline Stage Logs']']", "", action.SCROLLANDBOOLEAN, 10);
		 header = FindElements(driver, "//h3[text()='Pipeline Stage Logs']/ancestor::div[@class='bRelatedList']//div[@class='pbBody']//tr[1]/*", "Header");
		 values = FindElements(driver, "//h3[text()='Pipeline Stage Logs']/ancestor::div[@class='bRelatedList']//div[@class='pbBody']//tr[2]/*", "Values");
		int i = 1;
		for (String[] headerWithValue : headersWithValues) {
			appLog.info("From PAGE    : "+header.get(i).getText()+"  <<<<<>>>>> "+values.get(i).getText());
			appLog.info("fROM tESTcASE  : "+headerWithValue[0].replace("_", " ")+"  <<<<<>>>>> "+headerWithValue[1]);
			if(header.get(i).getText().contains(headerWithValue[0].replace("_", " ")) && values.get(i).getText().contains(headerWithValue[1])){
				appLog.info("Value matched : "+headerWithValue[1]);
			}else{
				flag=false;
				appLog.error("Value Not matched : "+headerWithValue[1]);
				BaseLib.sa.assertTrue(false, "Value Not matched : "+headerWithValue[1]);	
			}
			i++;
		}
		}else{
			ThreadSleep(2000);
			driver.navigate().refresh();
			ThreadSleep(5000);
			
					 header = FindElements(driver, "//table[@data-aura-class='uiVirtualDataTable']/thead/tr/th//a/span[2]", "Header");
					 values = FindElements(driver, "//table[@data-aura-class='uiVirtualDataTable']/tbody/tr/*", "Values");
					int i = 0;
					for (String[] headerWithValue : headersWithValues) {
						appLog.info("From PAGE    : "+header.get(i).getText()+"  <<<<<>>>>> "+values.get(i+1).getText());
						appLog.info("fROM tESTcASE  : "+headerWithValue[0].replace("_", " ")+"  <<<<<>>>>> "+headerWithValue[1]);
						if(/*header.get(i).getText().contains(headerWithValue[0].replace("_", " ").toUpperCase()) && */values.get(i+1).getText().contains(headerWithValue[1])){
							appLog.info("Value matched : "+headerWithValue[1]);
						}else{
							flag=false;
							appLog.error("Value Not matched : "+headerWithValue[1]);
							BaseLib.sa.assertTrue(false, "Value Not matched : "+headerWithValue[1]);	
						}
						i++;
					}
				}
		
		return flag;
	
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param environment
	 * @param mode
	 * @param stageValue
	 * @return true/false
	 */
	public boolean changeStageAndClickOnSaveButton(String environment,String mode,String stageValue){
		
		if(mode.equalsIgnoreCase(Mode.Lightning.toString())){
		
			click(driver, getdetailsTab_Lighting(environment, TabName.Pipelines, 10), "Details Tab", action.SCROLLANDBOOLEAN);
		
		}
			
		if (click(driver, getEditButton(environment, mode, 10), "Edit Button", action.BOOLEAN)) {
			if (mode.equalsIgnoreCase(Mode.Classic.toString())) {
				if (selectVisibleTextFromDropDown(driver, getPipeLineStageLabel(environment, mode, 10),
						"Stage : " + stageValue, stageValue)) {
					if(click(driver, getSaveButton(environment, mode, 10), "Save Button", action.SCROLLANDBOOLEAN)){
					return true;
					}
					
				}

			} else {
				WebElement ele = FindElement(driver, "//span[text()='Stage']/../following-sibling::div//a",
						"Stage Click", action.SCROLLANDBOOLEAN, 10);
				if (click(driver, ele, "Stage Click", action.SCROLLANDBOOLEAN)) {
					ele = FindElement(driver, "//div[@class='select-options']//li/a[@title='" + stageValue + "']",
							"Stage value : " + stageValue, action.SCROLLANDBOOLEAN, 10);
					if (click(driver, ele, "Stage Click : "+stageValue, action.SCROLLANDBOOLEAN)) {
						if(click(driver, getSaveButton(environment, mode, 10), "Save Button", action.SCROLLANDBOOLEAN)){
							ThreadSleep(3000);
							return true;
							}	
					}
					
				}
			}
		}
			
		return false;
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param environment
	 * @param mode
	 * @param RecordType
	 * @param rowValues
	 * @return true/false
	 */
	public SoftAssert verifyPipeLineStageLogForAllRows(String environment,String mode,RecordType RecordType,String[][] rowValues){
		
		SoftAssert saa = new SoftAssert();
		List<WebElement> rows = new ArrayList<WebElement>();
		List<WebElement> values = new ArrayList<WebElement>();
		if(mode.equalsIgnoreCase(Mode.Classic.toString())){
		
		FindElement(driver, "//div[@class='bRelatedList']//h3[text()='//div[@class='bRelatedList']//h3[text()='Pipeline Stage Logs']']", "Pipeline Stage Logs", action.SCROLLANDBOOLEAN, 10);
		
		rows = FindElements(driver, "//h3[text()='Pipeline Stage Logs']/ancestor::div[@class='bRelatedList']//div[@class='pbBody']//tr", "Header");
		 for (int i = 1; i < rows.size(); i++) {
			 values = FindElements(driver, "//h3[text()='Pipeline Stage Logs']/ancestor::div[@class='bRelatedList']//div[@class='pbBody']//tr["+(i+1)+"]/*", "Values");	
			 
			 for(int j=1;j<values.size()-3;j++){
					appLog.info("From PAGE    : "+values.get(j).getText());
					appLog.info("From testCase  : "+rowValues[i-1][j-1]); 
				 if(values.get(j).getText().contains(rowValues[i-1][j-1])){
					 appLog.info((i-1)+","+(j-1)+" : Value matched >> "+rowValues[i-1][j-1]); 
				 }else{
						appLog.error((i-1)+","+(j-1)+" : Value Not matched Actual :> "+values.get(j).getText()+"  \t Expected :> "+rowValues[i-1][j-1]);
						saa.assertTrue(false, (i-1)+","+(j-1)+" : Value Not matched Actual :> "+values.get(j).getText()+"  \t Expected :> "+rowValues[i-1][j-1]);	 
				 }
				 
			 }
		}
		
		}else{
					ThreadSleep(1000);
					rows = FindElements(driver, "//h1[text()='Pipeline Stage Logs']/../../../../../following-sibling::div//table/tbody/tr", "Rows");
					appLog.info("No. of Rows : "+rows.size());
					 for (int i = 0; i < rows.size(); i++) {
						 values = FindElements(driver, "//h1[text()='Pipeline Stage Logs']/../../../../../following-sibling::div//table/tbody/tr["+(i+1)+"]/*", "Values");	
							appLog.info("No. of Values : "+values.size());
						 for(int j=1;j<values.size()-3;j++){
							 appLog.info("Rows :  "+i+"<><> VALUES : "+j);
								appLog.info("From PAGE    : "+values.get(j).getText());
								appLog.info("fROM tESTcASE  : "+rowValues[i][j-1]); 
							 if(values.get(j).getText().contains(rowValues[i][j-1])){
								 appLog.info((i)+","+(j-1)+" : Value matched for "+rowValues[i][j-1]); 
							 }else{
									appLog.error((i)+","+(j-1)+" : Value Not matched Actual :> "+values.get(j).getText()+" \t Expected :> "+rowValues[i][j-1]);
									saa.assertTrue(false, (i)+","+(j-1)+" : Value Not matched Actual :> "+values.get(j).getText()+" \t Expected :> "+rowValues[i][j-1]);	 
							 }
							 
						 }
					}
			
			
		}
		return saa;
	
	}

	/**
	 * @author ANKIT JAISWAL
	 * @param environment
	 * @param mode
	 * @param tabName
	 * @param headerName
	 * @param headerNameList
	 * @param valueList
	 * @return true/false
	 */
	public SoftAssert verifyDataOnDueDiligenceAndDealEvaluationPage(String environment,String mode,RelatedTab tabName,Header headerName,String[] headerNameList,String[][] valueList) {
	SoftAssert result= new SoftAssert();
	String HeaderXpath="",dataRowXpath="",rowColumnXpath="",headerText="",pageName="",CommonXpath="";

//	String tabName="";
//	String headerName="";
	if(tabName.toString().contains("_")) {
		pageName=tabName.toString().replace("_", " ");
	}else {
		pageName=tabName.toString();
	}
	if(headerName.toString().equalsIgnoreCase(Header.Project.toString())) {
		headerText=Header.Project.toString();
		
	}else if (headerName.toString().equalsIgnoreCase(Header.OpenTask.toString())) {
		headerText=Header.OpenTask.toString();
	}else {
		headerText=Header.CompletedTask.toString();
	}
//	HeaderXpath="//li//a[@title='"+pageName+"']/ancestor::div[@role='tablist']/following-sibling::section//h2//a[contains(text(),'"+headerText+"')]/ancestor::article//thead//tr//span";
//	dataRowXpath="//li//a[@title='"+pageName+"']/ancestor::div[@role='tablist']/following-sibling::section//h2//a[contains(text(),'"+headerText+"')]/ancestor::article//tbody//tr";
	CommonXpath="//li/a[@title='"+pageName+"' or text()='"+pageName+"']/../../../..//h2//a[contains(text(),'"+headerText+"')]";
	HeaderXpath=CommonXpath+"/ancestor::article//thead//tr//span";
	List<WebElement> headerRowList= FindElements(driver, HeaderXpath, "header text list");
	if(!headerRowList.isEmpty()) {
		List<String> valueFromWeb = new ArrayList<String>();
		for (int j = 0; j < headerRowList.size(); j++) {
			String actualValue1=headerRowList.get(j).getText().trim();
			valueFromWeb.add(actualValue1);
		}
		List<String> headerNameList1= new ArrayList<String>();
		Collections.addAll(headerNameList1, headerNameList); 
		System.err.println("valueFromWeb : "+valueFromWeb+" valueAdded : "+headerNameList1);
		
		headerNameList1.replaceAll(String::toUpperCase);
		if (valueFromWeb.equals(headerNameList1)) {
			log(LogStatus.INFO, headerNameList1+" verified For  : "+headerText.toString()+" on "+tabName.toString()+" Page", YesNo.No);
		}else {
			log(LogStatus.FAIL, "Header List is not matched. Actual Result : "+valueFromWeb+" Expected Result :"+headerNameList1, YesNo.Yes);
			result.assertTrue(false, "Header List is not matched. Actual Result : "+valueFromWeb+" Expected Result :"+headerNameList1);
		}
	}else {
		log(LogStatus.FAIL, "Data Header List is not Found so cannot verify data for "+headerText+" on "+tabName.toString()+" Page", YesNo.Yes);
		result.assertTrue(false, "Data Header List is not Found so cannot verify data for "+headerText+" on "+tabName.toString()+" Page");
	}
	dataRowXpath=CommonXpath+"/ancestor::article//tbody/tr";
//	List<WebElement> headerTextList= FindElements(driver, HeaderXpath, "header text list");
	List<WebElement> DataRowList= FindElements(driver, dataRowXpath, "header text list");
	String actualValue="";
	
	if(!DataRowList.isEmpty()) {
		for (String[] value : valueList) {
			List<String> valueAdded = new ArrayList<String>();
			for (int k = 0; k < value.length; k++) {
				valueAdded.add(value[k].trim());
			}
			for(int i=0; i<DataRowList.size(); i++) {
				rowColumnXpath=CommonXpath+"/ancestor::article//tbody//tr["+(i+1)+"]/td";
				List<WebElement> rowColumnWebElementList= FindElements(driver, rowColumnXpath, "header text list");
				if(!rowColumnWebElementList.isEmpty()) {
					List<String> valueFromWeb = new ArrayList<String>();
					for (int j = 0; j < rowColumnWebElementList.size(); j++) {
						actualValue=rowColumnWebElementList.get(j).getText().trim();
						valueFromWeb.add(actualValue);
					}
					System.err.println("valueFromWeb : "+valueFromWeb+" valueAdded : "+valueAdded);
					if (valueFromWeb.equals(valueAdded)) {
						log(LogStatus.INFO, valueAdded+" verified For  : "+headerText.toString()+" on "+tabName.toString()+" Page", YesNo.No);
					break;	
					}
				}else {
					log(LogStatus.ERROR, "Data Column List is not Found so cannot verify data for "+headerText+" on "+tabName.toString()+" Page", YesNo.Yes);
					result.assertTrue(false, "Data Column List is not Found so cannot verify data for "+headerText+" on "+tabName.toString()+" Page");
				}
				if (i==DataRowList.size()-1) {
					log(LogStatus.ERROR, valueAdded+" is not verified For  : "+headerText.toString()+" on "+tabName.toString()+" Page", YesNo.Yes);
					result.assertTrue(false, valueAdded+" is not verified For  : "+headerText.toString()+" on "+tabName.toString()+" Page");	
				}
			}	
		}
	}else {
		log(LogStatus.ERROR, "Header Text and Data List is found empty so cannot verify data is on "+headerName.toString(), YesNo.Yes);
		result.assertTrue(false,"Header Text and Data List is found empty so cannot verify data is on "+headerName.toString());
	}
	return result;
}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param dragFromFolder
	 * @param dropLocImageName
	 * @return true/false
	 */
	public boolean dragDropFiles(String dragFromFolder, String dropLocImageName) {
		Screen screen = new Screen();
		try {
			System.err.println(System.getProperty("user.dir")+"\\"+dragFromFolder);
			Process process = Runtime.getRuntime()
					.exec(System.getProperty("user.dir") + "/OpenFolder.exe" + " " + dragFromFolder);
			process.waitFor();
			if (dragFromFolder.contains("\\")) {
				dragFromFolder = (dragFromFolder
						.split(Pattern.quote("\\")))[(dragFromFolder.split(Pattern.quote("\\")).length - 1)];
			}
			process = Runtime.getRuntime().exec(".\\AutoIT\\activateFilesToUpload.exe" + " " + dragFromFolder);
			process.waitFor();
			screen.keyDown(Key.CTRL);
			screen.type("a");
			screen.keyUp(Key.CTRL);
			screen.drag(".\\AutoIT\\Drag.jpg");
			screen.mouseMove(-150, -100);
			Runtime.getRuntime().exec(".\\AutoIT\\DocumentTaggingWin.exe");
			try{
				screen.wait(".\\AutoIT\\"+dropLocImageName, 10);
				screen.dropAt(".\\AutoIT\\"+dropLocImageName);
				
			} catch (Exception e){
				System.err.println("After exception is running.");
				screen.wait(".\\"+dropLocImageName, 10);
				screen.dropAt(".\\"+dropLocImageName);
			}
			process = Runtime.getRuntime()
					.exec(System.getProperty("user.dir") + "\\AutoIT\\CloseFolder.exe" + " " + dragFromFolder);
			process.waitFor();
			System.err.println("Successfully cdroped files");
		} catch (FindFailed | IOException | InterruptedException e) {
			appLog.info("Issue with drag and drop");
			return false;
		}
		return true;
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param environment
	 * @param mode
	 * @param pageName
	 * @param taskName
	 * @param dragFromFolder
	 * @param dropFolderPath
	 * @param uploadDocumentFolderOrDealName
	 * @param uploadUpdate
	 * @param checkFileNameIsselectedAndTagButtonEnable
	 * @return true/false
	 */
	public boolean tagOrUploadUpdateDocument(String environment, String mode,PageName pageName,String taskName,String dragFromFolder,String dropFolderPath,String uploadDocumentFolderOrDealName,UploadFileActions uploadUpdate, YesNo checkFileNameIsselectedAndTagButtonEnable) {
		boolean flag= false;
		List<String> droppedFileNames = new ArrayList<String>();
		CommonLib compare = new CommonLib();
		String uploadDocumentXpath="",fileCheckBox="";
		String dropImage = "DropLoc.JPG";
		WebElement ele= null;
		if(dropFolderPath==null) {
			uploadDocumentXpath="//span[text()='"+uploadDocumentFolderOrDealName+"']/../../following-sibling::ul/ul/li/a[text()='Upload Document']";
		}else {
			uploadDocumentXpath="//span[text()='"+uploadDocumentFolderOrDealName+"']/../../following-sibling::ul/li/a[text()='Upload Document']";
		}
		if(pageName.toString().equalsIgnoreCase(PageName.TaskRayPage.toString())) {
			switchToFrame(driver, 60, getFrame(PageName.NavatarDocumentsPopUpFrameOnTaskRayPage, 60));
		}else {
			switchToFrame(driver,30,getFrame(PageName.NavatarDocumentPopUpOnPipeLinePage, 30));
		}
		if(click(driver,getTagOrUploadDocumentsButton(30), "tag or document button", action.SCROLLANDBOOLEAN)) {
			log(LogStatus.PASS, "clicked on tag or document button", YesNo.No);
			ThreadSleep(5000);
			String parentID=switchOnWindow(driver);
			if(parentID!=null) {
				ThreadSleep(3000);
				if(matchTitle(driver, "Tag Documents", 60)) {
					log(LogStatus.PASS, "tag document window title is matched. window is open successfully and ready to upload document", YesNo.No);
					if(dropFolderPath!=null) {
						if(verifyFolderStructure(driver, dropFolderPath)) {
							log(LogStatus.PASS, "Folder path is open successfully : "+dropFolderPath, YesNo.No);
						}else {
							log(LogStatus.FAIL, "Not able to open folder path : "+dropFolderPath+" so cannot upload documents", YesNo.Yes);
							driver.close();
							driver.switchTo().window(parentID);
							return false;
						}
					}else {
						log(LogStatus.INFO, "Uploading document on root level.....", YesNo.No);
					}
					ele= isDisplayed(driver, FindElement(driver, uploadDocumentXpath, "upload document link", action.SCROLLANDBOOLEAN,30),"visibility",30,"upload document link");
					if(ele!=null) {
						if(click(driver,ele,"upload document link", action.SCROLLANDBOOLEAN)) {
							log(LogStatus.PASS, "clicked on upload document link", YesNo.No);
							if(dragDropFiles(dragFromFolder, dropImage)){
								log(LogStatus.PASS, "Successfully drag and drop files in upload document window", YesNo.No);
								List<WebElement>droppedFiles =getdroppedFileNamesListInUploadDocument();
								if(!droppedFiles.isEmpty()) {
									for(int i = 0; i < droppedFiles.size(); i++){
										droppedFileNames.add(getText(driver, droppedFiles.get(i), "Dropped Files", action.BOOLEAN).trim());
									}
									Collections.sort(droppedFileNames,compare);
									String previousuploadedFiles = ExcelUtils.readData("FilePath", excelLabel.TestCases_Name, currentlyExecutingTC,excelLabel.uploadFileName);
									String newlyUploadedFiles = createStringOutOfList(droppedFileNames);
									if(previousuploadedFiles!=null && !previousuploadedFiles.isEmpty()){
										newlyUploadedFiles = previousuploadedFiles+"<break>"+newlyUploadedFiles;
									}
									if (ExcelUtils.writeData(newlyUploadedFiles, "FilePath", excelLabel.TestCases_Name, currentlyExecutingTC,excelLabel.uploadFileName)){
										appLog.info("written uploaded file data to excel");
									}
									else {
										appLog.error("could not write uploaded files information to excel");
									}
									if(click(driver, getUploadDocumentSaveBtn(30), "save button", action.SCROLLANDBOOLEAN)) {
										log(LogStatus.PASS, "Clicked on save button", YesNo.No);
										
										
										if(uploadUpdate.toString().equalsIgnoreCase(UploadFileActions.Update.toString())) {
											if(click(driver, getSimpleupdateAllButton(10), "Update all button", action.BOOLEAN)) {
												log(LogStatus.PASS, "clicked on update all button successfully", YesNo.No);
											
											}else {
												log(LogStatus.FAIL, "Not able to click on update all button so cannot update file ", YesNo.Yes);
												driver.close();
												driver.switchTo().window(parentID);
												return false;
											}
										}else if(uploadUpdate.toString().equalsIgnoreCase(UploadFileActions.IgnoreAll.toString())) {
											if(click(driver, getIgnoreAllButton(10), "ignore all button", action.BOOLEAN)) {
												log(LogStatus.PASS, "clicked on ignore all button successfully", YesNo.No);
											
											}else {
												log(LogStatus.FAIL, "Not able to click on ignore all button so cannot close update popUp", YesNo.Yes);
												driver.close();
												driver.switchTo().window(parentID);
												return false;
											}
										}
										if(!uploadUpdate.toString().equalsIgnoreCase(UploadFileActions.IgnoreAll.toString())) {
											if(getUploadDocumentConfirmationPopUpText(60)!=null) {
												if (getUploadDocumentConfirmationPopUpText(10).getText().trim().equalsIgnoreCase(DealPageErrorMessage.uploadDocumentErrorMsg)) {
													appLog.info("Confirmation message is matched Successfully.");
												} else {
													log(LogStatus.FAIL, "Confirmation PopUp text message is not matched. Expected :  "+DealPageErrorMessage.uploadDocumentErrorMsg, YesNo.Yes);
												}
												if(click(driver, getUploadDocumentConfirmationPopUpCloseBtn(10), "close button", action.BOOLEAN)) {
													log(LogStatus.PASS, "Clicked on confirmation popUp Close button", YesNo.No);
													if(checkFileNameIsselectedAndTagButtonEnable.toString().equalsIgnoreCase(YesNo.Yes.toString())) {
														if(!droppedFileNames.isEmpty()) {
															for (int i = 0; i < droppedFileNames.size(); i++) {
																if(dropFolderPath==null) {
																	fileCheckBox="//span[text()='"+uploadDocumentFolderOrDealName+"']/../../following-sibling::ul/ul/li[text()='"+droppedFileNames.get(i)+"']/input";
																}else {
																	fileCheckBox="//span[text()='"+uploadDocumentFolderOrDealName+"']/../../following-sibling::ul/li[text()='"+droppedFileNames.get(i)+"']/input";
																}
																ele= FindElement(driver, fileCheckBox,droppedFileNames.get(i)+" check box xpath", action.SCROLLANDBOOLEAN,5);
																if(ele!=null) {
																	if(isSelected(driver, ele, droppedFileNames.get(i)+" check box xpath")) {
																		log(LogStatus.PASS, droppedFileNames.get(i)+" file check box is checked", YesNo.No);
																	}else {
																		log(LogStatus.FAIL, droppedFileNames.get(i)+" file check box is not checked after uplaod the document ", YesNo.Yes);
																		BaseLib.sa.assertTrue(false, droppedFileNames.get(i)+" file check box is not checked after uplaod the document");
																	}
																}else {
																	log(LogStatus.FAIL, droppedFileNames.get(i)+" file check box is not found so cannot check its selected or not after upload", YesNo.Yes);
																	BaseLib.sa.assertTrue(false, droppedFileNames.get(i)+" file check box is not found so cannot check its selected or not after upload");
																}
															}
														}else {
															log(LogStatus.FAIL, "Not able to get uploaded file names during drag and drop file so cannot checked its selected or not after uploading", YesNo.Yes);
															BaseLib.sa.assertTrue(false, "Not able to get uploaded file names during drag and drop file so cannot checked its selected or not after uploading");
														}
														if(isEnabled(driver, getTagDocumentsBtn(5), "tag documents button")) {
															log(LogStatus.PASS, "tag documents button is enable after uploading documents ", YesNo.No);
														}else {
															log(LogStatus.FAIL, "tag documents button is not enable after uploading documents ", YesNo.Yes);
															BaseLib.sa.assertTrue(false, "tag documents button is not enable after uploading documents ");
														}
													}
												
												}else {
													log(LogStatus.FAIL, "Not able to click on confirmation PopUp close Button", YesNo.Yes);
													driver.close();
												}
											}else {
												log(LogStatus.FAIL, "confirmation pop up text is not visible so cannot click on close button", YesNo.Yes);
												driver.close();
											}
										}else {
											if(checkFileNameIsselectedAndTagButtonEnable.toString().equalsIgnoreCase(YesNo.Yes.toString())) {
												if(!droppedFileNames.isEmpty()) {
													for (int i = 0; i < droppedFileNames.size(); i++) {
														if(dropFolderPath==null) {
															fileCheckBox="//span[text()='"+uploadDocumentFolderOrDealName+"']/../../following-sibling::ul/ul/li[text()='"+droppedFileNames.get(i)+"']/input";
														}else {
															fileCheckBox="//span[text()='"+uploadDocumentFolderOrDealName+"']/../../following-sibling::ul/li[text()='"+droppedFileNames.get(i)+"']/input";
														}
														ele= FindElement(driver, fileCheckBox,droppedFileNames.get(i)+" check box xpath", action.SCROLLANDBOOLEAN,5);
														if(ele!=null) {
															if(isSelected(driver, ele, droppedFileNames.get(i)+" check box xpath")) {
																log(LogStatus.FAIL, droppedFileNames.get(i)+" file check box is checked after click on ignore all button", YesNo.No);
																BaseLib.sa.assertTrue(false, droppedFileNames.get(i)+" file check box is checked after click on ignore all button");
															}else {
																log(LogStatus.PASS, droppedFileNames.get(i)+" file check box is not checked after click on ignore all button", YesNo.Yes);
															}
														}else {
															log(LogStatus.FAIL, droppedFileNames.get(i)+" file check box is not found so cannot check its selected or not after upload", YesNo.Yes);
															BaseLib.sa.assertTrue(false, droppedFileNames.get(i)+" file check box is not found so cannot check its selected or not after upload");
														}
													}
												}else {
													log(LogStatus.FAIL, "Not able to get uploaded file names during drag and drop file so cannot checked its selected or not after uploading", YesNo.Yes);
													BaseLib.sa.assertTrue(false, "Not able to get uploaded file names during drag and drop file so cannot checked its selected or not after uploading");
												}
												if(!isEnabled(driver, getTagDocumentsBtn(5), "tag documents button")) {
													log(LogStatus.PASS, "tag documents button is not enable after click on ignore all button", YesNo.No);
												}else {
													log(LogStatus.FAIL, "tag documents button is enable after click on ignore all button", YesNo.Yes);
													BaseLib.sa.assertTrue(false, "tag documents button is enable after click on ignore all button");
												}
											}
										}
										if(clickUsingJavaScript(driver,getTagDocumentsCancelBtn(30),"cancel button")) {
											log(LogStatus.PASS, "clicked on cancel button and tag document pop up is closed ", YesNo.No);
											driver.switchTo().window(parentID);
											flag =true;
											
										}else {
											log(LogStatus.FAIL, "Not able to click on cancel so cannot close tag document window ", YesNo.Yes);
											sa.assertTrue(false, "Not able to click on cancel so cannot close tag document window ");
										}
									}else {
										log(LogStatus.FAIL, "Not able to click on save button so cannot upload documents ", YesNo.Yes);
										driver.close();
									}
								}else {
									log(LogStatus.FAIL, "Not able to get upload dragged file names so cannot write in excel sheet", YesNo.Yes);
									driver.close();
								}

							}else {
								log(LogStatus.FAIL, "Not able to drag and drop document in upload document window ",YesNo.Yes);
								driver.close();
							}
						}else {
							log(LogStatus.FAIL, "Not able to click on upload document link so cannot upload document in folder : "+uploadDocumentFolderOrDealName,YesNo.Yes);
							driver.close();
						}
					}else {
						log(LogStatus.FAIL, "upload document link is not visible in tag document window so cannot upload document : "+uploadDocumentFolderOrDealName, YesNo.Yes);
						driver.close();
					}
				}else {
					log(LogStatus.FAIL, "tag document window title is not matched so cannot upload documents", YesNo.No);
					driver.close();
				}
				driver.switchTo().window(parentID);
			}else {
				log(LogStatus.FAIL, "No new window is found so cannot verify folder structure in Navatar Docuemnts",YesNo.Yes);
				sa.assertTrue(false, "No new window is found so cannot verify folder structure in Navatar Docuemnts");
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on tag or documents button so cannot verify folder structure in Navatar Docuemnts", YesNo.Yes);
			sa.assertTrue(false, "Not able to click on tag or documents button so cannot verify folder structure in Navatar Docuemnts");
		}
		return flag;
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param environment
	 * @param mode
	 * @param pageName
	 * @param folderpath
	 * @param dealNameOrDocumentFolderName
	 * @param tagDocumentList
	 * @param selectOption
	 * @param parentIdAndchildWindowId TODO
	 * @return result in list empty or failed messages
	 */
	public List<String> tagDocuments(String environment, String mode,PageName pageName,String folderpath,String dealNameOrDocumentFolderName,String tagDocumentList,SelectOption selectOption, String[] parentIdAndchildWindowId){
		List<String> result = new ArrayList<String>();
		boolean flag= false;
		String parentID =null;
		String secondChildWindowId=null;
		if(pageName.toString().equalsIgnoreCase(PageName.TaskRayPage.toString())) {
			switchToFrame(driver, 60, getFrame(PageName.NavatarDocumentsPopUpFrameOnTaskRayPage, 60));
		}else if(pageName.toString().equalsIgnoreCase(PageName.DealPage.toString()) || pageName.toString().equalsIgnoreCase(PageName.FundsPage.toString())) {
			switchToFrame(driver,30,getFrame(PageName.NavatarDocumentPopUpOnPipeLinePage, 30));
		}else {
			driver.switchTo().frame(0);
		}
		if(click(driver,getTagOrUploadDocumentsButton(60), "tag or document button", action.SCROLLANDBOOLEAN)) {
			log(LogStatus.PASS, "clicked on tag or document button", YesNo.No);
			ThreadSleep(5000);
			if(parentIdAndchildWindowId!=null) {
				Set<String> lst1 = driver.getWindowHandles();
				Iterator<String> I2 = lst1.iterator();
				while (I2.hasNext()) {
					String windowID = I2.next();
					if (windowID.equalsIgnoreCase(parentIdAndchildWindowId[0]) || windowID.equalsIgnoreCase(parentIdAndchildWindowId[1])) {
						appLog.info("Parent id or first window id is Matched");
					} else {
						secondChildWindowId = windowID;
						appLog.info("got it tag document Window id");
						driver.switchTo().window(secondChildWindowId);
						break;
					}
				}
				parentID=parentIdAndchildWindowId[0];
			}else {
				parentID=switchOnWindow(driver);
			}
			if(parentID!=null) {
				if(parentIdAndchildWindowId!=null) {
					if(secondChildWindowId!=null) {
						log(LogStatus.PASS, "switched on document tag window successfully", YesNo.No);
					}else {
						log(LogStatus.FAIL, "Not able to switch tag document window so cannot tag documents", YesNo.Yes);
						result.add("Not able to switch tag document window so cannot tag documents");
						return result;
					}
				}
				ThreadSleep(3000);
				if(matchTitle(driver, "Tag Documents", 60)) {
					log(LogStatus.PASS, "tag document window title is matched. window is open successfully and ready to tag documents", YesNo.No);
					if(folderpath!=null) {
						if(selectOption.toString().equalsIgnoreCase(SelectOption.OneByOneSelect.toString())){
							if(traverseImport(driver, folderpath, tagDocumentList)) {
								log(LogStatus.PASS, "Documents is selected : "+tagDocumentList, YesNo.No);
								flag =true;
							}else {
								log(LogStatus.FAIL, "Not able to select Documents : "+tagDocumentList, YesNo.Yes);
							}
						}else {
							if(verifyFolderStructure(driver, folderpath)) {
								log(LogStatus.PASS, "Clicked on Folder Structure  : "+folderpath, YesNo.No);
								if(clickSelectOrDeSelectLink(dealNameOrDocumentFolderName)) {
									log(LogStatus.PASS, "All Documents is selected Successfully in folder "+dealNameOrDocumentFolderName, YesNo.No);
									flag =true;
								}else {
									log(LogStatus.FAIL, "Not able to click on Select/DeSelect All link so cannot select documents in folder "+dealNameOrDocumentFolderName, YesNo.Yes);
								}
							}else {
								log(LogStatus.FAIL, "Not able to click on Folder Structure : "+folderpath+" so cannot select all documents in folder "+dealNameOrDocumentFolderName, YesNo.Yes);
							}
						}
					}else {
						if(clickSelectOrDeSelectLink(dealNameOrDocumentFolderName)) {
							log(LogStatus.PASS, "All Documents is selected Successfully in folder "+dealNameOrDocumentFolderName, YesNo.No);
							flag =true;
						}else {
							log(LogStatus.FAIL, "Not able to click on Select/DeSelect All link so cannot select documents in folder "+dealNameOrDocumentFolderName, YesNo.Yes);
						}
					}
					if(flag) {
						if(clickUsingJavaScript(driver, getTagDocumentsBtn(10), "tag documents button")) {
							log(LogStatus.PASS, "Clicked on tag documents button", YesNo.No);
							ThreadSleep(1000);
							String expMsg=DealPageErrorMessage.tagDocumentErrorMsg;
							 if(isAlertPresent(driver)) {
								 String msg = switchToAlertAndGetMessage(driver, 30, action.GETTEXT);
									switchToAlertAndAcceptOrDecline(driver, 30, action.ACCEPT);
									if(msg.equalsIgnoreCase(expMsg)) {
										log(LogStatus.PASS, "Tag documents Error Message is verified: "+expMsg, YesNo.No);
									}else {
										log(LogStatus.PASS, "Error message is not matched. Expected: "+expMsg+" Actual Result: "+msg, YesNo.Yes);
										result.add("Error message is not matched. Expected: "+expMsg+" Actual Result: "+msg);
										sa.assertTrue(false, "Error message is not matched. Expected: "+expMsg+" Actual Result: "+msg);
									}
							 }else {
								log(LogStatus.PASS, "Alert is not present so cannot check error message : "+expMsg, YesNo.Yes);
								result.add("Alert is not present so cannot check error message : "+expMsg);
								sa.assertTrue(false, "Alert is not present so cannot check error message : "+expMsg);
							}
						}else {
							log(LogStatus.FAIL, "Not able to click on tag documents button so cannot tag documents", YesNo.Yes);
							result.add("Not able to click on tag documents button so cannot tag documents");
							driver.close();
							
						}
					}else {
						log(LogStatus.FAIL, "Not able to tag documents so closing tag documents window and moving on parent window", YesNo.Yes);
						result.add("Not able to tag documents so closing tag documents window and moving on parent window");
						driver.close();
					}
				}else {
					log(LogStatus.FAIL, "tag document window title is not matched so cannot upload documents", YesNo.No);
					result.add("tag document window title is not matched so cannot upload documents");
					driver.close();
				}
				if(parentIdAndchildWindowId!=null) {
					driver.switchTo().window(parentIdAndchildWindowId[1]);
				}else {
					driver.switchTo().window(parentID);
					
				}
			}else {
				log(LogStatus.FAIL, "No new window is found so cannot verify folder structure in Navatar Docuemnts",YesNo.Yes);
				result.add("No new window is found so cannot verify folder structure in Navatar Docuemnts");
				sa.assertTrue(false, "No new window is found so cannot verify folder structure in Navatar Docuemnts");
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on tag or documents button so cannot tag documents "+tagDocumentList, YesNo.Yes);
			result.add("Not able to click on tag or documents button so cannot tag documents "+tagDocumentList);
			sa.assertTrue(false, "Not able to click on tag or documents button so cannot tag documents "+tagDocumentList);
		}
		return result;
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param folderName
	 * @return true/false
	 */
	public boolean clickSelectOrDeSelectLink(String folderName) {
		String folderSelectDeSelectXpath="//span[text()='"+folderName+"']/following-sibling::span";
		WebElement ele= FindElement(driver, folderSelectDeSelectXpath,folderSelectDeSelectXpath+" select deselect xpath", action.BOOLEAN,5);
		if(ele!=null) {
			String id=ele.getAttribute("id");
			((JavascriptExecutor) driver).executeScript("document.getElementById('"+id+ "').setAttribute('style', 'display: inline; padding-top: 7px;');");
			ele=FindElement(driver, folderSelectDeSelectXpath+"/a",folderSelectDeSelectXpath+" select deselect Link xpath", action.SCROLLANDBOOLEAN,5);
			return click(driver,ele,folderSelectDeSelectXpath+" select deselect Link xpath", action.BOOLEAN);
		}else {
			log(LogStatus.FAIL, "Not able to find folder Name xpath : "+folderName+" so cannot click on Select/DeSelectAll Link ", YesNo.Yes);
		}
		return false;
		
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param environment
	 * @param mode
	 * @param pageName
	 * @param tagDocumentList
	 * @return empty list if execute successfully
	 */
	public List<String> verifyTaggedDocumentOnNavatarDocumentPopUp(String environment, String mode,PageName pageName,String tagDocumentList) {
		List<String> res=  new ArrayList<String>();
		if(pageName.toString().equalsIgnoreCase(PageName.TaskRayPage.toString())) {
			switchToFrame(driver, 10, getFrame(PageName.TaskRayPage, 60));
			switchToFrame(driver, 10, getFrame(PageName.NavatarDocumentsPopUpFrameOnTaskRayPage, 10));
		}else if(pageName.toString().equalsIgnoreCase(PageName.DealPage.toString()) || pageName.toString().equalsIgnoreCase(PageName.FundsPage.toString())) {
			switchToFrame(driver,10,getFrame(PageName.NavatarDocumentPopUpOnPipeLinePage, 10));
		}else {
			driver.switchTo().frame(0);
		}
		if(click(driver, getRefreshButton(30), "refresh button", action.BOOLEAN)) {
			List<WebElement> taggedDocumentList=getTaggedDocumentListOnNavatarDocumentsPopUp();
			List<WebElement> removeLinkList=getTaggedDocumentRemoveLinkListOnNavatarDocumentsPopUp();
			String[] splitedDocList=tagDocumentList.split("<break>");
			if(!taggedDocumentList.isEmpty() && !removeLinkList.isEmpty()) {
				for (int i = 0; i <splitedDocList.length ; i++) {
					for (int j = 0; j < taggedDocumentList.size(); j++) {
						String docName=taggedDocumentList.get(j).getText().trim();
						String removeLink = removeLinkList.get(j).getText().trim();
						if(docName.equalsIgnoreCase(splitedDocList[i]) && removeLink.equalsIgnoreCase("Remove")) {
							log(LogStatus.PASS, "tagged document name is matched and remove link is available in fornt of document name ", YesNo.No);
							break;
						}else {
							if(j==taggedDocumentList.size()-1) {
								log(LogStatus.FAIL, "document is not available in navatar document pop up:  "+splitedDocList[i], YesNo.Yes);
								res.add("document is not available in navatar document pop up:  "+splitedDocList[i]);
							}
						}
					}
				}
			}else {
				log(LogStatus.FAIL, "Not able to fond tagged documents list and remove link list so cannot verify tagged documents on "+pageName, YesNo.Yes);
				res.add("Not able to fond tagged documents list and remove link list so cannot verify tagged documents on "+pageName);
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on refresh button so cannot verify tagged documents on "+pageName, YesNo.Yes);
			res.add("Not able to click on refresh button so cannot verify tagged documents on "+pageName);
		}
		switchToDefaultContent(driver);
		return res;
		
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param environment
	 * @param mode
	 * @param pageName
	 * @param folderpath
	 * @param dealNameOrDocumentFolderName
	 * @param tagDocumentList
	 * @return empty list string if execute successfully
	 */
	public List<String> verifyTaggedDocumentIsSelectedInTagDocumentPopUp(String environment, String mode,PageName pageName,String folderpath,String dealNameOrDocumentFolderName,String tagDocumentList){
		List<String> result= new ArrayList<String>();
		String fileCheckBox="";
		WebElement ele= null;
		String[] taggedDocList=tagDocumentList.split("<break>");
		if(pageName.toString().equalsIgnoreCase(PageName.TaskRayPage.toString())) {
			switchToFrame(driver, 10, getFrame(PageName.TaskRayPage, 60));
			switchToFrame(driver, 10, getFrame(PageName.NavatarDocumentsPopUpFrameOnTaskRayPage, 10));
		}else {
			switchToFrame(driver,10,getFrame(PageName.NavatarDocumentPopUpOnPipeLinePage, 10));
		}
		if(click(driver,getTagOrUploadDocumentsButton(30), "tag or document button", action.SCROLLANDBOOLEAN)) {
			log(LogStatus.PASS, "clicked on tag or document button", YesNo.No);
			ThreadSleep(5000);
			String parentID=switchOnWindow(driver);
			if(parentID!=null) {
				ThreadSleep(3000);
				if(matchTitle(driver, "Tag Documents", 60)) {
					log(LogStatus.PASS, "tag document window title is matched. window is open successfully and ready to verify tag documents check box", YesNo.No);
					if(folderpath!=null) {
						if(verifyFolderStructure(driver, folderpath)) {
							log(LogStatus.PASS, "Clicked on Folder Structure  : "+folderpath, YesNo.No);
							
						}else {
							log(LogStatus.FAIL, "Not able to click on Folder Structure : "+folderpath+" so cannot select all documents in folder "+dealNameOrDocumentFolderName, YesNo.Yes);
							result.add("Not able to click on Folder Structure : "+folderpath+" so cannot select all documents in folder "+dealNameOrDocumentFolderName);
							return result;
						}
					}
					for (int i = 0; i < taggedDocList.length; i++) {
						if(folderpath==null) {
							fileCheckBox="//span[text()='"+dealNameOrDocumentFolderName+"']/../../following-sibling::ul/ul/li[text()='"+taggedDocList[i]+"']/input";
						}else {
							fileCheckBox="//span[text()='"+dealNameOrDocumentFolderName+"']/../../following-sibling::ul/li[text()='"+taggedDocList[i]+"']/input";
						}
						ele= FindElement(driver, fileCheckBox,taggedDocList[i]+" check box xpath", action.SCROLLANDBOOLEAN,5);
						if(ele!=null) {
							if(isSelected(driver, ele, taggedDocList[i]+" check box xpath")) {
								log(LogStatus.PASS, taggedDocList[i]+" file check box is checked", YesNo.No);
								
							}else {
								log(LogStatus.FAIL, taggedDocList[i]+" file check box is not checked after uplaod the document ", YesNo.Yes);
								result.add(taggedDocList[i]+" file check box is not checked after uplaod the document");
							}
						}else {
							log(LogStatus.FAIL, taggedDocList[i]+" file check box is not found so cannot check its selected or not after upload", YesNo.Yes);
							result.add(taggedDocList[i]+" file check box is not found so cannot check its selected or not after upload");
						}
					}
				}else {
					log(LogStatus.FAIL, "tag document window title is not matched so cannot verify tagged document check box :"+tagDocumentList, YesNo.No);
					result.add("tag document window title is not matched so cannot verify tagged document check box :"+tagDocumentList);
					
				}
				driver.close();
				driver.switchTo().window(parentID);
			}else {
				log(LogStatus.FAIL, "No new window is found so cannot verify verify tagged document check box :"+tagDocumentList,YesNo.Yes);
				result.add("No new window is found so cannot verify verify tagged document check box :"+tagDocumentList);
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on tag or documents button so cannot verify tagged document check box :"+tagDocumentList, YesNo.Yes);
			result.add("Not able to click on tag or documents button so cannot verify tagged document check box :"+tagDocumentList);
		}
		return result;
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param environment
	 * @param mode
	 * @param pageName
	 * @param tagDocumentList
	 * @return empty list if execute successfully
	 */
	public List<String> removeTaggedDocumentOnNavatarDocumentPopUp(String environment, String mode,PageName pageName,String tagDocumentList) {
		List<String> res=  new ArrayList<String>();
		if(pageName.toString().equalsIgnoreCase(PageName.TaskRayPage.toString())) {
			switchToFrame(driver, 10, getFrame(PageName.TaskRayPage, 60));
			switchToFrame(driver, 10, getFrame(PageName.NavatarDocumentsPopUpFrameOnTaskRayPage, 10));
		}else if(pageName.toString().equalsIgnoreCase(PageName.DealPage.toString())) {
			switchToFrame(driver,10,getFrame(PageName.NavatarDocumentPopUpOnPipeLinePage, 10));
		}else {
			driver.switchTo().frame(0);
		}
		if(click(driver, getRefreshButton(30), "refresh button", action.BOOLEAN)) {
			List<WebElement> taggedDocumentList=getTaggedDocumentListOnNavatarDocumentsPopUp();
			List<WebElement> removeLinkList=getTaggedDocumentRemoveLinkListOnNavatarDocumentsPopUp();
			String[] splitedDocList=tagDocumentList.split("<break>");
			if(!taggedDocumentList.isEmpty() && !removeLinkList.isEmpty()) {
				for (int i = 0; i <splitedDocList.length ; i++) {
					for (int j = 0; j < taggedDocumentList.size(); j++) {
						String docName=taggedDocumentList.get(j).getText().trim();
						String removeLink = removeLinkList.get(j).getText().trim();
						if(docName.equalsIgnoreCase(splitedDocList[i]) && removeLink.equalsIgnoreCase("Remove")) {
							log(LogStatus.PASS, "tagged document name is matched and remove link is available in fornt of document name ", YesNo.No);
							if(click(driver, removeLinkList.get(j), "remove link", action.SCROLLANDBOOLEAN)) {
								log(LogStatus.PASS, "clicked on remove link document "+docName,YesNo.No);
								if(click(driver, getNavatarDocumentPopUpRemoveButton(20), docName+" : remove button", action.BOOLEAN)) {
									log(LogStatus.PASS, "clicked on remove button", YesNo.No);
									break;
								}else {
									log(LogStatus.FAIL, "Not able to click on remove button so cannot remove document "+docName, YesNo.Yes);
									res.add("Not able to click on remove link so cannot remove document "+docName);
								}
							}else {
								log(LogStatus.FAIL, "Not able to click on remove link so cannot remove document "+docName, YesNo.Yes);
								res.add("Not able to click on remove link so cannot remove document "+docName);
							}
						}else {
							if(j==taggedDocumentList.size()-1) {
								log(LogStatus.FAIL, "document is not available in navatar document pop up:  "+splitedDocList[i], YesNo.Yes);
								res.add("document is not available in navatar document pop up:  "+splitedDocList[i]);
							}
						}
					}
				}
			}else {
				log(LogStatus.FAIL, "Not able to fond tagged documents list and remove link list so cannot verify tagged documents on "+pageName, YesNo.Yes);
				res.add("Not able to fond tagged documents list and remove link list so cannot verify tagged documents on "+pageName);
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on refresh button so cannot verify tagged documents on "+pageName, YesNo.Yes);
			res.add("Not able to click on refresh button so cannot verify tagged documents on "+pageName);
		}
		switchToDefaultContent(driver);
		return res;
		
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param environment
	 * @param mode
	 * @param pageName
	 * @param documentName
	 * @param updatedDocumentName
	 * @param boxUserName
	 * @param BoxPassword
	 * @param BoxLoginStatus
	 * @param ErrorMsgCheck
	 * @return true/false
	 */
	public boolean verifyDocumentOpenAndDownloadFunctionality(String environment, String mode,PageName pageName,String documentName, String updatedDocumentName,String boxUserName,String BoxPassword, YesNo BoxLoginStatus, YesNo ErrorMsgCheck,String[] parentIdAndchildWindowId) {
		boolean flag= false;
		String parentID =null;
		String secondChildWindowId=null;
		String documentLink="";
		BoxPageBusinesslayer box = new BoxPageBusinesslayer(driver);
		if(pageName.toString().equalsIgnoreCase(PageName.TaskRayPage.toString())) {
			switchToFrame(driver, 10, getFrame(PageName.TaskRayPage, 60));
			switchToFrame(driver, 10, getFrame(PageName.NavatarDocumentsPopUpFrameOnTaskRayPage, 10));
		}else if (pageName.toString().equalsIgnoreCase(PageName.FormulaFieldTagDocuments.toString()) || pageName.toString().equalsIgnoreCase(PageName.CommUnityUserNavatarDocumentPage.toString())) {
			
				driver.switchTo().frame(0);
		}else {
			switchToFrame(driver,10,getFrame(PageName.NavatarDocumentPopUpOnPipeLinePage, 10));
		}
		if(click(driver, getRefreshButton(30), "refresh button", action.BOOLEAN)) {
			List<WebElement> taggedDocumentList=getTaggedDocumentListOnNavatarDocumentsPopUp();
			if(!taggedDocumentList.isEmpty()) {
				for (int j = 0; j < taggedDocumentList.size(); j++) {
					String docName=taggedDocumentList.get(j).getText().trim();

					if(docName.equalsIgnoreCase(documentName)) {
						log(LogStatus.PASS, "tagged document name is matched and remove link is available in fornt of document name ", YesNo.No);
						if(pageName.toString().equalsIgnoreCase(PageName.FormulaFieldTagDocuments.toString()) || pageName.toString().equalsIgnoreCase(PageName.CommUnityUserNavatarDocumentPage.toString())) {
							documentLink=taggedDocumentList.get(j).getAttribute("href");
							String splitedURL = documentLink.split("'")[1];
							System.err.println("URL : "+splitedURL);
							try {
								documentLink =URLDecoder.decode(splitedURL, "UTF-8");
								System.err.println("Decode URL : "+documentLink);
							} catch (UnsupportedEncodingException e) {
								// TODO Auto-generated catch block
								log(LogStatus.FAIL, "decode document URL "+documentName+" so cannot check document", YesNo.Yes);
								e.printStackTrace();
								driver.close();
								driver.switchTo().window(parentID);
								return false;
							}
						}else {
							documentLink=taggedDocumentList.get(j).getAttribute("href");
						}
						
						if(clickUsingJavaScript(driver, taggedDocumentList.get(j), "document link")) {
							log(LogStatus.PASS, "Clicked on Doc Name Link : "+docName, YesNo.No);
							
							if(parentIdAndchildWindowId!=null) {
								Set<String> lst1 = driver.getWindowHandles();
								Iterator<String> I2 = lst1.iterator();
								while (I2.hasNext()) {
									String windowID = I2.next();
									if (windowID.equalsIgnoreCase(parentIdAndchildWindowId[0]) || windowID.equalsIgnoreCase(parentIdAndchildWindowId[1])) {
										appLog.info("Parent id or first window id is Matched");
									} else {
										secondChildWindowId = windowID;
										appLog.info("got it tag document Window id");
										driver.switchTo().window(secondChildWindowId);
										break;
									}
								}
								parentID=parentIdAndchildWindowId[0];
							}else {
								parentID=switchOnWindow(driver);
							}
							if(parentID!=null) {
								log(LogStatus.PASS, "Box window is open", YesNo.No);
								if(BoxLoginStatus.toString().equalsIgnoreCase(YesNo.No.toString())) {
									if(box.boxLogin(boxUserName,BoxPassword, BoxLoginStatus)) {
										log(LogStatus.PASS, "Successfully Login in Box : "+boxUserName, YesNo.No);
										System.err.println("Link : "+documentLink);
										driver.get(documentLink);

									}else {
										log(LogStatus.PASS, "Not able to login in Box : "+boxUserName+"so cannot check document : "+documentName, YesNo.Yes);
										driver.close();
										driver.switchTo().window(parentID);
										return false;
									}
								}else {
									log(LogStatus.PASS, "user is already loggedIn in Box "+boxUserName, YesNo.No);
								}
								
								if(ErrorMsgCheck.toString().equalsIgnoreCase(YesNo.No.toString())) {
									WebElement ele = isDisplayed(driver, FindElement(driver, "//span[@class='menu-toggle' or @class='btn-content']/span[text()='Open']", "document open button", action.BOOLEAN, 60), "visibility", 60, "document open button in box");
									if(ele!=null) {
										log(LogStatus.PASS, "Document is open Successfully "+docName, YesNo.No);
										if(updatedDocumentName!=null) {
											docName=updatedDocumentName;
										}
										String x ="//span[@class='item-name'][text()='"+docName+"']";
										ele = isDisplayed(driver, FindElement(driver,x, "document name xpath", action.BOOLEAN, 10), "visibility", 10, "document name xpath");
										if(ele!=null) {
											log(LogStatus.PASS, "Document name : "+docName+" is matched successfully", YesNo.No);
											flag=true;
										}else {
											log(LogStatus.FAIL, "Document name : "+docName+" is not matched", YesNo.Yes);
										}
									}else {
										log(LogStatus.FAIL, "Document is not open in box : "+docName, YesNo.Yes);
									}
								}else {
									String expMsg=documentNotFoundErrorMsg;
									WebElement ele = getDocumentNotFoundErrorMsg(10);
									if(ele!=null) {
										String msg = ele.getText().trim();
										if(msg.equalsIgnoreCase(expMsg)) {
											log(LogStatus.PASS, "Error Message is verified "+expMsg, YesNo.No);
											flag=true;
										}else {
											log(LogStatus.FAIL,  "Error Message is not  verified for document  "+docName+" Actual Result: "+msg+" Expected Result : "+expMsg, YesNo.Yes);
										}
									}else {
										log(LogStatus.FAIL,"Error Message xpath is not found "+expMsg, YesNo.Yes);
									}
								}
								driver.close();
								driver.switchTo().window(parentID);
								return flag;
							}else {
								log(LogStatus.FAIL, "No new window is open after click on document name "+docName+" so cannot check document open or download fuctionality",YesNo.Yes);
							}
						}else {
							log(LogStatus.FAIL, "Not able to click on document Name link : "+docName, YesNo.Yes);
						}
						break;
					}else {
						if(j==taggedDocumentList.size()-1) {
							log(LogStatus.FAIL, "document is not available in navatar document pop up:  "+documentName, YesNo.Yes);
						}
					}
				}
			}else {
				log(LogStatus.FAIL, "Not able to fond tagged documents list and remove link list so cannot verify tagged documents on "+pageName, YesNo.Yes);
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on refresh button so cannot verify tagged documents on "+pageName, YesNo.Yes);
		}
		switchToDefaultContent(driver);
		return flag;
		
	}
	
	
	public boolean verifyDocumentOpenAndDownloadFunctionality(String environment, String mode,PageName pageName,String documentName) {
		boolean flag= false;
		if(pageName.toString().equalsIgnoreCase(PageName.TaskRayPage.toString())) {
			switchToFrame(driver, 10, getFrame(PageName.TaskRayPage, 60));
			switchToFrame(driver, 10, getFrame(PageName.NavatarDocumentsPopUpFrameOnTaskRayPage, 10));
		}else {
			switchToFrame(driver,10,getFrame(PageName.NavatarDocumentPopUpOnPipeLinePage, 10));
		}
		if(click(driver, getRefreshButton(30), "refresh button", action.BOOLEAN)) {
			List<WebElement> taggedDocumentList=getTaggedDocumentListOnNavatarDocumentsPopUp();
			if(!taggedDocumentList.isEmpty()) {
				for (int j = 0; j < taggedDocumentList.size(); j++) {
					String docName=taggedDocumentList.get(j).getText().trim();
					if(docName.equalsIgnoreCase(documentName)) {
						log(LogStatus.PASS, "tagged document name is matched and remove link is available in fornt of document name ", YesNo.No);
						if(clickUsingJavaScript(driver, taggedDocumentList.get(j), "document link")) {
							log(LogStatus.PASS, "Clicked on Doc Name Link : "+docName, YesNo.No);
							String parentId=switchOnWindow(driver);
							if(parentId!=null) {
								ThreadSleep(5000);
								log(LogStatus.PASS, "Navatar Document window is open", YesNo.No);
								WebElement ele = getDocumentNotFoundErrorMsgForIP(60);
								if(ele!=null) {
									log(LogStatus.PASS, "Error Message is verified : Document not found. ", YesNo.No);
										flag=true;
									}else {
										log(LogStatus.FAIL,  "Error Message is not  verified for document  "+docName+" Expected Result : Document not found.", YesNo.Yes);
									}
								driver.close();
								driver.switchTo().window(parentId);
								return flag;
							}else {
								log(LogStatus.FAIL, "No new window is open after click on document name "+docName+" so cannot check document open or download fuctionality",YesNo.Yes);
							}
						}else {
							log(LogStatus.FAIL, "Not able to click on document Name link : "+docName, YesNo.Yes);
						}
						break;
					}else {
						if(j==taggedDocumentList.size()-1) {
							log(LogStatus.FAIL, "document is not available in navatar document pop up:  "+documentName, YesNo.Yes);
						}
					}
				}
			}else {
				log(LogStatus.FAIL, "Not able to fond tagged documents list and remove link list so cannot verify tagged documents on "+pageName, YesNo.Yes);
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on refresh button so cannot verify tagged documents on "+pageName, YesNo.Yes);
		}
		switchToDefaultContent(driver);
		return flag;
		
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param folderStructure
	 * @param destinationFolder
	 * @param DocOrFolderName
	 * @param boxActions
	 * @return empty list if execute successfully
	 */
	public List<String> MoveOrCopyFileOrFolderInBoxFromDocumentTab(String folderStructure,String destinationFolder,String DocOrFolderName,BoxActions boxActions) {
		String[] folderstruct=null;
		if(folderStructure!=null) {

			folderstruct = folderStructure.split("/");
			
		}
		String[] dstFolderStruct=destinationFolder.split("/");
		String[] documentOrFolderNames=DocOrFolderName.split("<break>");
		List<String> res = new ArrayList<String>();
		String xpath="";
		WebElement ele=null;
		ThreadSleep(10000);
		switchToFrame(driver, 30, getFrame(PageName.DocumentsPageFrameOnDealPage, 30));
		ThreadSleep(20000);
		driver.switchTo().frame(0);
		System.err.println("Successfully switch inside box frame ");
//		click(driver, getBoxHomeLogoOnDocumentTab(10), "box home logo link", action.BOOLEAN);
		if(folderStructure!=null) {
			for(int i=0; i<folderstruct.length; i++) {
				xpath="//a[contains(text(),'"+folderstruct[i]+"')]";
				ele=FindElement(driver, xpath, folderstruct[i]+" folder name xpath", action.SCROLLANDBOOLEAN, 10);
				if(ele!=null) {
					log(LogStatus.PASS, "folder name is found "+folderstruct[i], YesNo.No);
					if(click(driver, ele, folderstruct[i]+" folder name xpath", action.BOOLEAN)) {
						log(LogStatus.PASS, "Clicked on Folder Name : "+folderstruct[i], YesNo.No);
						
					}else {
						log(LogStatus.FAIL, "Not able to click on folder structure so cannot perform "+boxActions.toString()+" action in box ", YesNo.Yes);
						res.add("Not able to click on folder structure so cannot perform "+boxActions.toString()+" action in box ");
						switchToDefaultContent(driver);
						return res;
					}
				}else {
					log(LogStatus.FAIL, "Folder name is not found in box : "+folderstruct[i]+" so cannot perform  "+boxActions.toString()+" action in box",YesNo.Yes);
					res.add("Folder name is not found in box : "+folderstruct[i]+" so cannot perform  "+boxActions.toString()+" action in box");
					switchToDefaultContent(driver);
					return res;
				}
			}
		}else {
			ThreadSleep(20000);
		}
		for (int i = 0; i < documentOrFolderNames.length; i++) {
			if(clickOnFolderOrDocumentMoreOptions(documentOrFolderNames[i], boxActions)) {
				log(LogStatus.PASS, "Clicked on "+documentOrFolderNames[i]+" more button "+boxActions.toString(), YesNo.No);
				
				for(int i1=0; i1<dstFolderStruct.length; i1++) {
					xpath="//span[@title='"+dstFolderStruct[i1]+"']";
					ele=FindElement(driver, xpath, dstFolderStruct[i1]+" folder name xpath in move or copy popup", action.BOOLEAN, 10);
					if(ele!=null) {
						log(LogStatus.PASS, "folder name is found "+dstFolderStruct[i1]+" in move or copy popup", YesNo.No);
						if(clickUsingJavaScript(driver, ele, dstFolderStruct[i1]+" folder name xpath")) {
							log(LogStatus.PASS, "Clicked on Folder Name : "+dstFolderStruct[i1]+" in more or copy popup", YesNo.No);

						}else {
							log(LogStatus.FAIL, "Not able to click on folder structure in move or copy popup so cannot perform "+boxActions.toString()+" action in box ", YesNo.Yes);
							res.add("Not able to click on folder structure in move or copy popup so cannot perform "+boxActions.toString()+" action in box ");
							clickOnMoveOrCancelOrCopyButton(Buttons.cancel);
							return res;
						}
					}else {
						log(LogStatus.FAIL, "Folder name is not found in move or copy : "+dstFolderStruct[i1]+" so cannot move or copy ",YesNo.Yes);
						res.add("Folder name is not found in move or copy : "+dstFolderStruct[i1]+" so cannot move or copy ");
						clickOnMoveOrCancelOrCopyButton(Buttons.cancel);
						return res;
					}
				}
				if(clickOnMoveOrCancelOrCopyButton(Buttons.move)) {
					log(LogStatus.PASS, "clciked on move button", YesNo.No);
				}else {
					log(LogStatus.FAIL, "Not able to click on move button so cannot move file in "+destinationFolder,YesNo.Yes);
					res.add("Not able to click on move button so cannot move file in "+destinationFolder);
				}
			}else {
				log(LogStatus.FAIL, "Not able to click on "+documentOrFolderNames[i]+"  more options so cannot perform "+boxActions.toString(),YesNo.Yes);
				res.add("Not able to click on "+documentOrFolderNames[i]+"  more options so cannot perform "+boxActions.toString());
			}
		}
		switchToDefaultContent(driver);
		return res;
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param docmentOrFolderName
	 * @param boxActions
	 * @return true/false
	 */
	public boolean clickOnFolderOrDocumentMoreOptions(String docmentOrFolderName, BoxActions boxActions) {
		WebElement ele= null;
		String xpath="",xpath1="",renameXpath="", moreOptionsXpath="";
		xpath="//a[contains(text(),'"+docmentOrFolderName+"')]";
		ele= FindElement(driver, xpath,xpath+" xpath ", action.SCROLLANDBOOLEAN,5);
		if(ele!=null) {
			String id=ele.getAttribute("href").trim();
			String[] spltId = id.split("/");
			xpath1=xpath+"/../../../following-sibling::div[@class='file-list-item-actions']/button[@aria-label='More Options']";
			ele= FindElement(driver, xpath1,docmentOrFolderName+" more options button", action.SCROLLANDBOOLEAN,30);
			if(ele!=null) {
				if(clickUsingJavaScript(driver, ele, docmentOrFolderName+" more options button")) {
					log(LogStatus.PASS, docmentOrFolderName+" clicked on More Options", YesNo.No);
					xpath="//ul[@data-resin-file_id='"+spltId[spltId.length-1]+"' or @data-resin-folder_id='"+spltId[spltId.length-1]+"']";
					if(boxActions.toString().equalsIgnoreCase(BoxActions.Rename.toString()) || boxActions.toString().equalsIgnoreCase(BoxActions.Trash.toString()) ) {
						moreOptionsXpath="//li/button[text()='More Actions']";
						renameXpath="/following-sibling::ul/li/button[contains(text(),'"+boxActions.toString()+"')]";
						ThreadSleep(1000);
						ele= FindElement(driver, xpath+moreOptionsXpath,docmentOrFolderName+" more Actions xpath", action.SCROLLANDBOOLEAN,5);
						if(clickUsingJavaScript(driver, ele,docmentOrFolderName+" more options "+boxActions.toString()+" xpath")) {
							log(LogStatus.PASS, "clicked on "+boxActions.toString()+" button for document : "+docmentOrFolderName, YesNo.No);
							ThreadSleep(500);
							ele= FindElement(driver, xpath+moreOptionsXpath+renameXpath,docmentOrFolderName+" more options "+boxActions.toString()+" xpath", action.SCROLLANDBOOLEAN,5);
							if(clickUsingJavaScript(driver, ele,docmentOrFolderName+" more Actions "+boxActions.toString()+" xpath")) {
								log(LogStatus.PASS, "Clicked on "+boxActions.toString()+" successfully", YesNo.No);
								return true;
							}else {
								log(LogStatus.FAIL, "Not able to click on more actions "+boxActions.toString()+" for "+docmentOrFolderName, YesNo.Yes);
							}
						}else {
							log(LogStatus.FAIL, "Not able to click on docment "+docmentOrFolderName+" "+boxActions.toString(), YesNo.Yes);
						}
					}else {
						ThreadSleep(1000);
						moreOptionsXpath="//li/button[text()='"+boxActions.toString()+"']";
						ele= FindElement(driver, xpath+moreOptionsXpath,docmentOrFolderName+" more options "+boxActions.toString()+" xpath", action.SCROLLANDBOOLEAN,5);
						if(clickUsingJavaScript(driver, ele,docmentOrFolderName+" more options "+boxActions.toString()+" xpath")) {
							log(LogStatus.PASS, "clicked on "+boxActions.toString()+" button for document : "+docmentOrFolderName, YesNo.No);
							return true;
						}else {
							log(LogStatus.FAIL, "Not able to click on docment "+docmentOrFolderName+" "+boxActions.toString(), YesNo.Yes);
						}
					}
				}else {
					log(LogStatus.FAIL, "Not able to click on document "+docmentOrFolderName+" more options", YesNo.Yes);
				}
			}else {
				log(LogStatus.FAIL, "Not able to click on "+docmentOrFolderName+" on more options", YesNo.Yes);
			}
			
		}else {
			log(LogStatus.FAIL, "Not able to find "+docmentOrFolderName+" so cannot click on more options", YesNo.Yes);
		}
		return false;
		
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param button
	 * @return true/false
	 */
	public boolean clickOnMoveOrCancelOrCopyButton(Buttons button) {
		return clickUsingJavaScript(driver, getMoveOrCopyOrCancelButtonOnMoveOrCopyPopUp(button, 10),button.toString()+" button on move or copy popup");
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param folderStructure
	 * @param DocOrFolderName
	 * @param updateDocOrFolderName
	 * @param boxActions
	 * @return empty list if execute successfully
	 */
	public List<String> renameFileOrFolderInBoxFromDocumentTab(String folderStructure,String DocOrFolderName,String updateDocOrFolderName,BoxActions boxActions) {
		String[] folderstruct=null;
		if(folderStructure!=null) {

			folderstruct = folderStructure.split("/");

		}
		List<String> res = new ArrayList<String>();
		String xpath="";
		WebElement ele=null;
		ThreadSleep(10000);
		switchToFrame(driver, 30, getFrame(PageName.DocumentsPageFrameOnDealPage, 30));
		ThreadSleep(20000);
		driver.switchTo().frame(0);
		System.err.println("Successfully switch inside box frame ");
		//		click(driver, getBoxHomeLogoOnDocumentTab(10), "box home logo link", action.BOOLEAN);
		if(folderStructure!=null) {
			ThreadSleep(20000);
			for(int i=0; i<folderstruct.length; i++) {
				xpath="//a[contains(text(),'"+folderstruct[i]+"')]";
				ele=FindElement(driver, xpath, folderstruct[i]+" folder name xpath", action.SCROLLANDBOOLEAN, 60);
				if(ele!=null) {
					log(LogStatus.PASS, "folder name is found "+folderstruct[i], YesNo.No);
					if(clickUsingJavaScript(driver, ele, folderstruct[i]+" folder name xpath")) {
						log(LogStatus.PASS, "Clicked on Folder Name : "+folderstruct[i], YesNo.No);
						ThreadSleep(20000);

					}else {
						log(LogStatus.FAIL, "Not able to click on folder structure so cannot perform "+boxActions.toString()+" action in box ", YesNo.Yes);
						res.add("Not able to click on folder structure so cannot perform "+boxActions.toString()+" action in box ");
						switchToDefaultContent(driver);
						return res;
					}
				}else {
					log(LogStatus.FAIL, "Folder name is not found in box : "+folderstruct[i]+" so cannot perform  "+boxActions.toString()+" action in box",YesNo.Yes);
					res.add("Folder name is not found in box : "+folderstruct[i]+" so cannot perform  "+boxActions.toString()+" action in box");
					switchToDefaultContent(driver);
					return res;
				}
			}
		}else {
			ThreadSleep(20000);
		}
		if(clickOnFolderOrDocumentMoreOptions(DocOrFolderName, boxActions)) {
			log(LogStatus.PASS, "Clicked on "+DocOrFolderName+" more button "+boxActions.toString(), YesNo.No);
			ThreadSleep(2000);
			ele=getBoxRenameTextBoxInDocuments(20);
			if(ele!=null) {
				if(sendKeys(driver, ele, updateDocOrFolderName, "rename text box", action.SCROLLANDBOOLEAN)) {
					log(LogStatus.PASS, "Passed value in rename text box "+updateDocOrFolderName, YesNo.No);
					if(clickUsingJavaScript(driver, getBoxRenamePopUpOkayAndCancelInDocuments(Buttons.Okay, 10),"rename popup okay button")) {
						log(LogStatus.PASS, "clicked on rename okay button", YesNo.No);

					}else {
						log(LogStatus.FAIL, "Not able to click on okay button so cannot update "+DocOrFolderName+" to "+updateDocOrFolderName, YesNo.Yes);
						clickUsingJavaScript(driver, getBoxRenamePopUpOkayAndCancelInDocuments(Buttons.Okay, 10),"rename popup okay button");
						res.add("Not able to click on okay button so cannot update "+DocOrFolderName+" to "+updateDocOrFolderName);
					}
				}else {
					log(LogStatus.FAIL, "Not able to pass value in rename text "+updateDocOrFolderName+" so cannot update "+DocOrFolderName, YesNo.Yes);
					clickUsingJavaScript(driver, getBoxRenamePopUpOkayAndCancelInDocuments(Buttons.Okay, 10),"rename popup okay button");
					res.add("Not able to pass value in rename text "+updateDocOrFolderName+" so cannot update "+DocOrFolderName);
				}
			}else {
				log(LogStatus.FAIL, "Not able to find rename text box so cannot rename "+DocOrFolderName+" to "+updateDocOrFolderName, YesNo.Yes);
				res.add("Not able to find rename text box so cannot rename "+DocOrFolderName+" to "+updateDocOrFolderName);
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on "+DocOrFolderName+"  more options so cannot perform "+boxActions.toString(),YesNo.Yes);
			res.add("Not able to click on "+DocOrFolderName+"  more options so cannot perform "+boxActions.toString());
		}
		switchToDefaultContent(driver);
		return res;
	}
	
	
	/**
	 * @author ANKIT JAISWAL
	 * @param environment
	 * @param mode
	 * @param pageName
	 * @param folderpath
	 * @param documentName
	 * @return empty list if executed successfully
	 */
	public List<String> verifyFolderStructureInTagDocumentPopUp(String environment, String mode,PageName pageName,String folderpath, String documentName){
		List<String> result= new ArrayList<String>();
		if(pageName.toString().equalsIgnoreCase(PageName.TaskRayPage.toString())) {
			switchToFrame(driver, 10, getFrame(PageName.TaskRayPage, 60));
			switchToFrame(driver, 10, getFrame(PageName.NavatarDocumentsPopUpFrameOnTaskRayPage, 10));
		}else {
			switchToFrame(driver,10,getFrame(PageName.NavatarDocumentPopUpOnPipeLinePage, 10));
		}
		if(click(driver,getTagOrUploadDocumentsButton(30), "tag or document button", action.SCROLLANDBOOLEAN)) {
			log(LogStatus.PASS, "clicked on tag or document button", YesNo.No);
			ThreadSleep(5000);
			String parentID=switchOnWindow(driver);
			if(parentID!=null) {
				ThreadSleep(3000);
				if(matchTitle(driver, "Tag Documents", 60)) {
					log(LogStatus.PASS, "tag document window title is matched. window is open successfully and ready to verify tag documents check box", YesNo.No);
					
					if(verifyFolderStructure(driver, folderpath)) {
						log(LogStatus.PASS, "Clicked on Folder Structure  : "+folderpath, YesNo.No);
						
						if(documentName!=null) {
							String[] spltDocName=documentName.split("<break>");
							for (int i = 0; i < spltDocName.length; i++) {
								String xpath="//span[text()='"+folderpath.split("/")[folderpath.split("/").length-1]+"']/../preceding-sibling::span[2]/../following-sibling::ul//li[text()='"+spltDocName[i]+"']";
								WebElement ele = FindElement(driver, xpath, spltDocName[i]+" document xpath", action.SCROLLANDBOOLEAN,5);
								if(ele!=null) {
									log(LogStatus.PASS, "document is verified "+spltDocName[i], YesNo.No);
								}else {
									log(LogStatus.FAIL, "document is not verified "+spltDocName[i], YesNo.Yes);
									result.add("document is not verified "+spltDocName[i]);
								}
								
							}
						}
					}else {
						log(LogStatus.FAIL, "Not able to click on Folder Structure : "+folderpath, YesNo.Yes);
						result.add("Not able to click on Folder Structure : "+folderpath);
						return result;
					}
				}else {
					log(LogStatus.FAIL, "tag document window title is not matched so cannot verify folder structrue: "+folderpath, YesNo.No);
					result.add("tag document window title is not matched so cannot verify folder structure: "+folderpath);
					
				}
				driver.close();
				driver.switchTo().window(parentID);
			}else {
				log(LogStatus.FAIL, "No new window is found so cannot verify folder structrue: "+folderpath,YesNo.Yes);
				result.add("No new window is found so cannot verify folder structrue: "+folderpath);
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on tag or documents button so cannot verify folder structrue: "+folderpath, YesNo.Yes);
			result.add("Not able to click on tag or documents button so cannot verify folder structrue: "+folderpath);
		}
		return result;
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param environment
	 * @param mode
	 * @param dealName
	 * @param updatedDealName
	 * @return empty list if executed successfully
	 */
	public boolean updateDealName(String environment, String mode,String dealName,String updatedDealName) {
		if(click( driver, getEditButton(20),"edit button", action.BOOLEAN)) {
			log(LogStatus.PASS, "clicked on edit button", YesNo.No);
			ThreadSleep(5000);
			if(sendKeys(driver, getDealName(environment,mode,60), updatedDealName, "deal name text box ", action.SCROLLANDBOOLEAN)) {
				log(LogStatus.PASS, "pass value in deal Name text box : "+updatedDealName ,YesNo.No);
				if (click(driver, getSaveButton(environment, mode, 60), "Save Button", action.BOOLEAN)) {
					return true;
				} else {
					log(LogStatus.ERROR, "Not able to click on deal Save button so cannot create dea; "+dealName, YesNo.Yes);
				}
			}else {
				log(LogStatus.FAIL, "Not able to pass deal "+updatedDealName+" so cannot update deal Name",YesNo.Yes);
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on edit button so cannot update deal Name "+updatedDealName,YesNo.Yes);
		}
		return false;
	}

	/**
	 * @author ANKIT JAISWAL
	 * @param folderStructure
	 * @param DocOrFolderName
	 * @param boxActions
	 * @return empty list of string if executed successfully.
	 */
	public List<String> deleteFileOrFolderInBoxFromDocumentTab(String folderStructure,String DocOrFolderName,BoxActions boxActions) {
		String[] folderstruct=null;
		if(folderStructure!=null) {

			folderstruct = folderStructure.split("/");
			
		}
		String[] documentOrFolderNames=DocOrFolderName.split("<break>");
		List<String> res = new ArrayList<String>();
		String xpath="";
		WebElement ele=null;
		ThreadSleep(10000);
		switchToFrame(driver, 30, getFrame(PageName.DocumentsPageFrameOnDealPage, 30));
		ThreadSleep(10000);
		driver.switchTo().frame(0);
		System.err.println("Successfully switch inside box frame ");
//		click(driver, getBoxHomeLogoOnDocumentTab(10), "box home logo link", action.BOOLEAN);
		if(folderStructure!=null) {
			for(int i=0; i<folderstruct.length; i++) {
				xpath="//a[contains(text(),'"+folderstruct[i]+"')]";
				ele=FindElement(driver, xpath, folderstruct[i]+" folder name xpath", action.SCROLLANDBOOLEAN, 10);
				if(ele!=null) {
					log(LogStatus.PASS, "folder name is found "+folderstruct[i], YesNo.No);
					if(clickUsingJavaScript(driver, ele, folderstruct[i]+" folder name xpath")) {
						log(LogStatus.PASS, "Clicked on Folder Name : "+folderstruct[i], YesNo.No);
						
					}else {
						log(LogStatus.FAIL, "Not able to click on folder structure so cannot perform "+boxActions.toString()+" action in box ", YesNo.Yes);
						res.add("Not able to click on folder structure so cannot perform "+boxActions.toString()+" action in box ");
						switchToDefaultContent(driver);
						return res;
					}
				}else {
					log(LogStatus.FAIL, "Folder name is not found in box : "+folderstruct[i]+" so cannot perform  "+boxActions.toString()+" action in box",YesNo.Yes);
					res.add("Folder name is not found in box : "+folderstruct[i]+" so cannot perform  "+boxActions.toString()+" action in box");
					switchToDefaultContent(driver);
					return res;
				}
			}
			ThreadSleep(10000);
		}else {
			ThreadSleep(10000);
		}
		for (int i = 0; i < documentOrFolderNames.length; i++) {
			if(clickOnFolderOrDocumentMoreOptions(documentOrFolderNames[i], boxActions)) {
				log(LogStatus.PASS, "Clicked on "+documentOrFolderNames[i]+" more button "+boxActions.toString(), YesNo.No);
				ThreadSleep(5000);
				ele = isDisplayed(driver, FindElement(driver, "//button[@class='btn btn-primary popup-confirm-btn']/span[text()='Okay']", "delete "+documentOrFolderNames[i]+" Okay button xpath", action.SCROLLANDBOOLEAN, 30), "visibility", 30,"delete "+documentOrFolderNames[i]+" Okay button xpath");
				if(clickUsingJavaScript(driver, ele, "delete "+documentOrFolderNames[i]+" Okay button xpath")) {
					log(LogStatus.PASS, "clicked on document/file "+documentOrFolderNames[i]+" Okay button", YesNo.No);
				}else {
					log(LogStatus.FAIL, "Not able to click on Okay button so cannot delete file/folder :  "+documentOrFolderNames[i], YesNo.Yes);
					res.add("Not able to click on Okay button so cannot delete file/folder :  "+documentOrFolderNames[i]);
				}
			}else {
				log(LogStatus.FAIL, "Not able to click on "+documentOrFolderNames[i]+"  more options so cannot perform "+boxActions.toString(),YesNo.Yes);
				res.add("Not able to click on "+documentOrFolderNames[i]+"  more options so cannot perform "+boxActions.toString());
			}
		}
		switchToDefaultContent(driver);
		return res;
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param environment
	 * @param mode
	 * @param dealName
	 * @return true/false
	 */
	public boolean clickOnCreatedDeal(String environment,String mode,String dealName){
		if(mode.equalsIgnoreCase(Mode.Classic.toString())){
		if (click(driver, getGoButton(60), "Go Button", action.BOOLEAN)) {
//			WebElement dealname = FindElement(driver,
//					"//div[@class='x-panel-bwrap']//span[text()='" + dealName + "']",
//					"Partnership Legal Name", action.BOOLEAN, 60);
			WebElement dealname = FindElement(driver,
					"//a[text()='" + dealName + "']",
					"Partnership Legal Name", action.BOOLEAN, 60);
			if (dealname != null) {
				if (click(driver, dealname, "Partnership Name", action.SCROLLANDBOOLEAN)) {
					appLog.info("Clicked on partnership name" + dealName + "successfully.");
					return true;
				} else {
					appLog.error("Not able to click on partnership name");
				}
			} else {
				appLog.error("Partnership name is not displaying");
			}
		} else {
			appLog.error("Not able to click on go button so cannot click on created partnership");
		}
	}else{
		if(clickOnAlreadyCreated_Lighting(environment, mode, TabName.DealTab, dealName, 30)){
			appLog.info("Clicked on partnership name" + dealName + "successfully.");
			return true;
		}else{
			appLog.error("Not able to click on partnership name : "+dealName);	
		}
	}
		return false;
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param environment
	 * @param mode
	 * @param dealType
	 * @param dealName
	 * @param accountName
	 * @param source
	 * @param isActive
	 * @param status
	 * @param stage
	 * @return true/false
	 */
	public boolean createDeal(String environment,String mode,String dealType, String dealName, String accountName,String source,boolean isActive,String status,String stage) {
		WebElement ele;
		boolean flag = false;
		ThreadSleep(2000);
		if(click(driver, getNewButton(environment,mode,60), "New Button", action.BOOLEAN)) {
			appLog.info("Clicked on New Button");	
			ThreadSleep(1000);
			if(mode.equalsIgnoreCase(Mode.Classic.toString())){
				if(selectVisibleTextFromDropDown(driver, getDealType(environment,mode,dealType,60), "Deal Type", dealType)){
					appLog.info("Selected Deal type : "+dealType);	

				}else{
					appLog.error("Not Able to Select Deal type : "+dealType);	
				}
			}else{
				ele = getDealType(environment,mode,dealType,60);
				if (click(driver, ele, dealType, action.SCROLLANDBOOLEAN)) {
					appLog.info(" Selected Deal type : "+dealType);	
				}else{
					appLog.error("Not Able to Select Deal type : "+dealType);	
				}
			}
			ThreadSleep(1000);
			if (click(driver, getContinueOrNextButton(environment,mode,60), "Continue Button", action.BOOLEAN)) {
				appLog.info("Clicked on Continue or Nxt Button");	
				ThreadSleep(1000);
				if (sendKeys(driver, getDealName(environment,mode,60), dealName, "Deal Name", action.BOOLEAN)) {
					appLog.info("Successfully Entered value on Deal Name TextBox : "+dealName);		
					ThreadSleep(1000);

					if (accountName != null) {
						if (sendKeys(driver, getLegalName(environment, mode, 60), accountName, "Legal Name",
								action.SCROLLANDBOOLEAN)) {
							if (mode.equalsIgnoreCase(Mode.Lightning.toString())) {
								ThreadSleep(1000);
								if (click(driver,FindElement(driver,"//div[contains(@class,'uiAutocomplete')]//a//div[@title='" + accountName+ "']","Account Name List", action.SCROLLANDBOOLEAN, 30),	accountName + "   :   Legal Name", action.BOOLEAN)) {
									appLog.info(accountName + "  is present in list.");
								} else {
									appLog.info(accountName + "  is not present in the list.");
								}
							}

						} else {
							appLog.error("Not able to enter legal name");
						}


					}

					if(source!=null) {

						if (mode.equalsIgnoreCase(Mode.Classic.toString())) {
							if(selectVisibleTextFromDropDown(driver, getDealSource(environment, mode,60), "Deal Source Drop Down", source)){
								appLog.info("Selected Deal source : "+source);	

							}else{
								appLog.error("Not Able to Select Deal source : "+source);	
							}
						}else {

							if (click(driver, getDealSource(environment, mode, 60), "Deal source : "+source, action.SCROLLANDBOOLEAN)) {
								ThreadSleep(2000);
								appLog.error("Clicked on Deal source");
								WebElement dealSourceEle = FindElement(driver,"//div[@class='select-options']//li/a[@title='"+source+"']", source,	action.SCROLLANDBOOLEAN, 10);
								ThreadSleep(2000);
								if (click(driver, dealSourceEle, source, action.SCROLLANDBOOLEAN)) {
									appLog.info("Selected Deal source : "+source);
								} else {
									appLog.error("Not able to Select on Deal source : "+source);
								}
							} else {
								appLog.error("Not able to Click on Deal source : ");
							}

						}
					}
					if (mode.equalsIgnoreCase(Mode.Classic.toString())) {
						if(selectVisibleTextFromDropDown(driver, getDealStatus(environment, mode,60), "Deal Status Drop Down", status)){
							appLog.info("Selected Deal Status : "+status);	
							ThreadSleep(1000);

							if(selectVisibleTextFromDropDown(driver, getDealStage(environment, mode,60), "Deal Stage Drop Down", stage)){
								appLog.info("Selected Deal stage : "+stage);	

							}else{
								appLog.error("Not Able to Select Deal stage : "+stage);	
							}

						}else{
							appLog.error("Not Able to Select Deal status : "+status);	
						}
					} else {
						if (click(driver, getDealStatus(environment, mode, 60), "Deal Status : "+status, action.SCROLLANDBOOLEAN)) {
							ThreadSleep(2000);
							appLog.error("Clicked on Deal Status");
							WebElement dealStatusEle = FindElement(driver,
									"//div[@class='select-options']//li/a[@title='"+status+"']", status,
									action.SCROLLANDBOOLEAN, 10);
							ThreadSleep(2000);
							if (click(driver, dealStatusEle, status, action.SCROLLANDBOOLEAN)) {
								appLog.info("Selected Deal Status : "+status);
								ThreadSleep(2000);

								if (click(driver, getDealStage(environment, mode, 60), "Deal Status : "+stage, action.SCROLLANDBOOLEAN)) {
									ThreadSleep(2000);
									appLog.error("Clicked on Deal stage");
									WebElement dealStageEle = FindElement(driver,"//div[@class='select-options']//li/a[@title='"+stage+"']", stage,action.SCROLLANDBOOLEAN, 10);
									ThreadSleep(2000);
									if (click(driver, dealStageEle, stage, action.SCROLLANDBOOLEAN)) {
										appLog.info("Selected Deal stage : "+stage);
									} else {
										appLog.error("Not able to Select on Deal stage : "+stage);
									}
								} else {
									appLog.error("Not able to Click on Deal stage : ");
								}

							} else {
								appLog.error("Not able to Select on Deal Status : "+status);
							}
						} else {
							appLog.error("Not able to Click on Deal Status : ");
						}

					}
					
					if (isActive) {
						ThreadSleep(2000);
						if (click(driver, getActiveCheckBox(environment, mode, 10), source, action.SCROLLANDBOOLEAN)) {
							appLog.info("Clicked on Active CheckBox ");
						} else {
							appLog.error("Not Able to Click on Active CheckBox ");
						}	
					}else{
						appLog.error("isActive : "+isActive);	
					}

				} else {
					appLog.error("Not Able to Entered value on Deal Name TextBox : "+dealName);	
				}
			}else{
				appLog.error("Not Able to Click on Continue or Nxt Button");	
			}

			if (click(driver, getSaveButton(environment,mode,30), "Save Button", action.SCROLLANDBOOLEAN)) {
				flag = true;
			}
			
		}else {
			appLog.error("Not Able to Click on New Button");	
		}

	
		return flag;
	}
	
	/**
	 * 
	 * @param excelPath
	 * @param dealName
	 * @param templateName
	 * @param targetName
	 * @param contactName
	 * @param phoneNumber
	 * @param emailID
	 * @param DealRoomAction
	 * @param DealRoom
	 * @param manageApproval
	 * @param watermarking
	 * @param manageApprovalSetting
	 * @param watermarkingSetting
	 * @return true/false
	 */
	public SoftAssert buildDealRoom(String excelPath, String dealName, String templateName, String targetName, String contactName,
			String phoneNumber, String emailID, DealRoomAction DealRoomAction, DealRoomAction DealRoom,
			DealRoomAction manageApproval, DealRoomAction watermarking, String manageApprovalSetting,
			String watermarkingSetting) {
		DealRoomManagerPageBusinessLayer drm = new DealRoomManagerPageBusinessLayer(driver);
		SoftAssert sa = new SoftAssert();
		if (clickOnCreatedDeal(SmokeCommonVariables.EnvironmentVariable,SmokeCommonVariables.ModeVariable, dealName)) {
			scrollDownThroughWebelement(driver, getDealRoomSection(60), "Deal Room Section");
			switchToFrame(driver, 60, getDealRoomFrame(60));
			if (click(driver, getBuildDealRoomButton(60), "Build Deal Room Button", action.BOOLEAN)) {
				if (getDealRoomContact(60).getAttribute("Value").isEmpty()) {
					sendKeys(driver, getDealRoomContact(60), contactName, "Contact Name Text Box",
							action.THROWEXCEPTION);
				}
				if (getDealRoomEmail(60).getAttribute("value").isEmpty()) {
					sendKeys(driver, getDealRoomEmail(60), emailID, "Email ID Text Box", action.THROWEXCEPTION);
				}
				if (getDealRoomPhone(60).getAttribute("value").isEmpty()) {
					sendKeys(driver, getDealRoomPhone(60), phoneNumber, "phone Number Text Box.",
							action.THROWEXCEPTION);
				}
				click(driver, getnextButtonBuildDealRoomStep1of3(60), "Next Button", action.THROWEXCEPTION);
				if (DealRoomAction.toString()
						.equalsIgnoreCase(CommonLib.DealRoomAction.IMPORTFOLDERTEMPLATE.toString())) {
					click(driver, getImportFolderTemplate(60), "Import Folder Template", action.THROWEXCEPTION);
					if (selectFolderTemplateToImport(templateName, "All Templates")) {
						if (click(driver, getFolderTemplateImportButton(60), "Folder Template Import Button",
								action.BOOLEAN)) {
							if (verifyFolderStructure(drm.folderStructureInExcel(excelPath), null, null, null)) {
								appLog.info("Folder structure is successfully verified.");
							} else {
								appLog.info("Folder Structure is not verified.");
							}
						} else {
							appLog.info("Not able to import folder template: " + templateName);
							exit("Not able to import folder template: " + templateName);
						}
					} else {
						appLog.info("Cannot select template: " + templateName);
						exit("Cannot select template: " + templateName);
					}
				} else if (DealRoomAction.toString()
						.equalsIgnoreCase(CommonLib.DealRoomAction.CREATEFOLDERTEMPLATE.toString())) {
					Map<String, String> s = drm.folderStructureInExcel(excelPath);
					Set<String> paths = s.keySet();
					Iterator<String> i = paths.iterator();
					i = paths.iterator();
					while (i.hasNext()) {
						String string = i.next();
						if (string.isEmpty())
							continue;
						System.out.println("\n\n\nCreating folder template\n\n\n");
						drm.createFolderStructure(string, s.get(string), "", "Deal Room");
					}
				} else {
					appLog.info("Building deal room without any folder structure");
				}
				click(driver, getNextButtonStep2of3(60), "Next Button", action.THROWEXCEPTION);
				if (DealRoom.toString().equalsIgnoreCase("WITHTARGET")) {
					String target[] = targetName.split(",");
					for (int i = 0; i < target.length; i++) {
						WebElement ele = FindElement(driver,
								"//span[@title='" + target[i] + "']/../preceding-sibling::span[1]/input",
								"Account Checkbox", action.BOOLEAN, 60);
						if (ele != null) {
							click(driver, ele, "Account Check Box", action.SCROLLANDTHROWEXCEPTION);
							String errormsg = getTargetSuccessfullyAddedMessage(60).getText().trim();
							sa.assertTrue(errormsg.contains(DealPageErrorMessage.targetSuccessfullyAddedMessage),
									"Target Success Message is not matched\tExpected: "
											+ DealPageErrorMessage.targetSuccessfullyAddedMessage + "\tActual: "
											+ errormsg);
							click(driver, getTargetAddedSuccessfullyMessageCloseButton(60), "close Button",
									action.SCROLLANDTHROWEXCEPTION);
						} else {
							appLog.info(target[i] + " is not present in the list.");
							exit(target[i]
									+ " is not present in the list. So cannot continue with the test case execution.");
						}
					}
				} else {
					appLog.info("building the deal room without target.");
				}
				click(driver, getDoneButton(60), "Done Button", action.SCROLLANDTHROWEXCEPTION);
				if (manageApproval != null) {
					if (manageApproval.toString().equalsIgnoreCase("active")) {
						if (getManageApprovalActiveStatusText(10) != null) {
							appLog.info("manage approval status is active and verified.");
						} else {
							appLog.info("Manage Approval Status is inactive.");
							sa.assertTrue(false,
									"Expected: manage approval status to be active.\tActual: Manage Approval Status is inactive.");
						}
					} else {
						if (getManageApprovalInactiveStatus(10) != null) {
							appLog.info("Manage approval status is inactive and veirified.");
						} else {
							appLog.info("Manage Approval Status is active.");
							sa.assertTrue(false,
									"Expected: manage approval status to be inactive.\tActual: Manage Approval Status is active.");
						}
					}
				} else {
					appLog.info("Skipping the manage approval status check.");
				}
				if (manageApprovalSetting != null) {
					if (manageApprovalSetting.equalsIgnoreCase(getManageApprovalSetting(60).getText())) {
						appLog.info("Manage Approval Setting is matched.");
					} else {
						appLog.info("Manage Approval Setting is: " + getManageApprovalSetting(60).getText());
						sa.assertTrue(false, "Manage Approval Setting is not matched\tExpected: "
								+ manageApprovalSetting + "\tActual: " + getManageApprovalSetting(60).getText());
					}
				} else {
					appLog.info("Skipping the manage approval Setting check.");
				}
				if (watermarking != null) {
					if (watermarking.toString().equalsIgnoreCase("active")) {
						if (getWatermarkingActiveStatusText(5) != null) {
							appLog.info("Watermarking status is active and verified.");
						} else {
							appLog.info("Watermarking Status is inactive.");
							sa.assertTrue(false,
									"Expected: Watermarking status to be active.\tActual: Watermarking Status is inactive.");
						}
					} else {
						if (getWatermarkingInactveStatusText(5) != null) {
							appLog.info("Watermarking status is inactive and veirified.");
						} else {
							appLog.info("Watermarking Status is active.");
							sa.assertTrue(false,
									"Expected: Watermarking status to be inactive.\tActual: Watermarking Status is active.");
						}
					}
				} else {
					appLog.info("Skipping the watermarking status check.");
				}
				if (watermarkingSetting != null) {
					if (watermarkingSetting.equalsIgnoreCase(getWatermarkingSetting(60).getText())) {
						appLog.info("Watermarking Setting is matched.");
					} else {
						appLog.info("Watermarking Setting is: " + getWatermarkingSetting(60).getText());
						sa.assertTrue(false, "Watermarking Setting is not matched\tExpected: " + watermarkingSetting
								+ "\tActual: " + getWatermarkingSetting(60).getText());
					}
				} else {
					appLog.info("Skipping the watermarking Setting check.");
				}
			} else {
				exit("Build Deal Room Button is not visible.");
			}
		} else {
			appLog.info("Deal is not found in the list.");
			exit("Deal is not found in the the list.");
		}
		return sa;
	}
	
	/**
	 * 
	 * @param folderTemplateName
	 * @param value
	 * @return true/false true/false
	 */
	public boolean selectFolderTemplateToImport(String folderTemplateName, String value) {
		selectVisibleTextFromDropDown(driver, getFolderTemplateFilterDropDown(60), "Filter", value);
		WebElement ele = FindElement(driver,
				"//span[@id='ImportFolderTemplateGrid-rows']//span[text()='" + folderTemplateName
						+ "']/../preceding-sibling::span//input",
				"Radio button of " + folderTemplateName, action.BOOLEAN, 60);
		if (click(driver, ele, "Radio button of " + folderTemplateName, action.SCROLLANDBOOLEAN)) {
			appLog.info("Successfully selected template: " + folderTemplateName);
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @param path
	 * @param institutionName
	 * @param checkStdFolder
	 * @param pageName
	 * @return true/false true/false
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public boolean verifyFolderStructure(Object path, String institutionName, String checkStdFolder, String pageName) {
		boolean flag = true;
		String insti;
		if (path == "") {
			if (verifyFolderPath("", institutionName, pageName)) {
				appLog.info("Successfully clicked on institution folder.");
				flag = true;
			} else {
				flag = false;
			}
		} else if (path instanceof Map) {
			Set<String> paths = ((Map) path).keySet();
			Iterator<String> i = paths.iterator();
			while (i.hasNext()) {
				String string = (String) i.next();
				if (institutionName != null) {
					if (!((Map) path).get(string).toString().equalsIgnoreCase("Standard")) {
						insti = null;
					} else {
						insti = institutionName;
					}
					if (((Map) path).get(string).toString().equalsIgnoreCase("Standard")
							&& checkStdFolder.equalsIgnoreCase("No")) {
						appLog.info("Standard folder will not be checked");
						break;
					}
					if (verifyFolderPath(string, insti, pageName)) {
						appLog.info("Folder path successfully found: " + string);
					} else {
						appLog.info("Folder path not found: " + string);
						flag = false;
					}
				} else {
					if (verifyFolderPath(string)) {
						appLog.info("Folder path successfully found: " + string);
					} else {
						appLog.info("Folder path not found: " + string);
						flag = false;
					}
				}
			}
		} else if (path instanceof String) {
			String paths[] = ((String) path).split(",");
			for (int i = 0; i < paths.length; i++) {
				if (checkStdFolder.equalsIgnoreCase("Traverse")) {
					if (verifyFolderPath(paths[i], institutionName, pageName)) {
						appLog.info("Folder path successfully found: " + paths[i]);
					} else {
						appLog.info("Folder path not found: " + paths[i]);
						flag = false;
					}
				} else if (verifyFolderPath(paths[i])) {
					appLog.info("Folder path successfully found: " + paths[i]);
				} else {
					appLog.info("Folder path not found: " + paths[i]);
					flag = false;
				}
			}
		}
		if (!flag) {
			appLog.info("Folder Structure is not verified.");
		}
		return flag;
	}
	
	
	/**
	 * 
	 * @param path
	 * @param instituionName
	 * @param pageName
	 * @return true/false true/false
	 */
	public boolean verifyFolderPath(String path, String instituionName, String pageName) {
		// xpath for contact page:
		// //span[contains(text(),'20feb')]/../../../following-sibling::ul/li/div//span[contains(text(),'shared')]/../../following-sibling::ul/li/div//span[contains(text(),'shared1')]
		String xpath = "";
		String path1[] = path.split("/");
		String path2[]=null;
		boolean flag;
		boolean flag1 = false;
		if(pageName!=null){
			if(pageName.equalsIgnoreCase("Contact Page")){
				flag=true;
			} else if (pageName.equalsIgnoreCase("Deal Page")){
				flag=true;
			} else {
				flag=false;
			}
		} else {
			flag=false;
		}
		if(pageName!=null&&pageName.equalsIgnoreCase("Target Side Traverse")){
			System.out.println(">>>>>>Path1 before split>>>>"+path1[0]);
			if(path1[0].contains("(Global)")||path1[0].contains("(Shared)")||path1[0].contains("(Internal)")){
				System.out.println("Global is found.");
				path2=path1[0].split("\\(");
				System.out.println(">>>>Path2>>>"+path2[0]);
				path1[0]=path2[0];
				System.out.println(">>>>Path1>>>"+path1[0]);
			}
			flag=false;
		}
		if(path==""){
			if(click(driver, FindElement(driver, "//span[contains(text(),'All Folders')]/../../../ul/li/div//span[text()='"+instituionName+"']", "Institution Folder", action.BOOLEAN, 60), "Institution Folder", action.SCROLLANDBOOLEAN)){
				appLog.info(instituionName+" :Account Folder is present.");
				return true;
			} else {
				appLog.info(instituionName + " :Account folder is not present.");
				return false;
			}
		}
		if (flag){
			xpath="//span[text()='"+instituionName+"']/../../../following-sibling::ul/li/div//span[text()='"+path1[0]+"']";
		} else {
			if (instituionName != null) {
				if (click(driver,
						FindElement(driver,
								"//span[contains(text(),'All Folders')]/../../../ul/li/div//span[text()='"
										+ instituionName + "']",
								"Institution Folder", action.BOOLEAN, 60),
						"Institution Folder", action.SCROLLANDBOOLEAN)) {
					xpath = "//span[contains(text(),'All Folders')]/../../../ul/li/div//span[contains(text(),'')]/../../following-sibling::ul//span[text()='"
							+ path1[0] + "']";
				} else {
					appLog.info(instituionName + " :Instituion folder is not present.");
					return false;
				}
			} else {
				xpath = "//span[contains(text(),'All Folders')]/../../../ul/li/div//span[text()='" + path1[0]
						+ "']";
			}
		}
		// path1 = path.split("/");
		List<String> notFoundFolders = new ArrayList<String>();
		boolean found = true;
		for (int i = 1; i <= path1.length; i++) {
			if (click(driver, FindElement(driver, xpath, "Folder: " + path1[i - 1], action.BOOLEAN, 10),
					"Folder: " + path1[i - 1], action.SCROLLANDBOOLEAN)) {
				// appLog.info("Folder Found: "+path1[i-1]);
			} else {
				// appLog.info("Folder Not Found: "+path1[i-1]);
				found = false;
				notFoundFolders.add(path1[i - 1]);
			}
			if (i != path1.length)
				xpath = xpath + "/../../following-sibling::ul/li/div//span[text()='" + path1[i] + "']";
		}
		if (!found) {
			for (int k = 0; k < notFoundFolders.size(); k++) {
				if (k == 0)
					appLog.info("List of not found folder of path \"" + path + "\" are: ");
				appLog.info(notFoundFolders.get(k));
			}
		} else {
			appLog.info("Folder path is verified: " + path);
		}
		if(found==false){
			if(isAlertPresent(driver)){
				String msg = switchToAlertAndGetMessage(driver, 30, action.GETTEXT);
				switchToAlertAndAcceptOrDecline(driver, 30, action.ACCEPT);
				if(msg.trim().toLowerCase().contains("error") || msg.trim().toLowerCase().contains("status") || msg.trim().toLowerCase().contains("code")){
					driver.navigate().refresh();
					if(getDealRoomFrame(10)!=null){
						scrollDownThroughWebelement(driver, getDealRoomSection(30), "Deal room view.");
						switchToFrame(driver, 30, getDealRoomFrame(30));
					}
					if(verifyFolderPath(path, instituionName, pageName)){
						found=true;
					} else {
						found = false;
					}
				} else {
					String loc = screenshot(currentlyExecutingTC);
					appLog.error("Folder verification failed due to some intermittent issue. Kindly check the screenshot: "+loc);
					BaseLib.sa.assertTrue(false, "Folder verification failed due to some intermittent issue. Kindly check the screenshot: "+loc);
				}
			} else if (FindElement(driver, "//img[@class='poweredByImage']", null, action.BOOLEAN, 0)!=null){
				flag1=true;
			} else if (FindElement(driver, "//img[@src='/resource/1511337238000/SiteSamples/img/clock.png']", null, action.BOOLEAN, 0)!=null){
				flag1=true;
			} else if (FindElement(driver, "//img[@src='/resource/1511337238000/SiteSamples/img/warning.gif']", null, action.BOOLEAN, 0)!=null){
				flag1=true;
			} else if (FindElement(driver, "//span[@class='title'][text()='Error: Error occurred while loading a Visualforce page.']", null, action.BOOLEAN, 0)!=null){
				flag1=true;
			}
			if(flag1){
				driver.navigate().refresh();
				if(getDealRoomFrame(10)!=null){
					scrollDownThroughWebelement(driver, getDealRoomSection(30), "Deal room view.");
					switchToFrame(driver, 30, getDealRoomFrame(30));
				}
				if(verifyFolderPath(path, instituionName, pageName)){
					found=true;
				} else {
					found = false;
				}
			}
			
		}
		return found;
	}

	
	/**
	 * 
	 * @param path
	 * @return true/false
	 */
	public boolean verifyFolderPath(String path) {
		String path1[] = path.split("/");
		List<String> notFoundFolders = new ArrayList<String>();
		boolean found = true;
		String xpath = "//a[contains(text(),'All Folders')]/../../../ul/li/div//a[contains(text(),'" + path1[0] + "')]";
		String xpath1 = "/../../following-sibling::ul//a[contains(text(),'" + "folderName" + "')]";
		for (int i = 1; i <= path1.length; i++) {
			if (FindElement(driver, xpath, "Folder: " + path1[i - 1], action.BOOLEAN, 10) != null) {
				// appLog.info("Folder Found: "+path1[i-1]);
			} else {
				// appLog.info("Folder Not Found: "+path1[i-1]);
				found = false;
				notFoundFolders.add(path1[i - 1]);
			}
			if (i != path1.length)
				xpath = xpath + "/../../following-sibling::ul//a[contains(text(),'" + path1[i] + "')]";
		}
		if (!found) {
			for (int k = 0; k < notFoundFolders.size(); k++) {
				if (k == 0)
					appLog.info("List of not found folder of path \"" + path + "\" are: ");
				appLog.info(notFoundFolders.get(k));
			}
		} else {
			appLog.info("Folder path is verified: " + path);
		}
		return found;
	}
		
		
	/**
	 * 
	 * @param timeOut
	 * @param path
	 * @param instituionName
	 * @param pageName
	 * @return true/false
	 */
	public boolean verifyFolderPath(int timeOut, String path, String instituionName, String pageName) {

		// xpath for contact page:

		// //span[contains(text(),'20feb')]/../../../following-sibling::ul/li/div//span[contains(text(),'shared')]/../../following-sibling::ul/li/div//span[contains(text(),'shared1')]
		String xpath = "";
		String xpath1 = "//span[contains(text(),'All Folders')]";
		String xpath2 = "";
		String xpath3 = "span";
		String path1[] = path.split("/");
		String path2[]=null;
		boolean flag;
		boolean flag1 = false;
		String expandXpath="";
		WebElement ele1=null;
		if(pageName!=null){
			if(pageName.equalsIgnoreCase("Contact Page")){
				flag=true;
			} else if (pageName.equalsIgnoreCase("Deal Page") && instituionName!=null){ //changed this line added && instituionName!=null
				flag=true;
			} else if (pageName.equalsIgnoreCase("Content Ordering Pop Up")){
				xpath1 = "//div[@id='ContentOrdering_ID']//span[contains(text(),'All Folders')]";
				xpath3 = "b";
				flag = false;
			} else if (pageName.equalsIgnoreCase("Bulk Download CRM") ||  pageName.equalsIgnoreCase("Target Side Bulk Download")){
				xpath1 = "//a[contains(text(),'All Folders')]";
				flag = false;
			} else {
				flag=false;
			}
		} else {
			flag=false;
		}
		System.err.println("Flag VALUE : "+flag);
		if(pageName!=null&& (pageName.equalsIgnoreCase("Target Side Traverse") || pageName.equalsIgnoreCase("Target Side Bulk Download")) ){
			System.out.println(">>>>>>Path1 before split>>>>"+path1[0]);
			if(path1[0].contains("(Global)")||path1[0].contains("(Shared)")||path1[0].contains("(Internal)")){
				System.out.println("Global is found.");
				path2=path1[0].split("\\(");
				System.out.println(">>>>Path2>>>"+path2[0]);
				path1[0]=path2[0];
				System.out.println(">>>>Path1>>>"+path1[0]);
			}
			flag=false;
		}
		if(path==""){
			System.err.println("B;anl path");
			if(click(driver, FindElement(driver, "//span[contains(text(),'All Folders')]/../../../ul/li/div//span[text()='"+instituionName+"']", "Institution Folder", action.BOOLEAN, timeOut), "Institution Folder", action.SCROLLANDBOOLEAN)){
				appLog.info(instituionName+" :Account Folder is present.");
				return true;
			} else {
				appLog.info(instituionName + " :Account folder is not present.");
				return false;
			}
		}
		if (flag){
			xpath="//span[text()='"+instituionName+"']/../../../following-sibling::ul/li/div//span[contains(text(),'"+path1[0]+"')]";
		} else {
			System.err.println("false flag :");
			if (instituionName != null) {

				if(pageName!=null && (pageName.equalsIgnoreCase("Bulk Download CRM") || pageName.equalsIgnoreCase("Target Side Bulk Download"))){
					xpath2 = xpath1+"/../../ul/li//a[text()='" + instituionName + "']";
				} else {
					xpath2 = xpath1+"/../../../ul/li/div//"+ xpath3 +"[text()='" + instituionName + "']";  
				}
				System.err.println("xpath2 value : "+xpath2);



				/*if (accountFolderInt==0) {
							if(pageName!=null && pageName.equalsIgnoreCase("Target Side Bulk Download")){
								click(driver, FindElement(driver, xpath2, "Folder: " , action.BOOLEAN, timeOut),
										"Folder: " , action.SCROLLANDBOOLEAN);
								accountFolderInt++;
							}
						}*/


				if(pageName!=null && (pageName.equalsIgnoreCase("Bulk Download CRM") ||pageName.equalsIgnoreCase("Target Side Bulk Download"))){
					expandXpath = xpath2+"/preceding-sibling::span[@class='dynatree-expander']";
					ele1 = FindElement(driver, expandXpath, "Expand Folder:  >>  ", action.BOOLEAN, 2);
					if (ele1!=null) {
						appLog.info("Folder Already expanded >> : "+expandXpath);
						if (pageName!=null && (pageName.equalsIgnoreCase("Bulk Download CRM") ||pageName.equalsIgnoreCase("Target Side Bulk Download"))) {
							xpath = xpath1 + "/../../ul/li//a[contains(text(),'" + instituionName+ "')]/../following-sibling::ul//a[text()='" + path1[0] + "']";
						} else {
							xpath = xpath1+ "/../../../ul/li/div//"+xpath3+"[contains(text(),'"+instituionName+"')]/../../../following-sibling::ul//span[contains(text(),'"+ path1[0] + "')]";
						}
					} else {
						if (click(driver, FindElement(driver, xpath2, "Institution Folder", action.BOOLEAN, timeOut),
								"Institution Folder", action.SCROLLANDBOOLEAN)) {
							if (pageName!=null && (pageName.equalsIgnoreCase("Bulk Download CRM") ||pageName.equalsIgnoreCase("Target Side Bulk Download"))) {
								xpath = xpath1 + "/../../ul/li//a[contains(text(),'" + instituionName+ "')]/../following-sibling::ul//a[text()='" + path1[0] + "']";
							} else {
								xpath = xpath1+ "/../../../ul/li/div//"+xpath3+"[contains(text(),'"+instituionName+"')]/../../../following-sibling::ul//span[contains(text(),'"+ path1[0] + "')]";
							}
						} else {
							appLog.info(instituionName + " :Instituion folder is not present.");
							return false;
						}
					}

				}else {
					if (click(driver, FindElement(driver, xpath2, "Institution Folder", action.BOOLEAN, timeOut),
							"Institution Folder", action.SCROLLANDBOOLEAN)) {
						if (pageName!=null && (pageName.equalsIgnoreCase("Bulk Download CRM") ||pageName.equalsIgnoreCase("Target Side Bulk Download"))) {
							xpath = xpath1 + "/../../ul/li//a[contains(text(),'" + instituionName+ "')]/../following-sibling::ul//a[text()='" + path1[0] + "']";
						} else {
							xpath = xpath1+ "/../../../ul/li/div//"+xpath3+"[contains(text(),'"+instituionName+"')]/../../../following-sibling::ul//span[contains(text(),'"+ path1[0] + "')]";
						}
					} else {
						appLog.info(instituionName + " :Instituion folder is not present.");
						return false;
					}
				}
			} else {
				if(pageName!=null && (pageName.equalsIgnoreCase("Bulk Download CRM") ||pageName.equalsIgnoreCase("Target Side Bulk Download"))){
					xpath = xpath1+"/../../ul/li//a[text()='" + path1[0]+ "']";
					//a[contains(text(),'All Folders')]/../../ul/li//a[text()='Internal(Internal)']
				} else {
					xpath = xpath1+"/../../../ul/li/div//span[contains(text(),'" + path1[0]
							+ "')]";
				}
			}
		}
		// path1 = path.split("/");
		List<String> notFoundFolders = new ArrayList<String>();
		boolean found = true;
		for (int i = 1; i <= path1.length; i++) {
			String tempXpath = "";
			if (i != path1.length) {
				if(pageName!=null && (pageName.equalsIgnoreCase("Bulk Download CRM") ||pageName.equalsIgnoreCase("Target Side Bulk Download"))){
					tempXpath = xpath + "/../following-sibling::ul/li//a[text()='" + path1[i] + "']";
				} else {
					tempXpath = xpath + "/../../following-sibling::ul/li/div//span[contains(text(),'" + path1[i] + "')]";
				}
			} else {
				tempXpath = xpath;
			}
			System.err.println("Xpath \t\t\n\n"+xpath+"\n\n\t\t");
			if(pageName!=null && (pageName.equalsIgnoreCase("Bulk Download CRM") ||pageName.equalsIgnoreCase("Target Side Bulk Download") )&& FindElement(driver, tempXpath, "folder path", action.BOOLEAN, 2)==null ){
				expandXpath = tempXpath+"/preceding-sibling::span[@class='dynatree-expander']";
				ele1 = FindElement(driver, expandXpath, "Expand Folder:  >>  "+expandXpath, action.BOOLEAN, 2);
				if (ele1!=null) {
					appLog.info("Folder Already expanded >> : "+expandXpath);
				} else {


					if (click(driver, FindElement(driver, xpath, "Folder: " + path1[i - 1], action.BOOLEAN, timeOut),
							"Folder: " + path1[i - 1], action.SCROLLANDBOOLEAN)) {

						// appLog.info("Folder Found: "+path1[i-1]);
					} else {
						// appLog.info("Folder Not Found: "+path1[i-1]);
						found = false;
						notFoundFolders.add(path1[i - 1]);
					}
				}
				if (i != path1.length) {
					if(pageName!=null &&  (pageName.equalsIgnoreCase("Bulk Download CRM") ||pageName.equalsIgnoreCase("Target Side Bulk Download") )){
						xpath = xpath + "/../following-sibling::ul/li//a[text()='" + path1[i] + "']";
					} else {
						xpath = xpath + "/../../following-sibling::ul/li/div//span[contains(text(),'" + path1[i] + "')]";
					}
				}
			} else {
				if(pageName!=null && (pageName.equalsIgnoreCase("Bulk Download CRM") ||pageName.equalsIgnoreCase("Target Side Bulk Download") )){
					appLog.info("Foldr Found Successfully : "+path1[i - 1]);
				} else {
					if (click(driver, FindElement(driver, xpath, "Folder: " + path1[i - 1], action.BOOLEAN, timeOut),
							"Folder: " + path1[i - 1], action.SCROLLANDBOOLEAN)) {
						// appLog.info("Folder Found: "+path1[i-1]);
					} else {
						// appLog.info("Folder Not Found: "+path1[i-1]);
						found = false;
						notFoundFolders.add(path1[i - 1]);
					}
					if (i != path1.length) {
						if(pageName!=null &&  (pageName.equalsIgnoreCase("Bulk Download CRM") ||pageName.equalsIgnoreCase("Target Side Bulk Download") )){
							xpath = xpath + "/../following-sibling::ul/li//a[text()='" + path1[i] + "']";
						} else {
							xpath = xpath + "/../../following-sibling::ul/li/div//span[contains(text(),'" + path1[i] + "')]";
						}
					}
				}
			}
		}
		if (!found) {
			for (int k = 0; k < notFoundFolders.size(); k++) {
				if (k == 0)
					appLog.info("List of not found folder of path \"" + path + "\" are: ");
				appLog.info(notFoundFolders.get(k));
			}
		} else {
			appLog.info("Folder path is verified: " + path);
		}
		if(found==false){
			if(isAlertPresent(driver)){
				String msg = switchToAlertAndGetMessage(driver, 30, action.GETTEXT);
				switchToAlertAndAcceptOrDecline(driver, 30, action.ACCEPT);
				if(msg.trim().toLowerCase().contains("error") || msg.trim().toLowerCase().contains("status") || msg.trim().toLowerCase().contains("code")){
					driver.navigate().refresh();
					if(getDealRoomFrame(10)!=null){
						scrollDownThroughWebelement(driver, getDealRoomSection(30), "Deal room view.");
						switchToFrame(driver, 30, getDealRoomFrame(30));
					}
					if(verifyFolderPath(timeOut, path, instituionName, pageName)){
						found=true;
					} else {
						found = false;
					}
				} else {
					String loc = screenshot(currentlyExecutingTC);
					appLog.error("Folder verification failed due to some intermittent issue. Kindly check the screenshot: "+loc);
					BaseLib.sa.assertTrue(false, "Folder verification failed due to some intermittent issue. Kindly check the screenshot: "+loc);
				}
			} else if (FindElement(driver, "//img[@class='poweredByImage']", null, action.BOOLEAN, 0)!=null){
				flag1=true;
			} else if (FindElement(driver, "//img[@src='/resource/1511337238000/SiteSamples/img/clock.png']", null, action.BOOLEAN, 0)!=null){
				flag1=true;
			} else if (FindElement(driver, "//img[@src='/resource/1511337238000/SiteSamples/img/warning.gif']", null, action.BOOLEAN, 0)!=null){
				flag1=true;
			} else if (FindElement(driver, "//span[@class='title'][text()='Error: Error occurred while loading a Visualforce page.']", null, action.BOOLEAN, 0)!=null){
				flag1=true;
			}
			if(flag1){
				driver.navigate().refresh();
				if(getDealRoomFrame(10)!=null){
					scrollDownThroughWebelement(driver, getDealRoomSection(30), "Deal room view.");
					switchToFrame(driver, 30, getDealRoomFrame(30));
				}
				if(verifyFolderPath(timeOut, path, instituionName, pageName)){
					found=true;
				} else {
					found = false;
				}
			}
		}
		return found;
	}
		
	/**
	 * 
	 * @param timeOut
	 * @param path
	 * @param institutionName
	 * @param checkStdFolder
	 * @param pageName
	 * @return true/false
	 */

	@SuppressWarnings({ "unchecked", "rawtypes"})
	public boolean verifyFolderStructure(int timeOut, Object path, String institutionName, String checkStdFolder, String pageName) {
		boolean flag = true;
		//accountFolderInt=0;
		String insti;
		if(path==""){
			if(verifyFolderPath("", institutionName, pageName)){
				appLog.info("Successfully clicked on institution folder.");
				flag = true;
			} else {
				flag=false;
			}
		} else if (path instanceof Map) {
			Set<String> paths = ((Map) path).keySet();
			Iterator<String> i = paths.iterator();
			while (i.hasNext()) {
				String string = (String) i.next();
				if (institutionName != null) {
					//System.err.println("((Map) path).get(string).toString() : "+((Map) path).get(string).toString());
					if (!((Map) path).get(string).toString().equalsIgnoreCase("Standard")) {
						insti = null;
					} else {
						insti = institutionName;
					}
					if (((Map) path).get(string).toString().equalsIgnoreCase("Standard")
							&& checkStdFolder.equalsIgnoreCase("No")) {
						appLog.info("Standard folder will not be checked");
						break;
					}

					if(pageName.equalsIgnoreCase("Bulk Download CRM") || pageName.equalsIgnoreCase("Target Side Bulk Download")){
						if(!string.contains("(Global)") && !string.contains("(Shared)") && !string.contains("(Internal)")){
							insti=institutionName;
						}

						if (pageName.equalsIgnoreCase("Target Side Bulk Download") && string.contains("(Internal)")) {
							continue;
						}
					}
					if (verifyFolderPath(timeOut, string, insti, pageName)) {
						appLog.info("Folder path successfully found: " + string);
					} else {
						appLog.info("Folder path not found: " + string);
						flag = false;
					}


				} else {
					if (verifyFolderPath(string)) {
						appLog.info("Folder path successfully found: " + string);
					} else {
						appLog.info("Folder path not found: " + string);
						flag = false;
					}
				}
			}
		} else if (path instanceof String) {
			String paths[] = ((String) path).split(",");
			for (int i = 0; i < paths.length; i++) {
				if(checkStdFolder.equalsIgnoreCase("Traverse")){
					if (verifyFolderPath(timeOut, paths[i], institutionName, pageName)) {
						appLog.info("Folder path successfully found: " + paths[i]);
					} else {
						appLog.info("Folder path not found: " + paths[i]);
						flag = false;
					}
				} else	if (verifyFolderPath(paths[i])) {
					appLog.info("Folder path successfully found: " + paths[i]);
				} else {
					appLog.info("Folder path not found: " + paths[i]);
					flag = false;
				}
			}
		}
		if (!flag) {
			appLog.info("Folder Structure is not verified.");
		}
		return flag;
	}

	/**
	 * 
	 * @param accountNameToWhichFilesToBeUploaded
	 * @param parentWinID
	 * @param checkMsg
	 * @param pgName
	 * @return true/false
	 */
	public boolean selectTargetOnUploadWindow(String accountNameToWhichFilesToBeUploaded, String parentWinID,
			String checkMsg,String pgName) {
		String uploadType=externalAdminUploadType.externalAdminSimpleUpload.toString();
		appLog.info("uploadType: "+uploadType);
		appLog.info("Passsed Page : "+pgName);
		boolean nextBtnFlag=false;
		if (uploadType.equalsIgnoreCase(pgName)) {
			appLog.info("Inside External Admin Simple Upload");
		}else{
			appLog.info("Inside Upload");
			if (accountNameToWhichFilesToBeUploaded != null) {
				String names[] = accountNameToWhichFilesToBeUploaded.split(",");
				for (int i = 0; i < names.length; i++) {
					WebElement ele = FindElement(driver, "//td[text()='" + names[i] + "']/preceding-sibling::td/input",
							names[i] + " :Account Check box", action.SCROLLANDBOOLEAN, 60);
					if (ele != null) {
						if (!click(driver, ele, names[i] + " :Account Check box", action.SCROLLANDBOOLEAN)) {
							appLog.info("Cannot select account: " + names[i]);
							driver.close();
							driver.switchTo().window(parentWinID);
							return false;
						}
					} else {
						appLog.info(names[i] + " :Account is not present in the list.");
						continue;
					}
				}
			} else {
				appLog.info("Will Continue upload with the default selection.");
			}	
		}

		if (uploadType.equalsIgnoreCase(pgName)) {
			ThreadSleep(3000);
			appLog.info("Inside External Admin Simple Upload1");
			if (CommonLib.isAlertPresent(driver)) {
				String msg = CommonLib.switchToAlertAndGetMessage(driver, 20, action.GETTEXT);
				switchToAlertAndAcceptOrDecline(driver, 60, action.ACCEPT);
				if (msg.trim().equalsIgnoreCase(DealPageErrorMessage.UploadSelectTargetMessage)) {
					// driver.switchTo().window(parentWinID);
					return true;
				} else {
					BaseLib.sa.assertTrue(false, "Error message on upload window is not matched.\tExpected: "
							+ DealPageErrorMessage.UploadSelectTargetMessage + "\tActual: " + msg);
					// driver.switchTo().window(parentWinID);
					return false;
				}
				/*} else if (checkMsg != null) {
					BaseLib.sa.assertTrue(false, "Select Atleast On Target Alert Is Not Showing On Upload Window.");
					appLog.info("There Is No Alert.");
					return false;*/
			} else {
				return true;
			}
		} else {
			appLog.info("Inside  Upload1");
			if(matchTitle(driver, "Upload Documents", 60)) {
				if (clickUsingCssSelectorPath(edriver, CssPath.cssPathForNextButton, "Next Button")) {
					appLog.info("clicked on Next Button");
					nextBtnFlag = true;
				} else {
					appLog.info("Not Able to click on Next Button");
				}
			}else {
				appLog.error("Not able to click on Next button so cannot upload document");
				return false;
			}
			if (!nextBtnFlag) {
				ThreadSleep(3000);
				if (CommonLib.isAlertPresent(driver)) {
					String msg = CommonLib.switchToAlertAndGetMessage(driver, 20, action.GETTEXT);
					switchToAlertAndAcceptOrDecline(driver, 60, action.ACCEPT);
					if (msg.trim().equalsIgnoreCase(DealPageErrorMessage.UploadSelectTargetMessage)) {
						// driver.switchTo().window(parentWinID);
						return true;
					} else {
						BaseLib.sa.assertTrue(false, "Error message on upload window is not matched.\tExpected: "
								+ DealPageErrorMessage.UploadSelectTargetMessage + "\tActual: " + msg);
						// driver.switchTo().window(parentWinID);
						return false;
					}
				} else if (checkMsg != null) {
					BaseLib.sa.assertTrue(false, "Select Atleast On Target Alert Is Not Showing On Upload Window.");
					appLog.info("There Is No Alert.");
					return false;
				} else {
					return true;
				}
			} else {
				ThreadSleep(3000);
				if (CommonLib.isAlertPresent(driver)) {
					String msg = CommonLib.switchToAlertAndGetMessage(driver, 20, action.GETTEXT);
					switchToAlertAndAcceptOrDecline(driver, 60, action.ACCEPT);
					if (msg.trim().equalsIgnoreCase(DealPageErrorMessage.UploadSelectTargetMessage)) {
						// driver.switchTo().window(parentWinID);
						return true;
					} else {
						BaseLib.sa.assertTrue(false, "Error message on upload window is not matched.\tExpected: "
								+ DealPageErrorMessage.UploadSelectTargetMessage + "\tActual: " + msg);
						// driver.switchTo().window(parentWinID);
						return false;
					}
				} else if (checkMsg != null) {
					BaseLib.sa.assertTrue(false, "Select Atleast On Target Alert Is Not Showing On Upload Window.");
					appLog.info("There Is No Alert.");
					return false;
				} else {
					return true;
				}
			}
		}

	}	

	/**
	 * 
	 * @param folderPath
	 * @param accountNameForFolder
	 * @param accountNameToWhichFilesToBeUploaded
	 * @param folderType
	 * @param uploadUpdate
	 * @param checkNoTargetMsg
	 * @param dragFromFolder
	 * @param pageName
	 * @param isMultiple
	 * @return true/false
	 */
	public boolean uploadFile(String folderPath, String accountNameForFolder,
			String accountNameToWhichFilesToBeUploaded, String folderType, String uploadUpdate, String checkNoTargetMsg,
			String dragFromFolder, String pageName, boolean isMultiple) {
		String parentWinID = driver.getWindowHandle();
		int counter = 0;
		try {
			if (pageName == null || pageName.equalsIgnoreCase("CRM")) {
				scrollDownThroughWebelement(driver, getDealRoomSection(60), "Deal Room Section");
				System.out.println("Searching for the frame");
				switchToFrame(driver, 60, getDealRoomFrame(60));
				System.err.println("Inside frame");

			}
			if (verifyFolderStructure(60, folderPath, accountNameForFolder, "Traverse", null)) {
				WebElement uploadIcon;
				if (pageName == null || pageName.equalsIgnoreCase("CRM")) {
					if(folderType.equalsIgnoreCase("Standard")){
						System.err.println("isMultiple : "+isMultiple);
						if (isMultiple) {
							appLog.info("Going to click on AddFileMultipleTargetsBtn");
							uploadIcon = getMultipleUploadIcon(10);		
						} else {
							appLog.info("Going to click on AddFileBtn");
							uploadIcon = getUploadIcon(60);
						}

					}else{
						appLog.info("Going to click on AddFileBtn");
						uploadIcon = getUploadIcon(60);
					}
				} else {
					uploadIcon = isDisplayed(driver,
							FindElement(driver, "//a[@id='addafilebtn']", "Upload Icon", action.BOOLEAN, 30),
							"Clickable", 30, "Upload Icon");
				}
				scrollDownThroughWebelement(driver, uploadIcon, "");
				for (int i = 0; i == 0; i++)
					if (clickUsingJavaScript(driver, uploadIcon, "upload Icon")) {
						parentWinID = switchOnWindow(driver);
						if (folderType.equalsIgnoreCase("Standard")) {
							if (checkNoTargetMsg != null) {
								if (checkNoTargetMsg.equalsIgnoreCase("Yes")) {
									selectTargetOnUploadWindow(accountNameForFolder, parentWinID, checkNoTargetMsg,pageName);
								}
							}
							if (!selectTargetOnUploadWindow(accountNameToWhichFilesToBeUploaded, parentWinID, null,pageName)) {
								return false;
							} else {
								appLog.info("Target Successfully Selected.");
							}

						} else {
							appLog.info("Will upload file directly without selecting account");
						}
						dragDropFiles(dragFromFolder);
						if (uploadUpdate.equalsIgnoreCase("update")) {
							if(folderType.equalsIgnoreCase("Standard")){
								click(driver, getSimpleupdateAllButton(10), "Update all button", action.BOOLEAN);
							}else {
								if (click(driver, getSimpleupdateAllButton(60), "Update all button", action.BOOLEAN)) {
									appLog.info("Successfully updated the duplicate files.");
								} else {
									appLog.info("Not able to update files.");
								}
							}
						} else if (uploadUpdate.equalsIgnoreCase("UpdateScreenExit") && !isMultiple) {
							appLog.info("Reached till duplicate doc pop up.");
							return true;
						}else if  (uploadUpdate.equalsIgnoreCase("Ignore") && !isMultiple) {
							appLog.info("Going to Click on Ignore All Button");
							WebElement ele;
							if (("ExternalAdmin").equalsIgnoreCase(pageName)) {
								ele = getIgnoreAllMAEA(60);
							} else {
								ele = getIgnoreAllMA(60);
							}
							if (click(driver, ele, "Ignore all button", action.BOOLEAN)) {
								appLog.info("Clicked on Ignore All Button");

								if (getUploadSaveButton(60)!=null) {
									appLog.info("Save Button is displaying after Clicking on Ignore All Button");
									driver.close();
									driver.switchTo().window(parentWinID);
									return true;
								} else {
									appLog.error("Save Button is not displaying after Clicking on Ignore All Button");
									BaseLib.sa.assertTrue(false,"Save Button is not displaying after Clicking on Ignore All Button");
								}
							} else {
								appLog.error("Not Able to Click on Ignore All Button");
								BaseLib.sa.assertTrue(false,"Not Able to Click on Ignore All Button");
							}
							driver.close();
							driver.switchTo().window(parentWinID);
							return false;
						}
						List<WebElement> files = draggedFilesInFileUploadAtCRMSide();
						if (files.isEmpty()) {
							driver.close();
							driver.switchTo().window(parentWinID);
							if (pageName == null || pageName.equalsIgnoreCase("CRM")) {
								System.out.println("Searching for the frame");
								switchToFrame(driver, 60, getDealRoomFrame(60));
							}
							if (counter > 1) {
								appLog.error(
										"Tried Upload for three times but still not able to upload file in folderpath: "
												+ folderPath);
								BaseLib.sa.assertTrue(false,
										"Tried Upload for two times but still not able to upload file in folderpath: "
												+ folderPath);
								return false;
							}
							counter++;
							i--;
							continue;
						}
						BaseLib.ListofUploadedfiles = new ArrayList<String>();
						for (int j = 0; j < files.size(); j++) {
							BaseLib.ListofUploadedfiles.add(files.get(j).getText().trim());
						}
//						if (!uploadUpdate.trim().equalsIgnoreCase("Update")) {
//							if (!folderType.equalsIgnoreCase("Internal")) {
//								for (int j = 0; j < files.size(); j++) {
//									BaseLib.uniquedocs.add(files.get(j).getText().trim());
//								}
//							}
//						}
						if (click(driver, getUploadSaveButton(60), "Upload Save Button", action.BOOLEAN)) {
							if(uploadUpdate.equalsIgnoreCase("UpdateScreenExit") && isMultiple) {
								appLog.info("Reached till multiple document upload duplicate doc pop up.");
								return true;
							}else if (uploadUpdate.equalsIgnoreCase("update")) {
								if(folderType.equalsIgnoreCase("Standard") && isMultiple){
									if (click(driver, getSimpleupdateAllButtonforStandardFolder(60), "Standard Update all button", action.BOOLEAN)) {
										appLog.info("Successfully1 updated the duplicate files.");
									} else {
										appLog.info("Not able to update1 files.");
									}
								}
							}else if (isMultiple && uploadUpdate.equalsIgnoreCase("Ignore")) {
								if (click(driver, getMultiUploadDuplicateFilePopUpIgnoreBtn(60), "Ignore all button", action.BOOLEAN)) {
									appLog.info("Clicked on Ignore All Button");
									driver.switchTo().window(parentWinID);
									return true;
								} else {
									appLog.error("Not Able to Click on Ignore All Button");
									BaseLib.sa.assertTrue(false,"Not Able to Click on Ignore All Button");
								}
								driver.close();
								driver.switchTo().window(parentWinID);
								return false;

							}
							if (getUploadConfirmationText(60) != null) {
								String confirmationMsg = getUploadConfirmationText(60).getText().trim();
								if (confirmationMsg.equalsIgnoreCase(DealPageErrorMessage.UploadConfirmationMessage)) {
									appLog.info("Confirmation message is matched Successfully.");
								} else {
									BaseLib.sa.assertTrue(false,
											"Error message on upload window is not matched.\tExpected: "
													+ DealPageErrorMessage.UploadConfirmationMessage + "\tActual: "
													+ confirmationMsg);
								}
								driver.close();
								driver.switchTo().window(parentWinID);
								return true;
							} else {
								appLog.info("Confirmation message is not displayed.");
								BaseLib.sa.assertTrue(false, "Confirmation message is not displayed.");
								return false;
							}
						}
					} else {
						appLog.info("Upload is not possible in this folder path: " + folderPath);
						return false;
					}
			} else {
				appLog.info("Cannot Find the folder path: " + folderPath + ".Kindly Re-check the path");
				return false;
			}
		} catch (Exception e) {
			if (isAlertPresent(driver)) {
				String msg = switchToAlertAndGetMessage(driver, 30, action.GETTEXT);
				switchToAlertAndAcceptOrDecline(driver, 30, action.ACCEPT);
			}
			driver.close();
			driver.switchTo().window(parentWinID);
		}
		return false;
	}	
	
	
	/**
	 * 
	 * @param dragFromFolder
	 * @throws Exception 
	 */
	public void dragDropFiles(String dragFromFolder) {
		Screen screen = new Screen();
		try {
			System.err.println(System.getProperty("user.dir")+"\\"+dragFromFolder);
			Process process = Runtime.getRuntime()
					.exec(System.getProperty("user.dir") + "/OpenFolder.exe" + " " + dragFromFolder);
			process.waitFor();
			if (dragFromFolder.contains("\\")) {
				dragFromFolder = (dragFromFolder
						.split(Pattern.quote("\\")))[(dragFromFolder.split(Pattern.quote("\\")).length - 1)];
			}
			process = Runtime.getRuntime().exec(".\\AutoIT\\activateFilesToUpload.exe" + " " + dragFromFolder);
			process.waitFor();
			screen.keyDown(Key.CTRL);
			screen.type("a");
			screen.keyUp(Key.CTRL);
			screen.drag(".\\AutoIT\\Drag.jpg");
			screen.mouseMove(-150, -100);
			Runtime.getRuntime().exec(".\\AutoIT\\UploadWindow.exe");
			try{
				screen.wait(".\\AutoIT\\DropLoc.jpg", 60);
			screen.dropAt(".\\AutoIT\\DropLoc.jpg");
				
			} catch (Exception e){
				System.err.println("After exception is running.");
				screen.wait(".\\AutoIT\\DropLoc.jpg", 60);
			screen.dropAt(".\\AutoIT\\DropLoc.jpg");
			}
			process = Runtime.getRuntime()
					.exec(System.getProperty("user.dir") + "\\AutoIT\\CloseFolder.exe" + " " + dragFromFolder);
			process.waitFor();
			System.err.println("Successfully cdroped files");
		} catch (FindFailed | IOException | InterruptedException e) {
			appLog.info("Issue with drag and drop");
		}
	}
	
	
	/**
	 * 
	 * @param dealName
	 * @param timeOut
	 * @return true/false
	 */
	public SoftAssert clearDealWorkspace(String dealName, int timeOut) {
		SoftAssert saa = new SoftAssert();
		boolean flag = false;
		if (click(driver, getClearDealRoomButton(timeOut), "Clear Deal Room button", action.SCROLLANDBOOLEAN)) {
			String parentid = switchOnWindow(driver);
			if(parentid!=null) {
				ThreadSleep(5000);
				if (clickUsingCssSelectorPath(edriver, CssPath.cssPathForClearDealRoomYesButton, "Clear Deal Room Popup Yes button")) {
					appLog.info("clicked on Clear Deal Room Popup Yes button");
					flag=true;
				} else {
					flag=false;
					appLog.info("Not Able to click on Clear Deal Room Popup Yes button");
					saa.assertTrue(false, "Not Able to click on Clear Deal Room Popup Yes button");
				}
			//	scn.nextLine();
				if (flag) {
					appLog.info("Clear Deal Room popup Yes Button is Clicked");
//					if (isAlertPresent(driver)) {
						String msg = switchToAlertAndGetMessage(driver, 30, action.GETTEXT);
						appLog.info(msg);
						switchToAlertAndAcceptOrDecline(driver, 30, action.ACCEPT);
						driver.switchTo().window(parentid);
//					} else {
//						driver.close();
//						driver.switchTo().window(parentid);
//						appLog.error("Alert Pop Up is not present so cannot click on Ok Button");
//						saa.assertTrue(false, "Alert Pop Up is not present so cannot click on Ok Button");
//					}
					driver.navigate().refresh();
					scrollDownThroughWebelement(driver, getDealRoomSection(timeOut), "Deal Room Section");
					switchToFrame(driver, 60, getDealRoomFrame(timeOut));
					if (getBuildDealRoomButton(30) != null) {
						appLog.info("Deal Room Button is visible after clear deal room");
					} else {
						appLog.error("Deal Room Button is visible after clear deal room");
						saa.assertTrue(false, "Deal Room Button is visible after clear deal room");
					}
				} else {
					driver.close();
					driver.switchTo().window(parentid);
					appLog.error("Not able to click on Yes Button so DealRoom :" + dealName);
					saa.assertTrue(false,
							"Not able to click on Yes Button so DealRoom :" + dealName);
				}
				
			}else {
				appLog.error("No new window is open after click on clear button");
				saa.assertTrue(false, "No new window is open after click on clear button");
			}
		} else {
			appLog.info("Not able to click on Clear Button so cannot clear DealRoom  : " + dealName);
			saa.assertTrue(false,
					"Not able to click on Clear Button so cannot clear DealRoom : " + dealName);
		}
		switchToDefaultContent(edriver);
		return saa;
	}
		
}
