package com.navatar.pageObjects;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import com.navatar.generic.EnumConstants.PageName;
import com.navatar.generic.EnumConstants.YesNo;
import com.navatar.generic.EnumConstants.action;
import com.relevantcodes.extentreports.LogStatus;
import static com.navatar.generic.CommonLib.*;

import java.util.ArrayList;
import java.util.List;

import static com.navatar.generic.CommonVariables.*;


public class BoxPageBusinesslayer extends BoxPage {
	
	
	public BoxPageBusinesslayer(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param boxUsername
	 * @param boxPassword
	 * @param directLoginOrLoginThroughUrl TODO
	 * @return true/false
	 */
	public boolean boxLogin(String boxUsername, String boxPassword, YesNo directLoginOrLoginThroughUrl){
		if(directLoginOrLoginThroughUrl.toString().equalsIgnoreCase(YesNo.No.toString())) {
			driver.get("https://"+BoxUrl);
		}
		if(sendKeys(driver, getBoxUserNameTextBox(30), boxUsername, "Username", action.BOOLEAN)){
			log(LogStatus.PASS, "passed box user name in text box : "+boxUsername, YesNo.No);
			if(click(driver, getBoxLoginNextButton(30), "Next Button", action.BOOLEAN)){
				log(LogStatus.PASS, "Clicked on Next Button", YesNo.No);
				if(sendKeys(driver, getBoxPasswordTextBox(30), boxPassword, "Box Password Text Box", action.BOOLEAN)){
					log(LogStatus.PASS, "Passed Value in password text box : "+boxPassword, YesNo.No);
					if(click(driver, getBoxLoginButton(30), "Login button", action.BOOLEAN)){
						log(LogStatus.PASS, "Clicked on Login Button", YesNo.No);
						if (matchTitle(driver, "All Files | Powered by Box", 60)) {
							log(LogStatus.PASS, "User is Successfully LoggedIn in Box", YesNo.No);
							return true;
						} else {
							log(LogStatus.FAIL, "User is not Successfully LoggedIn in Box", YesNo.Yes);
							exit("User is not Successfully LoggedIn in Box");
							return false;
						}
					} else {
						log(LogStatus.FAIL, "Login button cannot be clicked, So cannot continue.", YesNo.Yes);
						sa.assertTrue(false,"Login button cannot be clicked, So cannot continue.");
					}
				} else {
					log(LogStatus.FAIL, "Password is not passed, So cannot continue.", YesNo.Yes);
					sa.assertTrue(false,"Password is not passed, So cannot continue.");
				}
			} else {
				log(LogStatus.FAIL, "Cannot click on next button, So cannot continue.", YesNo.Yes);
				sa.assertTrue(false,"Cannot click on next button, So cannot continue.");
			}
		} else {
			log(LogStatus.FAIL, "Username is not passed, So cannot continue.", YesNo.Yes);
			sa.assertTrue(false,"Username is not passed, So cannot continue.");
		}
		return false;
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param password
	 * @param crmuserEMailID
	 * @return true/false
	 */
	public boolean ResetPassword(String password, String crmuserEMailID) {
		if(getEnterNewPasswordLabelText(60)!=null) {
			if(sendKeys(driver, getNewpasswordTextBox(30), password, "new password text box ", action.SCROLLANDBOOLEAN)) {
				log(LogStatus.PASS, "Passed Value in new password text box : "+password, YesNo.No);
				ThreadSleep(3000);
				if(sendKeys(driver, getConfirmPasswordTextBox(30), password, "confirm password text box ", action.SCROLLANDBOOLEAN)) {
					log(LogStatus.PASS, "Passed Value in confirm password text box : "+password, YesNo.No);
					ThreadSleep(3000);
					if(click(driver, getUpdateButton(30), "update button", action.SCROLLANDBOOLEAN)) {
						log(LogStatus.PASS, "clicked on update button ", YesNo.No);
						return true;
					}else {
						log(LogStatus.FAIL, "Not able to click on update button so cannot reset password of user :"+crmuserEMailID, YesNo.Yes);
					}
				}else {
					log(LogStatus.FAIL, "Not able to pass value in confirm password text box so cannot reset password "+crmuserEMailID,YesNo.Yes);
				}
			}else {
				log(LogStatus.FAIL, "Not able to pass value in new password text box so cannot reset password "+crmuserEMailID,YesNo.Yes);
			}
		}else {
			log(LogStatus.FAIL,"enter your new password label text is not visible so cannot reset the password",YesNo.Yes);

		}
		return false;
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param username
	 * @param userEmailID
	 * @return true/false
	 */
	public boolean inviteUserFromAdminConsole(String username,String userEmailID) {
		if(click(driver,getBoxAdminConsoleLink(30), "admin console link", action.SCROLLANDBOOLEAN)) {
			log(LogStatus.PASS, "clicked on admin console link ", YesNo.No);
			if(click(driver, getUserAndGroupLink(30),"user and group link", action.SCROLLANDBOOLEAN)) {
				log(LogStatus.PASS, "clicked on user and group link ", YesNo.No);
				if(click(driver, getUsersPlusButton(30),"click on user plus button", action.SCROLLANDBOOLEAN)) {
					log(LogStatus.PASS, "clicked on users + button", YesNo.No);
					if(sendKeys(driver, getNameTextOnNewUserAccountDetailsPage(30), username, "user name text box ", action.SCROLLANDBOOLEAN)) {
						log(LogStatus.PASS, "passed value in name text box : "+username, YesNo.No);
						if (sendKeys(driver, getEmailTextBoxOnUserAccountDetailsPage(30), userEmailID, "email text box ", action.SCROLLANDBOOLEAN)) {
							log(LogStatus.PASS, "passed value in email text box "+userEmailID, YesNo.No);
							ThreadSleep(2000);
							scrollDownThroughWebelement(driver,  getAddUserButton(30), "add user button");
							if(clickUsingJavaScript(driver,  getAddUserButton(30), "add user button")) {
								log(LogStatus.PASS, "clicked on add user button", YesNo.No);
								ThreadSleep(3000);	
								WebElement ele=FindElement(driver, "//ul[@id='subusers_new']/li//span[contains(text(),'"+userEmailID+"')]", "created users email id label text", action.SCROLLANDBOOLEAN, 30);
								if(ele!=null) {
									log(LogStatus.PASS, "User is added successfully "+username, YesNo.No);
									return true;
								}else {
									log(LogStatus.FAIL, "User email id is not matched after click on add user button so cannot invite user "+username, YesNo.Yes);
								}
							}else {
								log(LogStatus.FAIL, "Not able to click on add users button so cannot invite user "+username, YesNo.Yes);
							}
						}else {
							log(LogStatus.FAIL, "Not able to pass value in email text box "+userEmailID,YesNo.Yes);
						}
					}else {
						log(LogStatus.FAIL, "Not able to pass value in name text box :"+username, YesNo.Yes);
					}
				}else {
					log(LogStatus.FAIL, "Not able to click user plus button so cannot invite user "+username, YesNo.Yes);
				}
			}else {
				log(LogStatus.ERROR, "Not able to click on user and group link so cannot invite user "+username, YesNo.Yes);
			}

		}else {
			log(LogStatus.FAIL, "Not able to click on admin console link so cannot invite user "+username,YesNo.Yes);
		}
		return false;
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @return true/false
	 */
	public boolean boxLogOut() {
		if(clickUsingJavaScript(driver, getLogOutDownArrow(30), "logOut down arrow ")) {
			log(LogStatus.PASS, "Clicked on box down arrows", YesNo.No);
			ThreadSleep(3000);
			if(click(driver, getBoxLogOutLink(20), "box link", action.SCROLLANDBOOLEAN)) {
				log(LogStatus.PASS, "clicked on LogOut Link", YesNo.No);
				return true;
			}else {
				log(LogStatus.FAIL, "Not able to click on LogOut", YesNo.Yes);
			}
		}else {
			log(LogStatus.ERROR, "Not able to click on box down arrow ", YesNo.Yes);
		}
		return false;
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param folderstructure
	 * @return emopty list if execute successfully
	 */
	public List<String> createFolderStructureInBox(String[][] folderstructure) {
		List<String> res = new ArrayList<String>();
		WebElement ele=null;
		String createdFolderXpath="";
		ThreadSleep(20000);
		switchToFrame(driver, 30, getFrame(PageName.DocumentsPageFrameOnDealPage, 30));
		ThreadSleep(10000);
		driver.switchTo().frame(0);
		System.err.println("Successfully switch inside box frame ");
		for (String[] foldersName : folderstructure) {
			for(int i=0; i<foldersName.length; i++) {
				
				String[] splitedFoldersName=foldersName[i].split("/");
				
				for(int j=0; j<splitedFoldersName.length; j++) {
					if(j!=0) {
						createdFolderXpath="//a[@class='item-name-link'][contains(text(),'"+splitedFoldersName[j-1]+"')]";
						ele = FindElement(driver, createdFolderXpath,splitedFoldersName[j-1]+" created folder xpath ", action.SCROLLANDBOOLEAN,10);
						if(click(driver, ele,splitedFoldersName[j-1]+" created folder xpath ", action.BOOLEAN)) {
							
						}else {
							log(LogStatus.FAIL, "Not able to click on created folder "+splitedFoldersName[j-1]+" so cannot create all folders"+splitedFoldersName, YesNo.Yes);
							res.add("Not able to click on created folder "+splitedFoldersName[j-1]+" so cannot create all folders"+splitedFoldersName);
							break;
						}
					}
					ThreadSleep(2000);
					if(clickUsingJavaScript(driver, getNewButton(30), "new button")) {
						log(LogStatus.PASS, "clicked on new button", YesNo.No);
						ThreadSleep(2000);
						if(clickUsingJavaScript(driver, getCreateNewFolderLink(30), "create folder link")) {
							log(LogStatus.PASS, "clicked on create a folder link ", YesNo.No);
							ThreadSleep(3000);
							if(sendKeys(driver, getFolderNameTextBoxInCreateFolderPopUp(30), foldersName[i], "folder name text box ", action.SCROLLANDBOOLEAN)) {
								log(LogStatus.PASS, "enter folder name "+foldersName[i], YesNo.No);
								if(click(driver, getCreateBtnInCreateFolderPopUp(30), "create folder button", action.SCROLLANDBOOLEAN)) {
									log(LogStatus.PASS, "clicked on create folder button", YesNo.No);
									ThreadSleep(5000);
									createdFolderXpath="//a[@class='item-name-link'][contains(text(),'"+splitedFoldersName[j]+"')]";
									ele = FindElement(driver, createdFolderXpath,splitedFoldersName[j]+" created folder xpath ", action.SCROLLANDBOOLEAN,10);
									if(ele!=null) {
										log(LogStatus.PASS, "Created Folder is verifed: "+splitedFoldersName[j], YesNo.No);
									}else {
										log(LogStatus.FAIL, "created folder is not verified :"+splitedFoldersName[j], YesNo.Yes);
										res.add("created folder is not verified :"+splitedFoldersName[j]);
									}
								}else {
									log(LogStatus.FAIL, "Not able to click on create folder button so cannot create folder :"+foldersName[i], YesNo.Yes);
									res.add("Not able to click on create folder button so cannot create folder :"+foldersName[i]);
									break;
								}
							}else {
								log(LogStatus.FAIL, "Not able to enter folder name "+foldersName[i]+" so cannot create folder structure "+foldersName, YesNo.Yes);
								res.add("Not able to enter folder name "+foldersName[i]+" so cannot create folder structure "+foldersName);
								break;
							}
						}else {
							log(LogStatus.FAIL, "Not able to click on create new folder link so cannot create folder structure in box : "+foldersName, YesNo.Yes);
							res.add("Not able to click on create new folder link so cannot create folder structure in box : "+foldersName);
							break;
						}
					}else {
						log(LogStatus.FAIL, "Not able to click on new button so cannot create folder structure in box  : "+foldersName.toString(),YesNo.Yes);
						res.add("Not abke to click on new button so cannot create folder structure in box  : "+foldersName.toString());
						break;
					}
				}
			}
		}
		switchToDefaultContent(driver);
		return res;
	}
	
	
	/**
	 * @author ANKIT JAISWAL
	 * @param folderStructure
	 * @param DocOrFolderName
	 * @param boxActions
	 * @return empty list if execute successfully
	 */
	public List<String> DeleteFolderOrFileInBox(String folderStructure,String DocOrFolderName,BoxActions boxActions) {
		String[] folderstruct = folderStructure.split("/");
		String[] documentOrFolderNames=DocOrFolderName.split("<break>");
		List<String> res = new ArrayList<String>();
		String xpath="";
		WebElement ele=null;
		click(driver, getBoxHomeLogoLink(10), "box home logo link", action.BOOLEAN);
		for(int i=0; i<folderstruct.length; i++) {
			xpath="//a[text()='"+folderstruct[i]+"']";
			ele=FindElement(driver, xpath, folderstruct[i]+" folder name xpath", action.SCROLLANDBOOLEAN, 10);
			if(ele!=null) {
				log(LogStatus.PASS, "folder name is found "+folderstruct[i], YesNo.No);
				if(click(driver, ele, folderstruct[i]+" folder name xpath", action.BOOLEAN)) {
					log(LogStatus.PASS, "Clicked on Folder Name : "+folderstruct[i], YesNo.No);

				}else {
					log(LogStatus.FAIL, "Not able to click on folder structure so cannot perform "+boxActions.toString()+" action in box ", YesNo.Yes);
					res.add("Not able to click on folder structure so cannot perform "+boxActions.toString()+" action in box ");
					return res;
				}
			}else {
				log(LogStatus.FAIL, "Folder name is not found in box : "+folderstruct[i]+" so cannot perform  "+boxActions.toString()+" action in box",YesNo.Yes);
				res.add("Folder name is not found in box : "+folderstruct[i]+" so cannot perform  "+boxActions.toString()+" action in box");
				return res;
			}
		}
		for (int i = 0; i < documentOrFolderNames.length; i++) {
//			xpath="//a[@title='"+documentOrFolderNames[i]+"']/ancestor::div[@aria-colindex='2']/following-sibling::div[@aria-colindex='7']//button[@aria-label='More Options']";
//			ele=FindElement(driver, xpath, documentOrFolderNames[i]+" more button xpath", action.SCROLLANDBOOLEAN,10);
//			if(ele!=null) {
//				log(LogStatus.PASS, "found "+documentOrFolderNames[i]+" more button", YesNo.No);
				if(clickOnFolderOrDocumentMoreOptions(documentOrFolderNames[i], boxActions)) {
					log(LogStatus.PASS, "Clicked on "+documentOrFolderNames[i]+" more button "+boxActions.toString(), YesNo.No);
					if(boxActions.toString().equalsIgnoreCase(BoxActions.Trash.toString())) {
						ele=isDisplayed(driver, FindElement(driver, "//div[@role='alertdialog']//span[text()='Okay']", "Delete Item Okay button",action.SCROLLANDBOOLEAN,10),"visibility",5,"delete item Okay button");
						if(ele!=null) {
							if(click(driver, ele, "okay button xpath", action.SCROLLANDBOOLEAN)) {
								log(LogStatus.PASS, "clicked on Okay button", YesNo.No);

							}else {
								log(LogStatus.FAIL, "Not able to click on Okay button so cannot delete "+documentOrFolderNames[i], YesNo.Yes);
								clickOnCancelButton();
								res.add("Not able to click on Okay button so cannot delete "+documentOrFolderNames[i]);

							}
						}else {
							log(LogStatus.FAIL, documentOrFolderNames[i]+" delete okay button is not found so cannot delete document",YesNo.Yes);
							res.add(documentOrFolderNames[i]+" delete okay button is not found so cannot delete document");
						}
					}
				}else {
					log(LogStatus.FAIL, "Not able to click on "+documentOrFolderNames[i]+"  more options so cannot perform "+boxActions.toString(),YesNo.Yes);
					res.add("Not able to click on "+documentOrFolderNames[i]+"  more options so cannot perform "+boxActions.toString());
				}
//			}else {
//				log(LogStatus.FAIL, "Not able to find "+documentOrFolderNames[i]+" so cannot perform "+boxActions.toString(), YesNo.Yes);
//				res.add("Not able to find "+documentOrFolderNames[i]+" so cannot perform "+boxActions.toString());
//			}
		}
		return res;
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param docmentOrFolderName
	 * @param boxActions
	 * @return true/false
	 */
	public boolean clickOnFolderOrDocumentMoreOptions(String docmentOrFolderName, BoxActions boxActions) {
		WebElement ele= null;
		String xpath="//a[@title='"+docmentOrFolderName+"']/ancestor::div[@aria-colindex='2']/following-sibling::div[@aria-colindex='7']//button[@aria-label='More Options']";
		ele= FindElement(driver, xpath,xpath+" select deselect xpath", action.SCROLLANDBOOLEAN,5);
		if(ele!=null) {
			try {
				String id=ele.getAttribute("id").trim();
				((JavascriptExecutor) driver).executeScript("document.getElementById('"+id+ "').click();");
				xpath="//ul[@aria-labelledby='"+id+"']/li/span[text()='"+boxActions+"']";
				ele= FindElement(driver, xpath,boxActions.toString()+" button", action.SCROLLANDBOOLEAN,5);
				if(ele!=null) {
					return (click(driver, ele, boxActions.toString()+" button",action.SCROLLANDBOOLEAN));
				}else {
					log(LogStatus.PASS, docmentOrFolderName+" name "+boxActions.toString()+" is not found so cannot click on it", YesNo.Yes);
				}
			}catch (Exception e) {
				log(LogStatus.FAIL, "Not able to click on "+docmentOrFolderName+" more button.", YesNo.Yes);
			}
		}else {
			log(LogStatus.FAIL, "Not able to find folder Name xpath : "+docmentOrFolderName+" so cannot click on Select/DeSelectAll Link ", YesNo.Yes);
		}
		return false;
		
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @return true/false
	 */
	public boolean clickOnCancelButton() {
		WebElement ele=isDisplayed(driver, FindElement(driver, "//div[@role='alertdialog']//span[text()='Cancel']", "Delete Item Okay button",action.SCROLLANDBOOLEAN,10),"visibility",5,"delete item Okay button");
		return click(driver, ele, "Cancel button xpath", action.SCROLLANDBOOLEAN);
	}
	
	
	
	
	
	
}
