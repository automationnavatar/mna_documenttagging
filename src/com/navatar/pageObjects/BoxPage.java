/**
 * 
 */
package com.navatar.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static com.navatar.generic.CommonLib.*;

import java.util.List;

/**
 * @author Ankur Rana
 *
 */
public class BoxPage extends BasePageBusinessLayer {

	
	public BoxPage(WebDriver driver){
		super(driver);
	}
	
	@FindBy(xpath="//input[@name='login']")
	private WebElement boxUserNameTextBox;

	/**
	 * @return the userNameTextBox
	 */
	public WebElement getBoxUserNameTextBox(int timeOut) {
		return isDisplayed(driver, boxUserNameTextBox, "Visibility", timeOut, "Box Username Text Box");
	}
	
	@FindBy(xpath="//button[text()=' Next']")
	private WebElement boxLoginNextButton;

	/**
	 * @return the boxLoginNextButton
	 */
	public WebElement getBoxLoginNextButton(int timeOut) {
		return isDisplayed(driver, boxLoginNextButton, "Visibility", timeOut, "next button");
	}

	@FindBy(xpath="//input[@name='password']")
	private WebElement boxPasswordTextBox;

	/**
	 * @return the boxPasswordTextBox
	 */
	public WebElement getBoxPasswordTextBox(int timeOut) {
		return isDisplayed(driver, boxPasswordTextBox, "Visibility", timeOut, "Password text box");
	}
	
	@FindBy(xpath="//button[text()=' Log In']")
	private WebElement boxLoginButton;

	/**
	 * @return the boxLoginButton
	 */
	public WebElement getBoxLoginButton(int timeOut) {
		return isDisplayed(driver, boxLoginButton, "Visibility", timeOut, "Login Button");
	}
	
	@FindBy(xpath="//span[text()='Admin Console']")
	private WebElement boxAdminConsoleLink;

	/**
	 * @return the boxAdminConsoleLink
	 */
	public WebElement getBoxAdminConsoleLink(int timeOut) {
		return isDisplayed(driver, boxAdminConsoleLink, "Visibility", timeOut, "Admin Console link");
	}
	
	@FindBy(xpath="//span[text()='Reports']")
	private WebElement boxReportLink;

	/**
	 * @return the boxReportLink
	 */
	public WebElement getBoxReportLink(int timeOut) {
		return isDisplayed(driver, boxReportLink, "Visibility", timeOut, "Report Link");
	}
	
	@FindBy(xpath="//button[@data-resin-target='filteredfoldersandfiles']")
	private WebElement boxFilesAndFoldersReportExport;

	/**
	 * @return the boxFilesAndFoldersReportExport
	 */
	public WebElement getBoxFilesAndFoldersReportExport(int timeOut) {
		return isDisplayed(driver, boxFilesAndFoldersReportExport, "Visibility", timeOut, "");
	}
	
	@FindBy(xpath="//span[@class='box-ui-icon-edit']")
	private WebElement boxBrowseFolderEditIcon;

	/**
	 * @return the boxBrowseFolderEditIcon
	 */
	public WebElement getBoxBrowseFolderEditIcon(int timeOut) {
		return isDisplayed(driver, boxBrowseFolderEditIcon, "Visibility", timeOut, "Edit Icon");
	}
	
	@FindBy(xpath="//button[@data-resin-target='foldersAndFiles']")
	private WebElement boxExportReportLink;

	/**
	 * @return the boxExportReportLink
	 */
	public WebElement getBoxExportReportLink(int timeOut) {
		return isDisplayed(driver, boxExportReportLink, "Visibility", timeOut, "Export Report Link");
	}
	
	@FindBy(xpath="//input[@data-resin-target='searchFolders']")
	private WebElement folderSearchTextBox;

	/**
	 * @return the folderSearchTextBox
	 */
	public WebElement getFolderSearchTextBox(int timeOut) {
		return isDisplayed(driver, folderSearchTextBox, "Visibility", timeOut, "Search folder text box");
	}
	
	@FindBy(xpath="//button[@data-resin-target='add-folder']")
	private WebElement boxDoneButton;

	/**
	 * @return the searchDoneButton
	 */
	public WebElement getBoxDoneButton(int timeOut) {
		return isDisplayed(driver, boxDoneButton, "Visibility", timeOut, "Done button");
	}
	
	@FindBy(xpath="//a[@data-resin-target='viewexportedreportsfolder']")
	private WebElement viewReportsLink;

	/**
	 * @return the viewReportsLink
	 */
	public WebElement getViewReportsLink(int timeOut) {
		return isDisplayed(driver, viewReportsLink, "Visibility", timeOut, "View Reports Link");
	}
	
	@FindBy(xpath="//span[text()='Choose']/../..")
	private WebElement boxChooseButton;

	/**
	 * @return the searchDoneButton
	 */
	public WebElement getBoxChooseButton(int timeOut) {
		return isDisplayed(driver, boxChooseButton, "Visibility", timeOut, "Choose button");
	}
	
	@FindBy(xpath="(//button[@class='btn pagination-menu-button'])[1]")
	private WebElement pagiNationButton;

	/**
	 * @return the pageNationButton
	 */
	public WebElement getPagiNationButton(int timeOut) {
		return isDisplayed(driver, pagiNationButton, "Visibility", timeOut, "page nation button");
	}
	
	public List<WebElement> getPagiNationDropDownList(){
		return FindElements(driver, "//div[contains(@class,'dropdown-menu-element dropdown-menu-')]//li", "pagination drop down list");
	}
	
	@FindBy(xpath = "//a[@id='navItemUsersAndGroups']")
	private WebElement userAndGroupLink;
	
	
	public WebElement getUserAndGroupLink(int timeOut) {
		return isDisplayed(driver, userAndGroupLink, "Visibility", timeOut, "user and group link");
	}

	@FindBy(xpath = "//button[@id='add_users_button']")
	private WebElement usersPlusButton;
	
	public WebElement getUsersPlusButton(int timeOut) {
		return isDisplayed(driver, usersPlusButton, "Visibility", timeOut, "user + button");
	}

	@FindBy(id = "add_user_public_name")
	private WebElement nameTextOnNewUserAccountDetailsPage;
	
	
	public WebElement getNameTextOnNewUserAccountDetailsPage(int timeOut) {
		return isDisplayed(driver, nameTextOnNewUserAccountDetailsPage, "Visibility", timeOut, "name ext On New User Account Details Page");
	}

	@FindBy(id = "add_user_email")
	private WebElement emailTextBoxOnUserAccountDetailsPage;
	
	public WebElement getEmailTextBoxOnUserAccountDetailsPage(int timeOut) {
		return isDisplayed(driver, emailTextBoxOnUserAccountDetailsPage, "Visibility", timeOut, "email Text Box On User Account Details Page");
	}

	@FindBy(id = "add_user_button")
	private WebElement addUserButton;
	
	public WebElement getAddUserButton(int timeOut) {
		return isDisplayed(driver, addUserButton, "Visibility", timeOut, "add User Button");
	}

	@FindBy(xpath = "//a[@id='tabview27-tab-1']/div[text()='Managed Users']")
	private WebElement managedUserTab;
	
	
	public WebElement getManagedUserTab(int timeOut) {
		return isDisplayed(driver, managedUserTab, "Visibility", timeOut, "managed User Tab");
	}

	@FindBy(xpath = "//a[@id='tabview27-tab-2']/div[text()='External Users']")
	private WebElement externalUsersTab;
	
	public WebElement getExternalUsersTab(int timeOut) {
		return isDisplayed(driver, externalUsersTab, "Visibility", timeOut, "external Users Tab");
	}

	@FindBy(xpath = "//a[@id='tabview27-tab-3']/div[text()='Groups']")
	private WebElement groupsTab;

	public WebElement getGroupsTab(int timeOut) {
		return isDisplayed(driver, groupsTab, "Visibility", timeOut, "groups Tab");
	}
	
	@FindBy(xpath = "//button[@data-resin-target='accountmenu']//span[contains(@class,'avatar-initials')]")
	private WebElement logOutDownArrow;
	
	
	public WebElement getLogOutDownArrow(int timeOut) {
		return isDisplayed(driver, logOutDownArrow, "Visibility", timeOut, "log out down arrow ");
	}

	@FindBy(xpath = "//a[@href='/logout']")
	private WebElement boxlogOutLink;

	public WebElement getBoxLogOutLink(int timeOut) {
		return isDisplayed(driver, boxlogOutLink, "Visibility", timeOut, "logOut Link");
	}
	
	@FindBy(xpath = "//div[@data-resin-page='loginsignup']//div[text()='Enter your new password']")
	private WebElement enterNewPasswordLabelText;

	public WebElement getEnterNewPasswordLabelText(int timeOut) {
		return isDisplayed(driver, enterNewPasswordLabelText, "Visibility", timeOut, "enter new password label text");
	}
	
	@FindBy(xpath = "//input[@id='new_password']")
	private WebElement newpasswordTextBox;

	public WebElement getNewpasswordTextBox(int timeOut) {
		return isDisplayed(driver, newpasswordTextBox, "Visibility", timeOut, "new password text box");
	}
	
	@FindBy(xpath = "//input[@id='confirm_password']")
	private WebElement confirmPasswordTextBox;

	public WebElement getConfirmPasswordTextBox(int timeOut) {
		return isDisplayed(driver, confirmPasswordTextBox, "Visibility", timeOut, "confirm password text box");
	}
	
	@FindBy(xpath = "//button[@id='continue']")
	private WebElement updateButton;

	public WebElement getUpdateButton(int timeOut) {
		return isDisplayed(driver, updateButton, "Visibility", timeOut, "update button");
	}
	
	
	
	@FindBy(xpath = "//a[@aria-label='Create a new item']/span[contains(text(),'New')]")
	private WebElement newButton;

	public WebElement getNewButton(int timeOut) {
		return isDisplayed(driver, newButton, "Visibility", timeOut, "new button");
	}
	
	@FindBy(xpath = "//*[@aria-label='Create a new folder']")
	private WebElement createNewFolderLink;

	public WebElement getCreateNewFolderLink(int timeOut) {
		return isDisplayed(driver, createNewFolderLink, "Visibility", timeOut, "create a new folder button");
	}
	
	@FindBy(xpath = "//input[@name='folderName']")
	private WebElement folderNameTextBoxInCreateFolderPopUp;

	public WebElement getFolderNameTextBoxInCreateFolderPopUp(int timeOut) {
		return isDisplayed(driver, folderNameTextBoxInCreateFolderPopUp, "Visibility", timeOut, "folder name text box");
	}
	
	@FindBy(xpath = "//select[@name='permission']")
	private WebElement permissionDropDownListInCreateFolderPopUp;

	public WebElement getPermissionDropDownListInCreateFolderPopUp(int timeOut) {
		return isDisplayed(driver, permissionDropDownListInCreateFolderPopUp, "Visibility", timeOut, "premission drop down list");
	}
	
	@FindBy(xpath = "//button[@data-resin-target='createfolder']/span[contains(text(),'Create')]")
	private WebElement createBtnInCreateFolderPopUp;

	public WebElement getCreateBtnInCreateFolderPopUp(int timeOut) {
		return isDisplayed(driver, createBtnInCreateFolderPopUp, "Visibility", timeOut, "create button");
	}
	
	public List<WebElement> getBoxFolderStructureListOnDocumentsPage(){
		return FindElements(driver,"//a[contains(@data-resin-target,'openfolder') or contains(@data-resin-target,'moreoptions')]","Box folder structure in documents page ");
	}
	
	
	@FindBy(xpath = "//a[@aria-label='Home']")
	private WebElement boxHomeLogoLink;

	public WebElement getBoxHomeLogoLink(int timeOut) {
		return isDisplayed(driver, boxHomeLogoLink, "Visibility", timeOut, "home logo link");
	}
	
}
