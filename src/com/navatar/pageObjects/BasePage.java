/**
 * 
 */
package com.navatar.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import com.navatar.generic.BaseLib;
import com.navatar.generic.EnumConstants.*;
import static com.navatar.generic.CommonLib.*;
import java.util.List;


public abstract class BasePage extends BaseLib {
	
	protected WebDriver driver;

	public BasePage(WebDriver driver) {
		// TODO Auto-generated constructor stub
		PageFactory.initElements(driver, this);
		this.driver = driver;
	}

	@FindBy(xpath = "//a[@title='Logout']")
	private WebElement logout;
	
	@FindBy(xpath = "//a[contains(@href,'logout')]")
	private WebElement logout_Lightning;
	
	
	
	
	
	@FindBy(xpath = "//*[contains(text(),'Maintenance')]")
	private WebElement scheduledMaintenancePopUp;

	/**
	 * @return the scheduledMaintenancePopUp
	 */
	public WebElement getScheduledMaintenancePopUp(int timeOut) {
		return isDisplayed(driver, scheduledMaintenancePopUp, "Visibility", timeOut, "Scheduled Maintenance Pop Up", action.SCROLL);
	}
	
	@FindBy(xpath="//a[@class='continue']")
	private WebElement scheduledMaintenanceContinueLink;

	/**
	 * @return the scheduledMaintenanceContinueLink
	 */
	public WebElement getScheduledMaintenanceContinueLink(int timeOut) {
		return isDisplayed(driver, scheduledMaintenanceContinueLink, "Visibility", timeOut, "scheduled Maintenance Continue Link");
	}
	
	@FindBy(xpath="//a[@title='Contacts Tab']")
	private WebElement contactsTab;

	/**
	 * @return the contactsTab
	 */
	public WebElement getContactsTab(int timeOut) {
		return isDisplayed(driver, contactsTab, "Visibility", timeOut, "Contacts Tab");
	}
	
	@FindBy(xpath="//a[@title='Funds Tab']")
	private WebElement fundsTab;

	/**
	 * @return the fundsTab
	 */
	public WebElement getFundsTab(int timeOut) {
		return isDisplayed(driver, fundsTab, "Visibility", timeOut, "Funds Tab");
	}
	
	@FindBy(xpath="//a[@title='Institutions Tab']")
	private WebElement institutionsTab;

	/**
	 * @return the fundsTab
	 */
	public WebElement getInstitutionsTab(int timeOut) {
		return isDisplayed(driver, institutionsTab, "Visibility", timeOut, "Institutions Tab");
	}
	
	@FindBy(xpath="//a[@title='Fundraisings Tab']")
	private WebElement fundRaisingsTab;

	/**
	 * @return the fundRaisingsTab
	 */
	public WebElement getFundRaisingsTab(int timeOut) {
		return isDisplayed(driver, fundRaisingsTab, "Visibility", timeOut, "FundRaisings Tab");
	} 
	
	@FindBy(xpath="//select[@name='fcf']")
	private WebElement viewDropdown;

	/**
	 * @return the viewDropdown
	 */
	public WebElement getViewDropdown(int timeOut) {
		return isDisplayed(driver, viewDropdown, "Visibility", timeOut, "View Dropdown");
	}
	@FindBy(xpath="//input[@name='go']")
	private WebElement goButton;

	/**
	 * @return the goButton
	 */
	public WebElement getGoButton(int timeOut) {
		return isDisplayed(driver, goButton, "Visibility", timeOut, "Go Button");
	} 
	
	 
	@FindBy(xpath = "//a[text()='Close']")
	private WebElement lightingCloseButton;

	/**
	 * @return the lightingCloseButton
	 */
	public WebElement getLightingCloseButton(int timeOut) {
		return isDisplayed(driver, lightingCloseButton, "Visibility", timeOut, "Linghting Pop-Up Close Icon");
	}
	

	@FindBy(xpath = "//div[@id='userNavButton']/span")
	private WebElement userMenuTab;
	
	
	public List<WebElement> getUserMenuTab_Lightning(){
		return FindElements(driver, "//span[contains(@class,'userProfileCardTriggerRoot')]//img[@alt='User']", "user menu tab in lightning");
	}
	
	/**
	 * @return the userMenuTab
	 */
	public WebElement getUserMenuTab(int timeOut) {
			return isDisplayed(driver, userMenuTab, "Visibility", timeOut, "User Menu Tab");
			
	}

	
	@FindBy(xpath="//a[contains(@class,'menuTriggerLink slds-button')]//div[contains(@class,'tooltip-trigger uiTooltip')]")
	private WebElement settingIcon_Lighting;
	
	
	/**
	 * @return the settingTab_Lighting
	 */
	public WebElement getSettingLink_Lighting(int timeOut) {
		return isDisplayed(driver, settingIcon_Lighting, "Visibility", timeOut, "setting tab in lighting");
	}
	

	@FindBy(xpath="//div[@class='menuButton menuButtonRounded']")
	private WebElement UserNameAtUserMenuTab;
	
	
	/**
	 * @return the userNameAtUserMenuTab
	 */
	public WebElement getUserNameAtUserMenuTab(int timeOut) {
		return isDisplayed(driver, UserNameAtUserMenuTab, "Visibility", timeOut, "User Name at user menu tab");
	}
	
	@FindBy(xpath = "//a[text()='Settings']")
	private WebElement settingsLinkLightning;
	
	
	/**
	 * @return the settingsLinkLightning
	 */
	public WebElement getSettingsLinkLightning(int timeOut) {
		return isDisplayed(driver, settingsLinkLightning, "Visibility", timeOut, "settings Link on user menu Lightning");
	}

	@FindBy(xpath = "//a[text()='Display & Layout']")
	private WebElement displayAndLayout_Lightning;
	
	
	/**
	 * @return the displayAndLayout_Lightning
	 */
	public WebElement getDisplayAndLayout_Lightning(int timeOut) {
		return isDisplayed(driver, displayAndLayout_Lightning, "Visibility", timeOut, "displayAndLayout_Lightning");
	}
	
	@FindBy(xpath = "//a[text()='Record Page Settings']")
	private WebElement recordPageSettings;
	
	
	/**
	 * @return the recordPageSettings
	 */
	public WebElement getRecordPageSettings(int timeOut) {
		return isDisplayed(driver, recordPageSettings, "Visibility", timeOut, "recordPageSettings");
	}
	
	@FindBy(xpath = "//img[@alt='Select Related Lists']")
	private WebElement relatedListRecordPageSetting;
	
	

	/**
	 * @return the relatedListRecordPageSetting
	 */
	public WebElement getRelatedListRecordPageSetting(int timeOut) {
		return isDisplayed(driver, relatedListRecordPageSetting, "Visibility", timeOut, "relatedListRecordPageSetting");
	}
	
	@FindBy(xpath = "//img[@alt='Select Activity Timeline']")
	private WebElement activityTimelineRecordPageSetting;
	
	

	/**
	 * @return the activityTimelineRecordPageSetting
	 */
	public WebElement getActivityTimelineRecordPageSetting(int timeOut) {
		return isDisplayed(driver, activityTimelineRecordPageSetting, "Visibility", timeOut, "activityTimelineRecordPageSetting");
	}
	
	@FindBy(xpath = "//button[text()='Save']")
	private WebElement recordPageSettingSave;
	
	
	/**
	 * @return the recordPageSettingSave
	 */
	public WebElement getRecordPageSettingSave(int timeOut) {
		return isDisplayed(driver, recordPageSettingSave, "Visibility", timeOut, "recordPageSettingSave");
	}

	
	@FindBy(xpath = "(//a[@title='Logout'])[1]")
	private WebElement logOutButton;

	/**
	 * @return the logOutButton
	 */
	public WebElement getLogOutButton(int timeOut) {
		return isDisplayed(driver, logOutButton, "Visibility", timeOut, "Logout Button");
	}
	
	@FindBy(xpath = "(//input[@title='New User'])[1]")
	private WebElement newUserLink;

	/**
	 * @return the newUserLink
	 */
	public WebElement getNewUserLink(int timeOut) {
		return isDisplayed(driver, newUserLink, "Visibility", timeOut, "New User Button");
	}
	
	@FindBy(id="name_firstName")
	private WebElement userFirstName;

	/**
	 * @return the userFirstName
	 */
	public WebElement getUserFirstName(int timeOut) {
		return isDisplayed(driver, userFirstName, "Visibility", timeOut, "User First Name");
	}
	
	@FindBy(id="name_lastName")
	private WebElement userLastName;

	/**
	 * @return the userLastName
	 */
	public WebElement getUserLastName(int timeOut) {
		return isDisplayed(driver, userLastName, "Visibility", timeOut, "User Last Name");
	}
	@FindBy(id="user_license_id")
	private WebElement userUserLicenseDropDownList;

	/**
	 * @return the userUserLicenseDropDownList
	 */
	public WebElement getUserUserLicenseDropDownList(int timeOut) {
		return isDisplayed(driver, userUserLicenseDropDownList, "Visibility", timeOut, "User License Drop Down List");
	}
	
	@FindBy(xpath = "//select[@id='role']")
	private WebElement userRoleDropDownList;
	
	
	public WebElement getUserRoleDropDownList(int timeOut) {
		return isDisplayed(driver, userRoleDropDownList, "Visibility", timeOut, "user role drop down list");
	}

	@FindBy(id="Profile")
	private WebElement userProfileDropDownList;

	/**
	 * @return the userProfileDropDownList
	 */
	public WebElement getUserProfileDropDownList(int timeOut) {
		return isDisplayed(driver, userProfileDropDownList, "Visibility", timeOut, "User Profile Drop Down List");
	}
	
	@FindBy(id="Email")
	private WebElement userEmailId;

	/**
	 * @return the userEmailId
	 */
	public WebElement getUserEmailId(int timeOut) {
		return isDisplayed(driver, userEmailId, "Visibility", timeOut, "User Email Id");
	}
	
	@FindBy(xpath="//td[text()='Email']/../td[2]/a")
	private WebElement userEmailIDLabeltext;

	/**
	 * @return the userEmailIDLabeltext
	 */
	public WebElement getUserEmailIDLabeltext(int timeOut) {
		return isDisplayed(driver, userEmailIDLabeltext, "Visibility", timeOut, "Email ID Label Text");
	}
	
	
	
	@FindBy(xpath = "//a[contains(@title,'Manage Licenses')]")
	private WebElement manageLicensesLink;

	/**
	 * @return the manageLicensesLink
	 */
	public WebElement getManageLicensesLink(int timeOut) {
		return isDisplayed(driver, manageLicensesLink, "Visibility", timeOut, "Manage Licenses Link");
	}
	
	@FindBy(xpath = "//input[@title='Add Users']")
	private WebElement addUsersbutton;

	/**
	 * @return the addUsersbutton
	 */
	public WebElement getAddUsersbutton(int timeOut) {
		return isDisplayed(driver, addUsersbutton, "Visibility", timeOut, "Add Users Button");
	}
	
	
	
	@FindBy(xpath="//div[@class='listRelatedObject userBlock']//form/div[2]/table//tr[1]//a[contains(@title,'Active')]")
	private WebElement ActiveUserTab;

	/**
	 * @return the activeUserTab
	 */
	public WebElement getActiveUserTab(int timeOut) {
		return isDisplayed(driver, ActiveUserTab, "Visibility", timeOut, "Active User Tab");
	}
	
	@FindBy(xpath="//input[@title='Add']")
	private WebElement activeUserAddButton;

	/**
	 * @return the activeUserAddButton
	 */
	public WebElement getActiveUserAddButton(int timeOut) {
		return isDisplayed(driver, activeUserAddButton, "Visibility", timeOut, "Active User Add Button");
	}
	@FindBy(xpath="//label[text()='Salesforce CRM Content User']/../following-sibling::td/input")
	private WebElement SalesforceCRMContentUserCheckBox;

	/**
	 * @return the salesforceCRMContentUserCheckBox
	 */
	public WebElement getSalesforceCRMContentUserCheckBox(int timeOut) {
		return isDisplayed(driver, SalesforceCRMContentUserCheckBox, "Visibility", timeOut, "Salesforce CRM Content User Check Box");
	}
	
	@FindBy(xpath = "//h2[contains(text(),'Change Your Password')]")
	private WebElement chnageYourPassword;

	/**
	 * @return the chnageYourPassword
	 */
	public WebElement getChnageYourPassword(int timeOut) {
		return isDisplayed(driver, chnageYourPassword, "Visibility", timeOut, "Change password Text label");
	}
	
	@FindBy(id = "newpassword")
	private WebElement newPassword;

	/**
	 * @return the newPassword
	 */
	public WebElement getNewPassword(int timeOut) {
		return isDisplayed(driver, newPassword,"Visibility", timeOut, "New password Text Box");
	}
	
	@FindBy(id = "confirmpassword")
	private WebElement confimpassword;

	/**
	 * @return the confimpassword
	 */
	public WebElement getConfimpassword(int timeOut) {
		return isDisplayed(driver, confimpassword, "Visibility", timeOut, "Confirm password Text Box");
	}

	@FindBy(id = "question")
	private WebElement question;

	/**
	 * @return the question
	 */
	public WebElement getQuestion(int timeOut) {
		return isDisplayed(driver, question, "Visibility", timeOut, "Question drop Doown List");
	}

	@FindBy(id = "answer")
	private WebElement answer;

	/**
	 * @return the answer
	 */
	public WebElement getAnswer(int timeOut) {
		return isDisplayed(driver, answer, "Visibility", timeOut, "Answer Text Box");
	}

	@FindBy(xpath = "//button[contains(text(),' Change Password')]")
	private WebElement changePassword;

	/**
	 * @return the changePassword
	 */
	public WebElement getChangePassword(int timeOut) {
		return isDisplayed(driver, changePassword, "Visibility", timeOut, "Change Password Button");
	}

	@FindBy(xpath="//li[@id='AllTab_Tab']/a")
	private WebElement allTabBtn;

	/**
	 * @return the allTabBtn
	 */
	public WebElement getAllTabBtn(int timeOut) {
		return isDisplayed(driver, allTabBtn, "Visibility", timeOut, "All Tab Button");
	}
	
	@FindBy(xpath="//div[@class='bDescription']/a")
	private WebElement addTabLink;

	/**
	 * @return the addTabLink
	 */
	public WebElement getAddTabLink(int timeOut) {
		return isDisplayed(driver, addTabLink, "Visibility", timeOut, "Add a Tab Link");
	}
	@FindBy(xpath="//tr[@ class='last detailRow']//table//td[1]/select")
	private WebElement availableTabList;

	/**
	 * @return the availableTabList
	 */
	public WebElement getAvailableTabList(int timeOut) {
		return isDisplayed(driver, availableTabList, "Visibility", timeOut, "Available Tab Drop Down List");
	}
	
	@FindBy(xpath="//tr[@ class='last detailRow']//td[2]/div[2]/a")
	private WebElement customTabAddBtn;

	/**
	 * @return the customTabAddBtn
	 */
	public WebElement getCustomTabAddBtn(int timeOut) {
		return isDisplayed(driver, customTabAddBtn, "Visibility", timeOut, "Custom Tab Add Button");
	}
	
	@FindBy(xpath="(//div[contains(@class,'uiTooltip')])[7]")
	private WebElement salesForceLightingIcon;

	/**
	 * @return the salesForceLightingIcon
	 */
	public WebElement getSalesForceLightingIcon(int timeOut) {
		return isDisplayed(driver, salesForceLightingIcon, "Visibility", timeOut, "Sales Force Lighting Icon");
	}
	
	@FindBy(xpath = "//div[@class='oneUserProfileCard']//a[text()='Settings']")
	private WebElement settings_icon;

	public WebElement getSettings_icon(int timeOut) {
		return isDisplayed(driver, settings_icon, "Visibility", timeOut, "Settings link");
	}
	
	@FindBy(xpath = "//li[@data-node-id='ChangePassword']//a")
	private WebElement changePasswordLink;

	public WebElement getChangePasswordLink(int timeOut) {
		return isDisplayed(driver, changePasswordLink, "Visibility", timeOut, "change password link");
	}
	
	@FindBy(xpath = "//iframe[contains(@title,'Change My Password')]")
	private WebElement changePasswordIframe;

	public WebElement getChangePasswordIframe(int timeOut) {
		return isDisplayed(driver, changePasswordIframe, "Visibility", timeOut, "change password iframe");
	}
	
	@FindBy(xpath = "//input[@id='currentpassword']")
	private WebElement currentPassword;

	public WebElement getCurrentPassword(int timeOut) {
		return isDisplayed(driver, currentPassword, "Visibility", timeOut, "current password");
	}
	
	@FindBy(xpath = "//input[@id='newpassword']")
	private WebElement changePasswordNewPassword;

	public WebElement getChangePasswordNewPassword(int timeOut) {
		return isDisplayed(driver, changePasswordNewPassword, "Visibility", timeOut, "new password");
	}
	
	@FindBy(xpath = "//input[@id='confirmpassword']")
	private WebElement ChangepasswordconfirmPassword;

	public WebElement getChangepasswordconfirmPassword(int timeOut) {
		return isDisplayed(driver, ChangepasswordconfirmPassword, "Visibility", timeOut, "confirm new password");
	}
	
	
	@FindBy(xpath = "//button[@id='password-button']")
	private WebElement changePasswordSaveButton;

	public WebElement getChangePasswordSaveButton(int timeOut) {
		return isDisplayed(driver, changePasswordSaveButton, "Visibility", timeOut, "save button");
	}
	
	@FindBy(xpath = "//div[@class='setup change-password']/div[1]")
	private WebElement changePasswordSuccessfulMsg;

	public WebElement getChangePasswordSuccessfulMsg(int timeOut) {
		return isDisplayed(driver, changePasswordSuccessfulMsg, "Visibility", timeOut, "change password successful message ");
	}
	
}
