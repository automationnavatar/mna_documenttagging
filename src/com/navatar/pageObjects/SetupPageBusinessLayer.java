package com.navatar.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.navatar.generic.EnumConstants.Mode;
import com.navatar.generic.EnumConstants.YesNo;
import com.navatar.generic.EnumConstants.action;
import com.relevantcodes.extentreports.LogStatus;

import static com.navatar.generic.CommonLib.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import static com.navatar.generic.AppListeners.*;
public class SetupPageBusinessLayer extends SetupPage {

	public SetupPageBusinessLayer(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @author Ankit Jaiswal
	 * @param environment
	 * @param mode
	 * @param objectName
	 * @return true/false
	 */
	public boolean searchStandardOrCustomObject(String environment, String mode, object objectName) {
		if(mode.equalsIgnoreCase(Mode.Classic.toString())) {
			if(sendKeys(driver,getQucikSearchInSetupPage(environment, mode, 30),objectName.toString(),"quick search text box in setup page", action.SCROLLANDBOOLEAN)) {
				appLog.info("passed value in serach text box: "+objectName);
				return true;
			}else {
				appLog.error("Not able to search object in classic : "+objectName);
			}
		}else {
			if (click(driver, getObjectManager_Lighting(30), "object manager tab", action.SCROLLANDBOOLEAN)) {
				appLog.info("clicked on object manager tab");
				if(sendKeys(driver, getQuickSearchInObjectManager_Lighting(30), objectName.toString(), "quick search text box in lighting", action.SCROLLANDBOOLEAN)) {
					appLog.info("passed value in quick search text box: "+ objectName);
					return true;
				}else {
					appLog.error("Not able to search object in lighting : "+objectName);
				}
			} else {
				appLog.error("Not able to click on object manager tab so cannot search object: "+objectName);
			}
	}
		return false;
}

	/**
	 * @author Ankit Jaiswal
	 * @param userfirstname
	 * @param userlastname
	 * @param email
	 * @param userLicense
	 * @param userProfile
	 * @param role TODO
	 * @return true/false
	 */
	public boolean createPEUser(String environment, String mode,String userfirstname, String userlastname, String email, String userLicense,
			String userProfile, String role) {
			if (click(driver, getExpandUserIcon(environment, mode, 30), "expand User Icon", action.SCROLLANDBOOLEAN)) {
				appLog.info("clicked on user expand icon");
				if (click(driver, getUsersLink(environment, mode, 60), "User Link", action.SCROLLANDBOOLEAN)) {
					appLog.info("clicked on users link");
					if(mode.equalsIgnoreCase(Mode.Lightning.toString())) {
						ThreadSleep(2000);
						switchToFrame(driver, 60, getSetUpPageIframe(60));
					}
					if (click(driver, getNewUserLink(60), "New User Button", action.SCROLLANDBOOLEAN)) {
						appLog.info("clicked on new users button");
						if(mode.equalsIgnoreCase(Mode.Lightning.toString())) {
							switchToDefaultContent(driver);
							switchToFrame(driver, 60, getSetUpPageIframe(60));
						}
						if (sendKeys(driver, getUserFirstName(60), userfirstname, "User First Name",
								action.SCROLLANDBOOLEAN)) {
							if (sendKeys(driver, getUserLastName(60), userlastname, "User Last Name",
									action.SCROLLANDBOOLEAN)) {
								if (sendKeys(driver, getUserEmailId(60), email, "User Email Id",action.SCROLLANDBOOLEAN)) {
									if(role!=null) {
										if (selectVisibleTextFromDropDown(driver, getUserRoleDropDownList(60),"User role drop down list", role)) {
											appLog.info("select user role from drop downlist: " + role);
											
										}else {
											appLog.error("Not able to select role from drop down list: "+role+" so cannot create user.");
											return false;
										}
									}
									if (selectVisibleTextFromDropDown(driver, getUserUserLicenseDropDownList(60),
											"User License drop down list", userLicense)) {
										appLog.info("select user license from drop downlist: " + userLicense);
										if (selectVisibleTextFromDropDown(driver, getUserProfileDropDownList(60),
												"User profile drop down list", userProfile)) {
											appLog.info("select user profile from drop downlist: " + userProfile);
											if(click(driver, getSalesforceCRMContentUserCheckBox(60), "Salesforce CRM Content User check Box",
													action.SCROLLANDBOOLEAN)){
												if(mode.equalsIgnoreCase(Mode.Lightning.toString())) {
													if (click(driver, getCreateUserSaveBtn_Lighting(30), "Save Button",
															action.SCROLLANDBOOLEAN)) {
														appLog.info("clicked on save button");
														appLog.info("CRM User is created successfully: " + userfirstname
																+ " " + userlastname);
														switchToDefaultContent(driver);
														return true;
													} else {
														appLog.error(
																"Not able to click on save buttton so cannot create user: "
																		+ userfirstname + " " + userlastname);
													}
												}else {
													if (click(driver, getSaveButton(environment, mode, 20), "Save Button",
															action.SCROLLANDBOOLEAN)) {
														appLog.info("clicked on save button");
														appLog.info("CRM User is created successfully: " + userfirstname
																+ " " + userlastname);
														return true;
													} else {
														appLog.error(
																"Not able to click on save buttton so cannot create user: "
																		+ userfirstname + " " + userlastname);
													}
												}
											}else{
												appLog.info("Not able to click on content user checkbox");
											}
										} else {
											appLog.error("Not able to select profile from drop downlist: "
													+ userProfile + " so cannot create user: " + userfirstname + " "
													+ userlastname);
										}
										
									} else {
										appLog.error("Not able to select user license from drop downlist: "
												+ userLicense + " so cannot create user: " + userfirstname + " "
												+ userlastname);
									}
									
								} else {
									appLog.error("Not able to pass email id in text box: " + email
											+ " so cannot create user: " + userfirstname + " " + userlastname);
								}
								
							} else {
								appLog.error("Not able to pass user last name in text box: " + userlastname
										+ " so cannot create user: " + userfirstname + " " + userlastname);
							}
						} else {
							appLog.error("Not able pass user first name in text box: " + userfirstname
									+ " so cannot create user: " + userfirstname + " " + userlastname);
						}
						
					} else {
						appLog.error("Not able to click on new user button so cannot create user: " + userfirstname
								+ " " + userlastname);
					}
					
				} else {
					appLog.error("Not able to click on users link so cannot create user: " + userfirstname + " "
							+ userlastname);
				}
				
			} else {
				appLog.error("Not able to click on manage user expand icon so cannot create user: " + userfirstname
						+ " " + userlastname);
			}
			if(mode.equalsIgnoreCase(Mode.Lightning.toString())){
				switchToDefaultContent(driver);
			}
		return false;
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param environment
	 * @param mode
	 * @param userlastName
	 * @param emailId
	 * @return true/false
	 */
	public boolean updatecreatedUserDetails(String environment, String mode, String userlastName, String emailId) {
		boolean flag = false;
		HomePageBusineesLayer home = new HomePageBusineesLayer(driver);
		if (home.clickOnSetUpLink(environment, mode)) {
			log(LogStatus.PASS, "clicked on setup link", YesNo.No);
			String parentWindow = switchOnWindow(driver);
			if (parentWindow != null) {
				if (click(driver, getExpandUserIcon(environment, mode, 30), "expand User Icon", action.SCROLLANDBOOLEAN)) {
					log(LogStatus.PASS, "clicked on user expand icon", YesNo.No);
					if (click(driver, getUsersLink(environment, mode, 60), "User Link", action.SCROLLANDBOOLEAN)) {
						log(LogStatus.PASS, "clicked on user link ", YesNo.No);
						ThreadSleep(5000);
						if(switchToFrame(driver, 60, getSetUpPageIframe(60))) {
							ThreadSleep(10000);
							if(click(driver, getcreatedUserEditBtn(emailId, 30), "edit link", action.SCROLLANDBOOLEAN)) {
								log(LogStatus.PASS, "clicked on edit link of user "+emailId, YesNo.No);
								ThreadSleep(10000);
								if(switchToFrame(driver, 60, getSetUpPageIframe(60))) {
									if (sendKeys(driver, getUserLastName(60), userlastName, "User Last Name",action.SCROLLANDBOOLEAN)) {
										log(LogStatus.PASS, "passed user last name : "+userlastName, YesNo.No);
										if (click(driver, getCreateUserSaveBtn_Lighting(30), "Save Button",action.SCROLLANDBOOLEAN)) {
											log(LogStatus.PASS, "clicked on save button ", YesNo.No);
											flag= true;
											driver.close();
											driver.switchTo().window(parentWindow);
											
										} else {
											log(LogStatus.FAIL, "Not able to click on save button so cannot update user details", YesNo.Yes);
											driver.close();
											driver.switchTo().window(parentWindow);
										}
									}else {
										log(LogStatus.FAIL, "Not able to pass user last name "+userlastName, YesNo.Yes);
										driver.close();
										driver.switchTo().window(parentWindow);
									}
								}else {
									log(LogStatus.FAIL, "Not able to switch frame in edit users page so cannot click on edit button", YesNo.Yes);
									driver.close();
									driver.switchTo().window(parentWindow);
								}
							}else {
								log(LogStatus.FAIL, "Not able to click on edit link so cannot udate details ", YesNo.Yes);
								driver.close();
								driver.switchTo().window(parentWindow);
							}
						}else {
							log(LogStatus.FAIL, "Not able to switch frame in users page so cannot click on edit button", YesNo.Yes);
							driver.close();
							driver.switchTo().window(parentWindow);
						}
					}else {
						log(LogStatus.FAIL, "Not able to click on user link so cannot update user details", YesNo.Yes);
						driver.close();
						driver.switchTo().window(parentWindow);
					}
				}else {
					log(LogStatus.FAIL, "Not able to expand user options so cannot update user details", YesNo.Yes);
					driver.close();
					driver.switchTo().window(parentWindow);
				}
			}else {
				log(LogStatus.FAIL, "Not able to switch setup window so cannot update user details", YesNo.Yes);
			}
			
		}else {
			log(LogStatus.FAIL, "Not able to click on setup link so cannot update user details", YesNo.Yes);
		}
		return flag;
	}

	/**
	 * @author Ankit Jaiswal
	 * @param firstName
	 * @param lastName
	 * @return true/false
	 */
	public boolean installedPackages(String environment, String mode,String firstName, String lastName) {
		if(mode.equalsIgnoreCase(Mode.Lightning.toString())) {
			if(sendKeys(driver,getQucikSearchInSetupPage(environment, mode, 30),"Installed package","quick search text box in setup page", action.SCROLLANDBOOLEAN)) {
				log(LogStatus.INFO, "passed value in serach text box installed package ", YesNo.No);
				
			}else {
				log(LogStatus.INFO, "Not able to search installed package in search text box so cannot click on installed package link in lightning", YesNo.Yes);
				return false;
			}
		}
		if (click(driver, getInstalledPackageLink(environment, mode, 60), "Installed Package link", action.SCROLLANDBOOLEAN)) {
			log(LogStatus.INFO,"clicked on installed package link", YesNo.No);
			ThreadSleep(5000);
			if(mode.equalsIgnoreCase(Mode.Lightning.toString())) {
				switchToFrame(driver, 60, getSetUpPageIframe(60));
			}
			ThreadSleep(5000);
			if (click(driver, getManageLicensesLink(60), "Manage licenses link", action.SCROLLANDBOOLEAN)) {
				log(LogStatus.INFO,"clicked on manage licenses link", YesNo.No);
				if(mode.equalsIgnoreCase(Mode.Lightning.toString())) {
					switchToDefaultContent(driver);
					switchToFrame(driver, 30, getSetUpPageIframe(30));
				}
				if (click(driver, getAddUsersbutton(60), "Add Users link", action.BOOLEAN)) {
					log(LogStatus.INFO,"clicked on add users button", YesNo.No);
					if(mode.equalsIgnoreCase(Mode.Lightning.toString())) {
						switchToDefaultContent(driver);
					}
					if(mode.equalsIgnoreCase(Mode.Lightning.toString())) {
						switchToFrame(driver, 30, getInstalledPackageParentFrame_Lighting(20));
					}
					if (switchToFrame(driver, 30, getInstalledPackageFrame(20))) {
						for (int i = 0; i < 2; i++) {
							if (click(driver, getActiveUserTab(60), "Active Users Tab",
									action.SCROLLANDBOOLEAN)) {
								log(LogStatus.INFO,"Clicked on active user tab ", YesNo.No);
							} else {
								if (i == 1) {
									switchToDefaultContent(driver);
									log(LogStatus.INFO,"Not able to click on active user tab", YesNo.Yes);
								}
							}
						}
						WebElement ele = FindElement(driver,
								"//img[@title='Checked']/../..//span[contains(text(),'" + lastName + ", "
										+ firstName + "')]/../..//input",
										"Activate User Check Box", action.BOOLEAN, 20);
						if (ele != null) {
							for (int i = 0; i < 2; i++) {
								if (click(driver, ele, firstName + " " + lastName + " check box",
										action.BOOLEAN)) {
									ThreadSleep(2000);
									WebElement checkBox = FindElement(driver,
											"//img[@title='Checked']/../..//span[contains(text(),'" + lastName
											+ ", " + firstName + "')]/../..//input",
											"Activate User Check Box", action.BOOLEAN, 20);
									if (isSelected(driver, checkBox,
											firstName + " " + lastName + " check box")) {
										log(LogStatus.INFO,"clicked on user check box: " + firstName + " " + lastName, YesNo.No);
										if(mode.equalsIgnoreCase(Mode.Lightning.toString())) {
											switchToDefaultContent(driver);
											switchToFrame(driver, 60, getInstalledPackageParentFrame_Lighting(60));
										}else {
											switchToDefaultContent(driver);
										}
										if (click(driver, getActiveUserAddButton(60), "Active User Add Button",
												action.BOOLEAN)) {
											log(LogStatus.INFO,"clicked on add button", YesNo.No);
											log(LogStatus.INFO,"package is installed successfully: " + firstName + " "
													+ lastName, YesNo.No);
											switchToDefaultContent(driver);
											return true;

										} else {
											switchToDefaultContent(driver);
											log(LogStatus.INFO,"Not able to click on add button so cannot install user package: "
													+ firstName + " " + lastName, YesNo.Yes);
										}
									} else {
										if (i == 1) {
											switchToDefaultContent(driver);
											log(LogStatus.INFO,"username checkbox is not selected in istalled package : "
													+ firstName + " " + lastName, YesNo.Yes);
										}
									}
								} else {
									if (i == 1) {
										switchToDefaultContent(driver);
										log(LogStatus.INFO,"Not able to click on user check box: " + firstName + " "
												+ lastName, YesNo.Yes);
									}
								}
							}
						} else {
							switchToDefaultContent(driver);
							log(LogStatus.INFO,"create user " + firstName + " " + lastName
									+ " is not visible so cannot istall user package: " + firstName + " "
									+ lastName, YesNo.Yes);
						}
					} else {
						switchToDefaultContent(driver);
						log(LogStatus.INFO,"installed package frame is not loaded so cannot install user package: "
								+ firstName + " " + lastName, YesNo.Yes);
					}
				} else {
					log(LogStatus.INFO,"Not able to click on add users button so cannot install user package: "
							+ firstName + " " + lastName, YesNo.Yes);
					if(mode.equalsIgnoreCase(Mode.Lightning.toString())) {
						switchToDefaultContent(driver);
					}
				}
			} else {
				log(LogStatus.INFO,"Not able to click on manage licenses link so cannot install user package: "
						+ firstName + " " + lastName, YesNo.Yes);
				if(mode.equalsIgnoreCase(Mode.Lightning.toString())) {
					switchToDefaultContent(driver);
				}
			}
		} else {
			log(LogStatus.INFO,"Not able to click on installed packages link so cannot istall user package: "
					+ firstName + " " + lastName, YesNo.Yes);
		}
		if(mode.equalsIgnoreCase(Mode.Lightning.toString())) {
			switchToDefaultContent(driver);
		}
		return false;
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param environment
	 * @param mode
	 * @param objectName
	 * @param objectLeftSideActions
	 * @param dataType
	 * @param fieldLabelName
	 * @param formulaReturnType
	 * @param formulaText
	 * @return true/false
	 */
	public boolean addCustomFieldforFormula(String environment, String mode,object objectName, String objectLeftSideActions, String dataType,String fieldLabelName,String formulaReturnType,String formulaText) {
		WebElement ele=null;
		if(searchStandardOrCustomObject(environment, mode, objectName)) {
			log(LogStatus.PASS, "object searched : "+objectName.toString(), YesNo.No);
			ThreadSleep(5000);
			String xpath="//div[@data-aura-class='uiScroller']//a[text()='"+objectName.toString()+"']";
			ele=FindElement(driver, xpath,objectName.toString()+" xpath", action.SCROLLANDBOOLEAN,30);
			if(ele!=null) {
				if(click(driver, ele, objectName.toString()+" xpath ", action.SCROLLANDBOOLEAN)) {
					log(LogStatus.PASS, "clicked on object Name "+objectName, YesNo.No);
					ThreadSleep(5000);
					xpath="//div[@id='setupComponent']/div[@class='setupcontent']//ul/li/a[@data-list='"+objectLeftSideActions+"']";
					ele=FindElement(driver, xpath,objectLeftSideActions+" xpath", action.SCROLLANDBOOLEAN,20);
					if(click(driver, ele, objectLeftSideActions+" xpath ", action.SCROLLANDBOOLEAN)) {
						log(LogStatus.PASS, "clicked on object side action :  "+objectLeftSideActions, YesNo.No);
						ThreadSleep(5000);
						if(click(driver, getCustomFieldNewButton(30),"new button", action.SCROLLANDBOOLEAN)) {
							log(LogStatus.PASS, "clicked on new button", YesNo.No);
							ThreadSleep(1000);
							if(switchToFrame(driver, 30, getNewCustomFieldFrame(objectName,30))) {
								if(click(driver, getNewCustomFieldDataTypeOrFormulaReturnType(dataType, 30),"data type radio button", action.SCROLLANDBOOLEAN)) {
									log(LogStatus.PASS,dataType+" : data type radio button ",YesNo.No);
									if(click(driver, getCustomFieldNextBtn(30),"next button", action.SCROLLANDBOOLEAN)) {
										log(LogStatus.PASS, "Clicked on Step 1 Next button", YesNo.No);
										ThreadSleep(1000);
										if(sendKeys(driver, getFieldLabelTextBox(30), fieldLabelName, "field label name ", action.SCROLLANDBOOLEAN)) {
											log(LogStatus.PASS, "passed value in field label text box : "+fieldLabelName, YesNo.No);
											if(!formulaReturnType.equalsIgnoreCase("None")) {
												if(click(driver, getNewCustomFieldDataTypeOrFormulaReturnType(formulaReturnType, 30),"formula return type radio button", action.SCROLLANDBOOLEAN)) {
													log(LogStatus.PASS, "clicked on formula return type radio button : "+formulaReturnType, YesNo.No);
												}else {
													log(LogStatus.FAIL, "Not able to click on formula retun type radio button : "+formulaReturnType, YesNo.Yes);
													switchToDefaultContent(driver);
													return false;
												}
											}
											if(click(driver, getCustomFieldNextBtn(30),"next button", action.SCROLLANDBOOLEAN)) {
												log(LogStatus.PASS, "Clicked on Step 2 Next button", YesNo.No);
												ThreadSleep(2000);
												if(sendKeys(driver, getFormulaTextBox(30), formulaText,"formula text area", action.SCROLLANDBOOLEAN)) {
													log(LogStatus.PASS, "Passed Value in formula Text Box "+formulaText, YesNo.No);
													if(click(driver, getCustomFieldNextBtn(30),"next button", action.SCROLLANDBOOLEAN)) {
														log(LogStatus.PASS, "Clicked on Step 3 Next button", YesNo.No);
														ThreadSleep(1000);
														if(click(driver, getCustomFieldNextBtn(30),"next button", action.SCROLLANDBOOLEAN)) {
															log(LogStatus.PASS, "Clicked on Step 4 Next button", YesNo.No);
															ThreadSleep(1000);
															if(click(driver, getCustomFieldSaveBtn(30), "save button", action.SCROLLANDBOOLEAN)) {
																log(LogStatus.PASS, "clicked on save button", YesNo.No);
																switchToDefaultContent(driver);
																return true;
																
															}else {
																log(LogStatus.FAIL, "Not able to click on save button so cannot create custom field "+objectName, YesNo.Yes);
															}
															
														}else {
															log(LogStatus.FAIL, "Not able to click on Step 4 next button so cannot create custom field : "+objectName,YesNo.Yes);
														}
														
													}else {
														log(LogStatus.FAIL, "Not able to click on Step 3 next button so cannot create custom field : "+objectName,YesNo.Yes);
													}
													
													
												}else {
													log(LogStatus.FAIL, "Not able to enter formula in textarea "+formulaText+" so cannot create custom field",YesNo.Yes);
												}
											}else {
												log(LogStatus.FAIL, "Not able to click on Step 2 next button so cannot create custom field : "+objectName,YesNo.Yes);
											}
										}else {
											log(LogStatus.FAIL, "Not able to enter value in field label text box : "+fieldLabelName+" so cannot create custom field", YesNo.Yes);
										}
									}else {
										log(LogStatus.FAIL, "Not able to click on Step 1 next button so cannot create custom field", YesNo.Yes);
									}
								}else {
									log(LogStatus.FAIL, "Not able to click on data type radio button "+dataType+" so cannot create custom field", YesNo.Yes);
								}
							}else {
								log(LogStatus.FAIL, "Not able to switch in "+objectName+" new object frame so cannot add custom object", YesNo.Yes);
							}
						}else {
							log(LogStatus.FAIL, "Not able to click on New button so cannot add custom field in "+objectName.toString(), YesNo.Yes);
						}
						
					}else {
						log(LogStatus.FAIL, "Not able to click on object side action "+objectLeftSideActions+" so cannot add custom object ", YesNo.Yes);
					}
					
				}else {
					log(LogStatus.FAIL, "Not able to click on object Name "+objectName+" so cannot add custom object ", YesNo.Yes);
				}
			}else {
				log(LogStatus.FAIL, "Not able to found object : "+objectName.toString()+" so cannot add custom object", YesNo.Yes);
			}
		}else {
			log(LogStatus.FAIL, "Not able to search object "+objectName.toString()+" so cannot add custom object", YesNo.Yes);
		}
		switchToDefaultContent(driver);
		return false;
	}
	
	
	
	
}
