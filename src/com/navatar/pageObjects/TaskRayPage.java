package com.navatar.pageObjects;


import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.navatar.generic.EnumConstants.action;

import static com.navatar.generic.CommonLib.*;

public class TaskRayPage extends BasePageBusinessLayer {

	public TaskRayPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	@FindBy(xpath="//iframe[@title='TaskRay']")
	private WebElement taskRayFrame;

	/**
	 * @return the taskRayFrame
	 */
	public WebElement getTaskRayFrame(int timeOut) {
		return isDisplayed(driver, taskRayFrame, "Visibility", timeOut, "Task Ray Frame");
	}
	
	@FindBy(xpath="//img[@title='Fund Lookup (New Window)']")
	private WebElement fundLookUpIconOnNewProjectPopUp;

	/**
	 * @return the fundLookUpIconOnNewProjectPopUp
	 */
	public WebElement getFundLookUpIconOnNewProjectPopUp(int timeOut) {
		return isDisplayed(driver, fundLookUpIconOnNewProjectPopUp, "Visibility", timeOut, "fund look up icon on New Project Pop Up");
	}
	
	
	@FindBy(xpath = "//input[@class='fieldSet-Pipeline__c']")
	private WebElement pipeLineTextBox;
	
	public WebElement getPipeLineTextBox(int timeOut) {
		return isDisplayed(driver, pipeLineTextBox, "Visibility", timeOut, "pipeline text box");
	}
	
	
	@FindBy(xpath = "//input[@class='fieldSet-Fund__c']")
	private WebElement fundTextBox;
	

	public WebElement getFundTextBox(int timeOut) {
		return  isDisplayed(driver, fundTextBox, "Visibility", timeOut, "fund text box");
	}

	@FindBy(css = "#quick-add-text-btn")
	private WebElement newTaskButton;


	public WebElement getNewTaskButton(int timeOut) {
		return isDisplayed(driver, newTaskButton, "Visibility", timeOut, "new task button");
	}

	
	@FindBy(css = "input.fieldSet-Name")
	private WebElement taskNameTextBox;


	public WebElement getTaskNameTextBox(int timeOut) {
		return isDisplayed(driver, taskNameTextBox, "Visibility", timeOut, "task name text box");
	}
	
	@FindBy(xpath = "//a[@title='Project Lookup (New Window)']")
	private WebElement projectNameLookUpIcon;

	public WebElement getProjectNameLookUpIcon(int timeOut) {
		 return isDisplayed(driver, projectNameLookUpIcon, "Visibility", timeOut, "project name look up icon");
	}
	
	@FindBy(xpath = "//input[@title='Owner']")
	private WebElement createdTaskOwnerNameTextBox;

	public WebElement getCreatedTaskOwnerNameTextBox(int timeOut) {
		return isDisplayed(driver, createdTaskOwnerNameTextBox, "Visibility", timeOut, " created task owner name text box");
	}
	
	@FindBy(css = "select.fieldSet-TASKRAY__List__c")
	private WebElement statusDropDownList;


	public WebElement getStatusDropDownList(int timeOut) {
		return isDisplayed(driver, statusDropDownList, "Visibility", timeOut, "status drop down list");
	}
	
	
	public WebElement getStartOrEndDateTextBox(excelLabel excelLabel, int timeOut) {
		String xpath="//label[text()='"+excelLabel.toString().replace("_", " ")+"']/../../following-sibling::td//input";
		WebElement ele= FindElement(driver, xpath,excelLabel.toString()+" text box xpath ", action.SCROLLANDBOOLEAN, timeOut);
		return isDisplayed(driver, ele, "visibility", timeOut,excelLabel.toString()+" text box xpath ");
				
				
	}
	
	@FindBy(xpath = "//div[@class='editPage']//div[@class='modal-footer']/a[text()='Edit']")
	private WebElement createdTaskEditButton;
	
	public WebElement getCreatedTaskEditButton(int timeOut) {
		return isDisplayed(driver, createdTaskEditButton, "Visibility", timeOut, "created task edit button");
	}

	@FindBy(xpath = "//div[@class='editPage']//a[text()='Save & Close']")
	private WebElement createTaskSaveAndCloseBtn;


	public WebElement getCreateTaskSaveAndCloseBtn(int timeOut) {
		return isDisplayed(driver, createTaskSaveAndCloseBtn, "Visibility", timeOut, "save and close button");
	}
	
	@FindBy(xpath = "//iframe[@title='TaskRay']")
	private WebElement taskRayPageFrame;


	public WebElement getTaskRayPageFrame(int timeOut) {
		return isDisplayed(driver, taskRayPageFrame, "Visibility", timeOut, "task ray frame");
	}
	
	public WebElement getNewTaskSubTab(taskSubTab taskSubTab, int timeOut) {
		
		String xpath="//div[@aria-label='TaskRay App']//ul[@role='tablist']//li[@title='"+taskSubTab.toString()+"']";
//		WebElement ele= FindElement(driver, xpath, "task sub tab "+taskSubTab.toString(), action.BOOLEAN, timeOut);
		List< WebElement> ele = FindElements(driver, xpath,"task sub tab "+taskSubTab.toString());
		if(!ele.isEmpty()) {
			for(int i = 0; i < ele.size(); i++) {
				if(isDisplayed(driver, ele.get(i), "visibility",2,"task sub tab "+taskSubTab.toString())!=null) {
					return ele.get(i);
				}
			}
		}
		return null;
		
	}
	
	
	public WebElement getBoardFiltersOptions(BoardFilters boardFilters, int timeOut) {
		String boardFilter= boardFilters.toString().replace("_", " ");
		String xpath="//div[@id='left-sidebar-container']//a[@aria-describedby='entity-header'][text()='"+boardFilter+"']";
		WebElement ele= FindElement(driver, xpath,boardFilter+" tab xpath ", action.SCROLLANDBOOLEAN, timeOut);
		return isDisplayed(driver, ele, "visibility", timeOut,boardFilter+" tab xpath ");
	}
	
	
	
	@FindBy(xpath = "//input[@placeholder='Search for a project']")
	private WebElement searchAProjectTextBox;
	
	
	public WebElement getSearchAProjectTextBox(int timeOut) {
		return isDisplayed(driver, searchAProjectTextBox, "visibility", timeOut, "search project text box ");
	}

	public WebElement getProjects(String projectName, int timeOut) {
		String xpath="//div[@id='sidebar-list-projects-container']//span[text()='"+projectName+"']";
		WebElement ele= FindElement(driver, xpath,projectName+" name xpath ", action.SCROLLANDBOOLEAN, timeOut);
		return isDisplayed(driver, ele, "visibility", timeOut,projectName+" name xpath ");
	}
	
	
	public WebElement getSelectProjectFromDropDownInSearchAProject(String projectName, int timeOut) {
		String xpath="//div[@class='slds-lookup__menu slds-show']/ul/li/a[text()='"+projectName+"']";
		WebElement ele= FindElement(driver, xpath,projectName+" name xpath ", action.SCROLLANDBOOLEAN, timeOut);
		return isDisplayed(driver, ele, "visibility", timeOut,projectName+" name xpath ");
	}
	
	
	public WebElement getTaggedDocumentCountOnCreatedTask(TaskType taskType,String taskName,int timeOut) {
		String taskTYpe = taskType.toString().replace("_", " ");
		String xpath="//h5/span[text()='"+taskTYpe+"']/ancestor::div[@class='columnboard-header']/following-sibling::div/div/div/div/div[contains(@id,'task-column')]//span[text()='"+taskName+"']/../../..//div[@class='taskcard-tag-container']/div";
		WebElement ele= FindElement(driver, xpath,taskType+" document count xpath ", action.SCROLLANDBOOLEAN, timeOut);
		return isDisplayed(driver, ele, "visibility", timeOut,taskType+" document count xpath ");
	}
	
	
	public WebElement getCreatedTaskName(TaskType taskType,String taskName,int timeOut) {
		String taskTYpe = taskType.toString().replace("_", " ");
		String xpath="//h5/span[text()='"+taskTYpe+"']/ancestor::div[@class='columnboard-header']/following-sibling::div/div/div/div/div[contains(@id,'task-column')]//span[text()='"+taskName+"']";
		WebElement ele= FindElement(driver, xpath,taskName+"  xpath ", action.SCROLLANDBOOLEAN, timeOut);
		return isDisplayed(driver, ele, "visibility", timeOut,taskName+" xpath ");
	}
	
	
	public List<WebElement> getCreateTaskPopUpCrossIcon(){
		return FindElements(driver, "//button[contains(@class,'slds-button--icon-inverse')]/span[text()='Close']", "created task popup cross icon");
	}
	
	
	@FindBy(xpath = "//button[text()='Upload Files']")
	private WebElement uploadFilesButtonInCreatedTaskFilesTab;

	public WebElement getUploadFilesButtonInCreatedTaskFilesTab(int timeOut) {
		return isDisplayed(driver, uploadFilesButtonInCreatedTaskFilesTab, "visibility", timeOut,"upload file xpath");
	}
	
	
	@FindBy(xpath = "//div[contains(@class,'slds-has-error')]/div[@class='slds-form-element__help']")
	private WebElement errorMsgOnFileTab;

	public WebElement getErrorMsgOnFileTab(int timeOut) {
		return isDisplayed(driver, errorMsgOnFileTab, "visibility", timeOut,"error message on file tab");
	}
	
	@FindBy(xpath = "//div[@class='slds-float_left']//div[@class='project-more-actions-target']//button//button")
	private WebElement createdProjectMoreActions;

	public WebElement getCreatedProjectMoreActions(int timeOut) {
		return isDisplayed(driver, createdProjectMoreActions, "visibility", timeOut," created project more actions");
	}
	
	@FindBy(xpath = "//a[text()='Edit']")
	private WebElement createdProjectPopUpEditBtn;

	public WebElement getCreatedProjectPopUpEditBtn(int timeOut) {
		return isDisplayed(driver, createdProjectPopUpEditBtn, "visibility", timeOut,"created project edit button");
	}
	
	@FindBy(xpath = "//a[text()='Clone']")
	private WebElement createdProjectPopUpCloneBtn;
	
	
	
	public WebElement getCreatedProjectPopUpCloneBtn(int timeOut) {
		return isDisplayed(driver, createdProjectPopUpCloneBtn, "visibility", timeOut,"created project clone button");
	}

	@FindBy(xpath = "//div[@id='tether-destination']//ul/li//span[text()='Edit Project']")
	private WebElement createdProjectEditButton;

	public WebElement getCreatedProjectEditButton(int timeOut) {
		return isDisplayed(driver, createdProjectEditButton, "visibility", timeOut,"created project edit button");
	}
	
	public WebElement getProjectOrTaskHeaderLabelText(String projectNameOrTaskName, int timeOut) {
		return isDisplayed(driver, FindElement(driver, "(//div[text()='"+projectNameOrTaskName+"']/..)[1]", projectNameOrTaskName+" header text xpath", action.SCROLLANDBOOLEAN, timeOut), "visibility", timeOut, projectNameOrTaskName+" header xpath");
	}
	
	@FindBy(xpath = "//input[@placeholder='Add Team Member']")
	private WebElement addTeamMemeberTextBox;

	public WebElement getAddTeamMemeberTextBox(int timeOut) {
		return isDisplayed(driver, addTeamMemeberTextBox, "Visibility", timeOut, "add team memeber search text box");
	}
	
	@FindBy(xpath = "//div[@id='tr-sidebar']//span[text()='More']")
	private WebElement MoreIcon;

	public WebElement getMoreIcon(int timeOut) {
		return isDisplayed(driver,MoreIcon, "visibility", timeOut, "More Icon");
	}
	
	
	@FindBy(xpath = "//div[@id='sidebar-placeholder']//span[text()='HIDE']")
	private WebElement hidePlaceHolderIcon;
	
	public WebElement getHidePlaceHolderIcon(int timeOut) {
		return isDisplayed(driver,hidePlaceHolderIcon, "visibility", timeOut, "hide Icon");
	}
	
	@FindBy(xpath = "//span[text()='End Date']/../following-sibling::td[1]/span/span/span")
	private WebElement createTaskEndDateInViewMode;

	public WebElement getCreateTaskEndDateInViewMode(int timeOut) {
		return isDisplayed(driver, createTaskEndDateInViewMode, "visibility", timeOut,"create Task EndDate In View Mode");
	}
	
	@FindBy(xpath = "//span[text()='Start Date']/../following-sibling::td[1]/span/span/span")
	private WebElement createTaskStartDateInViewMode;
	
	public WebElement getCreateTaskStartDateInViewMode(int timeOut) {
		return isDisplayed(driver, createTaskStartDateInViewMode, "visibility", timeOut,"create Task StartDate In View Mode");
	}


	public WebElement getOwnerName(String ownerName, int timeOut) {
		String xpath="//div[@id='sidebar-list-contributors-container']//div[@title='"+ownerName+"']";
		return isDisplayed(driver, FindElement(driver, xpath, ownerName+" xpath", action.SCROLLANDBOOLEAN, timeOut), "visibility", timeOut, ownerName+" xpath");
	}
	
	@FindBy(xpath = "//div[@id='tr-app-container']//div[contains(@class,'new-features')]//button[text()='Dismiss']")
	private WebElement newFeaturePopUpDissmissBtn;

	public WebElement getNewFeaturePopUpDissmissBtn(int timeOut) {
		return isDisplayed(driver, newFeaturePopUpDissmissBtn, "visibility", timeOut,"new feature popup dismiss button");
	}
}
