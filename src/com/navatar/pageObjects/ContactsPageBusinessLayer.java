package com.navatar.pageObjects;

import static com.navatar.generic.AppListeners.appLog;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.poi.hssf.dev.ReSave;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.server.handler.SwitchToFrame;

import com.navatar.generic.BaseLib;
import com.navatar.generic.CommonLib;
import com.navatar.generic.ExcelUtils;
import com.navatar.generic.SmokeCommonVariables;
import com.navatar.generic.SoftAssert;
import com.navatar.generic.EnumConstants.ContactPageFieldLabelText;
import com.navatar.generic.EnumConstants.LimitedPartnerPageFieldLabelText;
import com.navatar.generic.EnumConstants.Mode;
import com.navatar.generic.EnumConstants.TabName;
import com.navatar.generic.EnumConstants.YesNo;
import com.navatar.generic.EnumConstants.action;
import com.navatar.generic.EnumConstants.excelLabel;
import com.relevantcodes.extentreports.LogStatus;

import static com.navatar.generic.CommonLib.*;

public class ContactsPageBusinessLayer extends ContactsPage {

	public ContactsPageBusinessLayer(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	
	/**
	 * @author Azhar Alam
	 * @param environment
	 * @param mode
	 * @param contactFirstName
	 * @param contactLastName
	 * @param legalName
	 * @param emailID
	 * @return true if able to create Contact
	 */
	public boolean createContact(String environment, String mode, String contactFirstName, String contactLastName,
			String legalName, String emailID, String otherLabelFields,String otherLabelValues,CreationPage creationPage) {
		InstitutionsPageBusinessLayer ins = new InstitutionsPageBusinessLayer(driver);
		String labelNames[]=null;
		String labelValue[]=null;
		if(otherLabelFields!=null && otherLabelValues !=null) {
			labelNames= otherLabelFields.split(",");
			labelValue=otherLabelValues.split(",");
		}
		if(creationPage.toString().equalsIgnoreCase(CreationPage.AccountPage.toString())) {
			if(mode.equalsIgnoreCase(Mode.Lightning.toString())) {
				
				if(ClickonRelatedTab_Lighting(environment,mode, RecordType.Contact)) {
					appLog.info("clicked on related list tab");
				}else {
					appLog.error("Not able to click on related list tab so cannot create contact: "+contactFirstName+" "+contactLastName);
					return false;
				}
			}
			if(click(driver, ins.getNewContactBtn(environment, mode, 30), "new contact button in "+mode, action.SCROLLANDBOOLEAN)) {
				appLog.info("clicked on new contact button in institution page");
			}else {
				appLog.error("Not able to click on new button on institution page so cannot create contact: "+contactFirstName+" "+contactLastName);
				return false;
			}
		}else {
			refresh(driver);
			ThreadSleep(3000);
			if(mode.equalsIgnoreCase(Mode.Lightning.toString())) {
				ThreadSleep(5000);
				if(clickUsingJavaScript(driver, getNewButton(environment, mode, 60), "new button")) {
					appLog.info("clicked on new button");
				}else {
					appLog.error("Not able to click on New Button so cannot create Contact: " + contactFirstName+" "+contactLastName);
					return false;
				}
			}else {
				ThreadSleep(5000);
				if (click(driver, getNewButton(environment,mode,60), "New Button", action.SCROLLANDBOOLEAN)) {
					appLog.info("clicked on new button");
				} else {
					appLog.error("Not able to click on New Button so cannot create Contact: " + contactFirstName+" "+contactLastName);
					return false;
				}
			}
		}
			ThreadSleep(2000);
			if (sendKeys(driver, getContactFirstName(environment, mode, 60), contactFirstName, "Contact first Name",
					action.BOOLEAN)) {
				if (sendKeys(driver, getContactLastName(environment, mode, 60), contactLastName, "Contact Last Name",
						action.BOOLEAN)) {
					
					if(creationPage.toString().equalsIgnoreCase(CreationPage.AccountPage.toString())) {
						
					}else {
						if (sendKeys(driver, getAccountName(environment, mode, 60), legalName, "Account Name",
								action.SCROLLANDBOOLEAN)) {
							if (mode.equalsIgnoreCase(Mode.Lightning.toString())) {
								ThreadSleep(1000);
								if (click(driver,
										FindElement(driver,
												"//div[contains(@class,'uiAutocomplete')]//a//div[@title='" + legalName
												+ "']",
												"Legal Name List", action.THROWEXCEPTION, 30),
										legalName + "   :   Account Name", action.BOOLEAN)) {
									appLog.info(legalName + "  is present in list.");
								} else {
									appLog.info(legalName + "  is not present in the list.");
									return false;
								}
							}
							
						} else {
							appLog.error("Not able to enter legal name");
							return false;
						}
					}
					
						if (sendKeys(driver, getEmailId(environment, mode, 60), emailID, "Email ID",
								action.SCROLLANDBOOLEAN)) {
							if(labelNames!=null && labelValue!=null) {
								for(int i=0; i<labelNames.length; i++) {
									WebElement ele = getContactPageTextBoxOrRichTextBoxWebElement(environment, mode, labelNames[i].trim(), 30);
									if(sendKeys(driver, ele, labelValue[i], labelNames[i]+" text box", action.SCROLLANDBOOLEAN)) {
										appLog.info("passed value "+labelValue[i]+" in "+labelNames[i]+" field");
									}else {
										appLog.error("Not able to pass value "+labelValue[i]+" in "+labelNames[i]+" field");
										BaseLib.sa.assertTrue(false, "Not able to pass value "+labelValue[i]+" in "+labelNames[i]+" field");
									}
								}
								
							}
							if (click(driver, getSaveButton(environment, mode, 60), "Save Button",
									action.SCROLLANDBOOLEAN)) {
								appLog.info("Clicked on save button");
								if(creationPage.toString().equalsIgnoreCase(CreationPage.AccountPage.toString())) {
									if(mode.equalsIgnoreCase(Mode.Lightning.toString())){
										if(clickOnGridSection_Lightning(environment, mode,RelatedList.Contacts, 30)) {
											WebElement ele = isDisplayed(driver, FindElement(driver, "//span[@title='Contact Name']/ancestor::table/tbody/tr/th/span/a", "Contact Name Text", action.SCROLLANDBOOLEAN, 30), "visibility", 20, "");
											if (ele != null) {
												String contactFullName = getText(driver,ele, "Contact Name",action.BOOLEAN);
												System.err.println("Contact Name : "+contactFullName);
												if (contactFullName.contains(contactFirstName + " " + contactLastName)) {
													appLog.info("Contact Created Successfully :" + contactFirstName + " "+ contactLastName);
													return true;
												} else {
													appLog.error("Contact did not get created successfully :" + contactFirstName
															+ " " + contactLastName);
												}
											} else {
												appLog.error("Not able to find contact name label");
											}
										}else {
											log(LogStatus.ERROR, "Not able to click on Contacts related list view all section so cannot verify Created Contact "+contactFirstName+" "+contactLastName, YesNo.Yes);
										}
									} else {
										if (getContactFullNameInViewMode(environment, mode, 60) != null) {
											String contactFullName = getText(driver,
													getContactFullNameInViewMode(environment, mode, 60), "Contact Name",
													action.BOOLEAN);
											System.err.println("Contact Name : "+contactFullName);
											if (contactFullName.contains(contactFirstName + " " + contactLastName)) {
												appLog.info("Contact Created Successfully :" + contactFirstName + " "
														+ contactLastName);
												if(labelNames!=null && labelValue!=null ) {
													for(int i=0; i<labelNames.length; i++) {
														if(fieldValueVerificationOnContactPage(environment, mode, null, labelNames[i].replace("_", " ").trim(),labelValue[i])){
															appLog.info(labelNames[i]+" label value "+labelValue[i]+" is matched successfully.");
														}else {
															appLog.info(labelNames[i]+" label value "+labelValue[i]+" is not matched successfully.");
															BaseLib.sa.assertTrue(false, labelNames[i]+" label value "+labelValue[i]+" is not matched.");
														}
													}
												}
												return true;
											} else {
												appLog.error("Contact did not get created successfully :" + contactFirstName
														+ " " + contactLastName);
											}
										} else {
											appLog.error("Not able to find contact name label");
										}
									}
								}else {
									if(mode.equalsIgnoreCase(Mode.Lightning.toString())) {
										ThreadSleep(2000);
										refresh(driver);
										ThreadSleep(5000);
									}

									if (getContactFullNameInViewMode(environment, mode, 60) != null) {
										String contactFullName = getText(driver,
												getContactFullNameInViewMode(environment, mode, 60), "Contact Name",
												action.BOOLEAN);
										System.err.println("Contact Name : "+contactFullName);
										if (contactFullName.contains(contactFirstName + " " + contactLastName)) {
											appLog.info("Contact Created Successfully :" + contactFirstName + " "
													+ contactLastName);
											if(labelNames!=null && labelValue!=null ) {
												for(int i=0; i<labelNames.length; i++) {
													if(fieldValueVerificationOnContactPage(environment, mode, null, labelNames[i].replace("_", " ").trim(),labelValue[i])){
														appLog.info(labelNames[i]+" label value "+labelValue[i]+" is matched successfully.");
													}else {
														appLog.info(labelNames[i]+" label value "+labelValue[i]+" is not matched successfully.");
														BaseLib.sa.assertTrue(false, labelNames[i]+" label value "+labelValue[i]+" is not matched.");
													}
												}
											}
											return true;
										} else {
											appLog.error("Contact did not get created successfully :" + contactFirstName
													+ " " + contactLastName);
										}
									} else {
										appLog.error("Not able to find contact name label");
									}
									
								}
								
							} else {
								appLog.info("Not able to click on save button");
							}

						} else {
							appLog.error("Not able to enter email id");
						}
					
				} else {
					appLog.error("Not able to enter last name in text box");
				}
			} else {
				appLog.error("Not able to enter first Name in text box");
			}
		return false;
	}

	/**
	 * @author ANKIT JAISWAL
	 * @param environment
	 * @param mode
	 * @param contactFirstName
	 * @param contactLastName
	 * @return true/false
	 */
	public boolean clickOnCreatedContact(String environment, String mode,String contactFirstName,String contactLastName){
		int i =1;
		if(mode.equalsIgnoreCase(Mode.Classic.toString())){
			if (getSelectedOptionOfDropDown(driver, getViewDropdown(60), "View dropdown", "text").equalsIgnoreCase("All Contacts")) {
				if (click(driver, getGoButton(60), "Go button", action.BOOLEAN)) {
				}
				else {
					appLog.error("Go button not found");
					return false;
				}
			}
			else{
				if (selectVisibleTextFromDropDown(driver, getViewDropdown(60),"View dropdown","All Contacts") ){
				}
				else {
					appLog.error("All Contacts  not found in dropdown");
					return false;
				}

			}
			WebElement contactName=null;
			if(contactFirstName==null){
				 contactName = FindElement(driver, "//div[@class='x-panel-bwrap']//a//span[contains(text(),'"+ contactLastName + "')]", "Contact Name", action.BOOLEAN, 10);
			} else {
				 contactName = FindElement(driver, "//div[@class='x-panel-bwrap']//a//span[contains(text(),'"
						+ contactLastName + ", " + contactFirstName + "')]", "Contact Name", action.BOOLEAN, 10);
			}

				if (contactName != null) {
					if (click(driver, contactName, "Contact Name", action.SCROLLANDBOOLEAN)) {
						appLog.info(
								"Clicked on created contact successfully :" + contactFirstName + " " + contactLastName);
						return true;

					} else {
						appLog.error("Not able to click on created contact");
						return false;
					}
				} else {
					while (true) {
						appLog.error("Contact is not Displaying on "+i+ " Page: " + contactLastName + ", " + contactFirstName);
						if (click(driver, getNextImageonPage(10), "Contact Page Next Button",
								action.SCROLLANDBOOLEAN)) {

							appLog.info("Clicked on Next Button");
							if(contactFirstName==null){
								 contactName = FindElement(driver, "//div[@class='x-panel-bwrap']//a//span[contains(text(),'"+ contactLastName + "')]", "Contact Name", action.BOOLEAN, 10);
							} else {
								 contactName = FindElement(driver, "//div[@class='x-panel-bwrap']//a//span[contains(text(),'"
										+ contactLastName + ", " + contactFirstName + "')]", "Contact Name", action.BOOLEAN, 10);
							}
							if (contactName != null) {
								if (click(driver, contactName, contactLastName + ", " + contactFirstName, action.SCROLLANDBOOLEAN)) {
									appLog.info("Clicked on Contact name : " + contactLastName + ", " + contactFirstName);
									return true;
									
								}
							}

							

						} else {
							appLog.error("Contact Not Available : " + contactLastName + ", " + contactFirstName);
							return false;
						}
						i++;
					}
				}
				
	}else{
		String concatFullName;
		if(contactFirstName==null){
			concatFullName=contactLastName;
		} else {
			concatFullName=contactFirstName+" "+contactLastName;
		}
		if(clickOnAlreadyCreated_Lighting(environment, mode, TabName.ContactTab, concatFullName, 30)){
			appLog.info("Clicked on Contact name : " + concatFullName);
			return true;
		}else{
			appLog.error("Contact Not Available : " + concatFullName);	
		}
	}
		return false;
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param environment
	 * @param mode
	 * @param tabName
	 * @param labelName
	 * @param labelValue
	 * @return true/false
	 */
	public boolean fieldValueVerificationOnContactPage(String environment, String mode, TabName tabName,
			String labelName,String labelValue) {
		String finalLabelName="";


		if (labelName.contains("_")) {
			if(labelName.equalsIgnoreCase(excelLabel.Asst_Phone.toString())) {
				finalLabelName= IndiviualInvestorFieldLabel.Asst_Phone.toString();
			}else {
				finalLabelName = labelName.replace("_", " ");
			}
		} else {
			finalLabelName = labelName;
		}
		String xpath = "";
		WebElement ele = null;
		if (mode.equalsIgnoreCase(Mode.Classic.toString())) {
			xpath = "(//td[text()='"+finalLabelName+"']/following-sibling::td)[1]";

		} else {

			xpath = "//span[@class='test-id__field-label'][text()='" + finalLabelName
					+ "']/../following-sibling::div/span/*/*";

		}

		if(finalLabelName.contains("Street") || finalLabelName.contains("City") || finalLabelName.contains("State") || finalLabelName.contains("Postal") || finalLabelName.contains("ZIP") || finalLabelName.contains("Zip")|| finalLabelName.contains("Country")) {

			if(mode.equalsIgnoreCase(Mode.Lightning.toString())) {
				//	xpath="//span[text()='Address Information']/../../following-sibling::div";
				if(finalLabelName.contains("Legal Name")){
					xpath="("+xpath+")[2]";
				}else if(finalLabelName.contains("Other Street") || finalLabelName.contains("Other City") || finalLabelName.contains("Other State") || finalLabelName.contains("Other Zip") || finalLabelName.contains("Other Country")) {
					xpath="//span[text()='Other Address']/../following-sibling::div//*//a";	
				}else{
					xpath="//span[text()='Mailing Address']/../following-sibling::div//*//a";
				}
			}else {
				if(finalLabelName.contains("Other Street") || finalLabelName.contains("Other City") || finalLabelName.contains("Other State") || finalLabelName.contains("Other Zip") || finalLabelName.contains("Other Country")) {
					xpath="(//td[text()='Other Address']/following-sibling::td//td)[1]";	
				}else{
					xpath="(//td[text()='Mailing Address']/following-sibling::td//td)[1]";
				}
			}
		}

		ele = /*isDisplayed(driver,*/
				FindElement(driver, xpath, finalLabelName + " label text in " + mode, action.SCROLLANDBOOLEAN, 5) /*,*/
				/*"Visibility", 5, finalLabelName + " label text in " + mode)*/;
		if (ele != null) {
			String aa = ele.getText().trim();
			appLog.info("<<<<<<<<     "+finalLabelName+ " : Lable Value is: "+aa+"      >>>>>>>>>>>");

			if (aa.isEmpty()) {
				appLog.error(finalLabelName + " Value is Empty label Value "+labelValue);
				return false;
			}

			if (labelName.equalsIgnoreCase(excelLabel.Phone.toString()) || labelName.equalsIgnoreCase(excelLabel.Fax.toString())||
					labelName.equalsIgnoreCase(ContactPageFieldLabelText.Mobile.toString()) ||
					labelName.equalsIgnoreCase(excelLabel.Asst_Phone.toString())) {

				if(aa.contains(labelValue) || aa.contains(changeNumberIntoUSFormat(labelValue))) {
					appLog.info(labelValue + " Value is matched successfully.");
					return true;

				}
			}else if(aa.contains(labelValue)) {
				appLog.info(labelValue + " Value is matched successfully.");
				return true;

			}else {
				appLog.info(labelValue + " Value is not matched. Expected: "+labelValue+" /t Actual : "+aa);
			}
		} else {
			appLog.error(finalLabelName + " Value is not visible so cannot matched  label Value "+labelValue);
		}
		return false;

	}
	
	/**
	 * @author AZHAR ALAM
	 * @param environment
	 * @param mode
	 * @param searchText
	 * @param lookUpValues
	 * @return TRUE/FALSE
	 */
	public boolean officeLocationInputValueAndSelect_Lighting(String environment,String mode,String searchText,String lookUpValues){
		String[] values = lookUpValues.split(",");
		WebElement ele=null;
		if (sendKeys(driver,getOfficeLocationTextBox_Lighting(environment, mode, 10), searchText, "Office Location Input Box", action.SCROLLANDBOOLEAN)) {
			ThreadSleep(2000);
				for(int i=0;i<values.length;i++){
					ele=isDisplayed(driver, FindElement(driver, "//div[@title='"+values[i]+"']",values[i]+" text value", action.SCROLLANDBOOLEAN, 20),"visibility", 20,values[i]+" text value");
				
					if(ele!=null) {
						appLog.info(values[i]+" is visible in look up popup");	
						
						if(i==values.length-1){
						ele=isDisplayed(driver, FindElement(driver, "//div[@title='"+values[0]+"']",values[0]+" text value", action.SCROLLANDBOOLEAN, 20),"visibility", 20,values[0]+" text value");
						if(click(driver, ele, values[0]+" text value", action.SCROLLANDBOOLEAN)) {
							appLog.info("clicked on "+values[0]+" in lookup pop up");
							return true;
						}
					}
						
					}else{
						appLog.error(values[i]+" is not visible in look up popup");
						return false;
					}
				}
				
			}
		return false;
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param contactFName
	 * @param contactLName
	 * @param contactEmailId
	 * @param profile
	 * @return TRUE/FALSE
	 */
	public List<String> createCommunityUser(String contactFName,String contactLName,String contactEmailId,String profile) {
		List<String> res = new ArrayList<String>();
		SetupPageBusinessLayer setup = new SetupPageBusinessLayer(driver);
		if(clickOnCreatedContact(SmokeCommonVariables.EnvironmentVariable,SmokeCommonVariables.ModeVariable, contactFName,contactLName)) {
			log(LogStatus.PASS, "clicked on create contact :  "+contactFName+" "+contactLName, YesNo.No);
			ThreadSleep(5000);
			if(click(driver, getShowMoreIcon(30), "show more icon", action.SCROLLANDBOOLEAN)) {
				log(LogStatus.PASS, "clicked on more icon", YesNo.No);
				ThreadSleep(2000);
				if(clickUsingJavaScript(driver, getMoreActionsOptions("Enable Customer User", 30), "Enable Customer User xpath")) {
					log(LogStatus.PASS, "clicked on Enable Customer User link", YesNo.No);
					ThreadSleep(20000);
					if(switchToFrame(driver, 60, setup.getSetUpPageIframe(60))) {
						log(LogStatus.PASS, "switched in frame ", YesNo.No);
						if(getAttribute(driver,getUserFirstName(60), "user First Name", "value").equalsIgnoreCase(contactFName)) {
							log(LogStatus.PASS, "Contact First Name is prefilled in first name text box",YesNo.No);
						}else {
							log(LogStatus.FAIL, "Contact First Name is not prefilled in first name text box ", YesNo.Yes);
							res.add("Contact First Name is not prefilled in first name text box ");
						}
						if(getAttribute(driver,getUserLastName(60), "user Last Name", "value").equalsIgnoreCase(contactLName)) {
							log(LogStatus.PASS, "Contact Last Name is prefilled in last name text box",YesNo.No);
						}else {
							log(LogStatus.FAIL, "Contact Last Name is not prefilled in last name text box ", YesNo.Yes);
							res.add("Contact Last Name is not prefilled in last name text box ");
						}
						if(getAttribute(driver,getUserEmailId(60), "user emial id", "value").equalsIgnoreCase(contactEmailId)) {
							log(LogStatus.PASS, "Contact Email Id is prefilled in Email Id text box",YesNo.No);
						}else {
							log(LogStatus.FAIL, "Contact Email Id is not prefilled in Email Id text box ", YesNo.Yes);
							res.add("Contact Email Id is not prefilled in Email Id text box ");
						}
						if (selectVisibleTextFromDropDown(driver, getUserProfileDropDownList(60),"User profile drop down list", profile)) {
							log(LogStatus.PASS, "select user profile from drop downlist: " + profile, YesNo.No);
							if(clickUsingJavaScript(driver,  getSalesforceCRMContentUserCheckBox(60), "Salesforce CRM Content User check Box")){
								log(LogStatus.PASS, "UnChecked Content user check box ", YesNo.No);
								if (clickUsingCssSelectorPath(driver, CssPath.userSaveBtn, "user save button")) {
									log(LogStatus.PASS, "clicked on save button", YesNo.No);
//										switchToDefaultContent(driver);
//										ThreadSleep(1000);
//										if(isAlertPresent(driver)) {
											if(switchToAlertAndAcceptOrDecline(driver, 30, action.ACCEPT)) {
												log(LogStatus.PASS, "Alert Message is Accepted successfully",YesNo.No);
												log(LogStatus.PASS, "CRM User is created successfully: " + contactFName+ " " + contactLName, YesNo.No);
											}else {
												log(LogStatus.FAIL, "Not able to accept alert message so cannot create community user :"+contactFName+" "+contactLName,YesNo.Yes);
												res.add("Not able to accept alert message so cannot create community user :"+contactFName+" "+contactLName);
											}
//										}else {
//											log(LogStatus.FAIL, "Alert is not present", YesNo.Yes);
//											res.add("Alert is not present");
//										}
								} else {
									log(LogStatus.FAIL, "Not able to click on save buttton so cannot create user: "+contactFName+ " " +contactLName, YesNo.Yes);
									res.add("Not able to click on save buttton so cannot create user: "+contactFName+ " " +contactLName);
								}
							}else {
								log(LogStatus.FAIL, "Not able to uncheck content user check box so cannot create community user "+contactFName+" "+contactLName, YesNo.Yes);
								res.add("Not able to uncheck content user check box so cannot create community user "+contactFName+" "+contactLName);
							}
						}else {
							log(LogStatus.FAIL, "Not able to select profile : "+profile+" so cannot create community user "+contactFName+" "+contactLName, YesNo.Yes);
							res.add("Not able to select profile : "+profile+" so cannot create community user "+contactFName+" "+contactLName);
						}
						
					}else {
						log(LogStatus.FAIL, "Not able to switch on Setup Page frame so cannot create community User", YesNo.Yes);
						res.add("Not able to switch on Setup Page frame so cannot create community User");
					}
					
					
				}else {
					log(LogStatus.FAIL, "Not able to click on Enable Customer User link so cannot create community user "+contactFName+" "+contactLName, YesNo.Yes);
					res.add("Not able to click on Enable Customer User link so cannot create community user "+contactFName+" "+contactLName);
				}
			}else {
				log(LogStatus.FAIL, "Not able to click on show more icon so cannot enable create community user "+contactFName+" "+contactLName, YesNo.Yes);
				res.add("Not able to click on show more icon so cannot enable create community user "+contactFName+" "+contactLName);
			}
		}else {
			log(LogStatus.FAIL, "Not able to click on create Contact "+contactFName+" "+contactLName+" so cannot create community user", YesNo.Yes);
			res.add("Not able to click on create Contact "+contactFName+" "+contactLName+" so cannot create community user");
			
		}
		return res;
	}
	
	
	
	
	
}
