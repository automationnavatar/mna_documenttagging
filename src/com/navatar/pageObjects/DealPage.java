package com.navatar.pageObjects;

import org.apache.poi.util.SuppressForbidden;
import org.bridj.cpp.std.list;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

import com.navatar.generic.BaseLib;
import com.navatar.generic.CommonLib;
import com.navatar.generic.EnumConstants.Buttons;
import com.navatar.generic.EnumConstants.Header;
import com.navatar.generic.EnumConstants.HeaderSideButton;
import com.navatar.generic.EnumConstants.InstitutionPageFieldLabelText;
import com.navatar.generic.EnumConstants.Mode;
import com.navatar.generic.EnumConstants.action;
import com.navatar.generic.EnumConstants.excelLabel;

import static com.navatar.generic.CommonLib.*;
import java.util.List;
import java.util.zip.CheckedOutputStream;


public class DealPage extends BasePageBusinessLayer {
	////////////////////////////////////////////

	public DealPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	


	@FindBy(xpath = "//table[@class='detailList']//select")
	private WebElement dealType_Classic;

	@FindBy(xpath = "//table[@class='detailList']//select")
	private WebElement dealType_Lighting;

	/**
	 * @return the getDealType
	 */
	public WebElement getDealType(String environment,String mode,String dealType,int timeOut) {

		if(mode.equalsIgnoreCase(Mode.Classic.toString())){
			return isDisplayed(driver, dealType_Classic, "Visibility", timeOut, "Deal Type Classic");
		}else{
			String xpath = "//span[text()='"+dealType+"']/../..";
			WebElement ele = FindElement(driver, xpath, "Deal Type", action.SCROLLANDBOOLEAN, timeOut);
			return isDisplayed(driver, ele, "Visibility", timeOut, "Deal Type Lighting");
		}

	}

	@FindBy(xpath = "//input[@title='Continue']")
	private WebElement continueButton_Classic;

	@FindBy(xpath = "//span[text()='Next']")
	private WebElement nextButton_Lighting;

	/**
	 * @return the continueButton
	 */
	public WebElement getContinueOrNextButton(String environment,String mode,int timeOut) {


		if(mode.equalsIgnoreCase(Mode.Classic.toString())){
			return isDisplayed(driver, continueButton_Classic, "Visibility", timeOut, "Continue Button");
		}else{
			return isDisplayed(driver, nextButton_Lighting, "Visibility", timeOut, "Next Button");
		}
	}

	@FindBy(xpath = "//input[@id='Name']")
	private WebElement dealName_Classic;

	@FindBy(xpath = "//span[text()='Deal Name']/../following-sibling::input")
	private WebElement dealName_Lighting;

	/**
	 * @return the dealName
	 */
	public WebElement getDealName(String environment,String mode,int timeOut) {

		if(mode.equalsIgnoreCase(Mode.Classic.toString())){
			return isDisplayed(driver, dealName_Classic, "Visibility", timeOut, "Deal Name");
		}else{
			return isDisplayed(driver, dealName_Lighting, "Visibility", timeOut, "Deal Name");
		}
	}

	@FindBy(xpath="//label[text()='Account']/../following-sibling::td/span/input")
	private WebElement accountName_Classic;

	@FindBy(xpath="//span[text()='Account']/../following-sibling::div//input[@title='Search Accounts']")
	private WebElement accountName_Lighting;

	/**
	 * @return the legalName
	 */
	public WebElement getLegalName(String environment,String mode,int timeOut) {
		if(mode.equalsIgnoreCase(Mode.Classic.toString())){
			return isDisplayed(driver, accountName_Classic, "Visibility", timeOut, "Legal Name Classic");
		}else{
			return isDisplayed(driver, accountName_Lighting, "Visibility", timeOut, "Legal Name Lighting");
		}

	} 


	@FindBy(xpath="//div[@class='requiredInput']//select")
	private WebElement fundType_Classic;

	@FindBy(xpath="//span[text()='Fund Type']/../..//div[@class='uiMenu']//a[@class='select']")
	private WebElement fundType_Lighting;

	/**
	 * @return the fundType
	 */
	public WebElement getFundType(String environment,String mode,int timeOut) {
		if(mode.equalsIgnoreCase(Mode.Classic.toString())){
			return isDisplayed(driver, fundType_Classic, "Visibility", timeOut, "Fund Type Classic");
		}else{
			return isDisplayed(driver, fundType_Lighting, "Visibility", timeOut, "Fund Type Lighting");
		}

	}

	@FindBy(xpath="//label[text()='Status']/../following-sibling::td//select")
	private WebElement dealStatusDropDownList_Classic;

	@FindBy(xpath="//span[text()='Status']/../..//div[@class='uiMenu']//a[@class='select']")
	private WebElement dealStatusDropDownList_Lighting;

	/**
	 * @return the fundType
	 */
	public WebElement getDealStatus(String environment,String mode,int timeOut) {
		if(mode.equalsIgnoreCase(Mode.Classic.toString())){
			return isDisplayed(driver, dealStatusDropDownList_Classic, "Visibility", timeOut, "Deal Status Classic");
		}else{
			return isDisplayed(driver, dealStatusDropDownList_Lighting, "Visibility", timeOut, "Deal Status Lighting");
		}

	}

	@FindBy(xpath="//label[text()='Stage']/../following-sibling::td//select")
	private WebElement dealStageDropDownList_Classic;

	@FindBy(xpath="//span[text()='Stage']/../..//div[@class='uiMenu']//a[@class='select']")
	private WebElement dealStageDropDownList_Lighting;

	/**
	 * @return the fundType
	 */
	public WebElement getDealStage(String environment,String mode,int timeOut) {
		if(mode.equalsIgnoreCase(Mode.Classic.toString())){
			return isDisplayed(driver, dealStageDropDownList_Classic, "Visibility", timeOut, "Deal Stage Classic");
		}else{
			return isDisplayed(driver, dealStageDropDownList_Lighting, "Visibility", timeOut, "Deal Stage Lighting");
		}

	}


	@FindBy(xpath="//label[text()='Source']/../following-sibling::td//select")
	private WebElement dealSourceDropDownList_Classic;

	@FindBy(xpath="//span[text()='Source']/../..//div[@class='uiMenu']//a[@class='select']")
	private WebElement dealSourceDropDownList_Lighting;

	/**
	 * @return the fundType
	 */
	public WebElement getDealSource(String environment,String mode,int timeOut) {
		if(mode.equalsIgnoreCase(Mode.Classic.toString())){
			return isDisplayed(driver, dealSourceDropDownList_Classic, "Visibility", timeOut, "Deal Source Classic");
		}else{
			return isDisplayed(driver, dealSourceDropDownList_Lighting, "Visibility", timeOut, "Deal Source Lighting");
		}

	}
	


	@FindBy(xpath="//label[text()='Active']/../following-sibling::td/input")
	private WebElement activeCheckBox_Classic;

	@FindBy(xpath="//span[text()='Active']/../following-sibling::input")
	private WebElement activeCheckBox_Lighting;

	/**
	 * @return the fundType
	 */
	public WebElement getActiveCheckBox(String environment,String mode,int timeOut) {
		if(mode.equalsIgnoreCase(Mode.Classic.toString())){
			return isDisplayed(driver, activeCheckBox_Classic, "Visibility", timeOut, "Active CheckBox Classic");
		}else{
			return isDisplayed(driver, activeCheckBox_Lighting, "Visibility", timeOut, "Active CheckBox Lighting");
		}

	}
	
	@FindBy(xpath="//td[@id='topButtonRow']//input[@title='Add Targets']")
	private WebElement addTargetBtn_Classic;
	
	@FindBy(xpath="//*[@title='Add Targets']")
	private WebElement addTarget_Lightning;
	
	public WebElement getAddtargetBtn(String environment,String mode,int timeOut) {
		if(mode.equalsIgnoreCase(Mode.Classic.toString())){
			return isDisplayed(driver, addTargetBtn_Classic, "Visibility", timeOut, "add target button");
		}else{
			return isDisplayed(driver, addTarget_Lightning, "Visibility", timeOut, "add target button");
		}
	}
	
	@FindBy(xpath="//a[@id='tab-scoped-1__item']")
	private WebElement SearchBasedOnPastDeals;

	/**
	 * @return the searchBasedOnExistingFundsDownArrow
	 */
	public WebElement getSearchBasedOnPastDeals(int timeOut) {
		return isDisplayed(driver, SearchBasedOnPastDeals, "Visibility", timeOut, "Search Based On Past Deals");
	}
	
	@FindBy(xpath="//div[@id='filterGridContactDiv_Add_Invite_TargetsID']//div[@id='list2']/a")
	private WebElement onlyContactsRoles;

	public WebElement getOnlyContactsRoles(int timeOut) {
		return isDisplayed(driver, onlyContactsRoles, "Visibility", timeOut, "only contact roles");
	}
	
	@FindBy(xpath="//div[@id='filterGridContactDiv_Add_Invite_TargetsID']//div[@id='list1']/a")
	private WebElement AllContactsOptions;

	public WebElement getAllContactsOptions(int timeOut) {
		return isDisplayed(driver, AllContactsOptions, "Visibility", timeOut, "all contacts options");
	}
	
	@FindBy(xpath="//img[@title='Deal Lookup (New Window)']")
	private WebElement selectDealNameFromSearchBasedOnPastDeal;
	
	/**
	 * @return the selectFundNameFromSearchBasedOnExistingFundLookUpIcon
	 */
	public WebElement getSelectDealNameFromSearchBasedOnPastDeal(int timeOut) {
		return isDisplayed(driver, selectDealNameFromSearchBasedOnPastDeal, "Visibility", timeOut, "select deal name from search based on past deal look up icon");
	}
	
	@FindBy(xpath="//a[@id='tab-scoped-2__item']")
	private WebElement searchBasedOnAccountAndContacts;

	public WebElement getSearchBasedOnAccountAndContacts(int timeOut) {
		return isDisplayed(driver, searchBasedOnAccountAndContacts, "Visibility", timeOut, "search based on account and contact");
	}
	
	@FindBy(xpath="//a[@id='Advancefilterapply']")
	private WebElement searchForTargetApplyBtn;

	/**
	 * @return the searchBasedOnAccountsAndContactsSearchBtn
	 */
	public WebElement getSearchForTargetApplyBtn(int timeOut) {
		return isDisplayed(driver, searchForTargetApplyBtn, "Visibility", timeOut, "Apply button");
	}
	
	@FindBy(xpath="//span[@id='grid_addtargets']//span[@class='aw-bars-box ']")
	private WebElement selectTargetGridScrollBox;

	public WebElement getSelectTargetGridScrollBox(int timeOut) {
		return isDisplayed(driver, selectTargetGridScrollBox, "Visibility", timeOut, "select target grid scroll box");
	}
	
	@FindBy(xpath="//a[@title='Add to Target List'][contains(@style,'display: block;')]")
	private WebElement addToTargetListBtn;

	public WebElement getAddToTargetListBtn(int timeOut) {
		return isDisplayed(driver, addToTargetListBtn, "Visibility", timeOut, "add to target list button");
	}
	
	@FindBy(xpath="//iframe[@title='Add Targets']")
	private WebElement addtargetIframe;

	public WebElement getAddtargetIframe(int timeOut) {
		return isDisplayed(driver, addtargetIframe, "Visibility", timeOut, "add target iframe");
	}
	
	@FindBy(xpath="//input[@id='pageid:frm:ATBtn']")
	private WebElement addATargetBtn;

	public WebElement getAddATargetBtn(int timeOut) {
		return isDisplayed(driver, addATargetBtn, "Visibility", timeOut, "add a target button");
	}
	
	@FindBy(xpath="//input[@name='pageid:frm:j_id43']")
	private WebElement addTargetOkBtn;

	public WebElement getAddTargetOkBtn(int timeOut) {
		return isDisplayed(driver, addTargetOkBtn, "Visibility", timeOut, "add a target ok button");
	}
	
	@FindBy(xpath="//td[@id='topButtonRow']//input[@title='Add Targets']")
	private WebElement manageTargetBtn_Classic;
	
	@FindBy(xpath="//a[@title='Add Targets']")
	private WebElement manageTarget_Lightning;
	
	public WebElement getManageTargetBtn(String environment,String mode,int timeOut) {
		if(mode.equalsIgnoreCase(Mode.Classic.toString())){
			return isDisplayed(driver, manageTargetBtn_Classic, "Visibility", timeOut, "Manage target button");
		}else{
			return isDisplayed(driver, manageTarget_Lightning, "Visibility", timeOut, "Manage target button");
		}
	}
	
	@FindBy(xpath="//label[text()='Status']/../following-sibling::td//select")
	private WebElement statusDropDownEditModeClassic;

	/**
	 * @return the statusDropDownEditModeClassic
	 */
	public WebElement getStatusDropDownEditModeClassic(int timeOut) {
		return isDisplayed(driver, statusDropDownEditModeClassic, "Visibility", timeOut, "Status drop down");
	}
	
	@FindBy(xpath="//label[text()='Stage']/../following-sibling::td//select")
	private WebElement StageDropDownEditModeClassic;

	/**
	 * @return the StageDropDownEditModeClassic
	 */
	public WebElement getStageDropDownEditModeClassic(int timeOut) {
		return isDisplayed(driver, StageDropDownEditModeClassic, "Visibility", timeOut, "Stage drop down");
	}
	
	@FindBy(xpath="//span[@class='label inputLabel uiPicklistLabel-left form-element__label uiPicklistLabel']//span[text()='Status']/../following-sibling::div//a")
	private WebElement statusPicklistLightining;

	/**
	 * @return the statusPicklistLightining
	 */
	public WebElement getStatusPicklistLightining(int timeOut) {
		return isDisplayed(driver, statusPicklistLightining, "Visibility", timeOut, "Status PickList");
	}
	
	@FindBy(xpath="//span[@class='label inputLabel uiPicklistLabel-left form-element__label uiPicklistLabel']//span[text()='Stage']/../following-sibling::div//a")
	private WebElement stagePicklistLightining;

	/**
	 * @return the StagePicklistLightining
	 */
	public WebElement getStagePicklistLightining(int timeOut) {
		return isDisplayed(driver, stagePicklistLightining, "Visibility", timeOut, "Stage PickList");
	}

	@FindBy(xpath="//a[contains(@onclick,'Advance_filter_apply()')]")
	private WebElement applyBtn;

	public WebElement getApplyBtn(int timeOut) {
		return isDisplayed(driver, applyBtn, "Visibility", timeOut, "apply button");
	}
	
	@FindBy(xpath="//input[@id='searchcon_grid']")
	private WebElement emailProspectSearchTextBox;

	/**
	 * @return the emailProspectSearchTextBox
	 */
	public WebElement getEmailProspectSearchTextBox(int timeOut) {
		return isDisplayed(driver, emailProspectSearchTextBox, "Visibility", timeOut, "email prospect search text box");
	}
	
	
	@FindBy(xpath="//label[@id='total_records']")
	private WebElement emailProspectSelctContactRecordCount;

	/**
	 * @return the emailProspectSelctContactRecordCount
	 */
	public WebElement getEmailProspectSelctContactRecordCount(int timeOut) {
		return isDisplayed(driver, emailProspectSelctContactRecordCount, "Visibility", timeOut, "email prospect select contact record count");
	}
	
	
	@FindBy(xpath="//div[@id='displayFilterText_Add_Invite_Targets']//div/img")
	private WebElement emailProspectHideSearchPopUp;

	/**
	 * @return the emailProspectHideSearchPopUp
	 */
	public WebElement getEmailTargetHideSearchPopUp(int timeOut) {
		return isDisplayed(driver, emailProspectHideSearchPopUp, "Visibility", timeOut, "email target hide search pop up");
	}
	
	@FindBy(xpath="(//div[@class='float_r']//a[@title='Next'])[2]")
	private WebElement emailProspectStep1NextBtn;

	/**
	 * @return the emailProspectStep1NextBtn
	 */
	public WebElement getEmailTargetStep1NextBtn(int timeOut) {
		return isDisplayed(driver, emailProspectStep1NextBtn, "Visibility", timeOut, "email target steps 1 next button");
	}
	
	@FindBy(xpath="//select[@name='pageid:frm:sl1']")
	private WebElement emailProspectFolderDropDownList;

	/**
	 * @return the emailProspectFolderDropDownList
	 */
	public WebElement getEmailProspectFolderDropDownList(int timeOut) {
		return isDisplayed(driver, emailProspectFolderDropDownList, "Visibility", timeOut, "email target folder drop downlist");
	}
	
	@FindBy(xpath="(//div[@class='float_r']//a[@title='Next'])[4]")
	private WebElement emailProspectStep2NextBtn;

	/**
	 * @return the emailProspectStep2NextBtn
	 */
	public WebElement getEmailTargetStep2NextBtn(int timeOut) {
		return isDisplayed(driver, emailProspectStep2NextBtn, "Visibility", timeOut, "email target steps 2 nect button");
	}
	
			
	@FindBy(xpath="(//div[@class='float_r']//a[@title='Send'])[2]")
	private WebElement emailProspectSendBtn;

	/**
	 * @return the emailProspectSendBtn
	 */
	public WebElement getEmailTargetSendBtn(int timeOut) {
		return isDisplayed(driver, emailProspectSendBtn, "Visibility", timeOut, "email target send button");
	}
	
	
	
	@FindBy(xpath="//div[@class='slds-box slds-theme--success']//p[1]")
	private WebElement emailTargetSendEmailCongratulationsErrorMsg;

	/**
	 * @return the emailProspectSendEmailCongratulationsErrorMsg
	 */
	public WebElement getEmailTargetSendEmailCongratulationsErrorMsg(int timeOut) {
		return isDisplayed(driver, emailTargetSendEmailCongratulationsErrorMsg, "Visibility", timeOut, "email target Send Email Congratulations Error Msg");
	}
	
	@FindBy(xpath="//iframe[@title='Email Targets']")
	private WebElement EmailTargetFrame_Lightning;
	
	
	/**
	 * @return the marketInitiativeFrame_Lightning
	 */
	public WebElement getEmailTargetFrame_Lightning(int timeOut) {
		return isDisplayed(driver,EmailTargetFrame_Lightning, "Visibility", timeOut, "market Initiative Frame Lightning");
	}
	

	
	@FindBy(xpath="//span[@id='targetgrid-scroll-box']")
	private WebElement targetGridScrollBox;
	
	/**
	 * @return the selectProspectsGridScrollBox
	 */
	public WebElement getTargetGridScrollBox(int timeOut) {
		
			return isDisplayed(driver, targetGridScrollBox, "Visibility", timeOut, "TArget scroll box grid");
		
	}
	
	@FindBy(xpath="//div[@id='Id_ContactName_Popup']//a[@title='Close']")
	private WebElement crossIconOnPastDealPopUpForContact;
	
	/**
	 * @return the crossIconOnPastDealPopUpForContact
	 */
	public WebElement getCrossIconOnPastDealPopUpForContact(int timeOut) {
		
			return isDisplayed(driver, crossIconOnPastDealPopUpForContact, "Visibility", timeOut, "cross Icon OnPastDeal PopUp ForContact");
		
	}
	
	@FindBy(xpath="//div[@id='Id_AccountName_Popup']//a[@title='Close']")
	private WebElement crossIconOnPastDealPopUpForAccount;
	
	/**
	 * @return the crossIconOnPastDealPopUpForAccount
	 */
	public WebElement getCrossIconOnPastDealPopUpForAccount(int timeOut) {
		
			return isDisplayed(driver, crossIconOnPastDealPopUpForAccount, "Visibility", timeOut, "cross Icon OnPastDeal PopUp ForAccount");
		
	}
	
	
	@FindBy(xpath="(//div[@id='filterGridContactDiv_Add_Invite_TargetsID']//center/a[@title='Search'])[1]")
	private WebElement searchBtnOnAddTarget;

	/**
	 * @return the searchForAContactTextBox
	 */
	public WebElement getSearchBtnOnAddTarget(int timeOut) {
		return isDisplayed(driver, searchBtnOnAddTarget, "Visibility", timeOut, "Search Button on Add Target");
	}

	@FindBy(xpath="//a[@title='Clear'][contains(@onclick,'clearSearch_BasedAccountContact()')]")
	private WebElement addTargetClearBtn;

	/**
	 * @return the addProspectClearBtn
	 */
	public WebElement getAddTargetClearBtn(int timeOut) {
		return isDisplayed(driver, addTargetClearBtn, "Visibility", timeOut, "Add Target Clear Btn");
	}
	
	@FindBy(xpath="//div[contains(@class,'ColumnstoDisplay_FancyboxA')]//button[@title='Cancel']")
	private WebElement wrenchIconCancelBtn;

	/**
	 * @return the infoLink
	 */
	public WebElement getWrenchIconCancelBtn(int timeOut) {
		return isDisplayed(driver, wrenchIconCancelBtn, "Visibility", timeOut, "Wrench Icon Cancel Btn");
	}
	
	@FindBy(xpath="//div[contains(@class,'dispalyErrorMessagePopup')]//div[@class='PopupContentStart']")
	private WebElement pleaseSelectApplyCriteriaPopUpMsgonDealPage;

	/**
	 * @return the selectAReportsErrorMsg
	 */
	public WebElement getPleaseSelectApplyCriteriaPopUpMsgonDealPage(int timeOut) {
		return isDisplayed(driver, pleaseSelectApplyCriteriaPopUpMsgonDealPage, "Visibility", timeOut, "please Select A Apply Criteria PopUp Msg");
	}
	
	@FindBy(xpath="//div[@class='filterGridContactDiv']//a[@title='Apply'][contains(text(),'Apply')]")
	private WebElement emailProspectApplyBtnonDealPage;

	/**
	 * @return the emailProspectApplyBtn
	 */
	public WebElement getEmailProspectApplyBtnOnDealPage(int timeOut) {
		return isDisplayed(driver, emailProspectApplyBtnonDealPage, "Visibility", timeOut, "email prospect apply button");
	}
	
	
	@FindBy(xpath="//div[contains(@class,'dispalyErrorMessagePopup')]//div//input[@title='OK']")
	private WebElement pleaseSelectApplyCriteriaPopUpOkBtnOnDealPage;

	/**
	 * @return the selectAReportsErrorMsg
	 */
	public WebElement getPleaseSelectApplyCriteriaPopUpOkBtnOnDealPage(int timeOut) {
		return isDisplayed(driver, pleaseSelectApplyCriteriaPopUpOkBtnOnDealPage, "Visibility", timeOut, "please Select A Apply Criteria PopUp Ok Btn");
	}
	
	@FindBy(xpath="(//p[text()=' Email ']/../../../following-sibling::div[@id='home_xx' or @id='op1']//a[@title='Cancel'])[1]")
	private WebElement step1CancelBtnOnDealPage;

	/**
	 * @return the infoLink
	 */
	public WebElement getStep1CancelBtnOnDealPage(int timeOut) {
		return isDisplayed(driver, step1CancelBtnOnDealPage, "Visibility", timeOut, "Step1Cancel Btn");
	}
	
	@FindBy(xpath="(//p[text()=' Email ']/../../../following-sibling::div[@id='home_xx' or @id='op1']//a[@title='Cancel'])[2]")
	private WebElement step1CancelBottomBtnOnDealPage;

	/**
	 * @return the infoLink
	 */
	public WebElement getStep1CancelBottomBtnOnDealPage(int timeOut) {
		return isDisplayed(driver, step1CancelBottomBtnOnDealPage, "Visibility", timeOut, "Step1Cancel  BottonBtn");
	}
	
	@FindBy(xpath="(//p[text()=' Email ']/../../../following-sibling::div[@id='home_xx' or @id='op1']//a[@title='Next'])[1]")
	private WebElement step1NextBtnOnDealPage;

	/**
	 * @return the infoLink
	 */
	public WebElement getStep1NextBtnOnDealPage(int timeOut) {
		return isDisplayed(driver, step1NextBtnOnDealPage, "Visibility", timeOut, "Step1Next Btn");
	}
	
	@FindBy(xpath="(//p[text()=' Email ']/../../../following-sibling::div[@id='home_xx' or @id='op1']//a[@title='Next'])[2]")
	private WebElement step1NextBottomBtnOnDealPage;

	/**
	 * @return the infoLink
	 */
	public WebElement getStep1NextBottomBtnOnDealPage(int timeOut) {
		return isDisplayed(driver, step1NextBottomBtnOnDealPage, "Visibility", timeOut, "Step1Next  BottonBtn");
	}
	
	@FindBy(xpath="(//p[text()=' Email ']/../../../following-sibling::div[@id='home_xx' or @id='op1']//a[@title='Previous'])[1]")
	private WebElement step2PreviousBtnOnDealPage;

	/**
	 * @return the infoLink
	 */
	public WebElement getStep2PreviousBtnOnDealPage(int timeOut) {
		return isDisplayed(driver, step2PreviousBtnOnDealPage, "Visibility", timeOut, "Step1 Previous Btn");
	}
	
	@FindBy(xpath="(//p[text()=' Email ']/../../../following-sibling::div[@id='home_xx' or @id='op1']//a[@title='Previous'])[2]")
	private WebElement step2PreviousBottomBtnOnDealPage;

	/**
	 * @return the infoLink
	 */
	public WebElement getStep2PreviousBottomBtnOnDealPage(int timeOut) {
		return isDisplayed(driver, step2PreviousBottomBtnOnDealPage, "Visibility", timeOut, "Step1Previous  BottonBtn");
	}
	
	
	public WebElement getProjectOpenAndCompleteTaskNameLinkOnDealPrep(String projectNameOrTaskName, int timeOut) {
		return FindElement(driver, "//a[text()='"+projectNameOrTaskName+"']", projectNameOrTaskName+" link xpath", action.SCROLLANDBOOLEAN, timeOut);
	}
	
	public WebElement getOwnerNameLink(String projectNameOrTaskName,String ownerName, int timeOut) {
		String xpath="//a[text()='"+projectNameOrTaskName+"']/ancestor::td/following-sibling::td[contains(@data-label,'Owner:')]//a[text()='"+ownerName+"']";
		return isDisplayed(driver, FindElement(driver, xpath, ownerName+" link xpath", action.SCROLLANDBOOLEAN, timeOut), "visibility", timeOut, ownerName+" link xpath");
	}
	
	
	public WebElement getOwnerNameLabelTextOnUserPage(String crmUserName, int timeOut) {
		String xpath="//div[@class='flexipageComponent']//*[text()='"+crmUserName+"']";
		return isDisplayed(driver, FindElement(driver, xpath, crmUserName+" label text xpath", action.SCROLLANDBOOLEAN, timeOut), "visibility", timeOut, crmUserName+" label text xpath");
	}
	
	
	
	@FindBy(xpath="//span[text()='Pipeline Stage Logs']/ancestor::article//span[text()='View All']")
	private WebElement pipeLineStageLogViewAll_Lighting;
	
	
	/**
	 * @param environment
	 * @param timeOut
	 * @return
	 */
	public WebElement getPipeLineStageLogViewAll_Lighting(String environment,int timeOut) {
		CommonLib.scrollThroughOutWindow(driver);
		return isDisplayed(driver, pipeLineStageLogViewAll_Lighting, "Visibility", timeOut, "PipeLine Stage Log View All");
	
	}
	
	@FindBy(xpath="//label[text()='Stage']/../following-sibling::td//span/select")
	private WebElement pipeLineStageLabel_Classic;
	
	
	/**
	 * @param environment
	 * @param timeOut
	 * @return
	 */
	public WebElement getPipeLineStageLabel(String environment,String mode,int timeOut) {
		return isDisplayed(driver, pipeLineStageLabel_Classic, "Visibility", timeOut, "PipeLine Stage Label classic");
	
	}
	
	
	public WebElement getpipeLInePageTextBoxAllWebElement(String environment,String mode, String labelName, int timeOut) {
		WebElement ele=null;
		String xpath ="",inputXpath="",finalXpath="",finalLabelName="",xpath1="",xpath2="";
		if(labelName.contains("_")) {
			finalLabelName=labelName.replace("_", " ");
		}else {
			finalLabelName=labelName;
		}
		if(mode.equalsIgnoreCase(Mode.Classic.toString())) {
			
		}else {
			xpath="//span[text()='"+finalLabelName+"']";
			inputXpath="/..//following-sibling::input";
			xpath1="/../following-sibling::div/div/div/div[contains(@class,'autocompleteWrapper')]/input";
			xpath2="/..//following-sibling::div//a";
			if(labelName.toString().equalsIgnoreCase(InstitutionPageFieldLabelText.Parent_Institution.toString())) {
				inputXpath="/..//following-sibling::div//input[@title='Search Institutions']";
			}
			
		}
		if(labelName.equalsIgnoreCase(excelLabel.Company_Name.toString())) {
			finalXpath=xpath+xpath1;
		}else if (labelName.equalsIgnoreCase(excelLabel.Stage.toString())) {
			finalXpath=xpath+xpath2;
		}else {
			finalXpath=xpath+inputXpath;
		}
		ele=isDisplayed(driver, FindElement(driver, finalXpath, finalLabelName+" text box in "+mode, action.SCROLLANDBOOLEAN,30), "Visibility", timeOut, finalLabelName+"text box in "+mode);
		return ele;
		
	}
	
	
	@FindBy(xpath="//input[@name='save']")
	private WebElement saveButtonClassic;
	
	@FindBy(xpath="//button[@title='Save']/span[text()='Save']")
	private WebElement saveButtonLighting;

	/**
	 * @return the saveButton
	 */
	public WebElement getSaveButton(String environment,String mode,int timeOut) {
		if(mode.equalsIgnoreCase(Mode.Classic.toString())){
			return isDisplayed(driver, saveButtonClassic, "Visibility", timeOut, "Save Button Classic");
		}else{
			return isDisplayed(driver, saveButtonLighting, "Visibility", timeOut, "Save Button Lighting");
		}
		
	}
	
	
	public WebElement getDocumentButton(String openTaskName, int timeOut) {
//		String xpath="//a[text()='"+openTaskName+"']/../../../following-sibling::td//button[text()='Documents']";
		String xpath="//a[text()='"+openTaskName+"']/ancestor::td[contains(@data-label,'Name:')]/following-sibling::td[@class='actions']//button[text()='Documents']";
		return isDisplayed(driver, FindElement(driver, xpath,"task name "+openTaskName+" documents buton", action.SCROLLANDBOOLEAN,timeOut), "Visibility", timeOut,"task name "+openTaskName+" documents buton");
				
	}
	
	
	public WebElement getCompleteLink(String openTaskName, int timeOut) {
		String xpath="//a[text()='"+openTaskName+"']/../../../following-sibling::td//a[text()='Complete']";
		
		return isDisplayed(driver, FindElement(driver, xpath,"task name "+openTaskName+" complete link", action.SCROLLANDBOOLEAN,timeOut), "Visibility", timeOut,"task name "+openTaskName+" complete link");
				
	}
	
	
	
	@FindBy(xpath = "//h2[text()='Navatar Documents']")
	private WebElement navatarDocumentsLabelText;


	public WebElement getNavatarDocumentsLabelText(int timeOut) {
		return isDisplayed(driver, navatarDocumentsLabelText, "Visibility", timeOut, "navatar Documents Label Text");
	}

	
	@FindBy(xpath = "//form[@id='pg:frm1']//button[@onclick='getAlltaggedDocuments();' or @title='Refresh']")
	private WebElement refreshButton;


	public WebElement getRefreshButton(int timeOut) {
		return isDisplayed(driver, refreshButton, "Visibility", timeOut, "refresh Button");
	}
	
	public WebElement taskLabelNameOnNavtarDocumentsPopUp(String taskName, int timeOut) {
		String xpath="//label[@id='pg:frm1:lblTaggedField']/span[text()='"+taskName+"']";
		return isDisplayed(driver, FindElement(driver, xpath, "open task name "+taskName+" on navatar documents", action.BOOLEAN,timeOut), "Visibility", timeOut,"open task name "+taskName+" on navatar documents");

	}
	
	@FindBy(xpath = "//footer/button[@title='Cancel']")
	private WebElement cancelButtonOnNavatarDocumentsPopUp;


	public WebElement getCancelButtonOnNavatarDocumentsPopUp(int timeOut) {
		return isDisplayed(driver, cancelButtonOnNavatarDocumentsPopUp, "Visibility", timeOut, "cancel Button On Navatar Documents PopUp");
	}
	
	
	@FindBy(xpath = "//header/button[@title='Close']")
	private WebElement crossIconOnNavatarDocumentPopUp;


	public WebElement getCrossIconOnNavatarDocumentPopUp(int timeOut) {
		return isDisplayed(driver, crossIconOnNavatarDocumentPopUp, "Visibility", timeOut, "cross Icon On Navatar Document PopUp");
	}
	
	
	
	public List<WebElement> getDocumentNameAndActionColumnOnNavatarDocumentPopUp(){
		return FindElements(driver, "//table[@id='pg:frm1:tagDocTable']//tr/th/div", "Document Name And Action Column list On Navatar Document popup");
	}
	
	public List<WebElement> getdroppedFileNamesListInUploadDocument() {
		return FindElements(driver, "//img[@action='modify']/..", "document name list in upload document window");
	}
	
	
	@FindBy(css = "#btnSaveTree")
	private WebElement uploadDocumentSaveBtn;


	public WebElement getUploadDocumentSaveBtn(int timeOut) {
		return isDisplayed(driver, uploadDocumentSaveBtn, "Visibility", timeOut, "upload document button");
	}
	
	
	@FindBy(xpath = "//div[contains(@class,'Confirmation_fancybox')]/div[2]")
	private WebElement uploadDocumentConfirmationPopUpText;


	public WebElement getUploadDocumentConfirmationPopUpText(int timeOut) {
		 return isDisplayed(driver, uploadDocumentConfirmationPopUpText, "Visibility", timeOut, "upload document confirmation popup text");
	}
	
	@FindBy(xpath = "//div[contains(@class,'Confirmation_fancybox')]/div/input")
	private WebElement uploadDocumentConfirmationPopUpCloseBtn;


	public WebElement getUploadDocumentConfirmationPopUpCloseBtn(int timeOut) {
		return isDisplayed(driver, uploadDocumentConfirmationPopUpCloseBtn, "Visibility", timeOut, "upload document confirmation popup close button");
	}
	
	@FindBy(xpath="//div[contains(@class,'ConfirmationDupFiles')]//a[@title='Update All']")
	private WebElement simpleupdateAllButton;

	/**
	 * @return the updateAllButton
	 */
	public WebElement getSimpleupdateAllButton(int timeOut) {
		return isDisplayed(driver, simpleupdateAllButton, "Visibility", timeOut, "simple upload updateAll Button");
	}
	
	
	@FindBy(xpath = "//div[contains(@class,'ConfirmationDupFiles')]//a[@title='Ignore All']")
	private WebElement IgnoreAllButton;
	
	
	public WebElement getIgnoreAllButton(int timeOut) {
		return isDisplayed(driver, IgnoreAllButton, "Visibility", timeOut, "ignore all Button");
	}



	@FindBy(css = "input#lbtOnlinImportSave")
	private WebElement tagDocumentsBtn;


	public WebElement getTagDocumentsBtn(int timeOut) {
		return isDisplayed(driver, tagDocumentsBtn, "Visibility", timeOut, "tag documents button");
	}
	
	
	public List<WebElement> getTaggedDocumentListOnNavatarDocumentsPopUp(){
		return FindElements(driver, "//table/tbody[@id='pg:frm1:tagDocTable:tb']/tr/td[1]/a", "tagged document list on navatar document list");
	}
	
	public List<WebElement> getTaggedDocumentRemoveLinkListOnNavatarDocumentsPopUp(){
		return FindElements(driver, "//table/tbody[@id='pg:frm1:tagDocTable:tb']/tr/td[2]/a", "tagged document list on navatar document list");
	}
	
	
	public WebElement getDocumentRemoveLinkOnNavatarDocumentPopUp(String docName, int timeOut) {
		String xpath="//table/tbody[@id='pg:frm1:tagDocTable:tb']/tr/td[1]/a[contains(text(),'"+docName+"')]/../following-sibling::td/a";
		return isDisplayed(driver, FindElement(driver, xpath,docName+" name remove link ", action.BOOLEAN,timeOut), "Visibility", timeOut,docName+" name remove link ");
	}
	
	
	
	public WebElement getHeaderSideButtons(Header header, HeaderSideButton headersidebutton, int timeOut) {
		String xpath="//a[contains(text(),'"+header.toString().replace("_", " ")+"')]/../../../../following-sibling::div//span[text()='"+headersidebutton.toString().replace("_", " ")+"']";
		
		return isDisplayed(driver, FindElement(driver, xpath,header.toString()+"  header side button "+headersidebutton.toString(), action.BOOLEAN,timeOut), "Visibility", timeOut,header.toString()+"  header side button "+headersidebutton.toString());
	
	}
	
	@FindBy(xpath = "//a[@title='Remove']")
	private WebElement navatarDocumentPopUpRemoveButton;


	public WebElement getNavatarDocumentPopUpRemoveButton(int timeOut) {
		return isDisplayed(driver, navatarDocumentPopUpRemoveButton, "Visibility", timeOut, "document remove button");
	}
	
	@FindBy(xpath = "//a[@title='Cancel']")
	private WebElement navatarDocumentPopUpCancelButton;


	public WebElement getNavatarDocumentPopUpCancelButton(int timeOut) {
		return isDisplayed(driver, navatarDocumentPopUpCancelButton, "Visibility", timeOut, "document cancel button");
	}
	
	
	
	@FindBy(xpath = "//div[contains(@class,'DealRoomName_fancybox')]/div[2]/p")
	private WebElement navatarDocumentRemovePopUpErrorMsg;
	
	
	public WebElement getNavatarDocumentRemovePopUpErrorMsg(int timeOut) {
		return isDisplayed(driver, navatarDocumentRemovePopUpErrorMsg, "Visibility", timeOut, "navatar document remove popup error message");
	}

	@FindBy(xpath = "//div[@class='error-container']/div/h3/span")
	private WebElement documentNotFoundErrorMsg;


	public WebElement getDocumentNotFoundErrorMsg(int timeOut) {
		return isDisplayed(driver, documentNotFoundErrorMsg, "Visibility", timeOut, "document not found error message");
	}
	
	@FindBy(xpath = "//a[@data-resin-target='homebutton']")
	private WebElement boxHomeLogoOnDocumentTab;


	public WebElement getBoxHomeLogoOnDocumentTab(int timeOut) {
		return isDisplayed(driver, boxHomeLogoOnDocumentTab, "Visibility", timeOut, "box home page logo xpath on document tab");
	}
	
	
	public WebElement getMoveOrCopyOrCancelButtonOnMoveOrCopyPopUp(Buttons button, int timeOut) {
		String xpath="//button[@data-resin-target='"+button+"']";
		return isDisplayed(driver, FindElement(driver, xpath,"move or copy popup "+button.toString()+" button", action.BOOLEAN,timeOut), "Visibility", timeOut,"move or copy popup "+button.toString()+" button");
	}
	
	@FindBy(xpath = "//div[@aria-label='Rename file' or @aria-label='Rename folder']//input[@class='popup-prompt-input']")
	private WebElement boxRenameTextBoxInDocuments;


	public WebElement getBoxRenameTextBoxInDocuments(int timeOut) {
		return isDisplayed(driver, boxRenameTextBoxInDocuments, "Visibility", timeOut, "Box rename text box in documents tab");
	}
	
	
	public WebElement getBoxRenamePopUpOkayAndCancelInDocuments(Buttons button, int timeOut) {
		String xpath="//div[@aria-label='Rename file' or @aria-label='Rename folder']//button[text()='"+button+"']";
		return isDisplayed(driver, FindElement(driver, xpath,"box rename popup "+button.toString()+" button", action.BOOLEAN,timeOut), "Visibility", timeOut,"box rename popup "+button.toString()+" button");
	}
	
	public WebElement getcreatedPipeLineShowMoreActionIcon(String pipeLineName, int timeOut) {
		String xpath="//a[text()='"+pipeLineName+"']/../../following-sibling::td//a[@title='Show 2 more actions']";
		return isDisplayed(driver, FindElement(driver, xpath,pipeLineName+" show more icon", action.BOOLEAN,timeOut), "Visibility", timeOut,pipeLineName+" show more icon");
	}
	
	@FindBy(xpath = "//button[@name='Edit']")
	private WebElement editButton;


	public WebElement getEditButton(int timeOut) {
		return isDisplayed(driver, editButton, "Visibility", timeOut, "edit button");
	}
	
	
	public WebElement getTagDocumentCustomFieldLink(int timeOut) {
		String xpath="//*[text()='Test Tag Documents']/../following::span//a[text()='Tag Documents']";
		return isDisplayed(driver, FindElement(driver, xpath,"tag document link", action.BOOLEAN,timeOut), "Visibility", timeOut,"tag document link");
	}
	
	
	@FindBy(xpath = "//input[@value='Login']")
	private WebElement BoxLoginButtonOnDocumentPage;


	public WebElement getBoxLoginButtonOnDocumentPage(int timeOut) {
		return isDisplayed(driver,BoxLoginButtonOnDocumentPage, "Visibility", timeOut,"box login button");
	}
	
	@FindBy(xpath = "//input[@value='Create Folder']")
	private WebElement createFolderButtonOnDocumentsPage;


	public WebElement getCreateFolderButtonOnDocumentsPage(int timeOut) {
		return isDisplayed(driver,createFolderButtonOnDocumentsPage, "Visibility", timeOut,"create folder button");
	}
	
	@FindBy(xpath = "//h3[text()='Document not found.']")
	private WebElement documentNotFoundErrorMsgForIP;


	public WebElement getDocumentNotFoundErrorMsgForIP(int timeOut) {
		return isDisplayed(driver,documentNotFoundErrorMsgForIP, "Visibility", timeOut,"document not found for ip");
	}
	
	
	@FindBy(id="globFolder")
	private WebElement globalFolderRadioButton;

	/**
	 * @return the globalFolderRadioButton
	 */
	public WebElement getGlobalFolderRadioButton(int timeOut) {
		return isDisplayed(driver, globalFolderRadioButton, "Visibility", timeOut, "Global Folder Radio Button");
	}
	@FindBy(id="shrdFolder")
	private WebElement sharedFolderradioButton;

	/**
	 * @return the sharedFolderradioButton
	 */
	public WebElement getSharedFolderradioButton(int timeOut) {
		return isDisplayed(driver, sharedFolderradioButton, "Visibility", timeOut, "Shared Folder Radio Button");
	}
	@FindBy(id="intFolder")
	private WebElement internalFolderRadioButton;

	/**
	 * @return the internalFolderRadioButton
	 */
	public WebElement getInternalFolderRadioButton(int timeOut) {
		return isDisplayed(driver, internalFolderRadioButton, "Visibility", timeOut, "Internal Folder Radio Button");
	}
	
	@FindBy(id="DealRoom:formBDR:BDR:CompBuiltDealRoom:NewFolderDesc")
	private WebElement addFolderDescription;

	/**
	 * @return the addFolderDescription
	 */
	public WebElement getAddFolderDescription(int timeOut) {
		return isDisplayed(driver, addFolderDescription, "Visibility", timeOut, "Add Folder Description Text box");
	}
	
	@FindBy(id="DealRoom:formBDR:BDR:CompBuiltDealRoom:NewFolderName")
	private WebElement folderNametextBox;

	/**
	 * @return the folderNametextBox
	 */
	public WebElement getFolderNametextBox(int timeOut) {
		return isDisplayed(driver, folderNametextBox, "Visibility", timeOut, "Folder Name Text Box");
	}
	
	@FindBy(xpath="//span[@id='errforInvalidCharFirstFund' and contains(@style,'display: block;')]")
	private WebElement folderSpecialCharacterErrorMessage;

	/**
	 * @return the folderSpecialCharacterErrorMessage
	 */
	public WebElement getFolderSpecialCharacterErrorMessage(int timeOut) {
		return isDisplayed(driver, folderSpecialCharacterErrorMessage, "Visibility", timeOut, "Folder Special Character Error Message.");
	}
	@FindBy(xpath="//span[@id='err1stLvlFolderForGlobal' and contains(@style,'display: block;')]")
	private WebElement invalidFolderNameErrorMessage;

	/**
	 * @return the invalidFolderNameErrorMessage
	 */
	public WebElement getInvalidFolderNameErrorMessage(int timeOut) {
		return isDisplayed(driver, invalidFolderNameErrorMessage, "Visibility", timeOut, "Invalid Folder Name");
	}
	@FindBy(xpath="//div[contains(@id,'parentFolderDiv')]//a[@title='Cancel']")
	private WebElement addFolderCancelButton;

	/**
	 * @return the addFolderCancelButton
	 */
	public WebElement getAddFolderCancelButton(int timeOut) {
		return isDisplayed(driver, addFolderCancelButton, "Visibility", timeOut, "Add Folder Cancel Button");
	}
	
	@FindBy(xpath="//span[@id='errforInvalidCharFirstDesc' and contains(@style,'display: block;')]")
	private WebElement invalidFolderDescriptionErrorMessage;

	/**
	 * @return the invalidFolderDescriptionErrorMessage
	 */
	public WebElement getInvalidFolderDescriptionErrorMessage(int timeOut) {
		return isDisplayed(driver, invalidFolderDescriptionErrorMessage, "Visibility", timeOut, "Add Folder Description Error Message");
	}
	
	@FindBy(xpath="//a[@id='ImportActive']")
	private WebElement folderTemplateImportButton;

	/**
	 * @return the folderTemplateImportButton
	 */
	public WebElement getFolderTemplateImportButton(int timeOut) {
		return isDisplayed(driver, folderTemplateImportButton, "Visibility", timeOut, "Folder template import Button");
	}
	
	@FindBy(id="ClearActive")
	private WebElement clearAllFoldersButton;

	/**
	 * @return the clearAllFoldersButton
	 */
	public WebElement getClearAllFoldersButton(int timeOut) {
		return isDisplayed(driver, clearAllFoldersButton, "Visibility", timeOut, "Clear All Folder Button");
	}
	
	@FindBy(xpath="//span[contains(@onclick,'hideClearAllFolder')]/../following-sibling::p")
	private WebElement confirmDeletionMessageForAllFolder;

	/**
	 * @return the confirmDeletionMessageForAllFolder
	 */
	public WebElement getConfirmDeletionMessageForAllFolder(int timeOut) {
		return isDisplayed(driver, confirmDeletionMessageForAllFolder, "Visibility", timeOut, "Confirm Deletion Error Message For All Folders");
	}
	
	@FindBy(xpath="//a[contains(@onclick,'funClearAllFolder')]")
	private WebElement confirmDeleteAllFolderYesButton;

	/**
	 * @return the confirmDeleteAllFolderYesButton
	 */
	public WebElement getConfirmDeleteAllFolderYesButton(int timeOut) {
		return isDisplayed(driver, confirmDeleteAllFolderYesButton, "Visibility", timeOut, "Confirm Delete All Folder Yes Button");
	}
	
	@FindBy(xpath="//div[@class='filterGridContactDivBDR']//div[@class='error_txt']")
	private WebElement applybuttonInFilterBuildDealRoomStep3of3ErrorMessage;

	/**
	 * @return the applybuttonInFilterBuildDealRoomStep3of3ErrorMessage
	 */
	public WebElement getApplybuttonInFilterBuildDealRoomStep3of3ErrorMessage(int timeOut) {
		return isDisplayed(driver, applybuttonInFilterBuildDealRoomStep3of3ErrorMessage, "Visibility", timeOut, "Apply Button Filter Build Deal Room Step 3of3 Error Message");
	}
	@FindBy(xpath="(//img[@title='Remove Row'])[1]")
	private WebElement removeRowIconForSecondRow;

	/**
	 * @return the removeRowFirstIcon
	 */
	public WebElement getRemoveRowIconForSecondRow(int timeOut) {
		return isDisplayed(driver, removeRowIconForSecondRow, "Visibility", timeOut, "Remove Row Icon For Second Row");
	}
	
	@FindBy(id="stdFolder")
	private WebElement standardFolderRadioButton;

	/**
	 * @return the standardFolderRadioButton
	 */
	public WebElement getStandardFolderRadioButton(int timeOut) {
		return isDisplayed(driver, standardFolderRadioButton, "Visibility", timeOut, "standard folder Radio Button");
	}
	
	@FindBy(xpath="//input[@id='checkboxstandard_grid1']")
    private WebElement manageFolderStandardRadioButton;

	/**
	 * @return the manageApprovalsStandardRadioButton
	 */
	public WebElement getManageFolderStandardRadioButton(int timeOut) {
		return isDisplayed(driver, manageFolderStandardRadioButton, "Visibility", timeOut, "Manage Approvals Standard Radio button");
	}
	
	
	
	@FindBy(xpath="//textarea[@id='input_dscp_AddPopUp']")
	private WebElement manageFolderStandardFolderDescription;

	/**
	 * @return the manageFolderStandardFolderDescription
	 */
	public WebElement getManageFolderStandardFolderDescription(int timeOut) {
		return isDisplayed(driver, manageFolderStandardFolderDescription, "Visibility", timeOut, "Manage folder Standard Folder Description");
	}
	
	@FindBy(xpath="//a[contains(@onclick,'CreateGlobal_pop')]")
	private WebElement manageFolderSaveButton;

	/**
	 * @return the manageFolderSaveButton
	 */
	public WebElement getManageFolderSaveButton(int timeOut) {
		return isDisplayed(driver, manageFolderSaveButton, "Visibility", timeOut, "Manage Folder Save Button");
	} 
	
	@FindBy(xpath="//div[contains(@id,'parentFolderDiv')]//a[contains(@onclick,'checkAndCreateFolder')]")
	private WebElement folderSaveButton;

	/**
	 * @return the buildDealRoom2of3saveFolderButton
	 */
	public WebElement getfolderSaveButton(int timeOut) {
		return isDisplayed(driver, folderSaveButton, "Visibility", timeOut, "Save Folder Button");
	}
	
	@FindBy(id="DealRoom:formBDR:BDR:CompBuiltDealRoom:folderName")
	private WebElement childFolderNameTextBox;

	/**
	 * @return the childFolderName
	 */
	public WebElement getchildFolderNameTextBox(int timeOut) {
		return isDisplayed(driver, childFolderNameTextBox, "Visibility", timeOut, "Child Folder Name text box");
	}
	
	@FindBy(id="DealRoom:formBDR:BDR:CompBuiltDealRoom:folderDesc")
	private WebElement childFolderDescriptionTextBox;

	/**
	 * @return the childFolderDescriptionTextBox
	 */
	public WebElement getChildFolderDescriptionTextBox(int timeOut) {
		return isDisplayed(driver, childFolderDescriptionTextBox, "Visibility", timeOut, "Child Folder Description Text box");
	}
	@FindBy(xpath="//div[contains(@id,'subFolderDiv')]//a[contains(@onclick,'checkAndCreateFolder')]")
	private WebElement childLevelFolderSaveButton;

	/**
	 * @return the childLevelFolderSaveButton
	 */
	public WebElement getChildLevelFolderSaveButton(int timeOut) {
		return isDisplayed(driver, childLevelFolderSaveButton, "Visibility", timeOut, "Child Level Folder Save Button");
	}
	
	@FindBy(xpath="//input[@id='foldeglobaladdfund']")
	private WebElement manageFolderStandardFolderNameText;

	/**
	 * @return the manageFolderStandardFolderNameText
	 */
	public WebElement getManageFolderStandardFolderNameText(int timeOut) {
		return isDisplayed(driver, manageFolderStandardFolderNameText, "Visibility", timeOut, "Manage Folder Standard Folder Name Text Box");
	}
	
	@FindBy(xpath = "//div[@class='slds-p-around_medium']/p[2]")
	private WebElement NoWorkSpaceBuildErrorMessageOnDocumentTaggingPage;
	
	
	public WebElement getNoWorkSpaceBuildErrorMessageOnDocumentTaggingPage(int timeOut) {
		return isDisplayed(driver, NoWorkSpaceBuildErrorMessageOnDocumentTaggingPage, "Visibility", timeOut, "No WorkSpace Build Error Message On Document Tagging Page");
	}
	
	@FindBy(xpath="//a[@id='BuilddealroomButton']")
	private WebElement BuildDealRoomButton;

	
	/**
	 * @return the buildDealRoomButton
	 */
	public WebElement getBuildDealRoomButton(int timeOut) {
		if(isDisplayed(driver, BuildDealRoomButton, "Visibility", timeOut, "Build Deal Room Button")!=null){
			return BuildDealRoomButton;
		} else {
			driver.navigate().refresh();
			scrollDownThroughWebelement(driver, getDealRoomSection(60), "Deal Room View.");
			switchToFrame(driver, 60, getDealRoomFrame(60));
			if(isDisplayed(driver, BuildDealRoomButton, "Visibility", timeOut, "Build Deal Room Button")!=null){
				return BuildDealRoomButton;
			} else {
				return null;
			}
		}
	}
	
	@FindBy(id="DealRoom:formBDR:BDR:CompBuiltDealRoom:delRomName")
	private WebElement dealRoomName;

	/**
	 * @return the dealRoomName
	 */
	public WebElement getDealRoomName(int timeOut) {
		return isDisplayed(driver, dealRoomName, "Visibility", timeOut, "Deal Room Name");
		
	}
	
	@FindBy(id="DealRoom:formBDR:BDR:CompBuiltDealRoom:delRomCon")
	private WebElement dealRoomContact;

	/**
	 * @return the dealRoomContact
	 */
	public WebElement getDealRoomContact(int timeOut) {
		return isDisplayed(driver, dealRoomContact, "Visibility", timeOut, "Deal Room Contact");
	}
	
	@FindBy(id="errMsgTemplateImported")
	private WebElement errorMessageTemplateImport;
	/**
	 * @return the errorMessageTemplateImport
	 */
	public WebElement getErrorMessageTemplateImport(int timeOut) {
		return isDisplayed(driver, errorMessageTemplateImport, "Visibility", timeOut, "Error Message Template Import Error Msg");
	}
	
	@FindBy(xpath="//div[@id='errMsgTemplateImported']/../following-sibling::div/a")
	private WebElement templateImportErrorMsgCloseButton;
	
	/**
	 * @return the templateImportErrorMsgCloseButton
	 */
	public WebElement getTemplateImportErrorMsgCloseButton(int timeOut) {
		return isDisplayed(driver, templateImportErrorMsgCloseButton, "Visibility", timeOut, "close Button");
	}
	
	@FindBy(id="DealRoom:formBDR:BDR:CompBuiltDealRoom:delRomPhone")
	private WebElement dealRoomPhone;

	/**
	 * @return the dealRoomPhone
	 */
	public WebElement getDealRoomPhone(int timeOut) {
		return isDisplayed(driver, dealRoomPhone, "Visibility", timeOut, "Deal Room Phone");
	}

	@FindBy(id="DealRoom:formBDR:BDR:CompBuiltDealRoom:delRomeMail")
	private WebElement dealRoomEmail;

	/**
	 * @return the dealRoomEmail
	 */
	public WebElement getDealRoomEmail(int timeOut) {
		return isDisplayed(driver, dealRoomEmail, "Visibility", timeOut, "Desl Room Email");
	}
	
	@FindBy(id="DealRoom:formBDR:BDR:CompBuiltDealRoom:delRomDesc")
	private WebElement dealRoomDescription;

	/**
	 * @return the dealRoomDescription
	 */
	public WebElement getDealRoomDescription(int timeOut) {
		return isDisplayed(driver, dealRoomDescription, "Visibility", timeOut, "Deal RoomDescription");
	}
	
	@FindBy(xpath="//div[@id='BuildDealRoom_step_1of3']//a[@title='Cancel']")
	private WebElement cancelButtonBuildDealRoomStep1of3;

	/**
	 * @return the cancelButton
	 */
	public WebElement getCancelButton(int timeOut) {
		return isDisplayed(driver, cancelButtonBuildDealRoomStep1of3, "Visibility", timeOut, "Cancel Button Build Deal Room Step 1 of 3");
	}
	
	@FindBy(xpath="//div[@id='BuildDealRoom_step_1of3']//a[@title='Next']")
	private WebElement nextButtonBuildDealRoomStep1of3;

	/**
	 * @return the nextButton
	 */
	public WebElement getnextButtonBuildDealRoomStep1of3(int timeOut) {
		return isDisplayed(driver, nextButtonBuildDealRoomStep1of3, "Visibility", timeOut, "Next button Build Deal Room Step 1 of 3");
	}
	
	@FindBy(xpath="//div[@id='BuildDealRoom_step_1of3']//a[@title='Close']")
	private WebElement bildDealRoom1of3CloseIcon;

	/**
	 * @return the closeIcon
	 */
	public WebElement getbildDealRoom1of3CloseIcon(int timeOut) {
		return isDisplayed(driver, bildDealRoom1of3CloseIcon, "Visibility", timeOut, "Close Icon Build Deal Room Step 1 of 3");
	}
	@FindBy(xpath="//a[@title='Import Folder Template']")
	private WebElement importFolderTemplate;

	/**
	 * @return the importFolderTemplate
	 */
	public WebElement getImportFolderTemplate(int timeout) {
		return isDisplayed(driver, importFolderTemplate, "Visibility", timeout, "Import Folder Template Button");
	}
	
	@FindBy(xpath="(//a[@title='Clear All Folders'])[1]")
	private WebElement clearAllFolders;

	/**
	 * @return the clearAllFolders
	 */
	public WebElement getClearAllFolders(int timeout) {
		return isDisplayed(driver, clearAllFolders, "Visibility", timeout, "Clear All Folders Button");
	}
	
	@FindBy(xpath="//div[@id='BDRstep_2of3']//a[@title='Close']")
	private WebElement closeIconBuildDealRoomStep2of3;

	/**
	 * @return the closeIconBuildDealRoomStep2of3
	 */
	public WebElement getCloseIconBuildDealRoomStep2of3(int timeOut) {
		return isDisplayed(driver, closeIconBuildDealRoomStep2of3, "Visibility", timeOut, "Close Icon Build Deal Room Step 2 of 3");
	}
	
	@FindBy(xpath="//div[@id='BDRstep_2of3']/div[3]/a[1]")
	private WebElement cancelButtonBuildDealRoomStep2of3;

	/**
	 * @return the cancelButtonBuildDealRoomStep2of3
	 */
	public WebElement getCancelButtonBuildDealRoomStep2of3(int timeOut){
		return isDisplayed(driver, cancelButtonBuildDealRoomStep2of3, "Visibility", timeOut, "Cancel Button Build Deal Room Step 2 of 3");
	}

	@FindBy(xpath="//div[@id='BDRstep_2of3']//a[@title='Next']")
	private WebElement nextButtonStep2of3;

	/**
	 * @return the nextButtonStep2of3
	 */
	public WebElement getNextButtonStep2of3(int timeOut) {
		return isDisplayed(driver, nextButtonStep2of3, "Visibility", timeOut, "Next Button Build Deal Room Step 2 of 3");
	}
	
	@FindBy(xpath="//table[@id='advancefltr_tablecustom']//select[@id='a1aa']")
	private WebElement filterTargetFieldDropdown;

	/**
	 * @return the filterTargetFieldDropdown
	 */
	public WebElement getFilterTargetFieldDropdown(int timeOut) {
		return isDisplayed(driver, filterTargetFieldDropdown, "Visibility", timeOut, "Filter Target Field DropDown");
	}
	
	@FindBy(xpath="(//table[@id='advancefltr_tablecustom']//select)[2]")
	private WebElement filterTargetOperatorDropdown;

	/**
	 * @return the filterTargetOperatorDropdown
	 */
	public WebElement getFilterTargetOperatorDropdown(int timeOut) {
		return isDisplayed(driver, filterTargetOperatorDropdown, "Visibility", timeOut, "Filter Target Operator Dropdown");
	}
	
	@FindBy(xpath="//table[@id='advancefltr_tablecustom']//input")
	private WebElement filterTargetValueTextBox;

	/**
	 * @return the filterTargetValueTextBox
	 */
	public WebElement getFilterTargetValueTextBox(int timeOut) {
		return isDisplayed(driver, filterTargetValueTextBox, "Visibility", timeOut, "Filter Target Value TextBox");
	}
	
	@FindBy(xpath="//span[@id='advanceflter_AddRowNew']")
	private WebElement addRowLink;

	/**
	 * @return the addRowLink
	 */
	public WebElement getAddRowLink(int timeOut) {
		return isDisplayed(driver, addRowLink, "Visibility", timeOut, "Add Row link");
	}
	
	@FindBy(xpath="//div[@class='filterGridContactDivBDR']//div//a[@title='Apply']")
	private WebElement applyButtonInFilterSection;

	/**
	 * @return the applyButtonInFilterSection
	 */
	public WebElement getApplyButtonInFilterSection(int timeOut) {
		return isDisplayed(driver, applyButtonInFilterSection, "Visibility", timeOut, "Apply Button in filter Section");
	}
	
	@FindBy(xpath="//div[@class='filterGridContactDivBDR']//div//a[@title='Clear']")
	private WebElement clearButtonInFilterSection;

	/**
	 * @return the clearButtonInFilterSection
	 */
	public WebElement getClearButtonInFilterSection(int timeOut) {
		return isDisplayed(driver, clearButtonInFilterSection, "Visibility", timeOut, "Clear Button In Filter Section");
	}
 
	@FindBy(xpath="//div[text()='Target Accounts']//a//img")
	private WebElement wrenchIcon;

	/**
	 * @return the wrenchIcon
	 */
	public WebElement getWrenchIcon(int timeOut) {
		return isDisplayed(driver, wrenchIcon, "Visibility", timeOut, "Wrench Icon");
	}
	
	@FindBy(xpath="//div[@id='DealRoomBuiltId']//following-sibling::div//a[@title='Done']")
	private WebElement doneButton;

	/**
	 * @return the doneButton
	 */
	public WebElement getDoneButton(int timeOut) {
		return isDisplayed(driver, doneButton, "Visibility", timeOut, "Done Button");
	}

	@FindBy(xpath="//div[@id='targetpopup']//following-sibling::div//a[@title='Done']")
	private WebElement doneButtonMT;

	/**
	 * @return the doneButton
	 */
	public WebElement getDoneButtonMT(int timeOut) {
		return isDisplayed(driver, doneButtonMT, "Visibility", timeOut, "Done Button");
	}

	@FindBy(xpath="(//div[@id='step_3of3']//div)[1]//a[@title='Close']")
	private WebElement closeIconBuildDealRoomStep3;

	/**
	 * @return the closeIconBuildDealRoomStep3
	 */
	public WebElement getCloseIconBuildDealRoomStep3(int timeOut) {
		return isDisplayed(driver, closeIconBuildDealRoomStep3, "Visibility", timeOut, "Close Icon build Deal Room Step 3");
	}
	
	
@FindBy(xpath="//a[@title='Manage Approvals']")
private WebElement manageApprovalsLinkOnDealRoomBuilt;

/**
 * @return the manageApprovalslink
 */
public WebElement getManageApprovalslink(int timeOut) {
	return isDisplayed(driver, manageApprovalsLinkOnDealRoomBuilt, "Visibility", timeOut, "Manage Approvals link on Deal Room Built PopUp");
}

@FindBy(xpath="//a[@title='Watermarking']")
private WebElement watermarkingLinkOnDealRoomBuilt;

/**
 * @return the watermarkingLinkOnDealRoomBuilt
 */
public WebElement getWatermarkingLinkOnDealRoomBuilt(int timeOut) {
	return isDisplayed(driver, watermarkingLinkOnDealRoomBuilt, "Visibility", timeOut, "Watermarking link on Deal Room Built popup");
}
	
@FindBy(xpath="//a[@class='btn_active ShowHide_WorkspaceBuilt']")
private WebElement closeButtonOnDealRoomBuilt;

/**
 * @return the closeButtonOnDealRoomBuilt
 */
public WebElement getCloseButtonOnDealRoomBuilt(int timeOut) {
	return isDisplayed(driver, closeButtonOnDealRoomBuilt, "Visibility", timeOut, "Close Button Deal Room Built popup");
}

@FindBy(xpath="//span[@class='fancybox-close ShowHide_WorkspaceBuilt']")
private WebElement closeIconOnDealRoomBuilt;

/**
 * @return the closeIconOnDealRoomBuilt
 */
public WebElement getCloseIconOnDealRoomBuilt(int timeOut) {
	return isDisplayed(driver, closeIconOnDealRoomBuilt, "Visibility", timeOut, "CloseIcon On Deal Room Built");
}
	
@FindBy(xpath = "//span[text()='Deal Room']")
private WebElement dealRoomSection;

/**
 * @return the dealRoomSection
 */
public WebElement getDealRoomSection(int timeOut) {
	return isDisplayed(driver, dealRoomSection, "Visibility", timeOut, "Deal Room Section");
}



@FindBy(xpath = "//span[text()='Deal Room']/../../..//iframe")
private WebElement dealRoomFrame;

/**
 * @return the errorMessageFrame
 */
public WebElement getDealRoomFrame(int timeOut) {
	return isDisplayed(driver, dealRoomFrame, "Visibility", timeOut, "Error Message Frame");
}

@FindBy(id="DealRoom:formBDR:BDR:CompBuiltDealRoom:selectlstId")
private WebElement folderTemplateFilterDropDown;

/**
 * @return the folderTemplateFilterDropDown
 */
public WebElement getFolderTemplateFilterDropDown(int timeOut) {
	return isDisplayed(driver, folderTemplateFilterDropDown, "Visibility", timeOut, "Folder template filter drop Down");
}

@FindBy(id="errMsgTagetAddBDR")
private WebElement targetSuccessfullyAddedMessage;

/**
 * @return the targetSuccessfullyAddedMessage
 */
public WebElement getTargetSuccessfullyAddedMessage(int timeOut) {
	return isDisplayed(driver, targetSuccessfullyAddedMessage, "Visibility", timeOut, "target Successfully added Message");
}

@FindBy(xpath="//div[@id='errMsgTagetAddBDR']/../following-sibling::div//a")
private WebElement targetAddedSuccessfullyMessageCloseButton;

/**
 * @return the targetAddedSuccessfullyMessageCloseButton
 */
public WebElement getTargetAddedSuccessfullyMessageCloseButton(int timeOut) {
	return isDisplayed(driver, targetAddedSuccessfullyMessageCloseButton, "Visibility", timeOut, "Close Button");
}

@FindBy(id="activestatusAppr")
public WebElement manageApprovalActiveStatusText;

/**
 * @return the manageApprovalActiveStatusText
 */
public WebElement getManageApprovalActiveStatusText(int timeOut) {
	return isDisplayed(driver, manageApprovalActiveStatusText, "Visibility", timeOut, "Manage Approval Acive Status text");
}

@FindBy(id="activestatusMwm")
private WebElement watermarkingActiveStatusText;

/**
 * @return the watermarkingActiveStatusText
 */
public WebElement getWatermarkingActiveStatusText(int timeOut) {
	return isDisplayed(driver, watermarkingActiveStatusText, "Visibility", timeOut, "Watermarking active status text");
}

@FindBy(id="inactivestatusMwm")
private WebElement watermarkingInactveStatusText;

/**
 * @return the watermarkingInactveStatusText
 */
public WebElement getWatermarkingInactveStatusText(int timeOut) {
	return isDisplayed(driver, watermarkingInactveStatusText, "Visibility", timeOut, "watermarking Inactive Status text");
}

@FindBy(id="inactivestatusAppr")
public WebElement manageApprovalInactiveStatus;

/**
 * @return the manageApprovalInactiveStatus
 */
public WebElement getManageApprovalInactiveStatus(int timeOut) {
	return isDisplayed(driver, manageApprovalInactiveStatus, "Visibility", timeOut, "Manage Approval inactive Status Text");
}

@FindBy(xpath="//a[@title='Manage Approvals']/following-sibling::b")
private WebElement manageApprovalSetting;

/**
 * @return the manageApprovalSetting
 */
public WebElement getManageApprovalSetting(int timeOut) {
	return isDisplayed(driver, manageApprovalSetting, "Visibility", timeOut, "Manage Approval Setting");
}

@FindBy(xpath="//a[@title='Watermarking']/following-sibling::b")
private WebElement watermarkingSetting;

/**
 * @return the manageApprovalSetting
 */
public WebElement getWatermarkingSetting(int timeOut) {
	return isDisplayed(driver, watermarkingSetting, "Visibility", timeOut, "Watermarking Setting");
}


@FindBy(xpath="//div[@id='DealRoomBuiltId']//a[@title='Close']")
private WebElement buildDealRoomCloseButton;

/**
 * @return the buildDealRoomCloseButton
 */
public WebElement getBuildDealRoomCloseButton(int timeOut) {
	//WebElement ele= BaseLib.edriver.findElement(By.cssSelector("div#DealRoomBuiltId a[title=Close]"));
	//return ele;
	return isDisplayed(driver, buildDealRoomCloseButton, "Visibility", timeOut, "Build Deal Room Close Button");
}

@FindBy(xpath="//a[@id='AddFileMultipleTargetsBtn']/img")
private WebElement multipleUploadIcon;

/**
 * @return the multipleUploadIcon
 */
public WebElement getMultipleUploadIcon(int timeOut) {
	return isDisplayed(driver, multipleUploadIcon, "Visibility", timeOut, "multipleUploadIcon Icon");
}

@FindBy(id="AddFileBtn")
private WebElement uploadIcon;

/**
 * @return the uploadIcon
 */
public WebElement getUploadIcon(int timeOut) {
	return isDisplayed(driver, uploadIcon, "Visibility", timeOut, "Upload Icon");
}
@FindBy(xpath="(//a[@title='Ignore All'])[2]")
private WebElement ignoreAllMAEA;

/**
 * @return the ignoreAllMA
 */
public WebElement getIgnoreAllMAEA(int timeOut) {
	return isDisplayed(driver, ignoreAllMAEA, "Visibility", timeOut, "Ignore All Button");
}


@FindBy(xpath="//a[@title='Ignore All']")
private WebElement ignoreAllMA;

/**
 * @return the ignoreAllMA
 */
public WebElement getIgnoreAllMA(int timeOut) {
	return isDisplayed(driver, ignoreAllMA, "Visibility", timeOut, "Ignore All Button");
}

@FindBy(xpath="//*[@id='btnSave']")
private WebElement uploadSaveButton;

/**
 * @return the uploadSaveButton
 */
public WebElement getUploadSaveButton(int timeOut) {
	return isDisplayed(BaseLib.edriver, uploadSaveButton, "Visibility", timeOut, "Upload Save Button");
}

public List<WebElement> draggedFilesInFileUploadAtCRMSide(){
		return FindElements(driver, "//div[@id='divselectInvestor']//table//span/img/..", "Dragged1 Files in Uplod Files At CRM Side");
		
	}

@FindBy(xpath="//div[contains(@class,'ConfirmationImport')]//a[@title='Update All']")
private WebElement simpleupdateAllButtonforStandardFolder;

/**
 * @return the simpleupdateAllButtonforStandardFolder
 */
public WebElement getSimpleupdateAllButtonforStandardFolder(int timeOut) {
	return isDisplayed(driver, simpleupdateAllButtonforStandardFolder, "Visibility", timeOut, "simple upload updateAll Button for standard folder");
}

@FindBy(xpath="(//a[@title='Ignore All'])[1]")
private WebElement multiUploadDuplicateFilePopUpIgnoreBtn;

/**
 * @return the multiUploadDuplicateFilePopUpIgnoreBtn
 */
public WebElement getMultiUploadDuplicateFilePopUpIgnoreBtn(int timeOut) {
	return isDisplayed(driver, multiUploadDuplicateFilePopUpIgnoreBtn, "Visibility", timeOut, "multi upload duplicate file pop up ignore al button");
}

@FindBy(xpath="(//div[text()='Confirmation '][1]/following-sibling::div)[1]")
private WebElement uploadConfirmationText;

/**
 * @return the uploadConfirmationText
 */
public WebElement getUploadConfirmationText(int timeOut) {
	return isDisplayed(driver, uploadConfirmationText, "Visibility", timeOut, "Upload confirmation message.");
}

@FindBy(xpath="//a[@value='Clear Deal Room']")
private WebElement clearDealRoomButton;

/**
 * @return the clearDealRoomButton
 */
public WebElement getClearDealRoomButton(int timeOut) {
	return isDisplayed(driver, clearDealRoomButton, "Visibility", timeOut, "Clear Deal Room Button");
}
	
}