package com.navatar.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import static com.navatar.generic.CommonLib.*;
import static com.navatar.generic.CommonVariables.URL;
import static com.navatar.pageObjects.DealPageErrorMessage.documentNotFoundErrorMsg;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.navatar.generic.ExcelUtils;
import com.navatar.generic.SmokeCommonVariables;
import com.navatar.generic.EnumConstants.Header;
import com.navatar.generic.EnumConstants.Mode;
import com.navatar.generic.EnumConstants.PageName;
import com.navatar.generic.EnumConstants.YesNo;
import com.navatar.generic.EnumConstants.action;
import com.relevantcodes.extentreports.LogStatus;

public class CommUnityUserPageBusinessLayer extends CommUnityUserPage {

	public CommUnityUserPageBusinessLayer(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @author ANKIT JAISWAL
	 * @param headerName
	 * @param timeOut
	 * @return true/false
	 */
	public boolean clickOnOpenOrCompleteHeaderLink(Header headerName, int timeOut) {
		refresh(driver);
		if(click(driver, getOpenAndCompleteTaskLink(headerName, timeOut),headerName+" xpath", action.SCROLLANDBOOLEAN)) {
			log(LogStatus.PASS,"Clicked on "+headerName.toString()+" Link",YesNo.No);
			return true;
		}else {
			log(LogStatus.FAIL,"Not able to click on "+headerName.toString()+" link",YesNo.Yes);
		}
		return false;
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param taskName
	 * @param pipeLineName
	 * @param endDate
	 * @return empty list if execute successfully
	 */
	public List<String> verifyOpenOrCompleteTask(String taskName, String pipeLineName, List<String> endDate){
		String[] splitedTaskName= taskName.split(",");
		String[] splitedpipeLineName=pipeLineName.split(",");
		List<String> res = new ArrayList<String>();
		for (int i = 0; i < splitedTaskName.length; i++) {
			String xpath="//a[text()='"+splitedTaskName[i]+"']/ancestor::td[contains(@data-label,'Name:')]/following-sibling::td[contains(@data-label,'Deal:')]/div/*[text()='"+splitedpipeLineName[i]+"']";
			if(!endDate.isEmpty()) {
				if(endDate.get(i)!=null) {
					String xpath1 ="/../../following-sibling::td[contains(@data-label,'End Date:')]/div/*[text()='"+endDate.get(i)+"' or text()='"+getDateFormate("MMM d, yyyy",endDate.get(i))+"']";
					xpath=xpath+xpath1;
				}
			}
			WebElement ele= FindElement(driver, xpath, splitedTaskName[i]+" xpath", action.SCROLLANDBOOLEAN,10);
			if(ele!=null) {
				log(LogStatus.PASS,"task name "+splitedTaskName[i]+" and pipeLine Name "+splitedpipeLineName[i]+" is verified ",YesNo.No);
				
			}else {
				log(LogStatus.FAIL,"task name "+splitedTaskName[i]+" and pipeLine Name "+splitedpipeLineName[i]+" is not verified ",YesNo.Yes);
				res.add("task name "+splitedTaskName[i]+" and pipeLine Name "+splitedpipeLineName[i]+" is not verified ");
			}
		}
		return res;
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param environment
	 * @param mode
	 * @param taskName
	 * @param documentName
	 * @param updatedDocumentName
	 * @param boxUserName
	 * @param BoxPassword
	 * @param BoxLoginStatus
	 * @param ErrorMsgCheck
	 * @return true/false
	 */
	public boolean verifyDocumentFunctionality(String environment, String mode,String taskName,String documentName, String updatedDocumentName,String boxUserName,String BoxPassword, YesNo BoxLoginStatus, YesNo ErrorMsgCheck) {
		boolean flag = false;
		String firstWindow = null;
		String secondWindow = null;
		WebElement ele= null;
		BoxPageBusinesslayer box = new BoxPageBusinesslayer(driver);
		DealPageBusinessLayer pipe = new DealPageBusinessLayer(driver);
	
		String xpath="//a[text()='"+taskName+"']/ancestor::td[contains(@data-label,'Name:')]/following-sibling::td[contains(@data-label,'View:')]/div//a[text()='Documents']";
		ele= FindElement(driver, xpath, taskName+"  document xpath", action.SCROLLANDBOOLEAN,10);
		if(ele!=null) {
			if(click(driver, ele, taskName+" document link xpath", action.SCROLLANDBOOLEAN)) {
				log(LogStatus.PASS, "clicked on document link ", YesNo.Yes);
				String parentId= switchOnWindow(driver);
				if(parentId!=null) {
					Set<String> lst = driver.getWindowHandles();
					Iterator<String> I1 = lst.iterator();
					while (I1.hasNext()) {
						String windowID = I1.next();
						if (windowID.equalsIgnoreCase(parentId)) {
							appLog.info("Parent Id is Matched");
						} else {
							firstWindow = windowID;
							appLog.info("Storged child Window Id");
							break;
						}
					}
					switchToFrame(driver,10,getFrame(PageName.NavatarDocumentPopUpOnPipeLinePage, 10));
					if(click(driver, pipe.getRefreshButton(30), "refresh button", action.BOOLEAN)) {
						List<WebElement> taggedDocumentList=pipe.getTaggedDocumentListOnNavatarDocumentsPopUp();
						if(!taggedDocumentList.isEmpty()) {
							for (int j = 0; j < taggedDocumentList.size(); j++) {
								String docName=taggedDocumentList.get(j).getText().trim();

								if(docName.equalsIgnoreCase(documentName)) {
									log(LogStatus.PASS, "tagged document name is matched and remove link is available in fornt of document name ", YesNo.No);
									String documentLink=taggedDocumentList.get(j).getAttribute("href");
									if(clickUsingJavaScript(driver, taggedDocumentList.get(j), "document link")) {
										log(LogStatus.PASS, "Clicked on Doc Name Link : "+docName, YesNo.No);
										
										Set<String> lst1 = driver.getWindowHandles();
										Iterator<String> I2 = lst1.iterator();
										while (I2.hasNext()) {
											String windowID = I2.next();
											if (windowID.equalsIgnoreCase(parentId) || windowID.equalsIgnoreCase(firstWindow)) {
												appLog.info("Parent Id or first window Id is Matched");
											} else {
												secondWindow = windowID;
												appLog.info("Storged  child child Window Id");
												driver.switchTo().window(secondWindow);
												break;
											}
										}
										if(secondWindow!=null) {
											log(LogStatus.PASS, "Box window is open", YesNo.No);
											if(BoxLoginStatus.toString().equalsIgnoreCase(YesNo.No.toString())) {
												if(box.boxLogin(boxUserName,BoxPassword, BoxLoginStatus)) {
													log(LogStatus.PASS, "Successfully Login in Box : "+boxUserName, YesNo.No);
													driver.get(documentLink);

												}else {
													log(LogStatus.PASS, "Not able to login in Box : "+boxUserName+"so cannot check document : "+documentName, YesNo.Yes);
													driver.close();
													driver.switchTo().window(firstWindow);
													driver.close();
													driver.switchTo().window(parentId);
													return false;
												}
											}else {
												log(LogStatus.PASS, "user is already loggedIn in Box "+boxUserName, YesNo.No);
											}
											
											if(ErrorMsgCheck.toString().equalsIgnoreCase(YesNo.No.toString())) {
												ele = isDisplayed(driver, FindElement(driver, "//span[@class='menu-toggle']/span[text()='Open']", "document open button", action.BOOLEAN, 60), "visibility", 20, "document open button in box");
												if(ele!=null) {
													log(LogStatus.PASS, "Document is open Successfully "+docName, YesNo.No);
													if(updatedDocumentName!=null) {
														docName=updatedDocumentName;
													}
													String x ="//span[@class='item-name'][text()='"+docName+"']";
													ele = isDisplayed(driver, FindElement(driver,x, "document name xpath", action.BOOLEAN, 10), "visibility", 10, "document name xpath");
													if(ele!=null) {
														log(LogStatus.PASS, "Document name : "+docName+" is matched successfully", YesNo.No);
														flag=true;
													}else {
														log(LogStatus.FAIL, "Document name : "+docName+" is not matched", YesNo.Yes);
													}
												}else {
													log(LogStatus.FAIL, "Document is not open in box : "+docName, YesNo.Yes);
												}
											}else {
												String expMsg=documentNotFoundErrorMsg;
												ele = pipe.getDocumentNotFoundErrorMsg(10);
												if(ele!=null) {
													String msg = ele.getText().trim();
													if(msg.equalsIgnoreCase(expMsg)) {
														log(LogStatus.PASS, "Error Message is verified "+expMsg, YesNo.No);
														flag=true;
													}else {
														log(LogStatus.FAIL,  "Error Message is not  verified for document  "+docName+" Actual Result: "+msg+" Expected Result : "+expMsg, YesNo.Yes);
													}
												}else {
													log(LogStatus.FAIL,"Error Message xpath is not found "+expMsg, YesNo.Yes);
												}
											}
											driver.close();
											driver.switchTo().window(firstWindow);
											driver.close();
											driver.switchTo().window(parentId);
											return flag;
										}else {
											log(LogStatus.FAIL, "No new window is open after click on document name "+docName+" so cannot check document open or download fuctionality",YesNo.Yes);
										}
									}else {
										log(LogStatus.FAIL, "Not able to click on document Name link : "+docName, YesNo.Yes);
									}
									break;
								}else {
									if(j==taggedDocumentList.size()-1) {
										log(LogStatus.FAIL, "document is not available in navatar document pop up:  "+documentName, YesNo.Yes);
										driver.close();
										driver.switchTo().window(parentId);
									}
								}
							}
						}else {
							log(LogStatus.FAIL, "Not able to fond tagged documents list and remove link list so cannot verify tagged documents on navatar document ", YesNo.Yes);
							driver.close();
							driver.switchTo().window(parentId);
						}
					}else {
						log(LogStatus.FAIL, "Not able to click on refresh button so cannot verify tagged documents on navatar document ", YesNo.Yes);
						driver.close();
						driver.switchTo().window(parentId);
					}
				}else {
					log(LogStatus.FAIL, "Not able to switch on navatar document window so cannot check document link functionality", YesNo.Yes);
					
				}
			}else {
				log(LogStatus.FAIL, "Not able to click on document link of task "+taskName, YesNo.Yes);
			}
		}else {
			log(LogStatus.FAIL, "Not able to found taskName "+taskName+" document link so cannot click on it ",YesNo.Yes);
			
		}
		return false;
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param username
	 * @param password
	 * @return
	 */
	public boolean CommUnityUserLogin(String username, String password) {
		driver.get("https://"+SmokeCommonVariables.CommUnityUserURL);
		sendKeys(driver, getCommUnityUserUserNameTextBox(20), username, "Username Text Box", action.THROWEXCEPTION);
		sendKeys(driver, getCommUnityUserPasswordTextBox(20), password, "Password Text Box", action.THROWEXCEPTION);
		click(driver, getLoginBtn(20), "Login Button", action.THROWEXCEPTION);
		ThreadSleep(1000);
		if (matchTitle(driver, "Home", 60)) {
			appLog.info("CommUnity User Successfully Logged In.");
			return true;
		} else {
			appLog.info("Kindly Check Username and Password.");
			exit("User is not able to log in.");
			return false;
		}
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param environment
	 * @param mode
	 * @return
	 */
	public boolean CommUnityUserlogout(String environment, String mode) {
		ThreadSleep(500);
		if (clickUsingJavaScript(driver, getCommUnityUserLogOutBtn(30), "Log out button")) {
			if (matchTitle(driver, "Login", 50)) {
				appLog.info("User successfully Logged Out");
				return true;
			}
			else {
				appLog.error("Not logged out");
			}
		}else {
			appLog.error("Log out button is not found");
		}
		return false;
	}
	
}
