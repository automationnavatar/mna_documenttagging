package com.navatar.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static com.navatar.generic.CommonLib.*;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.navatar.generic.EnumConstants.InstitutionPageFieldLabelText;
import com.navatar.generic.EnumConstants.Mode;
import com.navatar.generic.EnumConstants.action;
import com.navatar.generic.EnumConstants.excelLabel;

import static com.navatar.generic.CommonLib.*;

public class TargetPage extends BasePageBusinessLayer{


	public TargetPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//input[@id='Name']")
	private WebElement TargetName;

	/**
	 * @return the targetName
	 */

	public WebElement getTargetName(int timeOut) {
		return isDisplayed(driver, TargetName, "Visibility", timeOut, "Target Name");
	}

	@FindBy(xpath = "//label[text()='Deal']/../../../td[2]//span//input")
	private WebElement Deal;

	/**
	 * @return the deal
	 */
	public WebElement getDeal(int timeOut) {
		return isDisplayed(driver, Deal, "Visibility", timeOut, "Target Deal");
	}

	@FindBy(xpath = "//label[text()='Account Name']/../../../td[2]//span//input")
	private WebElement AccountName;

	/**
	 * @return the accountName
	 */
	public WebElement getAccountName(int timeOut) {
		return isDisplayed(driver, AccountName, "Visibility", timeOut, "Target Account Name");

	}

	@FindBy(xpath = "//label[text()='Stage']/../..//td[4]//span//select")
	private WebElement Stage;

	/**
	 * @return the stage
	 */
	public WebElement getStage(int timeOut) {
		return isDisplayed(driver, Stage, "Visibility", timeOut, "Target Stage");
	}

	@FindBy(xpath = "//label[text()='Status']/../following-sibling::td[1]//select")
	private WebElement Status;

	/**
	 * @return the status
	 */
	public WebElement getStatus(int timeOut) {
		return isDisplayed(driver, Status, "Visibility", timeOut, "Target Status");
	}

	@FindBy(xpath = "//div[@id='Name_ileinner']")
	private WebElement TargetNameLabelText;

	/**
	 * @return the targetnamelabeltext
	 */
	public WebElement getTargetnamelabeltext(int timeOut) {
		return isDisplayed(driver, TargetNameLabelText, "Visibility", timeOut, "Target Name");
	}

	@FindBy(xpath="(//span[text()='Deal']/../following-sibling::td/div)[1]")
	private WebElement DealLabelText;

	/**
	 * @return the deallabeltext
	 */
	public WebElement getDeallabeltext(int timeOut) {
		return isDisplayed(driver, DealLabelText, "Visibility", timeOut, "Target Deal Label Text");
	}
	
	@FindBy(xpath="(//span[text()='Account Name']/../following-sibling::td/div)[1]")
	private WebElement AccountNameLabelText;

	/**
	 * @return the accountnamelabeltext
	 */
	public WebElement getAccountnamelabeltext(int timeOut) {
		return isDisplayed(driver, AccountNameLabelText, "Visibility", timeOut, "Target Account name label text");
	}

	@FindBy(xpath="//label[text()='Stage']/../following-sibling::td//select")
	private WebElement stageDropDown;

	/**
	 * @return the stageDropDown
	 */
	public WebElement getStageDropDown(int timeOut) {
		return isDisplayed(driver, stageDropDown, "Visibility", timeOut, "Stage Drop Down");
	}
	
	@FindBy(xpath="//label[text()='Status']/../following-sibling::td//select")
	private WebElement statusDropDown;

	/**
	 * @return the statusDropDown
	 */
	public WebElement getStatusDropDown(int timeOut) {
		return isDisplayed(driver, statusDropDown, "Visibility", timeOut, "Status drop Down");
	}
	
	@FindBy(xpath="//label[text()='Teaser Sent']/../following-sibling::td[1]/input")
	private WebElement teaserSentCheckBox;

	/**
	 * @return the teaserSentCheckBox
	 */
	public WebElement getTeaserSentCheckBox(int timeOut) {
		return isDisplayed(driver, teaserSentCheckBox, "Visibility", timeOut, "");
	}
	
	@FindBy(xpath="//label[text()='Amount']/../following-sibling::td[1]/input")
	private WebElement amountTextBox;

	/**
	 * @return the amountTextBox
	 */
	public WebElement getAmountTextBox(int timeOut) {
		return isDisplayed(driver, amountTextBox, "Visibility", timeOut, "");
	}
   
	@FindBy(xpath="//label[text()='Date']/../following-sibling::td[1]//a")
	private WebElement currentDateLink;

	/**
	 * @return the currentDateLink
	 */
	public WebElement getCurrentDateLink(int timeOut) {
		return isDisplayed(driver, currentDateLink, "Visibility", timeOut, "");
	}

	@FindBy(xpath="//a[@title='Deal Room']")
	private WebElement dealRoomTab;

	/**
	 * @return the dealRoomTab
	 */
	public WebElement getDealRoomTab(int timeOut) {
		return isDisplayed(driver, dealRoomTab, "Visibility", timeOut, "Deal Room tab");
	}
	
	@FindBy(xpath="//label[text()='Amount']/../following-sibling::td/input")
	private WebElement amountTextBoxClassic;

	@FindBy(xpath="//span[text()='Amount']/../following-sibling::input")
	private WebElement amountTextBoxLightining;

	/**
	 * @return the amountTextBoxClassic
	 */
	public WebElement getAmountTextBox(String environment, String mode, int timeOut) {
		if(mode.equalsIgnoreCase(Mode.Classic.toString())){
			return isDisplayed(driver, amountTextBoxClassic, "Visibility", timeOut, "Amount text box");
		} else {
			return isDisplayed(driver, amountTextBoxLightining, "Visibility", timeOut, "Amount text box");
		}
	}
	
	@FindBy(xpath="//td[text()='Amount']/following-sibling::td/div")
	private WebElement amountTextBoxDisplayClassic;

	@FindBy(xpath="//span[text()='Amount']/../following-sibling::div//span/*/*")
	private WebElement amountTextBoxDisplayLightining;

	/**
	 * @return the amountTextBoxClassic
	 */
	public WebElement getAmountTextBoxDisplay(String environment, String mode, int timeOut) {
		if(mode.equalsIgnoreCase(Mode.Classic.toString())){
			return isDisplayed(driver, amountTextBoxDisplayClassic, "Visibility", timeOut, "Amount box");
		} else {
			return isDisplayed(driver, amountTextBoxDisplayLightining, "Visibility", timeOut, "Amount box");
		}
	}
	
	@FindBy(xpath="//a[@role='menuitem'][@title='Edit']")
	private WebElement contactRoleEditLinkLightining;

	/**
	 * @return the contactRoleEditLinkLightining
	 */
	public WebElement getContactRoleEditLinkLightining(String mode, int timeOut) {
		//return contactRoleEditLinkLightining;
		return isDisplayed(driver, contactRoleEditLinkLightining, "Visibility", timeOut, "Edit Link");
	}
	
	@FindBy(xpath="//a[@role='menuitem'][@title='Delete']")
	private WebElement contactRoleDeleteLinkLightining;

	/**
	 * @return the contactRoleDeleteLinkLightining
	 */
	public WebElement getContactRoleDeleteLinkLightining(String mode, int timeOut) {
		return isDisplayed(driver, contactRoleDeleteLinkLightining, "Visibility", timeOut, "Delete Link");
	}
	
	@FindBy(xpath="//label[text()='Role']/../following-sibling::td//select")
	private WebElement roleDropDownClassic;
	
	@FindBy(xpath="//span[text()='Role']/../following-sibling::div//a")
	private WebElement roleDropDownLightining;

	/**
	 * @return the roleDropDownLightining
	 */
	public WebElement getRoleDropDown(String mode, int timeOut) {
		if(mode.equalsIgnoreCase(Mode.Classic.toString())){
			return isDisplayed(driver, roleDropDownClassic, "Visibility", timeOut, "Role Drop Down");
		} else {
			return isDisplayed(driver, roleDropDownLightining, "Visibility", timeOut, "Role Drop Down");
		}
	}
	
	@FindBy(xpath="//td[@id='bottomButtonRow']/input[@title='Save']")
	private WebElement contactRoleBottomSaveButtonClassic;
	
	@FindBy(xpath="//button[@title='Save']//span[text()='Save']")
	private WebElement contactRoleBottomSaveButtonLightining;

	/**
	 * @return the bottomSaveButton
	 */
	public WebElement getBottomSaveButton(String mode, int timeOut) {
		if(mode.equalsIgnoreCase(Mode.Classic.toString())){
			return isDisplayed(driver, contactRoleBottomSaveButtonClassic, "Visibility", timeOut, "Save button");
		} else {
			return isDisplayed(driver, contactRoleBottomSaveButtonLightining, "Visibility", timeOut, "Save button");
		}
	}
	
	@FindBy(xpath="//span[text()='Primary']/../following-sibling::input")
	private WebElement primaryCheckBoxLightining;
	
	@FindBy(xpath="//label[text()='Primary']/../following-sibling::td/input")
	private WebElement primaryCheckBoxClassic;


	/**
	 * @return the primaryCheckBox
	 */
	public WebElement getPrimaryCheckBox(String mode, int timeOut) {
		if(mode.equalsIgnoreCase(Mode.Classic.toString())){
			return isDisplayed(driver, primaryCheckBoxClassic, "Visibility", timeOut, "Primary Check Box");
		} else {
			return isDisplayed(driver, primaryCheckBoxLightining, "Visibility", timeOut, "Primary Check Box");
		}
	}
	
	@FindBy(xpath="//h2[text()='Stage']/../following-sibling::div[2]//a[@title='Cancel']")
	private WebElement stagePopUpCancelButton;

	/**
	 * @return the stagePopUpCancelButton
	 */
	public WebElement getStagePopUpCancelButton(int timeOut) {
		return isDisplayed(driver, stagePopUpCancelButton, "Visibility", timeOut, "Stage Pop up Cancel button");
	}
	
	@FindBy(xpath="//h2[text()='Stage']/../following-sibling::div[2]//a[@title='OK']")
	private WebElement stagePopUpOkButton;

	/**
	 * @return the stagePopUpOkButton
	 */
	public WebElement getStagePopUpOkButton(int timeOut) {
		return isDisplayed(driver, stagePopUpOkButton, "Visibility", timeOut, "Stage Pop up Ok button");
	}
	
	@FindBy(xpath="//select[@id='pageid:frm:fieldRepeat:0:txtValue']")
	private WebElement stageDropDownManageTargetPage;

	/**
	 * @return the stageDropDownManageTargetPage
	 */
	public WebElement getStageDropDownManageTargetPage(int timeOut) {
		return isDisplayed(driver, stageDropDownManageTargetPage, "Visibility", timeOut, "Stage Drop down");
	}
	
	@FindBy(xpath="//select[@id='pageid:frm:fieldRepeat:1:txtValue']")
	private WebElement StatusDropDownManageTargetPage;

	/**
	 * @return the StatusDropDownManageTargetPage
	 */
	public WebElement getStatusDropDownManageTargetPage(int timeOut) {
		return isDisplayed(driver, StatusDropDownManageTargetPage, "Visibility", timeOut, "Status Drop down");
	}
	
	@FindBy(xpath="//input[@class='inputTextClass targetnavmnaI__Amount__c']")
	private WebElement amountPopUpTextBoxManageTargetPage;

	/**
	 * @return the amountPopUpTextBoxManageTargetPage
	 */
	public WebElement getAmountPopUpTextBoxManageTargetPage(int timeOut) {
		return isDisplayed(driver, amountPopUpTextBoxManageTargetPage, "Visibility", timeOut, "Amount");
	}
	
	@FindBy(xpath="//h2[text()='Amount']/../following-sibling::div[2]/a[@title='Cancel']")
	private WebElement amountTextBoxCancelButtonManageTargetPage;

	/**
	 * @return the amountTextBoxManageTargetPage
	 */
	public WebElement getAmountTextBoxCancelButtonManageTargetPage(int timeOut) {
		return isDisplayed(driver, amountTextBoxCancelButtonManageTargetPage, "Visibility", timeOut, "Amount text Box Cancel Button");
	}
	
	@FindBy(xpath="//h2[text()='Amount']/../following-sibling::div[2]/a[@title='OK']")
	private WebElement amountTextBoxOkButtonManageTargetPage;

	/**
	 * @return the amountTextBoxManageTargetPage
	 */
	public WebElement getAmountTextBoxOkButtonManageTargetPage(int timeOut) {
		return isDisplayed(driver, amountTextBoxOkButtonManageTargetPage, "Visibility", timeOut, "Amount text Box Ok Button");
	}
	
	@FindBy(xpath="(//a//span[text()='Save'])[1]")
	private WebElement manageTargetSaveButton;

	/**
	 * @return the manageTargetSaveButton
	 */
	public WebElement getManageTargetSaveButton(int timeOut) {
		return isDisplayed(driver, manageTargetSaveButton, "Visibility", timeOut, "Save button");
	}
	
	@FindBy(xpath="//input[@id='SearchContent2']")
	private WebElement searchBoxManageTargetPage;

	/**
	 * @return the searchBoxManageTargetPage
	 */
	public WebElement getSearchBoxManageTargetPage(int timeOut) {
		return isDisplayed(driver, searchBoxManageTargetPage, "Visibility", timeOut, "Search Box");
	}
	
	@FindBy(xpath="//input[@id='SearchContent2']/preceding-sibling::div/img")
	private WebElement searchIcon;

	/**
	 * @return the searchIcon
	 */
	public WebElement getSearchIcon(int timeOut) {
		return isDisplayed(driver, searchIcon, "Visibility", timeOut, "Search Icon");
	}
	
	@FindBy(xpath="//div[@id='divlinkdel']/a[text()='Delete Targets' or @title='Delete Targets']")
	private WebElement deleteTargetEnabledButton;

	/**
	 * @return the deleteTargetEnabledButton
	 */
	public WebElement getDeleteTargetEnabledButton(int timeOut) {
		return isDisplayed(driver, deleteTargetEnabledButton, "Visibility", timeOut, "Delete Target Button");
	}
	
	@FindBy(xpath="//h2[text()='Mass Delete Targets']/../following-sibling::div/div[@class='slds-modal__footer']/button[@title='No']")
	private WebElement massTargetDeleteNoButton;

	/**
	 * @return the massTargetDeleteNoButton
	 */
	public WebElement getMassTargetDeleteNoButton(int timeOut) {
		return isDisplayed(driver, massTargetDeleteNoButton, "Visibility", timeOut, "No button");
	}
	
	@FindBy(xpath="//h2[text()='Mass Delete Targets']/../following-sibling::div/div[@class='slds-modal__footer']/button[@title='Yes']")
	private WebElement massTargetDeleteYesButton;

	/**
	 * @return the massTargetDeleteYesButton
	 */
	public WebElement getMassTargetDeleteYesButton(int timeOut) {
		return isDisplayed(driver, massTargetDeleteYesButton, "Visibility", timeOut, "Yes button");
	}
	
	@FindBy(xpath="//div[@class='slds-icon icon-utility-custom-apps slds-icon-text-default']/img")
	private WebElement wrenchIcon;

	/**
	 * @return the wrenchIcon
	 */
	public WebElement getWrenchIcon(int timeOut) {
		return isDisplayed(driver, wrenchIcon, "Visibility", timeOut, "Wrench Icon");
	}
	
	@FindBy(xpath="//input[@id='searchcon_grid1ManageTarget']")
	private WebElement customizationPopUpSearchBox;

	/**
	 * @return the customizationPopUpSearchBox
	 */
	public WebElement getCustomizationPopUpSearchBox(int timeOut) {
		return isDisplayed(driver, customizationPopUpSearchBox, "Visibility", timeOut, "Search box");
	}
	
	@FindBy(xpath="//input[@id='searchcon_grid1ManageTarget']/preceding-sibling::a")
	private WebElement customizationPopUpSeachIcon;

	/**
	 * @return the customizationPopUpSeachIcon
	 */
	public WebElement getCustomizationPopUpSeachIcon(int timeOut) {
		return isDisplayed(driver, customizationPopUpSeachIcon, "Visibility", timeOut, "Search Icon");
	}
	
	@FindBy(xpath="//img[@id='lefttorightenableManageTarget']")
	private WebElement moveToRightButton;

	/**
	 * @return the moveToRightButton
	 */
	public WebElement getMoveToRightButton(int timeOut) {
		return isDisplayed(driver, moveToRightButton, "Visibility", timeOut, "Move Right Button");
	}
	
	@FindBy(xpath="//a[@id='_applyManageTarget']")
	private WebElement applyButton;

	/**
	 * @return the applyButton
	 */
	public WebElement getApplyButton(int timeOut) {
		return isDisplayed(driver, applyButton, "Visibility", timeOut, "Apply Button");
	}
	
	@FindBy(xpath="//a[@id='_rtdeManageTarget']")
	private WebElement revertToDefaultButton;

	/**
	 * @return the revertToDefaultButton
	 */
	public WebElement getRevertToDefaultButton(int timeOut) {
		return isDisplayed(driver, revertToDefaultButton, "Visibility", timeOut, "Revert To Default");
	}
	
	public WebElement getTargetPageTextBoxAllWebElement(String environment,String mode, String labelName, int timeOut) {
		WebElement ele=null;
		String xpath ="",inputXpath="",finalXpath="",finalLabelName="",xpath1="",xpath2="";
		if(labelName.contains("_")) {
			finalLabelName=labelName.replace("_", " ");
		}else {
			finalLabelName=labelName;
		}
		if(mode.equalsIgnoreCase(Mode.Classic.toString())) {
			
		}else {
			xpath="//span[text()='"+finalLabelName+"']";
			inputXpath="/..//following-sibling::input";
			xpath1="/../following-sibling::div/div/div/div[contains(@class,'autocompleteWrapper')]/input";
			xpath2="/..//following-sibling::div//a";
			if(labelName.toString().equalsIgnoreCase(InstitutionPageFieldLabelText.Parent_Institution.toString())) {
				inputXpath="/..//following-sibling::div//input[@title='Search Institutions']";
			}
			
		}
		if(labelName.equalsIgnoreCase(excelLabel.Deal.toString()) || labelName.equalsIgnoreCase(excelLabel.Account_Name.toString())) {
			finalXpath=xpath+xpath1;
		}else if (labelName.equalsIgnoreCase(excelLabel.Stage.toString())) {
			finalXpath=xpath+xpath2;
		}else {
			finalXpath=xpath+inputXpath;
		}
		ele=isDisplayed(driver, FindElement(driver, finalXpath, finalLabelName+" text box in "+mode, action.SCROLLANDBOOLEAN,30), "Visibility", timeOut, finalLabelName+"text box in "+mode);
		return ele;
		
	}
	

}
