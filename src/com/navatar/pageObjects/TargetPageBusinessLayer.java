package com.navatar.pageObjects;

import static com.navatar.generic.AppListeners.appLog;
import static com.navatar.generic.CommonLib.FindElement;
import static com.navatar.generic.CommonLib.FindElements;
import static com.navatar.generic.CommonLib.ThreadSleep;
import static com.navatar.generic.CommonLib.changeNumberIntoUSFormat;
import static com.navatar.generic.CommonLib.click;
import static com.navatar.generic.CommonLib.clickUsingJavaScript;
import static com.navatar.generic.CommonLib.isDisplayed;
import static com.navatar.generic.CommonLib.log;
import static com.navatar.generic.CommonLib.mouseOverOperation;
import static com.navatar.generic.CommonLib.refresh;
import static com.navatar.generic.CommonLib.selectVisibleTextFromDropDown;
import static com.navatar.generic.CommonLib.sendKeys;
import static com.navatar.generic.CommonLib.switchToFrame;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import com.relevantcodes.extentreports.LogStatus;
import com.navatar.generic.EnumConstants.*;

public class TargetPageBusinessLayer extends TargetPage {

	public TargetPageBusinessLayer(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @author ANKIT JAISWAL
	 * @param environment
	 * @param mode
	 * @param targetName
	 * @param dealName
	 * @param accountName
	 * @param otherFieldAndValues
	 * @return
	 */
	public boolean createTarget(String environment,String mode,String targetName,String dealName,String accountName,String[][] otherFieldAndValues ) {
//		if(otherFieldAndValues!=null) {
//			labelNames= otherLabelFields.split(",");
//			labelValue=otherLabelValues.split(",");
//		}
		refresh(driver);
		ThreadSleep(3000);
		if(mode.equalsIgnoreCase(Mode.Lightning.toString())) {
			ThreadSleep(10000);
			if(clickUsingJavaScript(driver, getNewButton(environment, mode, 60), "new button")) {
				appLog.info("clicked on new button");
				ThreadSleep(5000);
				if(sendKeys(driver, getTargetPageTextBoxAllWebElement(environment, mode, excelLabel.Target_Name.toString(),30), targetName, "target name text box ", action.SCROLLANDBOOLEAN)) {
					log(LogStatus.INFO, "Passed value in target name text box : "+targetName,YesNo.No);
					if(sendKeys(driver, getTargetPageTextBoxAllWebElement(environment, mode, excelLabel.Deal.toString(),30), dealName, "deal name text box ", action.SCROLLANDBOOLEAN)) {
						log(LogStatus.INFO, "Passed value in deal name text box : "+dealName,YesNo.No);
						ThreadSleep(1000);
						WebElement ele=FindElement(driver,
								"//div[contains(@class,'uiAutocomplete')]//a//div[@title='" + dealName+ "']","deal Name List", action.SCROLLANDBOOLEAN, 30);
						if (click(driver,ele,dealName+" text ", action.SCROLLANDBOOLEAN)) {
							log(LogStatus.INFO, "clicked on company Name suggest Text : "+dealName, YesNo.No);
							
							if (sendKeys(driver, getTargetPageTextBoxAllWebElement(environment, mode, excelLabel.Account_Name.toString(),30), accountName, "account name text box ", action.SCROLLANDBOOLEAN)) {
								log(LogStatus.INFO, "Passed value in account name text box : "+accountName,YesNo.No);
								
								ele=FindElement(driver,
										"//div[contains(@class,'uiAutocomplete')]//a//div[@title='" + accountName+ "'][contains(@class,'primaryLabel')]","deal Name List", action.SCROLLANDBOOLEAN, 30);
								
								if (click(driver,ele,accountName+" text ", action.SCROLLANDBOOLEAN)) {
									log(LogStatus.INFO, "clicked on Account Name suggest Text : "+accountName, YesNo.No);
								

									if (click(driver, getSaveButton(environment, mode, 60), "Save Button", action.BOOLEAN)) {
										ThreadSleep(3000);
										if(fieldValueVerificationOnTargetPage(environment, mode, TabName.Targets, excelLabel.Target_Name.toString(),accountName+"-"+dealName)) {
											log(LogStatus.PASS, excelLabel.Target_Name.toString()+" is verified "+targetName, YesNo.No);
											return true;
										}else {
											log(LogStatus.ERROR, excelLabel.Target_Name.toString()+" is not verified "+targetName, YesNo.Yes);
										}
									} else {
										log(LogStatus.ERROR, "Not able to click on pipeLine Save button so cannot create target "+targetName, YesNo.Yes);
									}
							
								}else {
									log(LogStatus.ERROR, "Not able to click on Account Name from suggestion drop down "+accountName,YesNo.Yes);
								}
							} else {
								log(LogStatus.ERROR, "Not able to pass value in Account text box so cannot create PipeLine: " + accountName, YesNo.Yes);
							}
						} else {
							log(LogStatus.ERROR, "Not able to click on deal Name from suggestion drop down "+dealName,YesNo.Yes);
						}
						
					}else {
						log(LogStatus.ERROR, "Not able to pass value in deal text box so cannot create PipeLine: " + dealName, YesNo.Yes);
					}
					
				}else {
					log(LogStatus.ERROR, "Not able to pass value in target name text box so cannot create PipeLine: " + targetName, YesNo.Yes);
				}
				
			}else {
				log(LogStatus.ERROR, "Not able to click on New Button so cannot create target: " + targetName, YesNo.Yes);
			}
		}
		return false;
	}
	
	/**
	/**
	 * @author Ankit Jaiswal
	 * @param environment
	 * @param mode
	 * @param targetName
	 * @return
	 */
	public boolean clickOnCreatedTargets(String environment,String mode,String targetName){
		if(mode.equalsIgnoreCase(Mode.Classic.toString())){
		if (click(driver, getGoButton(60), "Go Button", action.BOOLEAN)) {
			WebElement partnershipName = FindElement(driver,
					"//div[@class='x-panel-bwrap']//span[text()='" + targetName + "']",
					"Partnership Legal Name", action.BOOLEAN, 60);
			if (partnershipName != null) {
				if (click(driver, partnershipName, "Partnership Name", action.SCROLLANDBOOLEAN)) {
					appLog.info("Clicked on partnership name" + targetName + "successfully.");
					return true;
				} else {
					appLog.error("Not able to click on partnership name");
				}
			} else {
				appLog.error("Partnership name is not displaying");
			}
		} else {
			appLog.error("Not able to click on go button so cannot click on created partnership");
		}
	}else{
		if(clickOnAlreadyCreated_Lighting(environment, mode, TabName.Targets, targetName, 30)){
			appLog.info("Clicked on partnership name" + targetName + "successfully.");
			return true;
		}else{
			appLog.error("Not able to click on partnership name : "+targetName);
			return false;
		}
	}
		return false;
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param environment
	 * @param mode
	 * @param tabName
	 * @param labelName
	 * @param labelValue
	 * @return true/false
	 */
	public boolean fieldValueVerificationOnTargetPage(String environment, String mode, TabName tabName,
			String labelName,String labelValue) {
		String finalLabelName;
		if (labelName.contains("_")) {
			finalLabelName = labelName.replace("_", " ");
		} else {
			finalLabelName = labelName;
		}
		String xpath = "";
		WebElement ele = null;
		if (mode.equalsIgnoreCase(Mode.Classic.toString())) {
			if(labelName.toString().equalsIgnoreCase(excelLabel.Account_Name.toString()) || labelName.toString().equalsIgnoreCase(excelLabel.Deal.toString())) {
				xpath="(//span[text()='"+finalLabelName+"']/../following-sibling::td)[1]";
			}else {
				
				xpath = "(//td[text()='"+finalLabelName+"']/following-sibling::td)[1]";
			}

		}else {
			if(labelName.toString().equalsIgnoreCase(excelLabel.Account_Name.toString()) || labelName.toString().equalsIgnoreCase(excelLabel.Deal.toString())){
				xpath = "//span[text()='"+finalLabelName+"']/../following-sibling::div/span//a";	
			} else {
				xpath = "//span[text()='"+finalLabelName+"']/../following-sibling::div//span/*/*";
			}

		} 
		ele = isDisplayed(driver,
				FindElement(driver, xpath, finalLabelName + " label text in " + mode, action.SCROLLANDBOOLEAN, 20),
				"Visibility", 20, finalLabelName + " label text in " + mode);
		if (ele != null) {
			String aa = ele.getText().trim();
			appLog.info("Lable Value is: "+aa);
			if(labelValue.isEmpty() && aa.isEmpty()){
				appLog.info(labelValue + " Value is matched successfully.");
				return true;
			} else if(aa.contains(labelValue)) {
				appLog.info(labelValue + " Value is matched successfully.");
				return true;
			}else {
				appLog.info(labelValue + " Value is not matched. Expected: "+labelValue+" /t Actual : "+aa);
			}
		} else {
			appLog.error(finalLabelName + " Value is not visible so cannot matched  label Value "+labelValue);
		}
		return false;
	}


}
