package com.navatar.pageObjects;

public interface DealPageErrorMessage {

	
	public static String uploadDocumentErrorMsg="All documents have been added successfully.";
	public static String tagDocumentErrorMsg="Document(s) tagged successfully.";
	public static String documentNotFoundErrorMsg="Oops! We can't seem to find the page you're looking for.";
	public static String removeTaggedDocumentPopUpErrorMsg="Are you sure you want to remove this tagged document?";
	public static String viewInformationErrorMsg="Please register for Navatar Deal Room from the Navatar Deal Room Manager tab in order to view this information.";
	public static String NoDealRoomBuildErrorMsg="No Deal Room has been built for this Deal.";
	
	public static String targetSuccessfullyAddedMessage="Target added successfully.";
	public static String UploadSelectTargetMessage="Please select at least one target.";
	public static String UploadConfirmationMessage="All documents have been added successfully.";
	public static String DontAccessErrorMsg="You do not have access to this Deal Room. Please contact your Navatar Administrator";
	
}
