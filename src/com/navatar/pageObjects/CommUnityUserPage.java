package com.navatar.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.navatar.generic.EnumConstants.action;

import static com.navatar.generic.CommonLib.*;

import java.util.List;

public class CommUnityUserPage extends BasePageBusinessLayer {

	public CommUnityUserPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	
	public WebElement getOpenAndCompleteTaskLink(Header headerName, int timeOut) {
		String xpath="//div[contains(@class,'sdgborder')]//a[contains(text(),'Tasks: "+headerName+"')]";
		WebElement ele= FindElement(driver, xpath,headerName+" xpath ", action.SCROLLANDBOOLEAN, timeOut);
		return isDisplayed(driver, ele, "visibility", timeOut,headerName+" xpath ");
		
	}
	
	public WebElement getOpenTaskCompleteLink(String taskName, String pipeLineName, int timeOut) {
		String xpath="//a[text()='"+taskName+"']/ancestor::td[contains(@data-label,'Name:')]/following-sibling::td[contains(@data-label,'Deal:')]/div/*[text()='"+pipeLineName+"']/../../following-sibling::td[contains(@data-label,'Action:')]//a[text()='Complete']";
		WebElement ele= FindElement(driver, xpath,taskName+" complete link xpath", action.SCROLLANDBOOLEAN, timeOut);
		return ele;
		
	}
	
	
	public List<WebElement> getTabList(){
		return FindElements(driver, "//li[@data-aura-class='uiTabItem']/a", "tab list");
	}
	
	
	public WebElement getTaskNameLink(String taskName, int timeOut) {
		String xpath="//td[@data-label='Task: ']//a[text()='"+taskName+"']";
		WebElement ele= FindElement(driver, xpath,taskName+" xpath ", action.SCROLLANDBOOLEAN, timeOut);
		return isDisplayed(driver, ele, "visibility", timeOut,taskName+" xpath ");
	}
	
	public WebElement getcreateTaskPageFieldLabelText(String fieldName,String fieldValue, int timeOut) {
		String xpath="//span[text()='"+fieldName+"']/../following-sibling::div//*[text()='"+fieldValue+"']";
		WebElement ele= FindElement(driver, xpath,fieldValue+" xpath ", action.SCROLLANDBOOLEAN, timeOut);
		return isDisplayed(driver, ele, "visibility", timeOut,fieldValue+" xpath ");
	}
	
	@FindBy(xpath = "//input[@class='inputBox input'][@placeholder='Username']")
	private WebElement commUnityUserUserNameTextBox;

	public WebElement getCommUnityUserUserNameTextBox(int timeOut) {
		return  isDisplayed(driver, commUnityUserUserNameTextBox, "visibility", timeOut,"commUnity User UserName TextBox");
	}
	
	@FindBy(xpath = "//input[@class='inputBox input'][@placeholder='Password']")
	private WebElement commUnityUserPasswordTextBox;

	public WebElement getCommUnityUserPasswordTextBox(int timeOut) {
		return  isDisplayed(driver, commUnityUserPasswordTextBox, "visibility", timeOut,"commUnity User password TextBox");
	}
	
	@FindBy(xpath = "//button[contains(@class,'loginButton')]/span")
	private WebElement loginBtn;

	public WebElement getLoginBtn(int timeOut) {
		return isDisplayed(driver, loginBtn, "visibility", timeOut,"commUnity User login button");
	}
	
	public WebElement getTaskNameDocumentLink(String taskName, int timeOut) {
		String xpath="//td[@data-label='Task: ']//a[text()='"+taskName+"']/../../../following-sibling::td[@data-label='View Documents: ']//a";
		WebElement ele= FindElement(driver, xpath,taskName+"  document link xpath ", action.SCROLLANDBOOLEAN, timeOut);
		return isDisplayed(driver, ele, "visibility", timeOut,taskName+" document link xpath ");
	}
	
	
	public WebElement getTaskNameCompleteLink(String taskName, int timeOut) {
		String xpath="//td[@data-label='Task: ']//a[text()='"+taskName+"']/../../../following-sibling::td[@data-label='Complete Task: ']//a";
		WebElement ele= FindElement(driver, xpath,taskName+"  Complete link xpath ", action.SCROLLANDBOOLEAN, timeOut);
		return isDisplayed(driver, ele, "visibility", timeOut,taskName+" Complete link xpath ");
	}
	
	@FindBy(xpath = "//button[@title='Logout']")
	private WebElement commUnityUserLogOutBtn;

	public WebElement getCommUnityUserLogOutBtn(int timeOut) {
		return isDisplayed(driver, commUnityUserLogOutBtn, "visibility", timeOut,"commUnity User logout button");
	}
}
