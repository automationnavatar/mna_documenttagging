package com.navatar.pageObjects;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import com.navatar.generic.SoftAssert;
import com.navatar.generic.EnumConstants.PageName;
import com.navatar.generic.EnumConstants.TabName;
import com.navatar.generic.BaseLib;
import com.navatar.generic.CommonLib;
import com.navatar.generic.ExcelUtils;
import com.navatar.generic.SmokeCommonVariables;
import com.navatar.generic.EnumConstants;
import static com.navatar.generic.AppListeners.*;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Screen;
import static com.navatar.generic.CommonLib.*;
import static org.testng.Assert.assertNotNull;

public class DealRoomManagerPageBusinessLayer extends DealRoomManagerPage {

	public DealRoomManagerPageBusinessLayer(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	
	/**
	 * 
	 * @param dealName
	 * @param pageName
	 * @return true/false
	 */
	public boolean selectDealFromList(String dealName, String pageName) {
		if (pageName.equalsIgnoreCase("Admin Users")) {
			ThreadSleep(2000);
			if (sendKeys(driver, getDealSelectionList(60), dealName, "Deal Select TextBox", action.BOOLEAN)) {
				ThreadSleep(5000);
				if (clickUsingJavaScript(driver, FindElement(driver, "//ul[contains(@class,'ui-autocomplete')]//a[text()='"+dealName+"']", "Deal List", action.THROWEXCEPTION, 30),"Deal Name")) {
					appLog.info(dealName + " Deal is present in list.");
					return true;
				} else {
					appLog.info(dealName + " Deal is not present in the list.");
					return false;
				}
			} else {
				return false;
			}
		} else if (pageName.equalsIgnoreCase("Watermarking")) {
			if (selectVisibleTextFromDropDown(driver, getSelectDealRoom().get(0), "Select Deal Room Drop Down",
					dealName)) {
				appLog.info(dealName + " :Deal is successfully selected");
				return true;
			} else {
				appLog.info(dealName + " :Deal is not present in the drop down");
			}
			return false;
		} else if (pageName.equalsIgnoreCase("Other")) {
			if (getEditIconOnDRMTab(60) != null) {
				if (selectVisibleTextFromDropDown(driver, getSelectDealRoom().get(0), "Select Deal Room Drop Down",
						dealName)) {
					appLog.info(dealName + " :Deal is successfully selected");
					return true;
				} else {
					appLog.info(dealName + " :Deal is not present in the drop down");
				}
				return false;
			} else {
				if (selectVisibleTextFromDropDown(driver, getSelectDealRoom().get(1), "Select Deal Room Drop Down",
						dealName)) {
					appLog.info(dealName + " :Deal is successfully selected");
					return true;
				} else {
					appLog.info(dealName + " :Deal is not present in the drop down");
				}
				return false;
			}
		} else {
			appLog.info("Kindly pass the proper page name.");
			exit("Page name is wrong.");
			return false;
		}
	}
	
	/**
	 * 
	 * @param userName
	 * @param pageName
	 * @return true/false
	 */
	public boolean searchContact(String userName, String pageName) {
		if (pageName.equalsIgnoreCase("Admin Users") || pageName.equalsIgnoreCase("Internal Users")
				|| pageName.equalsIgnoreCase("Manage Approvals")) {
			if (sendKeys(driver, getuserGridSearchBox(120), userName, "Internal User Search Box", action.BOOLEAN)) {
			ThreadSleep(2000);
				if (getEditIconOnDRMTab(60) != null) {
					System.out.println("clicking on the view mode search icon");
					if (!click(driver, getSearchIcon().get(0), "Search Icon", action.BOOLEAN)) {
						return false;
					}
					return true;
				} else {
					System.out.println("clicking on the edit mode search icon");
					if (!click(driver, getSearchIcon().get(1), "Search Icon", action.BOOLEAN)) {
						return false;
					}
					return true;
				}
			}
			return false;
		} else if (pageName.equalsIgnoreCase("Contact Access")) {
			if (sendKeys(driver, getContactAccessSearchBox(30), userName, "Search Contacts", action.BOOLEAN)) {
				if (!click(driver, getSearchIcon().get(0), "Search Icon", action.BOOLEAN)) {
					return false;
				}
				return true;
			}
			return false;
		} else {
			exit("Issue in search functionality of page: " + pageName);
			return false;
		}
	}
	
	/**
	 * 
	 * @param dealName
	 * @param userName
	 * @param emailID
	 * @param pageName
	 * @return true/false
	 */
	public boolean giveDealAccessToUser(String dealName, String userName, String emailID, String pageName) {
		if (!selectDealFromList(dealName, pageName)) {
			appLog.info(dealName + " :Deal cannot be selected from the drop down.");
			return false;
		}
		click(driver, getEditIconOnDRMTab(120), "Edit icon", action.THROWEXCEPTION);
		if (!selectAdminUserFromUserGrid(userName,emailID)) {
			return false;
		}
		if (!click(driver, getAdminUserTabSaveButton(120), "Save Button", action.SCROLLANDBOOLEAN)) {
			return false;
		}

		if (!click(driver, getAdminUserSuccessSmallCloseButton(120), "Close Button", action.BOOLEAN)) {
			if (!click(driver, getAdminUserSuccessMediumCloseButton(120), "Close Button", action.BOOLEAN)) {
				appLog.info("User was already given access to " + dealName + " deal.");
				return true;
			}
		}
		return true;
	}
	
	/**
	 * 
	 * @param userName
	 * @param emailID
	 * @return true/false
	 */
	public boolean selectAdminUserFromUserGrid(String userName, String emailID) {
		System.out.println(userName);
		if (click(driver,
				FindElement(driver, "//input[@id='"+emailID+"']",
						userName + " :user radio button", action.BOOLEAN, 30),
				"Admin User", action.BOOLEAN)) {
			appLog.info(userName + " :Admin User is selected.");
			return true;
		} else {
			appLog.info(userName + " :Admin User cannot be selected or is not present in the list.");
			return false;
		}
	}
	
	/**
	 * 
	 * @param userName
	 * @param emailID
	 * @param mode
	 * @return true/false
	 */
	public boolean selectInternalUserFromUserGrid(String userName, String emailID, String mode) {
		System.out.println(userName);
		if (mode.equalsIgnoreCase("View")){
			if (click(driver,
					FindElement(driver, "//ab[text()='v"+emailID+"']/preceding-sibling::input",
							userName + " :user radio button", action.BOOLEAN, 30),
					"Admin User", action.BOOLEAN)) {
				System.out.println("Element is found");
				appLog.info(userName + " :Admin User is selected.");
				return true;
			} else {
				appLog.info(userName + " :Admin User cannot be selected or is not present in the list.");
				return false;
			}
		} else if (mode.equalsIgnoreCase("Edit")) {
			System.out.println("will search for user");
			if(click(driver,
					FindElement(driver, "//input[@id='"+emailID+"']",
							userName + " :user CheckBox", action.BOOLEAN, 60),
					"Admin User", action.BOOLEAN)){
				appLog.info(userName + " :Admin User is selected.");
				return true;
			} else {
				appLog.info(userName + " :Admin User is not selected.");
				return false;
			}
		} else {
			appLog.info(userName + " :Admin User cannot be selected or is not present in the list.");
			return false;
		}
	}
	
	/**
	 * 
	 * @param userFirstName
	 * @param userLastName
	 * @param expectedURL
	 */
	public void verifyErrorMessageOnUserGridForAdmin(String userFirstName, String userLastName, String expectedURL) {
		switchToFrame(driver, 60, getDealRoomManagerFrame(60));
		click(driver, getInformationIconOnAdminUsersTab(60), "Information icon", action.BOOLEAN);
		// ThreadSleep(10000);
		String actualUserName1 = isDisplayed(driver, FindElement(driver,
				"(//span[@id='grid_InformationUser-row-0']/span)[2]", "User Name", action.THROWEXCEPTION, 30),
				"Visibility", 60, "User Name").getText().toLowerCase();
		System.out.println(actualUserName1);
		Assert.assertTrue(actualUserName1.contains(userFirstName.toLowerCase() + " " + userLastName.toLowerCase()), "User Name is not Matched");
		appLog.info("User Name is verfied successfully");
		verifyNavatarSupportLink(expectedURL);
		switchToFrame(driver, 60, getDealRoomManagerFrame(60));
		click(driver, getCloseButtonInInformationPopup(30), "Close Button", action.THROWEXCEPTION);

	}
	
	/**
	 * 
	 * @param expectedURL
	 */
	public void verifyNavatarSupportLink(String expectedURL) {
		SoftAssert sa = new SoftAssert();
		click(driver, getNavatarSupportLink(60), "Navatar Support Link", action.THROWEXCEPTION);
		String parentwindow = switchOnWindow(driver);
		String actualURL = getURL(driver, 15);
		sa.assertTrue(actualURL.contains(expectedURL), "URL Not matched");
		driver.close();
		driver.switchTo().window(parentwindow);
		sa.assertAll();

	}
	
	
	/**
	 * clickOnDRMTab
	 */
	public void clickOnDRMTab() {
		clickOnTab(SmokeCommonVariables.EnvironmentVariable, SmokeCommonVariables.ModeVariable, TabName.DRMTab);
	}
	
	/**
	 * 
	 * @param firstName
	 * @param lastName
	 */
	public void checkFirstAndLastName(String firstName, String lastName) {
		sendKeys(driver, getFirstName(60), firstName, "" + firstName + "", action.THROWEXCEPTION);
		sendKeys(driver, getLastName(60), lastName, "" + lastName + "", action.THROWEXCEPTION);
	}

	/**
	 * clickonRegisterButton
	 */
	public void clickonRegisterButton() {
		click(driver, getRegisterButton(60), "Register Button", action.THROWEXCEPTION);
	}

	/**
	 * 
	 * @param userFirstName
	 * @param userLastName
	 * @param typeOfUser
	 * @param checkErrorMessage
	 * @param landingPage
	 * @throws Exception 
	 */
	public void UserRegistrationAndErrorMsgCheck(String userFirstName, String userLastName, String typeOfUser,
			String checkErrorMessage, String landingPage) throws Exception {
		SoftAssert sa = new SoftAssert();
		clickOnDRMTab();
		ThreadSleep(10000);
		switchToFrame(driver, 60,getFrame(PageName.DealRoomManagerParentFarme, 60));
		ThreadSleep(5000);
		switchToFrame(driver, 60, getDealRoomManagerFrame(60));
		if (typeOfUser.equalsIgnoreCase("SuperAdmin")) {
//			click(driver, getNavatarSalesTeamLink(60), "Navatar Sales Team Link.", action.THROWEXCEPTION);
//
//			if (!verifyNavatarSalesTeamLinkFunctionality("NavatarSalesTeamLink")) {
//			//	sa.assertTrue(false, "Verification of navatar sales team link is unsuccessfull.");
//			} else {
//				appLog.info("Verification of the Navatar Sales Team Link is successfull.");
//			}
			if(clickUsingJavaScript(driver, getStartButton(60), "Start Button")) {
				appLog.info("clicked on start button");
			}else {
				appLog.error("Not able to click on start button so cannot register super admin "+userFirstName+" "+userLastName);
				throw new Exception();
			}
			
		}
		if (checkErrorMessage.equalsIgnoreCase("Yes")) {
			getFirstName(60).clear();
			appLog.info("First Name Field is Cleared");
			clickonRegisterButton();
			String firstNameErrorMsg = getFirstNameErrorMsg(60).getText().trim();
			sa.assertTrue(firstNameErrorMsg.contains(DRMPageErrorMessage.pleaseEnterValue),
					"First Name Error Message is not matched.");
			appLog.info("First Name error message is verified successfully.");
			sendKeys(driver, getFirstName(60), ExcelUtils.readData("User", 1, 1), "First Name", action.THROWEXCEPTION);
			getLastName(60).clear();
			appLog.info("Last Name Field is cleard.");
			clickonRegisterButton();
			String lastNameErrorMsg = getLastNameErrorMsg(60).getText().trim();
			sa.assertTrue(lastNameErrorMsg.contains(DRMPageErrorMessage.pleaseEnterValue),
					"Last Name Error Message is not matched.");
			appLog.info("Last Name error message is verified successfully.");
			// sendKeys(driver,getLastName(60), ExcelUtils.readData("User", 1,
			// 1), "Last Name", action.THROWEXCEPTION);
			getFirstName(60).clear();
			checkFirstAndLastName(ExcelUtils.readData("User", 1, 7), ExcelUtils.readData("User", 1, 8));
			if (!click(driver, getRegisterButton(60), "Register Button", action.BOOLEAN)) {
				appLog.info("Register Button successfully clicked.");
			}
			String errorMsg = switchToAlertAndGetMessage(driver, 60, action.GETTEXT);
			sa.assertTrue(errorMsg.contains(DRMPageErrorMessage.specialCharacterErrorMsg),
					"Special Character Message is not verified.");
			switchToAlertAndAcceptOrDecline(driver, 60, action.ACCEPT);
			checkFirstAndLastName(ExcelUtils.readData("User", 1, 9), ExcelUtils.readData("User", 1, 10));
			if (!click(driver, getRegisterButton(60), "Register Button", action.BOOLEAN)) {
				appLog.info("Register Button clicked successfully.");
			}
			String maxCharErrorMsg = switchToAlertAndGetMessage(driver, 60, action.GETTEXT);
			sa.assertTrue(maxCharErrorMsg.contains(DRMPageErrorMessage.maxCharacterErrorMsg),
					"Max Character Message is not verified.");
			switchToAlertAndAcceptOrDecline(driver, 60, action.ACCEPT);
			isDisplayed(driver, getFirstName(60), "visibility", 60, "First Name").clear();
			isDisplayed(driver, getLastName(60), "visibility", 60, "First Name").clear();
			checkFirstAndLastName(userFirstName, userLastName);
		}
		if(clickUsingJavaScript(driver,  getRegisterButton(60), "Register Button")) {
			appLog.info("clicked on register button");
			ThreadSleep(2000);
			switchToDefaultContent(driver);
			String allowaccess = isDisplayed(driver, getAllowAccessLabelText(60), "visibility", 60, "Allow Access Text")
					.getText();
			sa.assertTrue(allowaccess.contains(DRMPageErrorMessage.allowAccessMsg), DRMPageErrorMessage.allowAccessMsg+" Text is not matched.");
			if (checkErrorMessage.equalsIgnoreCase("Yes")) {
				click(driver, getDenyButton(60), "Deny Button", action.THROWEXCEPTION);
				WebElement ele = isDisplayed(driver, getDeniedAccessLabeltext(20), "visibility", 20,
						"Remote Access denied Text.");
				if (ele!=null) {
					String accessdeniedtext =ele.getText().trim();	
					sa.assertTrue(accessdeniedtext.contains(DRMPageErrorMessage.loginAuthenticationErrorMsg),DRMPageErrorMessage.loginAuthenticationErrorMsg);
				} else {

				}
				driver.navigate().back();
			}
			click(driver, getAllowButton(60), "Allow Button", action.THROWEXCEPTION);
			ThreadSleep(10000);
			switchToFrame(driver, 60,getFrame(PageName.DealRoomManagerParentFarme, 60));
			ThreadSleep(5000);
			switchToFrame(driver, 60, getDealRoomManagerFrame(60));
			String registrationMsg = isDisplayed(driver, getRegistrationsuccessfulText(60), "visibility", 60,
					"Registration Successful Text").getText().trim();
			sa.assertTrue(registrationMsg.contains(DRMPageErrorMessage.registrationSuccessfulErrorMsg),
					"Registration Successful Text is not matched.");
			
			if(clickUsingJavaScript(driver, getRegistrationSuccessfulPopUpCloseButton(60), "Registration close button")) {
				appLog.info("clicked on registration close button");
				ThreadSleep(2000);
				if (typeOfUser.equalsIgnoreCase("SuperAdmin")) {
					appLog.info("SuperAdmin Registration is successfully.");
					String adminUserText = isDisplayed(driver, getAdminUserLabelText(60), "visibility", 60,
							"Admin User label text").getText().trim();
					sa.assertTrue(adminUserText.contains("Admin Users"), "Admin User label is not matached");
					appLog.info("Super Admin Default landing page is verified successfully.");
				}
				if (typeOfUser.equalsIgnoreCase("CRMUser")) {
					appLog.info("CRM User Registration is successfully.");
					String adminUserText = isDisplayed(driver, getDRMTabsLandingTextMsg(60), "visibility", 60,
							"Admin User label text").getText().trim();
					sa.assertTrue(adminUserText.contains(landingPage), "CRM User label is not matached");
					appLog.info("CRM User Default landing page is verified successfully.");
				}
			}else {
				appLog.error("Not able to click on registration popup Close button");
				throw new Exception();
			}
			
		}else {
			appLog.error("Not able to click on register button so cannot register super admin "+userFirstName+" "+userLastName);
			throw new Exception();
		}
		driver.switchTo().defaultContent();
		sa.assertAll();
	}
	
	/**
	 * 
	 * @param linkType
	 * @return true/false
	 */
	public boolean verifyNavatarSalesTeamLinkFunctionality(String linkType) {
		String activateWinPath = System.getProperty("user.dir") + "\\AutoIT\\ActivateMailWin.exe";
		String closeWinPath = System.getProperty("user.dir") + "\\AutoIT\\CloseMailWin.exe";
		String mailIDImagePath = System.getProperty("user.dir") + "\\AutoIT\\MailIDImage.jpeg";
		boolean flag = true;
		try {
			Process activateMailWin = Runtime.getRuntime().exec(activateWinPath);
			activateMailWin.waitFor();
			if (activateMailWin.exitValue() == 1) {
				appLog.info("Mail window is successfully launced.");
				System.out.println("kindly delete the mail id");
				// Scanner scan = new Scanner(System.in);
				// scan.nextLine();
				ThreadSleep(10000);
				Screen screen = new Screen();
				if(linkType.equalsIgnoreCase("NavatarSalesTeamLink")){
				try {
					screen.find(mailIDImagePath);
					appLog.info("MailID is Successfully matched.");
					flag = true;
				} catch (FindFailed e) {
					appLog.info("MailID is not matched.");
					flag = false;
				}
				} else {
					appLog.info("Mail Pop is verified Succesfully.");
				}
				try {
					Process closeWin = Runtime.getRuntime().exec(closeWinPath);
					closeWin.waitFor();
					if (closeWin.exitValue() == 1) {
						appLog.info("Successfully closed the mail Window.");
					}
				} catch (IOException e) {
					appLog.info("Kindly check weather CloseMailWin.exe file is present at the location " + closeWinPath
							+ "\nKindly close mail window manually.");
				}
				return flag;
			} else {
				appLog.info("Mail Window is not opening.");
//				exit("Mail Window is not opening.");
				return false;
			}
		} catch (IOException | InterruptedException e) {
			appLog.info("Kindly check weather ActivateMailWin.exe file is present at the location " + activateWinPath);
			return false;
		}

	}
	

	/**
	 * verifyErrorMessageOnUserGrid
	 */
	public void verifyErrorMessageOnUserGrid() {
		// switchToFrame(driver, 60,getDealRoomManagerFrame(60));
		SoftAssert sa = new SoftAssert();
		String errorMessage = isDisplayed(driver, getErrorMessageOnGlobalUserGrid(20), "Visibility", 30,
				"Error Message On User Grid").getText().trim();
		sa.assertTrue(errorMessage.contains(DRMPageErrorMessage.noDataDisplayErrorMessage),
				"No Data to Display error message is not showing ");
		appLog.info("No data to display error message is verified ");
		sa.assertAll();
	}
	
	/**
	 * verifyErrorMessageOnUserGridByDealOption
	 */
	public void verifyErrorMessageOnUserGridByDealOption() {
		// switchToFrame(driver, 60,getDealRoomManagerFrame(60));
		SoftAssert sa = new SoftAssert();
		click(driver, getEditIconOnDRMTab(30), "Edit icon on DRM Tab", action.THROWEXCEPTION);
		click(driver, getByDealRadioButton(30), "By Deal Radio Button", action.THROWEXCEPTION);
		String errorMessage = isDisplayed(driver, getErrorMessageOnByDealUserGrid(20), "Visibility", 30,
				"Error Message On User Grid").getText().trim();
		sa.assertTrue(errorMessage.contains(DRMPageErrorMessage.noDataDisplayErrorMessage),
				"No Data to Display error message is not showing ");
		appLog.info("No data to display error message is verified");
		sa.assertAll();
	}

	/**
	 * 
	 * @param giveAccessToUserName
	 * @param removeAccessOfUserName
	 * @param permissionType
	 * @param verifyInternalUserLink
	 * @throws Exception 
	 */
	public void provideGlobalUserAccess(String giveAccessToUserName, String removeAccessOfUserName, String permissionType,
			String verifyInternalUserLink) throws Exception {
		SoftAssert sf = new SoftAssert();
		if (permissionType.equalsIgnoreCase("Internal User")) {
			if(clickUsingJavaScript(driver, getInternalUserTab(60), "Internal User Tab")) {
				appLog.info("Internal User Tab clicked successfully");
			}else {
				appLog.error("Not able to click on internal user tab so cannot provide access to user : "+giveAccessToUserName);
				throw new Exception();

			}
		}
		 ThreadSleep(5000);
		 if(clickUsingJavaScript(driver, getEditIconOnDRMTab(120), "Edit icon on DRM Tab")) {
			 appLog.info("Clicked on Edit Icon ");
		 }else {
			 appLog.error("Not able to click on Edit Icon so cannot provide access to user : "+giveAccessToUserName);
			 throw new Exception();

		 }
		// ThreadSleep(20000);
		if (permissionType.equalsIgnoreCase("Admin User")) {
			WebElement adminusercheckbox = isDisplayed(driver,
					FindElement(driver,
							"//span[@id='grid_AdminUsers2']//span[text()='" + giveAccessToUserName
									+ "']/../..//input",
							"Users Name", action.THROWEXCEPTION, 60),
					"visibility", 60, "Internal User Check Box", action.THROWEXCEPTION);
			click(driver, adminusercheckbox, "Users Name", action.THROWEXCEPTION);
			appLog.info("Admin user checkbox is clicked successfully");
		} else if (permissionType.equalsIgnoreCase("Internal User")) {
			WebElement internalusercheckbox = isDisplayed(driver,
					FindElement(driver,
							"//span[@id='grid_InternalUsers2']//span[text()='" + giveAccessToUserName
									+ "']/../..//input",
							"Users Name", action.THROWEXCEPTION, 60),
					"visibility", 60, "Internal User Check Box", action.THROWEXCEPTION);
			 if(clickUsingJavaScript(driver, internalusercheckbox, giveAccessToUserName+ "Users Name check box")) {
				 appLog.info("Clicked user check box : "+giveAccessToUserName);
			 }else {
				 appLog.error("Not able to click on check Box so cannot provide access to user : "+giveAccessToUserName);
				 throw new Exception();

			 }
		}
		// ThreadSleep(5000);
		if (permissionType.equalsIgnoreCase("Internal User")) {
			if(clickUsingJavaScript(driver, getInternalUserSaveButton(120), "Save Button")) {
				appLog.info("clicked on save button");
				ThreadSleep(2000);
				if(clickUsingJavaScript(driver, getInternalUserCloseButton(120), "close button")) {
					appLog.info("clicked on Close button");
				}else {
					appLog.error("Not able to click on internal user tab Close button but provide access to user : "+giveAccessToUserName);
					
				}
			}else {
				appLog.error("Not able to click on internal user tab save button so cannot provide access to user : "+giveAccessToUserName);
				throw new Exception();
				
			}
		} else if (permissionType.equalsIgnoreCase("Admin User")) {
			click(driver, getAdminUserTabSaveButton(60), "Save Button", action.THROWEXCEPTION);
			if (getAdminUserSuccessSmallCloseButton(30) != null) {
				System.out.println("Found small Message box: " + getAdminUserSuccessSmallCloseButton(5).getText());
				click(driver,
						isDisplayed(driver, getAdminUserSuccessSmallCloseButton(20), "clickable", 20, "close button"),
						"close button", action.THROWEXCEPTION);
			} else {
				if (verifyInternalUserLink.equalsIgnoreCase("Yes")) {
					System.out.println("Checking internal user link");
					click(driver, getInternalUserLinkInAdminUserSuccessMsg(60), "Internal User Link",
							action.SCROLLANDTHROWEXCEPTION);
					String parentid = switchOnWindow(driver);
					switchToFrame(driver, 30, getDealRoomManagerFrame(30));
					sf.assertTrue(getInternalUserLabelText(60).getText().trim().contains("Internal Users"),
							"Internal User label is not displayed after click on internal user Link.");
//					WebDriver driver1=driver;
					driver.close();
					DealRoomManagerPageBusinessLayer drm=new DealRoomManagerPageBusinessLayer(driver);
					driver.switchTo().window(parentid);
					switchToFrame(driver, 30, getDealRoomManagerFrame(30));
					System.out.println("Checking internal user link");
					String s=getAdminUserSuccessMediumTextLabel(60).getText().trim();
					appLog.info("Error Message :" + s);
					if(s.contains(DRMPageErrorMessage.CRMUserAccessRemovalTextMessage2(removeAccessOfUserName))){
						System.out.println("First Message matched");
					}
					if(s.contains(DRMPageErrorMessage.CRMUserAccessRemovalTextMessage3(giveAccessToUserName))){
						System.out.println("second Message matched");
					}
					sf.assertTrue(
							s.contains(DRMPageErrorMessage.CRMUserAccessRemovalTextMessage1),
							"Success popup does not contain the error message1" + "Expected:"
									+ DRMPageErrorMessage.CRMUserAccessRemovalTextMessage1 + "\tActual:" + s);
					if(!s.contains(DRMPageErrorMessage.CRMUserAccessRemovalTextMessage2(removeAccessOfUserName))){
						sf.assertTrue(false,"Success popup does not contain the error message1" + "Expected:"
								+ DRMPageErrorMessage.CRMUserAccessRemovalTextMessage1 + "\tActual:" + s);
					}
					if(!s.contains(DRMPageErrorMessage.CRMUserAccessRemovalTextMessage3(giveAccessToUserName))){
						sf.assertTrue(false,"Success popup does not contain the error message2" + "Expected:"
								+ DRMPageErrorMessage.CRMUserAccessRemovalTextMessage2(removeAccessOfUserName) + "\tActual:" + s);
					}
//					sf.assertTrue(
//							s.contains(DRMPageErrorMessage.CRMUserAccessRemovalTextMessage2),
//							"Success popup does not contain the error message2" + "Expected:"
//									+ DRMPageErrorMessage.CRMUserAccessRemovalTextMessage2(removeAccessOfUserName) + "\tActual:" + s);
//					sf.assertTrue(
//							s.contains(DRMPageErrorMessage.CRMUserAccessRemovalTextMessage3),
//							"Success popup does not contain the error message3" + "Expected:"
//									+ DRMPageErrorMessage.CRMUserAccessRemovalTextMessage3(giveAccessToUserName) + "\tActual:" + s);
				}
				System.out.println("Checking medium success message");
				click(driver, isDisplayed(driver, getAdminUserSuccessMediumCloseButton(120), "clickable", 120,
						"close button"), "Close Button", action.THROWEXCEPTION);

			}
		}
		appLog.info("Close button clicked successfully");
		switchToDefaultContent(driver);
		sf.assertAll();
	}
	
	/**
	 * 
	 * @param userFirstName
	 * @param userLastName
	 * @return true/false
	 */
	public boolean verifyUserCheckboxInInternalUserTab(String userFirstName, String userLastName) {
		switchToFrame(driver, 120, getDealRoomManagerFrame(120));
		System.out.println("Switched inside the frame");
		ThreadSleep(10000);
		click(driver, getEditIconOnDRMTab(120), "Internal User Tab Edit Icon", action.THROWEXCEPTION);
		ThreadSleep(5000);
		if (!isEnabled(driver,
				FindElement(driver,
						"(//span[text()='" + userFirstName + " " + userLastName
								+ " (admin)']/../preceding-sibling::span)[2]/input",
						"CRM User Checkbox", action.THROWEXCEPTION, 60),
				"CRM User checkbox")) {
			appLog.info("CRM user is in non editable mode");
			return true;

		} else {
			appLog.info("CRM user is in editable mode");
			return false;
		}

	}
	
	/**
	 * 
	 * @param userFirstName
	 * @param userLastName
	 * @return true/false
	 */
	public boolean verifyUserCheckboxInManageApprovalTab(String userFirstName, String userLastName) {
		switchToFrame(driver, 120, getDealRoomManagerFrame(120));
		ThreadSleep(5000);
		click(driver, getManageApprovalsTab(60), "Manage Approval Tab", action.THROWEXCEPTION);
		ThreadSleep(2000);
		click(driver, getEditIconOnDRMTab(60), "Edit Icon", action.THROWEXCEPTION);
		if (!isEnabled(driver,
				FindElement(driver, "//span[text()='" + userFirstName + " " + userLastName + " (admin)']/../..//input",
						"CRM User Checkbox", action.THROWEXCEPTION, 120),
				"CRM User Checkbox")) {
			appLog.info("CRM user is in non editable mode");
			return true;
		} else {
			appLog.info("CRM user is in editable mode");
			return false;
		}

	}
	
	/**
	 * 
	 * @param typeOfSetting
	 * @param Action
	 */
	public void verifySettingActivationStatus(String typeOfSetting, String Action) {
		SoftAssert sf = new SoftAssert();
		switchToFrame(driver, 60, getDealRoomManagerFrame(60));
		if (Action.equalsIgnoreCase("Activate")) {
			ThreadSleep(2000);
			click(driver, getActivateButton(120), "Activate Button", action.THROWEXCEPTION);
			if (typeOfSetting.equalsIgnoreCase("Manage Approvals")) {
				click(driver, FindElement(driver, "//a[@id='upMASetting']", "Yes Button", action.THROWEXCEPTION, 120),
						"Yes Button ", action.THROWEXCEPTION);
			}
			ThreadSleep(5000);
			if (getManageApprovalStatus(60).getText().trim().contains("Active")) {
				appLog.info(typeOfSetting + " Status :" + getManageApprovalStatus(60).getText().trim());
				appLog.info(typeOfSetting + "Is Activated");
			} else {
				appLog.info(typeOfSetting + "is not activated");
				sf.assertTrue(false, typeOfSetting + "is not activated");
			}
		} else if (Action.equalsIgnoreCase("Deactivate")) {
			click(driver, getDeactivateButton(60), "Deactivate Button", action.THROWEXCEPTION);
			if (typeOfSetting.equalsIgnoreCase("Manage Approvals")) {
				click(driver,
						FindElement(driver, "//a[@id='upMASetting_de']", "Popup Yes Button",
								action.SCROLLANDTHROWEXCEPTION, 60),
						"Popup Yes Button", action.SCROLLANDTHROWEXCEPTION);
			}
			ThreadSleep(2000);
			if (getManageApprovalDeactivateStatus(60).getText().trim().contains("Inactive")) {
				appLog.info(typeOfSetting + "Status:" + getManageApprovalDeactivateStatus(60).getText().trim());
				appLog.info(typeOfSetting + "Is Deactivated");
			} else {
				appLog.info(typeOfSetting + "is not Deactivated");
				sf.assertTrue(false, typeOfSetting + "is not activated");
			}
		}
		sf.assertAll();
	}
	
	/**
	 * 
	 * @param Setting
	 */
	public void verifySwitchingSettingInManageApprovalAndWatermarking(String Setting) {
		SoftAssert sa = new SoftAssert();
		click(driver, getEditIconOnDRMTab(60), "Edit Icon", action.THROWEXCEPTION);
		if (Setting.equalsIgnoreCase("Manage Approvals")) {
			if (!isEnabled(driver, getByDealRoomRadioButton(120), "By Deal Room Radio Button")) {
				appLog.info("By Deal Room IS not enabled so user not able to switch setting in Manage Approvals tab");

			} else {
				appLog.info("By Deal Room IS enabled so user is able to switch settings in Manage Approvals tab");
				sa.assertTrue(false,
						"By Dealroom radio button is enabled so user is able to switch settings in Manage Approvals tab");
			}
		}
		if (Setting.equalsIgnoreCase("Watermarking")) {
			if (!isEnabled(driver, getWatermarkingEditByDealRadioButton(120), "By Deal Room Radio Button")) {
				appLog.info("By Deal Room IS not enabled so user not able to switch setting in watermarking tab");

			} else {
				appLog.info("By Deal Room IS enabled so user is able to switch settings in watermarking tab");
				sa.assertTrue(false,
						"By Dealroom radio button is enabled so user is able to switch settings in watermarking tab");
			}
		}
		sa.assertAll();
	}
	
	/**
	 * verifyWatermarkingSetting
	 */
	public void verifyWatermarkingSetting() {
		switchToFrame(driver, 120, getDealRoomManagerFrame(60));
		ThreadSleep(2000);
		click(driver, getWatermarkingTab(120), "WaterMarking", action.THROWEXCEPTION);
		ThreadSleep(2000);
		click(driver, getEditIconOnDRMTab(60), "Edit Icon", action.THROWEXCEPTION);
		ThreadSleep(5000);
		if (!isSelected(driver, getWatermarkingMyFirmNameCheckbox(120), "My firm checkbox")) {
			click(driver,
					FindElement(driver, "//table[@id='labelsTable']//input[@id='chkFirmName']",
							"My Firm Name Check Box", action.THROWEXCEPTION, 120),
					"My firm checkbox", action.THROWEXCEPTION);
			selectVisibleTextFromDropDown(driver, getFirmNameWatermarkingLocation(120), "My firm Label", "Top: Left");
		}
		click(driver, getWatermarkingSaveButton(120), "Save Button", action.SCROLLANDTHROWEXCEPTION);

	}
	
	/**
	 * verifyFirmProfile
	 */
	public void verifyFirmProfile() {
		SoftAssert sf = new SoftAssert();
		click(driver, getProfileTab(60), "Profile Tab", action.SCROLLANDTHROWEXCEPTION);
		click(driver, getMyFirmsProfileTab(120), "My Firm Profile", action.SCROLLANDTHROWEXCEPTION);
		ThreadSleep(2000);
		click(driver, getEditIconOnDRMTab(60), "Edit Icon", action.THROWEXCEPTION);
		if (isDisplayed(driver, getMyFirmProfileSaveButton(120), "Visibility", 120, "Save Button",
				action.SCROLLANDTHROWEXCEPTION) != null) {
			appLog.info("Save Button Is displayed so firm profile is editable");
			sf.assertTrue(true);
		} else {
			sf.assertTrue(false);
			appLog.info("Save button is not displayed so firm profile is non editable");
		}
		sf.assertAll();

	}
	
	/**
	 * 
	 * @param DealName1
	 * @param DealName2
	 * @param userFirstName
	 * @param userLastName
	 */
	public void verifyGlobalToByDealSettings(String DealName1, String DealName2, String userFirstName,
			String userLastName) {
		SoftAssert sf = new SoftAssert();
		clickOnDRMTab();
		switchToFrame(driver, 30, getDealRoomManagerFrame(30));
		click(driver, getEditIconOnDRMTab(60), "Edit Icon", action.THROWEXCEPTION);
		ThreadSleep(3000);
		click(driver, getByDealRadioButton(60), "By Deal Radio Button", action.THROWEXCEPTION);
		ThreadSleep(3000);
		sf.assertTrue(
				getErrorMessageOnByDealUserGrid(60).getText().trim()
						.contains(DRMPageErrorMessage.noDataDisplayErrorMessage),
				"Error Message is not verified after Select By deal option.");
		appLog.info("Error Message is verified on user grid successfully.");
		click(driver, getAdminUserTabSaveButton(60), "Save Button", action.SCROLLANDTHROWEXCEPTION);
		ThreadSleep(3000);
		sf.assertTrue(
				getByDealSaveErrorMessagewithoutSelectData(60).getText().trim()
						.contains(DRMPageErrorMessage.ByDealSaveErrorMessageWithoutSelectData),
				"By Deal Error Message is not verified.Expected: "
						+ DRMPageErrorMessage.ByDealSaveErrorMessageWithoutSelectData);
		appLog.info("By Deal Error Message without selecting deal and CRM User is verified Successfully. ");
		click(driver, getByDealSaveErrorMessageWithoutSelectDataCloseButton(60), "Close Button", action.THROWEXCEPTION);
		selectDealByDealSettings(DealName2);
		click(driver, getAdminUserTabSaveButton(60), "Save Button", action.SCROLLANDTHROWEXCEPTION);
		sf.assertTrue(getByDealSaveErrorMsgWithOutSelectUser(60).getText().trim().contains(DRMPageErrorMessage.ByDealErrorMessageWithOutSelectUser), "Error Message is not verified. Expected: "+DRMPageErrorMessage.ByDealErrorMessageWithOutSelectUser);
		click(driver, getByDealSaveCloseBtnWithOutSelectUser(60), "Close Button", action.SCROLLANDTHROWEXCEPTION);
		ThreadSleep(5000);
		click(driver,
				FindElement(driver,
						"//span[@id='grid_AdminUsers2-view']//span[text()='" + userFirstName + " " + userLastName
								+ "']/../..//input[@name='radio']",
						"User Radio Button in By Deal Grid", action.THROWEXCEPTION, 60),
				"User Radio Button in By deal Grid", action.THROWEXCEPTION);
		selectDealByDealSettings(DealName1);
		ThreadSleep(5000);
		sf.assertTrue(
				getByDealChangesLostErrorMessage(60).getText().trim()
						.contains(DRMPageErrorMessage.ByDealUnSavedErrorMessage1),
				"By Deal Unsaved Changes Lost Error Message is not verified. Expectedt: "
						+ DRMPageErrorMessage.ByDealUnSavedErrorMessage1);
		sf.assertTrue(
				getByDealChangesLostErrorMessage(60).getText().trim()
						.contains(DRMPageErrorMessage.ByDealUnSavedErrorMessage2),
				"By Deal Unsaved Changes Lost Error Message is not verified. Expected: "
						+ DRMPageErrorMessage.ByDealUnSavedErrorMessage2);
		appLog.info("By Deal Unsaved Changes Lost Error Message is verified successfully.");
		click(driver, getByDealchangesLostErrorMessageCloseIcon(60), "Close Icon", action.THROWEXCEPTION);
		selectDealByDealSettings(DealName1);
		click(driver, getByDealChnageLostErrorMessageSaveBtn(60), "Yes Button", action.THROWEXCEPTION);
		// ThreadSleep(3000);
		getSelectDealTextBox(10).clear();
		System.out.println("Suuccessfully cleared the text");
		selectDealByDealSettings(DealName1);
		click(driver,
				FindElement(driver,
						"//span[@id='grid_AdminUsers2-view']//span[text()='" + userFirstName + " " + userLastName
								+ "']/../..//input[@name='radio']",
						"User Radio Button in By Deal Grid", action.THROWEXCEPTION, 60),
				"User Radio Button in By deal Grid", action.THROWEXCEPTION);
		ThreadSleep(1000);
		click(driver, getAdminUserTabSaveButton(60), "Save Button", action.SCROLLANDTHROWEXCEPTION);
		sf.assertTrue(
				getByDealSaveErrorMessageText(60).getText().trim().contains(DRMPageErrorMessage.byDealSaveErrorMessage1),
				"By Deal Save Error Message is not verified. Expected: " + DRMPageErrorMessage.byDealSaveErrorMessage1);
		sf.assertTrue(
				getByDealSaveErrorMessageText(60).getText().trim().contains(DRMPageErrorMessage.byDealSaveErrorMessage2),
				"By Deal Save Error Message is not verified. Expected: " + DRMPageErrorMessage.byDealSaveErrorMessage2);
		sf.assertTrue(
				getByDealSaveErrorMessageText(60).getText().trim().contains(DRMPageErrorMessage.byDealSaveErrorMessage3),
				"By Deal Save Error Message is not verified. Expected: " + DRMPageErrorMessage.byDealSaveErrorMessage3);
		sf.assertTrue(
				getByDealSaveErrorMessageText(60).getText().trim().contains(DRMPageErrorMessage.byDealSaveErrorMessage4),
				"By Deal Save Error Message is not verified. Expected: " + DRMPageErrorMessage.byDealSaveErrorMessage4);
		appLog.info("By Deal Save Error Message is verified. successfully.");
		click(driver, getByDealSaveErrorMessageNoBtn(60), "No Button", action.THROWEXCEPTION);
		ThreadSleep(2000);
		click(driver, getAdminUserTabSaveButton(60), "Save Button", action.SCROLLANDTHROWEXCEPTION);
		click(driver, getByDealSaveErrorMessageNoBtn(60), "No Button", action.SCROLLANDTHROWEXCEPTION);
		if (isDisplayed(driver, getAdminUserTabSaveButton(120), "Visibility", 120, "Edit Icon") != null) {
			appLog.info("User Grid is open in Edit mode");
		} else {
			appLog.info("User Grid is not open in Edit mode");
			sf.assertTrue(false, "User Grid is not open in Edit mode");
		}
		ThreadSleep(1000);
		click(driver, getAdminUserTabSaveButton(60), "Save Button", action.SCROLLANDTHROWEXCEPTION);
		click(driver, getConfirmationPopupCloseIconWhileSwitchingSetting(120), "Close Icon",
				action.SCROLLANDTHROWEXCEPTION);
		if (isDisplayed(driver, getAdminUserTabSaveButton(120), "Visibility", 120, "Edit Icon") != null) {
			appLog.info("User Grid is open in Edit mode");
		} else {
			appLog.info("User Grid is not open in Edit mode");
			sf.assertTrue(false, "User Grid is not open in Edit mode");
		}
		ThreadSleep(1000);
		click(driver, getAdminUserTabSaveButton(60), "Save Button", action.SCROLLANDTHROWEXCEPTION);
		click(driver, getByDealSaveErrorMessageYesBtn(60), "Yes Button", action.SCROLLANDTHROWEXCEPTION);
		ThreadSleep(2000);
		click(driver, getByDealCloseBtn(60), "Close Button", action.THROWEXCEPTION);
		String actualresult = FindElement(driver,
				"//span[@id='grid_AdminUsers-view']//span[text()='" + userFirstName + " " + userLastName
						+ "']/../..//input[@name='radio']",
				"User Radio Button in By Deal Grid", action.THROWEXCEPTION, 60).getAttribute("checked");
		sf.assertTrue(actualresult.contains("true"), "Selected User is not verified. Expected:\"Checked = true\"");
		appLog.info("By Deal Selected User is Displayed after Save By Deal Settings.");
		sf.assertAll();

	}
	
	/**
	 * 
	 * @param DealName
	 * @return true/false
	 */
	public Boolean selectDealByDealSettings(String DealName) {
		sendKeys(driver, getSelectDealTextBox(60), DealName, "Select Deal Auto Complete Text Box",
				action.THROWEXCEPTION);
		ThreadSleep(1000);
		return click(driver, FindElement(driver, "//a[text()='" + DealName + "']", "" + DealName + "Auto Complete Text",
				action.BOOLEAN, 10), "Auto Complete Text Message", action.SCROLLANDBOOLEAN);
		// ThreadSleep(3000);
	}
	
	/**
	 * verifyRecordCount
	 */
	public void verifyRecordCount() {
		switchToFrame(driver, 120, getDealRoomManagerFrame(120));
		click(driver, getEditIconOnDRMTab(120), "Edit icon on deal room manager tab", action.THROWEXCEPTION);
		String ActualRecord = String.valueOf(getnoOfUsersInGrid(60).size());
		String noofRecordtext = getUserRecordCount(120).getText().trim();
		String[] ss1 = noofRecordtext.split(": ");
		SoftAssert sa = new SoftAssert();
		if(ActualRecord.contains(ss1[1])){
			appLog.info("Record Count matched : "+ss1[1]);	
		}else{
			appLog.error("Record Count does not matched");
			sa.assertTrue(false, "Record Count does not matched");	
		}
		click(driver, getAdminUserTabSaveButton(120), "Admin user tab save button", action.SCROLLANDTHROWEXCEPTION);
		switchToDefaultContent(driver);
	//	sa.assertAll();
	}
	
	/**
	 * 
	 * @param SettingType
	 * @param DealName
	 * @param userFirstName
	 * @param userLastName
	 * @param pagename
	 * @param switchManageApprovalAndWatermarkingSetting
	 * @param typeOfManageApprovalSetting
	 * @param typeOfWatermarkingSetting
	 * @param globalUseraccess
	 */
	public boolean switchSetting(String SettingType, String DealName, String userFirstName, String userLastName,
			String pagename, String switchManageApprovalAndWatermarkingSetting, String typeOfManageApprovalSetting,
			String typeOfWatermarkingSetting, String globalUseraccess) {
		SoftAssert sa = new SoftAssert();
		Boolean flag = false;
		
		ThreadSleep(2000);
		switchToFrame(driver, 120, getDealRoomManagerFrame(120));
		ThreadSleep(4000);
		if(click(driver, getAdminUserTab(120), "Admin User Tab", action.BOOLEAN)) {
			appLog.info("Clicked on Admin User Tab");
			if (SettingType.equalsIgnoreCase("Global")) {
				ThreadSleep(4000);
				if (!selectDealFromList(DealName, pagename)) {
					appLog.info("Deal Is not selected from select deal auto complete textbox");
					sa.assertTrue(false, "Deal Is not selected from select deal auto complete textbox");
				} else {
					appLog.info("Deal Is selected from select deal auto complete textbox");
				}
				ThreadSleep(5000);
				if(clickUsingJavaScript(driver, getEditIconOnDRMTab(120), "Edit icon on DRM tab")) {
					appLog.info("clicked on edit icon ");
					ThreadSleep(3000);
					if(clickUsingJavaScript(driver,getGlobalRadioButton(120), "Global Radio Button")) {
						appLog.info("clicked on global radio button ");
						ThreadSleep(10000);
						if (globalUseraccess.equalsIgnoreCase("Yes")) {
							WebElement adminusercheckbox = isDisplayed(driver,
									FindElement(driver,
											"//span[@id='grid_AdminUsers2']//span[text()='" + userFirstName + " " + userLastName
											+ "']/../..//input",
											"Users Name", action.THROWEXCEPTION, 60),
									"visibility", 60, "Internal User Check Box", action.THROWEXCEPTION);
							if(clickUsingJavaScript(driver, adminusercheckbox, "Users Name")) {
								appLog.info("clicked on "+userFirstName+" "+userLastName+" check box");
							}else {
								appLog.error("Not able to select user "+userFirstName+" "+userLastName+" from global setting");
							}
						} else {
							appLog.info("There is no need to provide user access when setting is global");
						}
						if(clickUsingJavaScript(driver, getAdminUserTabSaveButton(120), "Save Button")) {
							appLog.info("click on  admin user tab save button");
							ThreadSleep(2000);
							if(clickUsingJavaScript(driver, getByDealSaveErrorMessageYesBtn(120), "Switch Setting Error Message Yes Button")) {
								appLog.info("clicked on Yes button");
								ThreadSleep(2000);
								if (switchManageApprovalAndWatermarkingSetting.equalsIgnoreCase("Yes")) {
									changeManageApprovalSettingOnSuccessPopup(typeOfManageApprovalSetting);
									changeWatermarkingSettingOnSuccessPopup(typeOfWatermarkingSetting);
								}
								if(clickUsingJavaScript(driver, getSuccessLargeSaveButton(120), "Save Button")) {
									appLog.info("clicked on save button ");
									flag= true;
								}else {
									appLog.error("Not able to click on save button so cannot switch settings to by deal to global");
								}
							}else {
								appLog.error("Not able to click on Yes button so cannot switch setting  by deal to global");
							}
						}else {
							appLog.error("Not able to click on admin user tab save button so cannot switch setting by deal to global setting");
						}
					}else {
						appLog.error("Not able to select global radio button so cannot switch setting from by deal to global");
					}
				}else {
					appLog.error("Not able to click on edit icon so cannot switch setting to by deal to global");
				}
			} else if (SettingType.equalsIgnoreCase("By Deal")) {
				ThreadSleep(2000);
				if(clickUsingJavaScript(driver, getEditIconOnDRMTab(120), "Edit icon on DRM tab")) {
					if(clickUsingJavaScript(driver, getByDealRadioButton(120), "By deal radio button")) {
						appLog.info("clicked on by deal radio button");
						if(sendKeys(driver, getDealSelectionListEditMode(120), DealName, "Deal Select TextBox",action.SCROLLANDBOOLEAN)) {
							appLog.info("enter value in text box : "+DealName);
							ThreadSleep(2000);
							WebElement ele=FindElement(driver, "//ul[contains(@class,'ui-autocomplete')]//a[text()='"+DealName+"']", "Deal List", action.SCROLLANDBOOLEAN, 20);
							if(clickUsingJavaScript(driver,ele,DealName+" deal Name text")) {
								appLog.info("Clicked on deal Name "+DealName);
								ThreadSleep(5000);
								WebElement adminusercheckbox = isDisplayed(driver,
										FindElement(driver,
												"//span[@id='grid_AdminUsers2']//span[text()='" + userFirstName + " " + userLastName
												+ "']/../..//input",
												"Users Name", action.THROWEXCEPTION, 60),"visibility", 60, "Internal User Check Box", action.THROWEXCEPTION);
								if(clickUsingJavaScript(driver, adminusercheckbox, "user check box ")) {
									appLog.info("clicked on user name check box "+userFirstName+" "+userLastName);
									if(clickUsingJavaScript(driver, getAdminUserTabSaveButton(120), "Save Button")) {
										appLog.info("clicked on admin user tab save button");
										ThreadSleep(2000);
										if(clickUsingJavaScript(driver, getByDealSaveErrorMessageYesBtn(120), "Switch Setting Error Message Yes Button")) {
											appLog.info("clicked on Yes button");
											
											if (switchManageApprovalAndWatermarkingSetting.equalsIgnoreCase("Yes")) {
												changeManageApprovalSettingOnSuccessPopup(typeOfManageApprovalSetting);
												changeWatermarkingSettingOnSuccessPopup(typeOfWatermarkingSetting);
											}
											if(clickUsingJavaScript(driver, getSuccessLargeSaveButton(120), "Save Button")) {
												appLog.info("clicked on save button and setting is switched global to by deal ");
												flag= true;
											}else {
												appLog.error("Not able to click on save button so cannot switch setting global to by deal");
											}
										}else {
											appLog.error("Not able to click on yes button so cannot switch detting from global to by deal");
										}
										
									}else {
										appLog.info("Not able to click on admin user tab save button so cannot switch settings from global to by deal");
									}
								}else {
									appLog.error("Not able to select user "+userFirstName+" "+userLastName+" so cannot switch setting from global to by deal");
								}
							}else {
								appLog.error("Not able to select deal name from drop down "+DealName+" so cannot switch setttings from global to by deal");
							}
						}else {
							appLog.error("Not able to enter deal name in text box "+DealName+" so cannot switch settings to global to by deal");
						}
					}else {
						appLog.error("Not able to select by deal radio button so cannot switch setting to global to by deal");
					}
				}else {
					appLog.error("Not able to click on edit  icon so cannot switch swtting to global to by deal");
				}
			}
		}else {
			appLog.error("Not able to click on admin user tab so cannot switch settings");
			sa.assertTrue(false, "Not able to click on admin user tab so cannot switch settings");
		}
		switchToDefaultContent(driver);
		return flag;

	}
	
	/**
	 * 
	 * @param typeOFSetting
	 * @param pageName
	 * @return true/false
	 */
	public boolean verifySettingSelected(String typeOFSetting, String pageName) {
		ThreadSleep(2000);
		click(driver, getEditIconOnDRMTab(120), "Edit Icon On DRM Tab", action.SCROLLANDTHROWEXCEPTION);
		if (pageName.equalsIgnoreCase("Admin Users")) {
			if (typeOFSetting.equalsIgnoreCase("Global")) {
				if (!isSelected(driver, getGlobalRadioButton(120), "Admin User Global radio button")) {
					appLog.info("Global setting is not selected");
					// sa.assertTrue(false, "Global setting is not selected on
					// Admin users Page");
					return false;
				} else {
					appLog.info("Global Setting is selected on Admin users Page");
					click(driver, getAdminUserTabSaveButton(120), "Save Button", action.SCROLLANDTHROWEXCEPTION);
					return true;
				}
			} else if (typeOFSetting.equalsIgnoreCase("By Deal")) {
				if (!isSelected(driver, getByDealRadioButton(120), "Admin User By Deal Radio Button")) {
					appLog.info("By Deal Setting is not selected on Admin users Page");
					// sa.assertTrue(false, "By deal setting is not selected on
					// Admin users Page");
					return false;
				} else {
					appLog.info("By deal setting is selected on Admin Users page");
					click(driver, getAdminUserTabSaveButton(120), "Save Button", action.SCROLLANDTHROWEXCEPTION);
					return true;
				}
			}
		} else if (pageName.equalsIgnoreCase("Manage Approvals")) {
			if (typeOFSetting.equalsIgnoreCase("Global")) {
				if (!isSelected(driver, getManageApprovalEditGlobalRadioButton(120), "Global radio button")) {
					appLog.info("Global radio button is not selected");
					// sa.assertTrue(false, "Global radio button is not selected
					// on manage approval page");
					return false;
				} else {
					appLog.info("Global radio button is selected on manage approval page");
					click(driver, getManageApprovalSaveButton(120), "Save Button", action.SCROLLANDTHROWEXCEPTION);
					return true;
				}

			} else if (typeOFSetting.equalsIgnoreCase("By Deal")) {
				if (!isSelected(driver, getManageApprovalEditByDealRadioButton(120), "By Deal Radio Button")) {
					appLog.info("By Deal radio button is not selected");
					// sa.assertTrue(false, "By deal radio button is not
					// selected on manage approval page ");
					return false;
				} else {
					appLog.info("By Deal Radio button is selected on manage approval page");
					click(driver, getManageApprovalSaveButton(120), "Save Button", action.SCROLLANDTHROWEXCEPTION);
					return true;
				}

			}
		} else if (pageName.equalsIgnoreCase("Watermarking")) {
			if (typeOFSetting.equalsIgnoreCase("Global")) {
				if (!isSelected(driver, getWatermarkingEditGlobalRadioButton(120), "Global radio button")) {
					appLog.info("Global radio button is not selected on watermarking page");
					// sa.assertTrue(false, "Global radio button is not selected
					// on watermarking page");
					return false;
				} else {
					appLog.info("Global radio button is selected on watermarking page");
					click(driver, getWatermarkingSaveButton(120), "Save Button", action.SCROLLANDTHROWEXCEPTION);
					return true;
				}

			} else if (typeOFSetting.equalsIgnoreCase("By Deal")) {
				if (!isSelected(driver, getWatermarkingEditByDealRadioButton(120), "By Deal Radio Button")) {
					appLog.info("By Deal radio button is not selected on watermarking page");
					// sa.assertTrue(false, "By deal radio button is not
					// selected on watermarking page");
					return false;
				} else {
					appLog.info("By Deal Radio button is selected on watermarking page");
					click(driver, getWatermarkingSaveButton(120), "Save Button", action.SCROLLANDTHROWEXCEPTION);
					return true;
				}

			}
		}
		appLog.info("Page name is not correct.Kindly Check the page name");
		return false;

	}
	
	/**
	 * 
	 * @param pageName
	 */
	public void verifyInsufficientPermissions(String pageName) {
		SoftAssert sf = new SoftAssert();
		if (pageName.equalsIgnoreCase("Manage Approvals")) {
			click(driver, getManageApprovalsTab(60), "Manage Approval Tab", action.THROWEXCEPTION);
		}
		if (pageName.equalsIgnoreCase("Watermarking")) {
			click(driver, getWatermarkingTab(60), "Water Marking Tab", action.THROWEXCEPTION);
		}
		if (pageName.equalsIgnoreCase("My Firms Profile")) {
			click(driver, getProfileTab(60), "Profile Tab", action.THROWEXCEPTION);
			ThreadSleep(2000);
			click(driver, getMyFirmsProfileTab(60), "My Firm's Profile Tab", action.THROWEXCEPTION);
		}
		click(driver, getEditIconOnDRMTab(60), "Edit Icon", action.THROWEXCEPTION);
		ThreadSleep(2000);
		if (pageName.equalsIgnoreCase("Manage Approvals") || pageName.equalsIgnoreCase("Watermarking")) {
		ThreadSleep(2000);	
			sf.assertTrue(
					getInsufficientPermissionErrorMessage(60).getText().trim()
							.contains(DRMPageErrorMessage.InsufficientPermissionsErrorMsg),
					"" + pageName + " Insufficient Permission Error Message is not verified. Expected: "
							+ DRMPageErrorMessage.InsufficientPermissionsErrorMsg);
		}
		if (pageName.equalsIgnoreCase("My Firms Profile")) {
			sf.assertTrue(
					getInsufficientPermissionErrorMessage(60).getText().trim()
							.contains(DRMPageErrorMessage.MyFirmsProfileInsufficientPermissionsErrorMsg),
					"My Firm Profile Insufficient Permission Error Message is not verified. Expected: "
							+ DRMPageErrorMessage.MyFirmsProfileInsufficientPermissionsErrorMsg);
		}
		click(driver, getInsufficientPermissionErrorMessageCloseBtn(60), "Close Button", action.THROWEXCEPTION);
		sf.assertAll();

	}
	
	/**
	 * 
	 * @param excelPath
	 * @param folderTemplateName
	 * @param description
	 * @param pageName
	 * @return true/false
	 */
	public boolean createFolderTemplate(String excelPath, String folderTemplateName, String description, String pageName) {
		boolean flag;
		ThreadSleep(5000);
		if (click(driver, getAddFolderTemplateButton(60), "Add Folder Template Button", action.BOOLEAN)) {
			if (!sendKeys(driver, getFolderTemplateNameTextBox(60), folderTemplateName, "Folder template text box.",
					action.BOOLEAN)) {
				exit("Folder Template name text box is not visible Skipping this test case.");
				return false;
			}
			if (sendKeys(driver, getFolderTemplateDescription(60), description, "Description Text Box",
					action.BOOLEAN)) {
				appLog.info("Not able to enter description.");
				flag = false;
			}
			Map<String, String> s = folderStructureInExcel(excelPath);
			Set<String> paths = s.keySet();
			Iterator<String> i = paths.iterator();
			i = paths.iterator();
			while (i.hasNext()) {
				String string = i.next();
				if (string.isEmpty())
					continue;
				createFolderStructure(string, s.get(string), description, pageName);
			}
			if (click(driver, getFolderTemplatePageSaveButton(60), "Folder Template Save Button",
					action.SCROLLANDBOOLEAN)) {
				if (click(driver, getConfirmSaveYesButton(60), "Confirm Save Yes Button", action.BOOLEAN)) {
					if (click(driver, getGoBackLink(60), "Go back link", action.SCROLLANDBOOLEAN)) {
						if (FindElement(driver, "//a[@title='" + folderTemplateName + "']", "Template Name",
								action.BOOLEAN, 60) != null) {
							appLog.info(folderTemplateName + " :Template has been created successfully.");
							return true;
						}
					}
				}
				return false;
			} else {
				return false;
			}

		} else {
			appLog.info(
					"Add Folder Template button is not visible. Cannot create folder template. Skipping this testcase");
			exit("Add Folder Button is not visible so cannot continue with this testcase.");
			return false;
		}
	}
	
	/**
	 * 
	 * @param id
	 * @return true/false
	 */
	public boolean clickOnAddFolderButton(String id) {
		((JavascriptExecutor) driver).executeScript(
				"document.getElementById('" + id + "').setAttribute('style', 'display: inline-block;');");
		scrollDownThroughWebelement(driver, FindElement(driver, "//div[@id='" + id + "']/a[@title='Add a Folder']",
				"Add folder Button", action.BOOLEAN, 20), "Add Folder Button");
		return click(driver, FindElement(driver, "//div[@id='" + id + "']/a[@title='Add a Folder']",
				"Add folder Button", action.BOOLEAN, 20), "Add a folder", action.BOOLEAN);
	}
	
	/**
	 * 
	 * @param id
	 * @return true/false
	 */
	public boolean clickOnRenameFolderButton(String id) {
		((JavascriptExecutor) driver).executeScript(
				"document.getElementById('" + id + "').setAttribute('style', 'display: inline-block;');");
		scrollDownThroughWebelement(driver, FindElement(driver, "//div[@id='" + id + "']/a[@title='Rename Folder']",
				"Rename folder Button", action.BOOLEAN, 20), "Rename Folder Button");
		return click(driver, FindElement(driver, "//div[@id='" + id + "']/a[@title='Rename Folder']",
				"Rename folder Button", action.BOOLEAN, 20), "Rename a folder", action.BOOLEAN);
	}
	
	/**
	 * 
	 * @param folderName
	 * @param folderType
	 * @param description
	 * @param pageName
	 * @throws Exception 
	 */
	public void createParentFolder(String folderName, folderTypeforAll folderType, String description, String pageName) throws Exception {
		DealPageBusinessLayer deal = new DealPageBusinessLayer(driver);
		clickOnAddFolderButton("add10000");
		ThreadSleep(3000);
		scrollDownThroughWebelement(driver,FindElement(driver, "//div[@id='BDRstep_2of3']//a[@title='Close']", "close button scroll", action.SCROLLANDBOOLEAN, 10), "");
		if (folderType.toString().equalsIgnoreCase("Global")) {
			if (pageName.equalsIgnoreCase("Deal Room")) {
				if(clickUsingJavaScript(driver, deal.getGlobalFolderRadioButton(60), "Global Folder Radio Button")) {
					appLog.info("clicked on global radio button");
				}else {
					appLog.error("Not able to click on global radio button");
					throw new Exception();
				}
//				click(driver, deal.getGlobalFolderRadioButton(60), "Global Folder Radio Button", action.SCROLLANDTHROWEXCEPTION);
			} else if (pageName.equalsIgnoreCase("DRM")) {
				click(driver, getGlobalFolderRadioButton(60, PageName.FolderTemplate), "Global Folder Radio Button", action.SCROLLANDTHROWEXCEPTION);
			}

		} else if (folderType.toString().equalsIgnoreCase("Shared")) {
			if (pageName.equalsIgnoreCase("Deal Room")) {
				
				if(clickUsingJavaScript(driver, deal.getSharedFolderradioButton(60), "Shared Folder Radio Button")) {
					appLog.info("clicked on shared radio button");
				}else {
					appLog.error("Not able to click on shared radio button");
					throw new Exception();
				}
				
//				click(driver, deal.getSharedFolderradioButton(60), "Shared Folder Radio Button", action.SCROLLANDTHROWEXCEPTION);
			} else if (pageName.equalsIgnoreCase("DRM")) {
				click(driver, getSharedFolderRadioButton(60, PageName.FolderTemplate), "Shared Folder Radio Button", action.THROWEXCEPTION);
			}

		} else if (folderType.toString().equalsIgnoreCase("Internal")) {
			if (pageName.equalsIgnoreCase("Deal Room")) {
				
				if(clickUsingJavaScript(driver, deal.getInternalFolderRadioButton(60), "Internal Folder Radio Button")) {
					appLog.info("clicked on internal radio button");
				}else {
					appLog.error("Not able to click on internal radio button");
					throw new Exception();
				}
//				click(driver, deal.getInternalFolderRadioButton(60), "Internal Folder Radio Button",
//						action.SCROLLANDTHROWEXCEPTION);
			} else if (pageName.equalsIgnoreCase("DRM")) {
				click(driver, getInternalFolderRadioButton(60, PageName.FolderTemplate), "Internal Folder Radio Button", action.THROWEXCEPTION);
			}

		} else if (folderType.toString().equalsIgnoreCase("STD")) {
			if (pageName.equalsIgnoreCase("Deal Room")) {
				
				if(clickUsingJavaScript(driver, deal.getStandardFolderRadioButton(60), "Standard Folder Radio Button")) {
					appLog.info("clicked on standard radio button");
				}else {
					appLog.error("Not able to click on standard radio button");
					throw new Exception();
				}
//				click(driver, deal.getStandardFolderRadioButton(60), "Standard Folder Radio Button",
//						action.SCROLLANDTHROWEXCEPTION);
			} else if (pageName.equalsIgnoreCase("DRM")) {
				click(driver, getStandardFolderRadioButton(60, PageName.FolderTemplate), "Standard Folder Radio Button", action.THROWEXCEPTION);
			}else if(pageName.equalsIgnoreCase("Manage Folder")){
				click(driver, deal.getManageFolderStandardRadioButton(120), "Manage folder standard Radio button", action.SCROLLANDTHROWEXCEPTION);
			}

		}
		if (pageName.equalsIgnoreCase("Deal Room")) {
			sendKeys(driver, deal.getFolderNametextBox(60), folderName, "Parent Folder Name Text Box",
					action.SCROLLANDTHROWEXCEPTION);
		} else if (pageName.equalsIgnoreCase("DRM")) {
			sendKeys(driver, getParentFolderNameTextBox(60), folderName, "Parent Folder Name Text box",
					action.THROWEXCEPTION);
		}else if(pageName.equalsIgnoreCase("Manage Folder")){
			sendKeys(driver, deal.getManageFolderStandardFolderNameText(120), folderName, "Parent Folder Name Text box",
					action.SCROLLANDTHROWEXCEPTION);
		}
		if (pageName.equalsIgnoreCase("Deal Room")) {
			if (!sendKeys(driver, deal.getAddFolderDescription(60), description, "Parent Folder Description Text Box",
					action.SCROLLANDBOOLEAN)) {
				appLog.info("creating folder wihtout description.");
			}
		} else if (pageName.equalsIgnoreCase("DRM")) {
			if (!sendKeys(driver, getParentFolderDescriptionTextBox(60), description,
					"Parent Folder Description Text Box", action.SCROLLANDBOOLEAN)) {
				appLog.info("creating folder wihtout description.");
			}
		}else if (pageName.equalsIgnoreCase("Manage Folder")) {
			if (!sendKeys(driver, deal.getManageFolderStandardFolderDescription(60), description,
					"Parent Folder Description Text Box", action.SCROLLANDBOOLEAN)) {
				appLog.info("creating folder wihtout description.");
			}
		}
		if (pageName.equalsIgnoreCase("Deal Room")) {
			click(driver, deal.getfolderSaveButton(60), "Save Button", action.SCROLLANDTHROWEXCEPTION);
		} else if (pageName.equalsIgnoreCase("DRM")) {
			click(driver, getParentFolderSaveButton(60), "Save Button", action.SCROLLANDTHROWEXCEPTION);
		}else if(pageName.equalsIgnoreCase("Manage Folder")){
			click(driver, deal.getManageFolderSaveButton(120), "Save Button", action.SCROLLANDTHROWEXCEPTION);
		}
		}
	
	/**
	 * 
	 * @param folderName
	 * @param folderDescription
	 * @param pageName
	 * @throws Exception 
	 */
	public void createChildFolder(String folderName, String folderDescription, String pageName) throws Exception {
		DealPageBusinessLayer deal = new DealPageBusinessLayer(driver);
		if (pageName.equalsIgnoreCase("Deal Room")) {
			sendKeys(driver, deal.getchildFolderNameTextBox(60), folderName, "Child Folder Text Box.",
					action.SCROLLANDTHROWEXCEPTION);
			sendKeys(driver, deal.getChildFolderDescriptionTextBox(60), folderDescription,
					"Child Folder Description Text Box", action.SCROLLANDTHROWEXCEPTION);
			if(clickUsingJavaScript(driver, deal.getChildLevelFolderSaveButton(60), "Child Folder Save Button")) {
				appLog.info(folderName + "Child Folder is Created Successfully.");
				
			}else {
				appLog.error("Not able to click on child folder save button so cannot create folder "+folderName);
				throw new Exception();
			}
		} else if (pageName.equalsIgnoreCase("DRM")) {
			sendKeys(driver, getChildFolderName(60), folderName, "Child Folder Text Box.", action.SCROLLANDTHROWEXCEPTION);
			sendKeys(driver, getChildFolderDescriptionTextBox(60), folderDescription,
					"Child Folder Description Text Box", action.SCROLLANDTHROWEXCEPTION);
			click(driver, getChildFolderSaveButton(60), "Child Folder Save Button", action.SCROLLANDTHROWEXCEPTION);
			appLog.info(folderName + "Child Folder is Created Successfully.");
		}

	}
	
	/**
	 * 
	 * @param folderPath
	 * @param pFolderType
	 * @param description
	 * @param pageName
	 */
	public void createFolderStructure(String folderPath, String pFolderType, String description, String pageName) {
		String folderStruct[] = folderPath.split("/");
		String folderStructTemp=null;
		if(folderStruct[0].contains("(Global)")||folderStruct[0].contains("(Internal)")||folderStruct[0].contains("(Shared)")){
			folderStructTemp=(folderStruct[0].split("\\("))[0];
			folderStruct[0]=folderStructTemp;
		}
		String xpath1 = "//a[contains(text(),'All Folders')]/following-sibling::div";
		if (pageName.equalsIgnoreCase("Deal Room")) {
			xpath1 = "//a[contains(text(),'All Folders')]/following-sibling::div[@id='add10000']";
		}
		String xpath2 = "//a[contains(text(),'All Folders')]/../../../ul/li";
		String xpath3 = "//a[contains(text(),'All Folders')]/../../../ul/li/div//a[contains(text(),'" + folderStruct[0]
				+ "')]";
		String xpath4 = "/../../following-sibling::ul//a[contains(text(),'a')]";
		String xpath5 = "/../div";
		String xpath6 = "";
		for (int i = 0; i < folderStruct.length; i++) {
			if (checkElementVisibility(driver, FindElement(driver, xpath3, "folder", action.BOOLEAN, 0), "", 2)) {
				if (i != folderStruct.length - 1) {
					xpath3 = xpath3 + "/../../following-sibling::ul/li/div//a[contains(text(),'" + folderStruct[i + 1]
							+ "')]";
					System.out.println("\n\n\nSkipping the iteration: " + i + "\n\n\n\n");
					continue;
				}
			}
			if (i == 0) {
				String id = FindElement(driver, xpath1, "", action.BOOLEAN, 60).getAttribute("id");
				clickOnAddFolderButton(id);
				if (pFolderType.equalsIgnoreCase("Global"))
					try {
						createParentFolder(folderStruct[i], folderTypeforAll.GLOBAL, description, pageName);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				else if (pFolderType.equalsIgnoreCase("Shared"))
					try {
						createParentFolder(folderStruct[i], folderTypeforAll.SHARED, description, pageName);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				else if (pFolderType.equalsIgnoreCase("Standard"))
					try {
						createParentFolder(folderStruct[i], folderTypeforAll.STD, description, pageName);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				else if (pFolderType.equalsIgnoreCase("Internal"))
					try {
						createParentFolder(folderStruct[i], folderTypeforAll.INTERNAL, description, pageName);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				else
					try {
						createParentFolder(folderStruct[i], folderTypeforAll.STD, description, pageName);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			} else {
				int len = xpath3.length();
				String spitby = "/../../following-sibling::ul/li/div//a[contains(text(),'" + folderStruct[i] + "')]";
				int len1 = spitby.length();
				int lenReq = len - len1;
				String xp = xpath3.substring(0, lenReq);
				xp = xp + xpath5;
				System.out.println("Value of i: " + i + "XPath after split: " + xp);
				String id = FindElement(driver, xp, "", action.BOOLEAN, 10).getAttribute("id");
				clickOnAddFolderButton(id);
				try {
					createChildFolder(folderStruct[i], description, pageName);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (i != folderStruct.length - 1) {
				xpath3 = xpath3 + "/../../following-sibling::ul/li/div//a[contains(text(),'" + folderStruct[i + 1]
						+ "')]";
				System.out.println("xpath before split " + i + " creation: " + xpath3);
			}
		}
	}
	
	/**
	 * 
	 * @param userFirstName
	 * @param userLastName
	 * @param display
	 * @param pageName
	 */
	public void verifyDeactivateUserOnDRMPage(String userFirstName, String userLastName, String display,
			String pageName) {
		SoftAssert sa = new SoftAssert();
		if (pageName.equalsIgnoreCase("Admin User")) {
			if (display.equalsIgnoreCase("Yes")) {
				if (isDisplayed(driver,
						FindElement(driver, "//span[contains(@title,'" + userFirstName + " " + userLastName + "')]",
								"Deactivated CRM User", action.SCROLLANDTHROWEXCEPTION, 120),
						"Visibility", 120, "Deactivated CRM User") != null) {
					String actualResult = isDisplayed(driver,
							FindElement(driver, "//span[contains(@title,'" + userFirstName + " " + userLastName + "')]",
									"Deactivated CRM User", action.SCROLLANDTHROWEXCEPTION, 120),
							"Visibility", 120, "Deactivated CRM User").getText().trim();
					sa.assertTrue(actualResult.contains("(inactive)"),
							"(inactive) user is not displayed Admin User Page.");
					appLog.info("User is displaying as an inactive user on Admin User Page");
				} else {
					appLog.info("User is not displaying as an inactive user Admin User Page.");
					sa.assertTrue(false, "(inactive) user is not displayed on Admin User Page.");
				}
			}
		} else if (pageName.equalsIgnoreCase("Internal User")) {
			if (display.equalsIgnoreCase("Yes")) {
				ThreadSleep(2000);
				if (isDisplayed(driver,
						FindElement(driver, "//span[contains(text(),'" + userFirstName + " " + userLastName + "')]",
								"Deactivated CRM User", action.SCROLLANDTHROWEXCEPTION, 120),
						"Visibility", 120, "Deactivated CRM User") != null) {
					String actualResult = isDisplayed(driver,
							FindElement(driver, "//span[contains(text(),'" + userFirstName + " " + userLastName + "')]",
									"Deactivated CRM User", action.SCROLLANDTHROWEXCEPTION, 120),
							"Visibility", 120, "Deactivated CRM User").getText().trim();
					System.out.println(actualResult);
					sa.assertTrue(actualResult.contains("(inactive)"), "(inactive) user is not displayed.");
					appLog.info("User is displaying as an inactive user on Internal User Page");

				} else {
					appLog.info("User is not displaying as an inactive user on Internal User page.");
					sa.assertTrue(false, "User is not displaying as an inactive user on Internal User page.");

				}
			}
		}
		if (pageName.equalsIgnoreCase("Admin User")) {
			if (display.equalsIgnoreCase("No")) {
				if (isDisplayed(driver,
						FindElement(driver, "//span[contains(@title,'" + userFirstName + " " + userLastName + "')]",
								"Deactivated CRM User", action.SCROLLANDTHROWEXCEPTION, 120),
						"Visibility", 120, "Deactivated CRM User") == null) {
					appLog.info("Inactive user is not displaying on admin page.");

				} else {
					appLog.info("Inactive user  is  displaying on admin page");
					sa.assertTrue(false, "Inactive user  is  displaying on admin page.");

				}
			}
		}
		sa.assertAll();
	}
	
	/**
	 * 
	 * @param excelPath
	 * @return true/false
	 */
	public Map<String, String> folderStructureInExcel(String excelPath) {
		int i = 2;
		Map<String, String> struct = new LinkedHashMap<String, String>();
		while (true) {
			String value = ExcelUtils.readData(excelPath, "FolderTemp", i, 0);
			if (value != null) {
				int totalValues = ExcelUtils.getLastColumn(excelPath, "FolderTemp", i);
				for (int j = 1; j < totalValues; j++) {
					String path = ExcelUtils.readData(excelPath, "FolderTemp", i, j);
					String[] paths = path.split(",");
					for (int k = 0; k < paths.length; k++) {
						struct.put(paths[k], ExcelUtils.readData(excelPath, "FolderTemp", i, 0));
					}
				}
			} else {
				break;
			}
			i++;
		}
		return struct;
	}
	
	/**
	 * 
	 * @param typeOfManageApprovalSetting
	 */
	public void changeManageApprovalSettingOnSuccessPopup(String typeOfManageApprovalSetting) {
		if (typeOfManageApprovalSetting.equalsIgnoreCase("Global")) {
			click(driver, getSuccessPopupManageApprovalGlobalRadioButton(120),
					"Success popup manage approvals global radio button", action.SCROLLANDTHROWEXCEPTION);
			appLog.info("Success popup manage approvals global radio button clicked successfully");
		} else if (typeOfManageApprovalSetting.equalsIgnoreCase("By Deal")) {
			click(driver, getSuccessPopupManageApprovalByDealRadioButton(120),
					"Success popup manage approvals by deal radio button", action.SCROLLANDTHROWEXCEPTION);
			appLog.info("Success popup manage approvals by deal radio button clicked successfully");
		} else {
			appLog.info("There will be no change in the manage Approval setting.");
		}
	}
	
	/**
	 * 
	 * @param typeOfWatermarkingSetting
	 */
	public void changeWatermarkingSettingOnSuccessPopup(String typeOfWatermarkingSetting) {
		if (typeOfWatermarkingSetting.equalsIgnoreCase("Global")) {
			click(driver, getSuccessPopupWatermarkingGlobalRadioButton(120),
					"Success popup watermarking global radio button", action.SCROLLANDTHROWEXCEPTION);
			appLog.info("Success popup watermarking global radio button clicked successfully");
		} else if (typeOfWatermarkingSetting.equalsIgnoreCase("By Deal")) {
			click(driver, getSuccessPopupWatermarkingByDealRadioButton(120),
					"Success popup watermarking by deal radio button", action.SCROLLANDTHROWEXCEPTION);
			appLog.info("Success popup watermarking by deal radio button clicked successfully");
		} else {
			appLog.info("There will be no change in the watermarking setting.");
		}
	}
	
	/**
	 * 
	 * @param pageName
	 * @param switchSettingTo
	 */
	public void switchSettingGloablToByDeal(String pageName, String switchSettingTo) {
		switchToFrame(driver, 60, getDealRoomManagerFrame(60));
		if (pageName.equalsIgnoreCase("Manage Approvals")) {
			click(driver, getManageApprovalsTab(60), "Manage Approval Tab", action.THROWEXCEPTION);

		} else if (pageName.equalsIgnoreCase("WaterMarking")) {
			click(driver, getWatermarkingTab(60), "WaterMarking Tab", action.THROWEXCEPTION);
			ThreadSleep(3000);
		}
		ThreadSleep(3000);
		click(driver, getEditIconOnDRMTab(60), "Edit Icon", action.SCROLLANDTHROWEXCEPTION);
		if (switchSettingTo.equalsIgnoreCase("Global") && pageName.equalsIgnoreCase("Manage Approvals")) {
			if (!isSelected(driver, getByDealRoomRadioButton(60), "By Deal Radio Button")) {
				click(driver, getByDealRoomRadioButton(60), "Manage ApprovalsBy Deal Radio Button",
						action.THROWEXCEPTION);
				appLog.info("Clicked on Manage Approvals By Deal Radio Button.");
			} else {
				appLog.info("By Deal Radio Button is already selected.");
			}
			click(driver, getManageApprovalSaveButton(60), "Save Button", action.SCROLLANDTHROWEXCEPTION);
		} else if (switchSettingTo.equalsIgnoreCase("Global") && pageName.equalsIgnoreCase("WaterMarking")) {
			if (!isSelected(driver, getWatermarkingEditByDealRadioButton(60), "WaterMarking By Deal Radio Button")) {
				click(driver, getWatermarkingEditByDealRadioButton(60), "WaterMarking By deal Radio Button",
						action.THROWEXCEPTION);
				appLog.info("Clicked on WaterMarking By Deal Radio Button.");
				ThreadSleep(3000);
			} else {
				appLog.info("WaterMarking By Deal Radio Button is already selected.");
			}
			click(driver, getWatermarkingSaveButton(60), "Save Button", action.SCROLLANDTHROWEXCEPTION);
		}
		switchToDefaultContent(driver);
	}

	
	/**
	 * 
	 * @param folderTemplateName
	 * @return true/false
	 */
	public boolean deleteFolderTemplate(String folderTemplateName) {
		selectVisibleTextFromDropDown(driver, getFolderTemplateDisplayDropDownList(60), "Folder Display Drop Down",
				"All Templates");
		List<WebElement> createdTemplateName = getFolderTemplateGrid(60);
		if (createdTemplateName != null) {
			for (int i = 0; i < createdTemplateName.size(); i++) {
				if (createdTemplateName.get(i).getText().equalsIgnoreCase(folderTemplateName)) {
					createdTemplateName.get(i).click();
					click(driver, getFolderTemplateDeleteBtn(60), "Folder Template Delete Button",
							action.SCROLLANDBOOLEAN);
					click(driver, getFolderTemplateDeleteYesBtn(60), "Folder Template Delete Yes Button",
							action.SCROLLANDBOOLEAN);
					appLog.info("Folder Template is Deleted Successfully.");
					return true;
				}
			}
			return false;
		}
		return false;
	}
	
	/**
	 * 
	 * @param ContactFirstName
	 * @param ContactLastNAme
	 * @param DealName
	 * @return true/false
	 */
	public boolean removeContactAccessFromContactAccessTabInDRM(String ContactFirstName, String ContactLastNAme,
			String DealName) {
		boolean flag = false;
		clickOnDRMTab();
		switchToFrame(driver, 120, getDealRoomManagerFrame(120));
		if (!click(driver, getContactAccessTab(120), "Contact Access Tab", action.SCROLLANDBOOLEAN)) {
			appLog.info("Not Able to Click On Contact Access Tab");
			return flag;
		}else{
			appLog.info("Click On Contact Access Tab");
		}
		ThreadSleep(2000);
		sendKeys(driver, getContactAccessSearchBox(60), ContactFirstName + " " + ContactLastNAme,
				"Contact Access Search Box", action.SCROLLANDBOOLEAN);
		if (!click(driver, getSearchIcon().get(0), "Search Icon On Contact Access Popup", action.SCROLLANDBOOLEAN)) {
			appLog.info("Not Able to click on search Icon On Contact Access Popup");
			return flag;
		}else{
			appLog.info("Click On search Icon On Contact Access Popup");
		}
		WebElement removeLink = FindElement(driver,
				"//a[text()='" + ContactFirstName + " " + ContactLastNAme + "']/../..//span//a[text()='Remove']",
				"Remove link", action.SCROLLANDBOOLEAN, 60);
		if (removeLink != null) {
			if (click(driver, removeLink, "Remove Link", action.SCROLLANDBOOLEAN)) {
				WebElement removeLinkDeal = FindElement(driver, "//label[text()='" + DealName + "']/../..//a",
						"Remove link", action.SCROLLANDBOOLEAN, 60);
				if (removeLinkDeal != null) {
					if (click(driver, removeLinkDeal, "Remove link", action.SCROLLANDBOOLEAN)) {
						appLog.info("Clicked on Remove Link successfully");
						flag = true;
						return flag;
					} else {
						appLog.info("Remove link is null for Deal : " + DealName);
					}

				} else {
					appLog.info("Remove link is not displaying or it is not clickable for Deal : " + DealName);

				}
			} else {
				appLog.info("Remove link is not displaying or it is not clickable for Contact : " + ContactFirstName
						+ " " + ContactLastNAme);

			}
		} else {
			appLog.info("Remove link is null for Contact : " + ContactFirstName + " " + ContactLastNAme);

		}
		return flag;
	}

	
	
	/**
	 * 
	 * @param contactFullName
	 * @return true/false
	 */
	public boolean verifyContactInContactAccessTabGridInDRMPage(String contactFullName) {
		if (sendKeys(driver, getContactAccessSearchBox(60), contactFullName, "Contact Name", action.SCROLLANDBOOLEAN)) {
			ThreadSleep(2000);
			if (click(driver, getSearchIcon().get(0), "Search icon", action.SCROLLANDBOOLEAN)) {
				ThreadSleep(2000);
				WebElement ele = FindElement(driver, "//span[contains(@id,'grid_ContactAccess-cell-1')]//a",
						"Contact Name", action.SCROLLANDBOOLEAN, 60);
				if (ele != null) {
					if (ele.getText().trim().contains(contactFullName)) {
						appLog.info("Contact is dipslaying");
					} else {
						appLog.info("Contact is not displaying");
						return false;
					}
				} else {
					if (getErrorMessageOnGlobalUserGrid2(20) != null) {
						appLog.info("Contact is not displaying");
					} else {
						appLog.info("Contact is displaying");
						return false;
					}
				}
			} else {
				appLog.info("Not able to click on search icon");
				return false;

			}
		} else {
			appLog.info("Not able to enter value in searchbox");
			return false;
		}
		return true;
	}
	
	
	/**
	 * 
	 * @param dealName
	 * @return true/false
	 */
	public boolean verifyContactInRemoveContactAccessTabGridInDRMPage(String dealName) {
		if(!getdealNameInRemoveContactAccessTabGrid(120).isEmpty()){
		for (int i = 0; i < getdealNameInRemoveContactAccessTabGrid(120).size(); i++) {
			String dealNameInGrid = getdealNameInRemoveContactAccessTabGrid(120).get(i).getText().trim();
			if (dealNameInGrid.equalsIgnoreCase(dealName)) {
				appLog.info(dealName + " is displaying in Remove contact Access grid");
				return true;
			} else {
				appLog.info(dealName + " is not displaying in Remove contact Access grid");
			}
		}
	} else{
		appLog.error("pop is not displayed properly. Size is :"+getdealNameInRemoveContactAccessTabGrid(120).size());
	}
		return false;
	}
	
	/**
	 * 
	 * @param contactFullName
	 * @param contactEmailId
	 * @return true/false
	 */
	public boolean verifyContactEmailLink(String contactFullName,String contactEmailId){
			WebElement emailLink=FindElement(driver, "//span[text()='"+contactFullName+"']/../..//a[text()='"+contactEmailId+"']","Selected Contact Email Id Link in Contact Access grid", action.SCROLLANDBOOLEAN,60);
		
			if(!click(driver,emailLink,"Contact Email Id in Contact Access grid", action.SCROLLANDBOOLEAN)){
				appLog.info("Not able to click on email id link or ."+contactFullName+" is not avaliable in the contact grid.");
				return false;
			}

			if (!verifyNavatarSalesTeamLinkFunctionality("ContactEmailLink")) {
				appLog.info("Verification of Contact Email link is unsuccessfull.");
				return false;
			} else {
				appLog.info("Verification of Contact Email Link is successfull.");
			}
		
		return true;
		
	}
	
	/**
	 * 
	 * @param typeOfSetting
	 * @param Status
	 * @return true/false
	 */
	public boolean verifyManageAprovalAndWatermarkingStatus(String typeOfSetting,String Status){
		if(typeOfSetting.equalsIgnoreCase("Watermarking")){
			if(Status.equalsIgnoreCase("Active")){
				if (getManageApprovalStatus(60).getText().trim().contains("Active")) {
					appLog.info(typeOfSetting + " Status :" + getManageApprovalStatus(60).getText().trim());
					appLog.info(typeOfSetting + "Is Activated");
					return true;
				} else {
					appLog.info(typeOfSetting + "is not activated");
					return false;
				}
			}else if(Status.equalsIgnoreCase("Inactive")){
				if (getManageApprovalDeactivateStatus(60).getText().trim().contains("Inactive")) {
					appLog.info(typeOfSetting + "Status:" + getManageApprovalDeactivateStatus(60).getText().trim());
					appLog.info(typeOfSetting + "Is Deactivated");
					return true;
				} else {
					appLog.info(typeOfSetting + "is not Deactivated");
					return false;
				}	
			}
		}else if(typeOfSetting.equalsIgnoreCase("Manage Approvals")){
			if(Status.equalsIgnoreCase("Active")){
				if (getManageApprovalStatus(60).getText().trim().contains("Active")) {
					appLog.info(typeOfSetting + " Status :" + getManageApprovalStatus(60).getText().trim());
					appLog.info(typeOfSetting + "Is Activated");
					return true;
				} else {
					appLog.info(typeOfSetting + "is not activated");
					return false;
				}
			}else if(Status.equalsIgnoreCase("Inactive")){
				if (getManageApprovalDeactivateStatus(60).getText().trim().contains("Inactive")) {
					appLog.info(typeOfSetting + "Status:" + getManageApprovalDeactivateStatus(60).getText().trim());
					appLog.info(typeOfSetting + "Is Deactivated");
					return true;
					
				} else {
					appLog.info(typeOfSetting + "is not Deactivated");
					return false;
				}	
			}
		}
		appLog.info("Page name is not correct.Kindly Check the page name");
		return false;
		
	}
	
	/**
	 * 
	 * @param ContactName
	 * @return true/false
	 */
	public boolean selectUserInManageApprovals(String ContactName){
		WebElement userCheckbox=FindElement(driver, "//span[text()='"+ContactName+"']/../..//span[contains(@id,'grid_ManageApprovals2-cell-1')]//input", "User Checkbox", action.SCROLLANDBOOLEAN, 120);
		if(click(driver, userCheckbox, "User Checkbox", action.SCROLLANDBOOLEAN)){
			appLog.info("Clicked On User Checkbox in Manage Approvals");
			return true;
		}else{
			appLog.info("Not Able To Click On User Checkbox in Manage Approvals");
			return false;
		}
	}
	
	/**
	 * 
	 * @param UserFullName
	 * @param Setting
	 * @param dealName
	 * @param pageName
	 * @return true/false
	 */
	public boolean verifyUserSelectedAfterGivingAccess(String UserFullName,String Setting,String dealName,String pageName){
		if(Setting.equalsIgnoreCase("By Deal")){
			if(!selectDealFromList(dealName, pageName)){
				appLog.info("Not able to select the deal");
				return false;
			}
			WebElement userRadioButton=FindElement(driver, "//span[text()='"+UserFullName+"']/../..//input", "User Radio button", action.SCROLLANDBOOLEAN, 60);
			if(!isSelected(driver, userRadioButton, "User Radio Button")){
				appLog.info("User Is Not Selected");
				return false;
			}
		}
		else if(Setting.equalsIgnoreCase("Global")){
			WebElement userRadioButton=FindElement(driver, "//span[text()='"+UserFullName+"']/../..//input", "User Radio button", action.SCROLLANDBOOLEAN, 60);
			if(!isSelected(driver, userRadioButton, "User Radio Button")){
				appLog.info("User Is Not Selected");
				return false;
			}
		}else if (Setting.equalsIgnoreCase("GlobalEditMode")) {
			WebElement userRadioButton=FindElement(driver, "(//span[text()='"+UserFullName+"']/../..//input)[2]", "User Radio button", action.SCROLLANDBOOLEAN, 60);
			if(!isSelected(driver, userRadioButton, "User Radio Button")){
				appLog.info("User Is Not Selected");
				return false;
			}
		}
		return true;
	}
	
	/**
	 * 
	 * @param contactName
	 * @return true/false
	 */
	public boolean verifySearchResultOnManageApproval(String contactName){
		boolean flag= false;
		List<WebElement> ele=FindElements(driver, "//span[contains(@id,'grid_ManageApprovals-cell-0')]//span", "Users name");
		String[] contact=contactName.split(",");
		if(!ele.isEmpty()){
			for(int i=0;i<contact.length;i++){
				for(int j=0;j<ele.size();j++){
					if(ele.get(j).getText().trim().contains(contact[i])){
						appLog.info("Contact Name Is matched: "+contact[i]);
						flag=true;
						break;
					}else{
						if(j==ele.size()-1){
							appLog.info("Contact is not matched :"+contact[i]);
							flag=false;
							
						}
					}
				}
			}
		}else{
			appLog.info("There is no list of contact present in the grid");
			return false;
		}
		return flag;
	}
	
	/**
	 * 
	 * @param labelsAndLocation
	 * @return true/false
	 */
	public List<String> activateWatermarking(String labelsAndLocation){
		String[] labelsWithLocation = labelsAndLocation.split(",");
		List<String> notSetLabels = new ArrayList<String>();
		int j = 4;
		if(click(driver, getWatermarkingTab(60), "watermarking tab", action.BOOLEAN)){
			ThreadSleep(7000);
			if(click(driver, getEditIconOnDRMTab(60), "edit icon", action.THROWEXCEPTION));
			ThreadSleep(2000);
			for(int i = 0; i < labelsWithLocation.length ; i++){
				System.out.println("inside for");
				if((labelsWithLocation[i].split("-"))[0].equalsIgnoreCase("My Firm's Name") || labelsWithLocation[i].split("-")[0].equalsIgnoreCase("My Firm Name") || labelsWithLocation[i].split("-")[0].contains("Firm's Name") && (labelsWithLocation[i].split("-")[0].contains("Firm") && labelsWithLocation[i].split("-")[0].contains("Name") && labelsWithLocation[i].split("-")[0].contains("My"))){
					System.out.println("Inside if");
					if(getWatermarkingFirmNameLabel(60).isSelected()){
						appLog.info("firm name check box is already selected.");
						if(getMyFirmNameDropDown(30)!=null){
							if(selectVisibleTextFromDropDown(driver, getMyFirmNameDropDown(20), "My Firm Name Drop Down", labelsWithLocation[i].split("-")[1])){
								appLog.info("Successfully selected "+labelsWithLocation[i].split("-")[1]+" from the drop down");
							} else {
								appLog.error(labelsWithLocation[i].split("-")[1]+" value is not present in drop down");
								notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
							}
						} else {
							appLog.error("Firm Name Drop Down is not available.");
							notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
						}
					} else {
						if(click(driver, getWatermarkingFirmNameLabel(30), "watermarking firm name label", action.SCROLLANDBOOLEAN)){
							if(checkForAlert()){
								break;
							}
							appLog.info("Firm Name Label is successfully checked.");
							if(getMyFirmNameDropDown(30)!=null){
								if(selectVisibleTextFromDropDown(driver, getMyFirmNameDropDown(20), "My Firm Name Drop Down", labelsWithLocation[i].split("-")[1])){
									appLog.info("Successfully selected "+labelsWithLocation[i].split("-")[1]+" from the drop down");
								} else {
									appLog.error(labelsWithLocation[i].split("-")[1]+" value is not present in drop down");
									notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
								}
							} else {
								appLog.error("Firm Name Drop Down is not available.");
								notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
							}
						} else {
							if(checkForAlert()){
								break;
							}
							appLog.error("Firm Name label is not checked.");
							notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
						}
					}
				} else if((labelsWithLocation[i].split("-"))[0].equalsIgnoreCase("Target Account Name") || labelsWithLocation[i].split("-")[0].equalsIgnoreCase("Target Account") || labelsWithLocation[i].split("-")[0].contains("Target Name") && (labelsWithLocation[i].split("-")[0].contains("Target") && labelsWithLocation[i].split("-")[0].contains("Account") && labelsWithLocation[i].split("-")[0].contains("Name"))){
					if(getTargetAccountNameLabel(60).isSelected()){
						appLog.info("Target Account name check box is already selected.");
						if(getTargetAccountNameLabelDropDown(30)!=null){
							if(selectVisibleTextFromDropDown(driver, getTargetAccountNameLabelDropDown(30), "Target Account Name Drop Down", labelsWithLocation[i].split("-")[1])){
								appLog.info("Successfully selected "+labelsWithLocation[i].split("-")[1]+" from the drop down");
							} else {
								appLog.error(labelsWithLocation[i].split("-")[1]+" value is not present in drop down");
								notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
							}
						} else {
							appLog.error("Target Account Name Drop Down is not available.");
							notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
						}
					} else {
						if(click(driver, getTargetAccountNameLabel(30), "watermarking Target Account Name label", action.SCROLLANDBOOLEAN)){
							if(checkForAlert()){
								break;
							}
							appLog.info("Target Account Name Label is successfully checked.");
							if(getTargetAccountNameLabelDropDown(30)!=null){
								if(selectVisibleTextFromDropDown(driver, getTargetAccountNameLabelDropDown(30), "Target Account Name Drop Down", labelsWithLocation[i].split("-")[1])){
									appLog.info("Successfully selected "+labelsWithLocation[i].split("-")[1]+" from the drop down");
								} else {
									appLog.error(labelsWithLocation[i].split("-")[1]+" value is not present in drop down");
									notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
								}
							} else {
								appLog.error("Target Account Name Drop Down is not available.");
								notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
							}
						} else {
							if(checkForAlert()){
								break;
							}
							appLog.error("Target Account Name label is not checked.");
							notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
						}
					}
				} else if((labelsWithLocation[i].split("-"))[0].equalsIgnoreCase("Deal Room Name") || labelsWithLocation[i].split("-")[0].equalsIgnoreCase("Deal Name") || labelsWithLocation[i].split("-")[0].contains("Deal Room") && (labelsWithLocation[i].split("-")[0].contains("Deal") && labelsWithLocation[i].split("-")[0].contains("Room") && labelsWithLocation[i].split("-")[0].contains("Name"))){
					if(getDealRoomNameLabel(60).isSelected()){
						appLog.info("Deal Room Name check box is already selected.");
						if(getDealRoomNameLabelDropDown(30)!=null){
							if(selectVisibleTextFromDropDown(driver, getDealRoomNameLabelDropDown(30), "Deal Room Name Drop Down", labelsWithLocation[i].split("-")[1])){
								appLog.info("Successfully selected "+labelsWithLocation[i].split("-")[1]+" from the drop down");
							} else {
								appLog.error(labelsWithLocation[i].split("-")[1]+" value is not present in drop down");
								notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
							}
						} else {
							appLog.error("Deal Room Name Drop Down is not available.");
							notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
						}
					} else {
						if(click(driver, getDealRoomNameLabel(30), "watermarking Deal Room Name label", action.SCROLLANDBOOLEAN)){
							if(checkForAlert()){
								break;
							}
							appLog.info("Deal Room Name Label is successfully checked.");
							if(getDealRoomNameLabelDropDown(30)!=null){
								if(selectVisibleTextFromDropDown(driver, getDealRoomNameLabelDropDown(30), "Deal Room Name Drop Down", labelsWithLocation[i].split("-")[1])){
									appLog.info("Successfully selected "+labelsWithLocation[i].split("-")[1]+" from the drop down");
								} else {
									appLog.error(labelsWithLocation[i].split("-")[1]+" value is not present in drop down");
									notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
								}
							} else {
								appLog.error("Deal Room Name Drop Down is not available.");
								notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
							}
						} else {
							if(checkForAlert()){
								break;
							}
							appLog.error("Deal Room Name label is not checked.");
							notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
						}
					}
				} else if((labelsWithLocation[i].split("-"))[0].equalsIgnoreCase("Date and Time") || labelsWithLocation[i].split("-")[0].equalsIgnoreCase("Date") && (labelsWithLocation[i].split("-")[0].contains("Time") && labelsWithLocation[i].split("-")[0].contains("and"))){
					if(getDownloadDateLabel(30).isSelected()){
						appLog.info("Download Date check box is already selected.");
						if(getDownloadDateLabelDropDown(30)!=null){
							if(selectVisibleTextFromDropDown(driver, getDownloadDateLabelDropDown(30), "Date and Time Drop Down", labelsWithLocation[i].split("-")[1])){
								appLog.info("Successfully selected "+labelsWithLocation[i].split("-")[1]+" from the drop down");
							} else {
								appLog.error(labelsWithLocation[i].split("-")[1]+" value is not present in drop down");
								notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
							}
						} else {
							appLog.error("Date and Time Drop Down is not available.");
							notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
						}
					} else {
						if(click(driver, getDownloadDateLabel(30), "watermarking Date and Time label", action.SCROLLANDBOOLEAN)){
							if(checkForAlert()){
								break;
							}
							appLog.info("Date and Time Label is successfully checked.");
							if(getDownloadDateLabelDropDown(30)!=null){
								if(selectVisibleTextFromDropDown(driver, getDownloadDateLabelDropDown(30), "Date and Time Drop Down", labelsWithLocation[i].split("-")[1])){
									appLog.info("Successfully selected "+labelsWithLocation[i].split("-")[1]+" from the drop down");
								} else {
									appLog.error(labelsWithLocation[i].split("-")[1]+" value is not present in drop down");
									notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
								}
							} else {
								appLog.error("Date and Time Drop Down is not available.");
								notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
							}
						} else {
							if(checkForAlert()){
								break;
							}
							appLog.error("Download Date label is not checked.");
							notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
						}
					}
				} else if((labelsWithLocation[i].split("-"))[0].equalsIgnoreCase("IP Address") || labelsWithLocation[i].split("-")[0].equalsIgnoreCase("IP") && (labelsWithLocation[i].split("-")[0].contains("IP") && labelsWithLocation[i].split("-")[0].contains("Address"))){
					if(getIPAddressLabel(30).isSelected()){
						appLog.info("IP Address check box is already selected.");
						if(getIPAddressLabelDropDown(30)!=null){
							if(selectVisibleTextFromDropDown(driver, getIPAddressLabelDropDown(30), "IP Address Drop Down", labelsWithLocation[i].split("-")[1])){
								appLog.info("Successfully selected "+labelsWithLocation[i].split("-")[1]+" from the drop down");
							} else {
								appLog.error(labelsWithLocation[i].split("-")[1]+" value is not present in drop down");
								notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
							}
						} else {
							appLog.error("IP Address Drop Down is not available.");
							notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
						}
					} else {
						if(click(driver, getIPAddressLabel(30), "watermarking IP Address label", action.SCROLLANDBOOLEAN)){
							if(checkForAlert()){
								break;
							}
							appLog.info("IP Address Label is successfully checked.");
							if(getIPAddressLabelDropDown(30)!=null){
								if(selectVisibleTextFromDropDown(driver, getIPAddressLabelDropDown(30), "IP Address Drop Down", labelsWithLocation[i].split("-")[1])){
									appLog.info("Successfully selected "+labelsWithLocation[i].split("-")[1]+" from the drop down");
								} else {
									appLog.error(labelsWithLocation[i].split("-")[1]+" value is not present in drop down");
									notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
								}
							} else {
								appLog.error("IP Address Drop Down is not available.");
								notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
							}
						} else {
							if(checkForAlert()){
								break;
							}
							appLog.error("IP Address label is not checked.");
							notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
						}
					}
				} else if((labelsWithLocation[i].split("-"))[0].equalsIgnoreCase("Email Address") || labelsWithLocation[i].split("-")[0].equalsIgnoreCase("Email") && (labelsWithLocation[i].split("-")[0].contains("Email") && labelsWithLocation[i].split("-")[0].contains("Address"))){
					if(getEmailAddressLabel(30).isSelected()){
						appLog.info("Email Address check box is already selected.");
						if(getEmailAddressLabelDropDown(30)!=null){
							if(selectVisibleTextFromDropDown(driver, getEmailAddressLabelDropDown(30), "Email Address Drop Down", labelsWithLocation[i].split("-")[1])){
								appLog.info("Successfully selected "+labelsWithLocation[i].split("-")[1]+" from the drop down");
							} else {
								appLog.error(labelsWithLocation[i].split("-")[1]+" value is not present in drop down");
								notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
							}
						} else {
							appLog.error("Email Address Drop Down is not available.");
							notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
						}
					} else {
						if(click(driver, getEmailAddressLabel(30), "watermarking Email Address label", action.SCROLLANDBOOLEAN)){
							if(checkForAlert()){
								break;
							}
							appLog.info("Email Address Label is successfully checked.");
							if(getEmailAddressLabelDropDown(30)!=null){
								if(selectVisibleTextFromDropDown(driver, getEmailAddressLabelDropDown(30), "Email Address Drop Down", labelsWithLocation[i].split("-")[1])){
									appLog.info("Successfully selected "+labelsWithLocation[i].split("-")[1]+" from the drop down");
								} else {
									appLog.error(labelsWithLocation[i].split("-")[1]+" value is not present in drop down");
									notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
								}
							} else {
								appLog.error("Email Address Drop Down is not available.");
								notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
							}
						} else {
							if(checkForAlert()){
								break;
							}
							appLog.error("Email Address label is not checked.");
							notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
						}
					}
				} else {
					if(getCustomLabelCheckBox(30).isSelected()){
						appLog.info("Custom Label Check Box is Already Selected.");
					} else {
						if(click(driver, getCustomLabelCheckBox(60), "Custom Label CheckBox", action.SCROLLANDBOOLEAN)){
							appLog.info("Successfully selected custom label check box.");
						} else {
							appLog.error("custom label check box cannot be selected.");
							notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
							continue;
						}
					}
					if(j==4){
							if(sendKeys(driver, FindElement(driver, "//input[@id='pgid:frmid:tid"+j+"']", "Custom Label Text Field", action.BOOLEAN, 30), labelsWithLocation[i].split("-")[0], "Custom Label Text Field", action.SCROLLANDBOOLEAN)){
								j++;
								appLog.info("Successfully passed value to cutom label text box.");
							} else {
								appLog.error("Not Able to pass value to custom label text box.");
								notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
								continue;
							}
					} else {
						if(click(driver, getCustomLabelAddRowLink(30), "Custom Label Add Row Link", action.SCROLLANDBOOLEAN)){
							if(checkForAlert()){
								break;
							}
							if(sendKeys(driver, FindElement(driver, "//input[@id='pgid:frmid:tid"+j+"']", "Custom Label Text Field", action.BOOLEAN, 30), labelsWithLocation[i].split("-")[0], "Custom Label Text Field", action.SCROLLANDBOOLEAN)){
								j++;
								appLog.info("Successfully passed value to cutom label text box.");
							} else {
								appLog.error("Not Able to pass value to custom label text box.");
								notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
								continue;
							}
						} else {
							if(checkForAlert()){
								break;
							}
							appLog.error("Not able to click on add row link");
							notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
							continue;
						}
					}
					if(selectVisibleTextFromDropDown(driver, FindElement(driver, "//select[@id='pgid:frmid:mgr"+j+"']", "Custom Label Text Field", action.BOOLEAN, 30), "Cutom Label Drop Down", (labelsWithLocation[i].split("-"))[1])){
						appLog.info("Successfully selected "+(labelsWithLocation[i].split("-"))[1]+" from the drop down.");
					} else {
						appLog.error("Not able to selecte "+(labelsWithLocation[i].split("-"))[1]+" from the drop down.");
						notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
					}
				}
			}
			if(click(driver, getWatermarkingSaveButton(60), "Save Button", action.SCROLLANDBOOLEAN)){
				if(isAlertPresent(driver)){
					String msg = switchToAlertAndGetMessage(driver, 10, action.GETTEXT);
					switchToAlertAndAcceptOrDecline(driver, 10, action.ACCEPT);
					appLog.info(msg);
					driver.navigate().refresh();
					switchToFrame(driver, 10, getDealRoomManagerFrame(10));
					click(driver, getWatermarkingTab(60), "Watermarking Tab", action.BOOLEAN);
//					notSetLabels.add("No watermarking label is set. Due to: "+msg);
					return null;
				} else {
					appLog.info("Successfully saved the watermarking setting.");
				}
			} else {
				appLog.error("Not able to save watermarking setting.");
				notSetLabels.add("Not able to save watermarking setting.");
			}
		} else {
			appLog.error("watermarking tab cannot be clicked, So won't be able to activate watermarking setting.");
		}
		return notSetLabels;
	}
	
	/**
	 * 
	 * @param dropDownElement
	 * @param value
	 * @param elementName
	 * @return true/false
	 */
	public boolean selectLocation(WebElement dropDownElement, String value, String elementName){
		if(dropDownElement!=null){
			if(selectVisibleTextFromDropDown(driver, dropDownElement, elementName, value)){
				appLog.info("Successfully selected "+value+" from the drop down");
				return true;
			} else {
				appLog.error(value+" value is not present in drop down of "+elementName);
				return false;
			}
		} else {
			appLog.error(elementName+" Drop Down is not available.");
//			notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
			return false;
		}
	}
	
	/**
	 * 
	 * @return true/false
	 */
	public boolean checkForAlert(){
		if(isAlertPresent(driver)){
			appLog.info(switchToAlertAndGetMessage(driver, 20, action.GETTEXT));
			switchToAlertAndAcceptOrDecline(driver, 10, action.ACCEPT);
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * 
	 * @param folderTemplate
	 * @return true/false
	 */
	public boolean clickOnFolderTemplate(String folderTemplate){
		WebElement ele=FindElement(driver, "//span[contains(@id,'templateGrid-cell-0-')]//a[@title='"+folderTemplate+"']", "Folder Template", action.SCROLLANDBOOLEAN, 60);
		if(ele!=null){
			click(driver, ele, "Folder Template", action.SCROLLANDBOOLEAN);
			return true;
		}else{
			appLog.info("folder template is not present");
			return false;
		}
		
	}
	
	/**
	 * 
	 * @param labelsAndLocation
	 * @return true/false
	 */
	public List<String> reSetWatermarking(String labelsAndLocation){
		String[] labelsWithLocation = labelsAndLocation.split(",");
		List<String> notSetLabels = new ArrayList<String>();
		int j = 4;
		if(click(driver, getWatermarkingTab(60), "watermarking tab", action.BOOLEAN)){
			ThreadSleep(7000);
			if(click(driver, getEditIconOnDRMTab(60), "edit icon", action.THROWEXCEPTION));
			ThreadSleep(2000);
			for(int i = 0; i < labelsWithLocation.length ; i++){
				System.out.println("inside for");
				if((labelsWithLocation[i].split("-"))[0].equalsIgnoreCase("My Firm's Name") || labelsWithLocation[i].split("-")[0].equalsIgnoreCase("My Firm Name") || labelsWithLocation[i].split("-")[0].contains("Firm's Name") && (labelsWithLocation[i].split("-")[0].contains("Firm") && labelsWithLocation[i].split("-")[0].contains("Name") && labelsWithLocation[i].split("-")[0].contains("My"))){
					System.out.println("Inside if");
					if(getWatermarkingFirmNameLabel(60).isSelected()){
						appLog.info("firm name check box is already selected.");
						if(getMyFirmNameDropDown(30)!=null){
							if(selectVisibleTextFromDropDown(driver, getMyFirmNameDropDown(20), "My Firm Name Drop Down", labelsWithLocation[i].split("-")[1])){
								appLog.info("Successfully selected "+labelsWithLocation[i].split("-")[1]+" from the drop down");
							} else {
								appLog.error(labelsWithLocation[i].split("-")[1]+" value is not present in drop down");
								notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
							}
						} else {
							appLog.error("Firm Name Drop Down is not available.");
							notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
						}
					} else {
						if(click(driver, getWatermarkingFirmNameLabel(30), "watermarking firm name label", action.SCROLLANDBOOLEAN)){
							if(checkForAlert()){
								break;
							}
							appLog.info("Firm Name Label is successfully checked.");
							if(getMyFirmNameDropDown(30)!=null){
								if(selectVisibleTextFromDropDown(driver, getMyFirmNameDropDown(20), "My Firm Name Drop Down", labelsWithLocation[i].split("-")[1])){
									appLog.info("Successfully selected "+labelsWithLocation[i].split("-")[1]+" from the drop down");
								} else {
									appLog.error(labelsWithLocation[i].split("-")[1]+" value is not present in drop down");
									notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
								}
							} else {
								appLog.error("Firm Name Drop Down is not available.");
								notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
							}
						} else {
							if(checkForAlert()){
								break;
							}
							appLog.error("Firm Name label is not checked.");
							notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
						}
					}
				} else if((labelsWithLocation[i].split("-"))[0].equalsIgnoreCase("Target Account Name") || labelsWithLocation[i].split("-")[0].equalsIgnoreCase("Target Account") || labelsWithLocation[i].split("-")[0].contains("Target Name") && (labelsWithLocation[i].split("-")[0].contains("Target") && labelsWithLocation[i].split("-")[0].contains("Account") && labelsWithLocation[i].split("-")[0].contains("Name"))){
					if(getTargetAccountNameLabel(60).isSelected()){
						appLog.info("Target Account name check box is already selected.");
						if(getTargetAccountNameLabelDropDown(30)!=null){
							if(selectVisibleTextFromDropDown(driver, getTargetAccountNameLabelDropDown(30), "Target Account Name Drop Down", labelsWithLocation[i].split("-")[1])){
								appLog.info("Successfully selected "+labelsWithLocation[i].split("-")[1]+" from the drop down");
							} else {
								appLog.error(labelsWithLocation[i].split("-")[1]+" value is not present in drop down");
								notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
							}
						} else {
							appLog.error("Target Account Name Drop Down is not available.");
							notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
						}
					} else {
						if(click(driver, getTargetAccountNameLabel(30), "watermarking Target Account Name label", action.SCROLLANDBOOLEAN)){
							if(checkForAlert()){
								break;
							}
							appLog.info("Target Account Name Label is successfully checked.");
							if(getTargetAccountNameLabelDropDown(30)!=null){
								if(selectVisibleTextFromDropDown(driver, getTargetAccountNameLabelDropDown(30), "Target Account Name Drop Down", labelsWithLocation[i].split("-")[1])){
									appLog.info("Successfully selected "+labelsWithLocation[i].split("-")[1]+" from the drop down");
								} else {
									appLog.error(labelsWithLocation[i].split("-")[1]+" value is not present in drop down");
									notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
								}
							} else {
								appLog.error("Target Account Name Drop Down is not available.");
								notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
							}
						} else {
							if(checkForAlert()){
								break;
							}
							appLog.error("Target Account Name label is not checked.");
							notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
						}
					}
				} else if((labelsWithLocation[i].split("-"))[0].equalsIgnoreCase("Deal Room Name") || labelsWithLocation[i].split("-")[0].equalsIgnoreCase("Deal Name") || labelsWithLocation[i].split("-")[0].contains("Deal Room") && (labelsWithLocation[i].split("-")[0].contains("Deal") && labelsWithLocation[i].split("-")[0].contains("Room") && labelsWithLocation[i].split("-")[0].contains("Name"))){
					if(getDealRoomNameLabel(60).isSelected()){
						appLog.info("Deal Room Name check box is already selected.");
						if(getDealRoomNameLabelDropDown(30)!=null){
							if(selectVisibleTextFromDropDown(driver, getDealRoomNameLabelDropDown(30), "Deal Room Name Drop Down", labelsWithLocation[i].split("-")[1])){
								appLog.info("Successfully selected "+labelsWithLocation[i].split("-")[1]+" from the drop down");
							} else {
								appLog.error(labelsWithLocation[i].split("-")[1]+" value is not present in drop down");
								notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
							}
						} else {
							appLog.error("Deal Room Name Drop Down is not available.");
							notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
						}
					} else {
						if(click(driver, getDealRoomNameLabel(30), "watermarking Deal Room Name label", action.SCROLLANDBOOLEAN)){
							if(checkForAlert()){
								break;
							}
							appLog.info("Deal Room Name Label is successfully checked.");
							if(getDealRoomNameLabelDropDown(30)!=null){
								if(selectVisibleTextFromDropDown(driver, getDealRoomNameLabelDropDown(30), "Deal Room Name Drop Down", labelsWithLocation[i].split("-")[1])){
									appLog.info("Successfully selected "+labelsWithLocation[i].split("-")[1]+" from the drop down");
								} else {
									appLog.error(labelsWithLocation[i].split("-")[1]+" value is not present in drop down");
									notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
								}
							} else {
								appLog.error("Deal Room Name Drop Down is not available.");
								notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
							}
						} else {
							if(checkForAlert()){
								break;
							}
							appLog.error("Deal Room Name label is not checked.");
							notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
						}
					}
				} else if((labelsWithLocation[i].split("-"))[0].equalsIgnoreCase("Download Date") || labelsWithLocation[i].split("-")[0].equalsIgnoreCase("Downloaded Date") && (labelsWithLocation[i].split("-")[0].contains("Download") && labelsWithLocation[i].split("-")[0].contains("Date"))){
					if(getDownloadDateLabel(30).isSelected()){
						appLog.info("Download Date check box is already selected.");
						if(getDownloadDateLabelDropDown(30)!=null){
							if(selectVisibleTextFromDropDown(driver, getDownloadDateLabelDropDown(30), "Download Date Drop Down", labelsWithLocation[i].split("-")[1])){
								appLog.info("Successfully selected "+labelsWithLocation[i].split("-")[1]+" from the drop down");
							} else {
								appLog.error(labelsWithLocation[i].split("-")[1]+" value is not present in drop down");
								notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
							}
						} else {
							appLog.error("Download Date Drop Down is not available.");
							notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
						}
					} else {
						if(click(driver, getDownloadDateLabel(30), "watermarking Download Date label", action.SCROLLANDBOOLEAN)){
							if(checkForAlert()){
								break;
							}
							appLog.info("Download Date Label is successfully checked.");
							if(getDownloadDateLabelDropDown(30)!=null){
								if(selectVisibleTextFromDropDown(driver, getDownloadDateLabelDropDown(30), "Download Date Drop Down", labelsWithLocation[i].split("-")[1])){
									appLog.info("Successfully selected "+labelsWithLocation[i].split("-")[1]+" from the drop down");
								} else {
									appLog.error(labelsWithLocation[i].split("-")[1]+" value is not present in drop down");
									notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
								}
							} else {
								appLog.error("Download Date Drop Down is not available.");
								notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
							}
						} else {
							if(checkForAlert()){
								break;
							}
							appLog.error("Download Date label is not checked.");
							notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
						}
					}
				} else if((labelsWithLocation[i].split("-"))[0].equalsIgnoreCase("IP Address") || labelsWithLocation[i].split("-")[0].equalsIgnoreCase("IP") && (labelsWithLocation[i].split("-")[0].contains("IP") && labelsWithLocation[i].split("-")[0].contains("Address"))){
					if(getIPAddressLabel(30).isSelected()){
						appLog.info("IP Address check box is already selected.");
						if(getIPAddressLabelDropDown(30)!=null){
							if(selectVisibleTextFromDropDown(driver, getIPAddressLabelDropDown(30), "IP Address Drop Down", labelsWithLocation[i].split("-")[1])){
								appLog.info("Successfully selected "+labelsWithLocation[i].split("-")[1]+" from the drop down");
							} else {
								appLog.error(labelsWithLocation[i].split("-")[1]+" value is not present in drop down");
								notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
							}
						} else {
							appLog.error("IP Address Drop Down is not available.");
							notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
						}
					} else {
						if(click(driver, getIPAddressLabel(30), "watermarking IP Address label", action.SCROLLANDBOOLEAN)){
							if(checkForAlert()){
								break;
							}
							appLog.info("IP Address Label is successfully checked.");
							if(getIPAddressLabelDropDown(30)!=null){
								if(selectVisibleTextFromDropDown(driver, getIPAddressLabelDropDown(30), "IP Address Drop Down", labelsWithLocation[i].split("-")[1])){
									appLog.info("Successfully selected "+labelsWithLocation[i].split("-")[1]+" from the drop down");
								} else {
									appLog.error(labelsWithLocation[i].split("-")[1]+" value is not present in drop down");
									notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
								}
							} else {
								appLog.error("IP Address Drop Down is not available.");
								notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
							}
						} else {
							if(checkForAlert()){
								break;
							}
							appLog.error("IP Address label is not checked.");
							notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
						}
					}
				} else if((labelsWithLocation[i].split("-"))[0].equalsIgnoreCase("Email Address") || labelsWithLocation[i].split("-")[0].equalsIgnoreCase("Email") && (labelsWithLocation[i].split("-")[0].contains("Email") && labelsWithLocation[i].split("-")[0].contains("Address"))){
					if(getEmailAddressLabel(30).isSelected()){
						appLog.info("Email Address check box is already selected.");
						if(getEmailAddressLabelDropDown(30)!=null){
							if(selectVisibleTextFromDropDown(driver, getEmailAddressLabelDropDown(30), "Email Address Drop Down", labelsWithLocation[i].split("-")[1])){
								appLog.info("Successfully selected "+labelsWithLocation[i].split("-")[1]+" from the drop down");
							} else {
								appLog.error(labelsWithLocation[i].split("-")[1]+" value is not present in drop down");
								notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
							}
						} else {
							appLog.error("Email Address Drop Down is not available.");
							notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
						}
					} else {
						if(click(driver, getEmailAddressLabel(30), "watermarking Email Address label", action.SCROLLANDBOOLEAN)){
							if(checkForAlert()){
								break;
							}
							appLog.info("Email Address Label is successfully checked.");
							if(getEmailAddressLabelDropDown(30)!=null){
								if(selectVisibleTextFromDropDown(driver, getEmailAddressLabelDropDown(30), "Email Address Drop Down", labelsWithLocation[i].split("-")[1])){
									appLog.info("Successfully selected "+labelsWithLocation[i].split("-")[1]+" from the drop down");
								} else {
									appLog.error(labelsWithLocation[i].split("-")[1]+" value is not present in drop down");
									notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
								}
							} else {
								appLog.error("Email Address Drop Down is not available.");
								notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
							}
						} else {
							if(checkForAlert()){
								break;
							}
							appLog.error("Email Address label is not checked.");
							notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
						}
					}
				} else {
					if(getCustomLabelCheckBox(30).isSelected()){
						appLog.info("Custom Label Check Box is Already Selected.");
					} else {
						if(click(driver, getCustomLabelCheckBox(60), "Custom Label CheckBox", action.SCROLLANDBOOLEAN)){
							appLog.info("Successfully selected custom label check box.");
						} else {
							appLog.error("custom label check box cannot be selected.");
							notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
							continue;
						}
					}
					if(j==4){
						WebElement ele = FindElement(driver, "//input[@id='pgid:frmid:tid"+j+"']", "Custom Label Text Field", action.BOOLEAN, 30);
						if(ele.getAttribute("value").trim()==null || ele.getAttribute("value").trim()=="")
							if(sendKeys(driver, FindElement(driver, "//input[@id='pgid:frmid:tid"+j+"']", "Custom Label Text Field", action.BOOLEAN, 30), labelsWithLocation[i].split("-")[0], "Custom Label Text Field", action.SCROLLANDBOOLEAN)){
								j++;
								appLog.info("Successfully passed value to cutom label text box.");
							} else {
								appLog.error("Not Able to pass value to custom label text box.");
								notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
								continue;
							}
						else {
							String s = ele.getAttribute("value").trim();
							if(!s.equalsIgnoreCase(labelsWithLocation[i].split("-")[0])){
								if(sendKeys(driver, FindElement(driver, "//input[@id='pgid:frmid:tid"+j+"']", "Custom Label Text Field", action.BOOLEAN, 30), labelsWithLocation[i].split("-")[0], "Custom Label Text Field", action.SCROLLANDBOOLEAN)){
									j++;
									appLog.info("Successfully passed value to cutom label text box.");
								} else {
									appLog.error("Not Able to pass value to custom label text box.");
									notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
									continue;
								}
							} else {
								j++;
							}
							
						}
					} else {
						boolean flag = true;
						WebElement ele = FindElement(driver, "//input[@id='pgid:frmid:tid"+j+"']", "Custom Label Text Field", action.BOOLEAN, 30);
						if(ele!=null){
							if(ele.getAttribute("value").trim()!=null ||ele.getAttribute("value").trim()!=""){
								if(ele.getAttribute("value").trim().equalsIgnoreCase(labelsWithLocation[i].split("-")[0])){
									flag=false;
									j++;
								} else {
									flag=true;
								}
							}
						}
						if(click(driver, getCustomLabelAddRowLink(30), "Custom Label Add Row Link", action.SCROLLANDBOOLEAN)){
							if(checkForAlert()){
								switchToAlertAndAcceptOrDecline(driver, 3, action.ACCEPT);
							}
							if(flag)
								if(sendKeys(driver, FindElement(driver, "//input[@id='pgid:frmid:tid"+j+"']", "Custom Label Text Field", action.BOOLEAN, 30), labelsWithLocation[i].split("-")[0], "Custom Label Text Field", action.SCROLLANDBOOLEAN)){
									j++;
									appLog.info("Successfully passed value to cutom label text box.");
								} else {
									appLog.error("Not Able to pass value to custom label text box.");
									notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
									continue;
								}
						} else {
							if(checkForAlert()){
								break;
							}
							appLog.error("Not able to click on add row link");
							notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
							continue;
						}
					}
					if(selectVisibleTextFromDropDown(driver, FindElement(driver, "//select[@id='pgid:frmid:mgr"+j+"']", "Custom Label Text Field", action.BOOLEAN, 30), "Cutom Label Drop Down", (labelsWithLocation[i].split("-"))[1])){
						appLog.info("Successfully selected "+(labelsWithLocation[i].split("-"))[1]+" from the drop down.");
					} else {
						appLog.error("Not able to selecte "+(labelsWithLocation[i].split("-"))[1]+" from the drop down.");
						notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
					}
				}
			}
			if(click(driver, getWatermarkingSaveButton(60), "Save Button", action.SCROLLANDBOOLEAN)){
				if(isAlertPresent(driver)){
					String msg = switchToAlertAndGetMessage(driver, 10, action.GETTEXT);
					switchToAlertAndAcceptOrDecline(driver, 10, action.ACCEPT);
					appLog.info(msg);
					driver.navigate().refresh();
					switchToFrame(driver, 10, getDealRoomManagerFrame(10));
					click(driver, getWatermarkingTab(60), "Watermarking Tab", action.BOOLEAN);
//					notSetLabels.add("No watermarking label is set. Due to: "+msg);
					return null;
				} else {
					appLog.info("Successfully saved the watermarking setting.");
				}
			} else {
				appLog.error("Not able to save watermarking setting.");
				notSetLabels.add("Not able to save watermarking setting.");
			}
		} else {
			appLog.error("watermarking tab cannot be clicked, So won't be able to activate watermarking setting.");
		}
		return notSetLabels;
	}
	
	/**
	 * 
	 * @param TemplateName
	 * @param Description
	 * @param User
	 * @param CreatedOnDate
	 * @return true/false
	 */
	public boolean verifyfolderTemplateGrid(String TemplateName, String Description, String User,
			String CreatedOnDate) {
		WebElement FolderTemplateName = FindElement(driver,
				"//span[@id='templateGrid-rows']//a[@title='" + TemplateName + "']", "Template Name",
				action.SCROLLANDBOOLEAN, 60);
		WebElement FolderTemplateDescription = FindElement(driver,
				"(//span[@id='templateGrid-rows']//a[@title='" + TemplateName + "']/..//following-sibling::span)[1]",
				"Template Name Description", action.SCROLLANDBOOLEAN, 60);
		WebElement CreatedBy = FindElement(driver,
				"(//span[@id='templateGrid-rows']//a[@title='" + TemplateName + "']/..//following-sibling::span)[3]",
				"User Name", action.SCROLLANDBOOLEAN, 60);
		WebElement CreatedOn = FindElement(driver,
				"(//span[@id='templateGrid-rows']//a[@title='" + TemplateName + "']/..//following-sibling::span)[4]",
				"Created On Date", action.SCROLLANDBOOLEAN, 60);

		if (FolderTemplateName.getText().trim().equalsIgnoreCase(TemplateName)
				&& FolderTemplateDescription.getText().trim().equalsIgnoreCase(Description)
				&& CreatedBy.getText().trim().equalsIgnoreCase(User)
				&& (CreatedOnDate.contains(CreatedOn.getText().trim()))) {
		
		appLog.info("Folder template grid data is verified for :"+TemplateName);	
			
			return true;
		}
				
		return false;
	}
	
	/**
	 * 
	 * @param labelsAndLocation
	 * @return true/false
	 */
	public List<String> activateWatermarking2(String labelsAndLocation){
		String[] labelsWithLocation = labelsAndLocation.split(",");
		List<String> notSetLabels = new ArrayList<String>();
		int j = 4;
		ThreadSleep(7000);
			for(int i = 0; i < labelsWithLocation.length ; i++){
				System.out.println("inside for");
				if((labelsWithLocation[i].split("-"))[0].equalsIgnoreCase("My Firm's Name") || labelsWithLocation[i].split("-")[0].equalsIgnoreCase("My Firm Name") || labelsWithLocation[i].split("-")[0].contains("Firm's Name") && (labelsWithLocation[i].split("-")[0].contains("Firm") && labelsWithLocation[i].split("-")[0].contains("Name") && labelsWithLocation[i].split("-")[0].contains("My"))){
					System.out.println("Inside if");
					if(getWatermarkingFirmNameLabel(60).isSelected()){
						appLog.info("firm name check box is already selected.");
						if(getMyFirmNameDropDown(30)!=null){
							if(selectVisibleTextFromDropDown(driver, getMyFirmNameDropDown(20), "My Firm Name Drop Down", labelsWithLocation[i].split("-")[1])){
								appLog.info("Successfully selected "+labelsWithLocation[i].split("-")[1]+" from the drop down");
							} else {
								appLog.error(labelsWithLocation[i].split("-")[1]+" value is not present in drop down");
								notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
							}
						} else {
							appLog.error("Firm Name Drop Down is not available.");
							notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
						}
					} else {
						if(click(driver, getWatermarkingFirmNameLabel(30), "watermarking firm name label", action.SCROLLANDBOOLEAN)){
							if(checkForAlert()){
								break;
							}
							appLog.info("Firm Name Label is successfully checked.");
							if(getMyFirmNameDropDown(30)!=null){
								if(selectVisibleTextFromDropDown(driver, getMyFirmNameDropDown(20), "My Firm Name Drop Down", labelsWithLocation[i].split("-")[1])){
									appLog.info("Successfully selected "+labelsWithLocation[i].split("-")[1]+" from the drop down");
								} else {
									appLog.error(labelsWithLocation[i].split("-")[1]+" value is not present in drop down");
									notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
								}
							} else {
								appLog.error("Firm Name Drop Down is not available.");
								notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
							}
						} else {
							if(checkForAlert()){
								break;
							}
							appLog.error("Firm Name label is not checked.");
							notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
						}
					}
				} else if((labelsWithLocation[i].split("-"))[0].equalsIgnoreCase("Target Account Name") || labelsWithLocation[i].split("-")[0].equalsIgnoreCase("Target Account") || labelsWithLocation[i].split("-")[0].contains("Target Name") && (labelsWithLocation[i].split("-")[0].contains("Target") && labelsWithLocation[i].split("-")[0].contains("Account") && labelsWithLocation[i].split("-")[0].contains("Name"))){
					if(getTargetAccountNameLabel(60).isSelected()){
						appLog.info("Target Account name check box is already selected.");
						if(getTargetAccountNameLabelDropDown(30)!=null){
							if(selectVisibleTextFromDropDown(driver, getTargetAccountNameLabelDropDown(30), "Target Account Name Drop Down", labelsWithLocation[i].split("-")[1])){
								appLog.info("Successfully selected "+labelsWithLocation[i].split("-")[1]+" from the drop down");
							} else {
								appLog.error(labelsWithLocation[i].split("-")[1]+" value is not present in drop down");
								notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
							}
						} else {
							appLog.error("Target Account Name Drop Down is not available.");
							notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
						}
					} else {
						if(click(driver, getTargetAccountNameLabel(30), "watermarking Target Account Name label", action.SCROLLANDBOOLEAN)){
							if(checkForAlert()){
								break;
							}
							appLog.info("Target Account Name Label is successfully checked.");
							if(getTargetAccountNameLabelDropDown(30)!=null){
								if(selectVisibleTextFromDropDown(driver, getTargetAccountNameLabelDropDown(30), "Target Account Name Drop Down", labelsWithLocation[i].split("-")[1])){
									appLog.info("Successfully selected "+labelsWithLocation[i].split("-")[1]+" from the drop down");
								} else {
									appLog.error(labelsWithLocation[i].split("-")[1]+" value is not present in drop down");
									notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
								}
							} else {
								appLog.error("Target Account Name Drop Down is not available.");
								notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
							}
						} else {
							if(checkForAlert()){
								break;
							}
							appLog.error("Target Account Name label is not checked.");
							notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
						}
					}
				} else if((labelsWithLocation[i].split("-"))[0].equalsIgnoreCase("Deal Room Name") || labelsWithLocation[i].split("-")[0].equalsIgnoreCase("Deal Name") || labelsWithLocation[i].split("-")[0].contains("Deal Room") && (labelsWithLocation[i].split("-")[0].contains("Deal") && labelsWithLocation[i].split("-")[0].contains("Room") && labelsWithLocation[i].split("-")[0].contains("Name"))){
					if(getDealRoomNameLabel(60).isSelected()){
						appLog.info("Deal Room Name check box is already selected.");
						if(getDealRoomNameLabelDropDown(30)!=null){
							if(selectVisibleTextFromDropDown(driver, getDealRoomNameLabelDropDown(30), "Deal Room Name Drop Down", labelsWithLocation[i].split("-")[1])){
								appLog.info("Successfully selected "+labelsWithLocation[i].split("-")[1]+" from the drop down");
							} else {
								appLog.error(labelsWithLocation[i].split("-")[1]+" value is not present in drop down");
								notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
							}
						} else {
							appLog.error("Deal Room Name Drop Down is not available.");
							notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
						}
					} else {
						if(click(driver, getDealRoomNameLabel(30), "watermarking Deal Room Name label", action.SCROLLANDBOOLEAN)){
							if(checkForAlert()){
								break;
							}
							appLog.info("Deal Room Name Label is successfully checked.");
							if(getDealRoomNameLabelDropDown(30)!=null){
								if(selectVisibleTextFromDropDown(driver, getDealRoomNameLabelDropDown(30), "Deal Room Name Drop Down", labelsWithLocation[i].split("-")[1])){
									appLog.info("Successfully selected "+labelsWithLocation[i].split("-")[1]+" from the drop down");
								} else {
									appLog.error(labelsWithLocation[i].split("-")[1]+" value is not present in drop down");
									notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
								}
							} else {
								appLog.error("Deal Room Name Drop Down is not available.");
								notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
							}
						} else {
							if(checkForAlert()){
								break;
							}
							appLog.error("Deal Room Name label is not checked.");
							notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
						}
					}
				} else if((labelsWithLocation[i].split("-"))[0].equalsIgnoreCase("Date and Time") || labelsWithLocation[i].split("-")[0].equalsIgnoreCase("Date") && (labelsWithLocation[i].split("-")[0].contains("Time") && labelsWithLocation[i].split("-")[0].contains("and"))){
						if(getDownloadDateLabel(30).isSelected()){
						appLog.info("Download Date check box is already selected.");
						if(getDownloadDateLabelDropDown(30)!=null){
							if(selectVisibleTextFromDropDown(driver, getDownloadDateLabelDropDown(30), "Download Date Drop Down", labelsWithLocation[i].split("-")[1])){
								appLog.info("Successfully selected "+labelsWithLocation[i].split("-")[1]+" from the drop down");
							} else {
								appLog.error(labelsWithLocation[i].split("-")[1]+" value is not present in drop down");
								notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
							}
						} else {
							appLog.error("Download Date Drop Down is not available.");
							notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
						}
					} else {
						if(click(driver, getDownloadDateLabel(30), "watermarking Download Date label", action.SCROLLANDBOOLEAN)){
							if(checkForAlert()){
								break;
							}
							appLog.info("Download Date Label is successfully checked.");
							if(getDownloadDateLabelDropDown(30)!=null){
								if(selectVisibleTextFromDropDown(driver, getDownloadDateLabelDropDown(30), "Download Date Drop Down", labelsWithLocation[i].split("-")[1])){
									appLog.info("Successfully selected "+labelsWithLocation[i].split("-")[1]+" from the drop down");
								} else {
									appLog.error(labelsWithLocation[i].split("-")[1]+" value is not present in drop down");
									notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
								}
							} else {
								appLog.error("Download Date Drop Down is not available.");
								notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
							}
						} else {
							if(checkForAlert()){
								break;
							}
							appLog.error("Download Date label is not checked.");
							notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
						}
					}
				} else if((labelsWithLocation[i].split("-"))[0].equalsIgnoreCase("IP Address") || labelsWithLocation[i].split("-")[0].equalsIgnoreCase("IP") && (labelsWithLocation[i].split("-")[0].contains("IP") && labelsWithLocation[i].split("-")[0].contains("Address"))){
					if(getIPAddressLabel(30).isSelected()){
						appLog.info("IP Address check box is already selected.");
						if(getIPAddressLabelDropDown(30)!=null){
							if(selectVisibleTextFromDropDown(driver, getIPAddressLabelDropDown(30), "IP Address Drop Down", labelsWithLocation[i].split("-")[1])){
								appLog.info("Successfully selected "+labelsWithLocation[i].split("-")[1]+" from the drop down");
							} else {
								appLog.error(labelsWithLocation[i].split("-")[1]+" value is not present in drop down");
								notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
							}
						} else {
							appLog.error("IP Address Drop Down is not available.");
							notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
						}
					} else {
						if(click(driver, getIPAddressLabel(30), "watermarking IP Address label", action.SCROLLANDBOOLEAN)){
							if(checkForAlert()){
								break;
							}
							appLog.info("IP Address Label is successfully checked.");
							if(getIPAddressLabelDropDown(30)!=null){
								if(selectVisibleTextFromDropDown(driver, getIPAddressLabelDropDown(30), "IP Address Drop Down", labelsWithLocation[i].split("-")[1])){
									appLog.info("Successfully selected "+labelsWithLocation[i].split("-")[1]+" from the drop down");
								} else {
									appLog.error(labelsWithLocation[i].split("-")[1]+" value is not present in drop down");
									notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
								}
							} else {
								appLog.error("IP Address Drop Down is not available.");
								notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
							}
						} else {
							if(checkForAlert()){
								break;
							}
							appLog.error("IP Address label is not checked.");
							notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
						}
					}
				} else if((labelsWithLocation[i].split("-"))[0].equalsIgnoreCase("Email Address") || labelsWithLocation[i].split("-")[0].equalsIgnoreCase("Email") && (labelsWithLocation[i].split("-")[0].contains("Email") && labelsWithLocation[i].split("-")[0].contains("Address"))){
					if(getEmailAddressLabel(30).isSelected()){
						appLog.info("Email Address check box is already selected.");
						if(getEmailAddressLabelDropDown(30)!=null){
							if(selectVisibleTextFromDropDown(driver, getEmailAddressLabelDropDown(30), "Email Address Drop Down", labelsWithLocation[i].split("-")[1])){
								appLog.info("Successfully selected "+labelsWithLocation[i].split("-")[1]+" from the drop down");
							} else {
								appLog.error(labelsWithLocation[i].split("-")[1]+" value is not present in drop down");
								notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
							}
						} else {
							appLog.error("Email Address Drop Down is not available.");
							notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
						}
					} else {
						if(click(driver, getEmailAddressLabel(30), "watermarking Email Address label", action.SCROLLANDBOOLEAN)){
							if(checkForAlert()){
								break;
							}
							appLog.info("Email Address Label is successfully checked.");
							if(getEmailAddressLabelDropDown(30)!=null){
								if(selectVisibleTextFromDropDown(driver, getEmailAddressLabelDropDown(30), "Email Address Drop Down", labelsWithLocation[i].split("-")[1])){
									appLog.info("Successfully selected "+labelsWithLocation[i].split("-")[1]+" from the drop down");
								} else {
									appLog.error(labelsWithLocation[i].split("-")[1]+" value is not present in drop down");
									notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
								}
							} else {
								appLog.error("Email Address Drop Down is not available.");
								notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
							}
						} else {
							if(checkForAlert()){
								break;
							}
							appLog.error("Email Address label is not checked.");
							notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
						}
					}
				} else {
					if(getCustomLabelCheckBox(30).isSelected()){
						appLog.info("Custom Label Check Box is Already Selected.");
					} else {
						if(click(driver, getCustomLabelCheckBox(60), "Custom Label CheckBox", action.SCROLLANDBOOLEAN)){
							appLog.info("Successfully selected custom label check box.");
						} else {
							appLog.error("custom label check box cannot be selected.");
							notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
							continue;
						}
					}
					if(j==4){
						if(sendKeys(driver, FindElement(driver, "//input[@id='pgid:frmid:tid"+j+"']", "Custom Label Text Field", action.BOOLEAN, 30), labelsWithLocation[i].split("-")[0], "Custom Label Text Field", action.SCROLLANDBOOLEAN)){
							j++;
							appLog.info("Successfully passed value to cutom label text box.");
						} else {
							appLog.error("Not Able to pass value to custom label text box.");
							notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
							continue;
						}
					} else {
						if(click(driver, getCustomLabelAddRowLink(30), "Custom Label Add Row Link", action.SCROLLANDBOOLEAN)){
							if(checkForAlert()){
								break;
							}
							if(sendKeys(driver, FindElement(driver, "//input[@id='pgid:frmid:tid"+j+"']", "Custom Label Text Field", action.BOOLEAN, 30), labelsWithLocation[i].split("-")[0], "Custom Label Text Field", action.SCROLLANDBOOLEAN)){
								j++;
								appLog.info("Successfully passed value to cutom label text box.");
							} else {
								appLog.error("Not Able to pass value to custom label text box.");
								notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
								continue;
							}
						} else {
							if(checkForAlert()){
								break;
							}
							appLog.error("Not able to click on add row link");
							notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
							continue;
						}
					}
					if(selectVisibleTextFromDropDown(driver, FindElement(driver, "//select[@id='pgid:frmid:mgr"+j+"']", "Custom Label Text Field", action.BOOLEAN, 30), "Cutom Label Drop Down", (labelsWithLocation[i].split("-"))[1])){
						appLog.info("Successfully selected "+(labelsWithLocation[i].split("-"))[1]+" from the drop down.");
					} else {
						appLog.error("Not able to selecte "+(labelsWithLocation[i].split("-"))[1]+" from the drop down.");
						notSetLabels.add((labelsWithLocation[i].split("-"))[0]);
					}
				}
			}
			if(click(driver, getWatermarkingSaveButton(60), "Save Button", action.SCROLLANDBOOLEAN)){
				if(isAlertPresent(driver)){
					String msg = switchToAlertAndGetMessage(driver, 10, action.GETTEXT);
					switchToAlertAndAcceptOrDecline(driver, 10, action.ACCEPT);
					appLog.info(msg);
					driver.navigate().refresh();
					switchToFrame(driver, 10, getDealRoomManagerFrame(10));
					click(driver, getWatermarkingTab(60), "Watermarking Tab", action.BOOLEAN);
//					notSetLabels.add("No watermarking label is set. Due to: "+msg);
					return null;
				} else {
					appLog.info("Successfully saved the watermarking setting.");
				}
			} else {
				appLog.error("Not able to save watermarking setting.");
				notSetLabels.add("Not able to save watermarking setting.");
			}
		
		return notSetLabels;
	}
	
	/**
	 * 
	

	/**
	 * 
	 * @return true/false
	 */
	public boolean clickOnEditIcon() {
		try {
			if(click(driver,getEditIconOnDRMTab(60), "Edit Icon", action.BOOLEAN)){
				appLog.info("clicked on Edit Icon Successfully.");
				return true;
			}else {
				appLog.error("Edit Icon is not clickable.");
				 throw new Exception();
			}
		}catch (Exception e) {
			appLog.info("Trying once again...");
			for(int i=0; i<2; i++) {
				if(click(driver,getEditIconOnDRMTab(60), "Edit Icon", action.BOOLEAN)){
					appLog.info("clicked on Edit Icon Successfully.");
					break;
				}else {
					if(i==1) {
						appLog.error("Edit Icon is not clickable.");
						return false;						
					}
				}				
			}
			return true;
		}		
	}
	
	/**
	 * 
	 * @return true/false
	 */
	public boolean clickOnProfileTab() {
		try {
			if (click(driver,getProfileTab(60), "Profile Tab", action.SCROLLANDBOOLEAN)) {
				appLog.info("Clicked On Profile Tab");
				if(getProfileTabLabelText(20)!=null) {
					appLog.info("profile Tab is loaded successfully.");
				}		
			}else {
				appLog.error("Not able to click on Profile Tab");
				throw new Exception();
			}
		}catch (Exception e) {
			appLog.info("Trying one more time.....");
			if (click(driver,getProfileTab(60), "Profile Tab", action.SCROLLANDBOOLEAN)) {
				appLog.info("Clicked On Profile Tab");
				if(getProfileTabLabelText(20)!=null) {
					appLog.info("profile Tab is loaded successfully.");
				}		
			}else {
				appLog.error("Not able to click on Profile Tab");
				return false;
			}
		}
		return true;
	}
	
	/**
	 * 
	 * @return true/false
	 */
	public boolean clickOnMyFirmProfileTab() {
		try {	
			if (click(driver,getMyFirmsProfileTab(60), "My Firm Profile Tab", action.SCROLLANDBOOLEAN)) {
				appLog.info("Clicked On My Firm Profile Tab");
				if(getFirmProfileHeaderText(20)!=null) {
					appLog.info("My Firm Profile Tab is loaded successfully.");
				}		
			}else {
				appLog.error("Not able to click on My Firm Profile Tab");
				throw new Exception();
			}
		}catch (Exception e) {
			// TODO: handle exception
			appLog.info("Trying one more time....");
			if (click(driver,getMyFirmsProfileTab(60), "My Firm Profile Tab", action.SCROLLANDBOOLEAN)) {
				appLog.info("Clicked On My Firm Profile Tab");
				if(getFirmProfileHeaderText(20)!=null) {
					appLog.info("My Firm Profile Tab is loaded successfully.");
				}		
			}else {
				appLog.error("Not able to click on My Firm Profile Tab");
				return false;
			}
		}
		return true;
	}

	/**
	 * 
	 * @return true/false
	 */
	public List<String> getallDealNameFromDropDownList(){
		List<String> lst = new ArrayList<>();
		if(click(driver, getDealSelectionList(60), "Select Deal text box", action.BOOLEAN)){
			List<WebElement> deallist=dealList();
			if(!deallist.isEmpty()){
				for(int i=0; i<deallist.size(); i++){
					lst.add(deallist.get(i).getText().trim());
				}
			}else {
				appLog.error("No deal are available in the list");
				return null;
			}
		}else {
			appLog.error("Not able to click on Select Deal text box so cannot get all deal name list");
			return null;
		}
		return lst;
	}
	
	/**
	 * 
	 * @param fname
	 * @param lname
	 * @return true/false
	 */
	public boolean checkGlobalSettingforCRMUser(String fname, String lname) {
		if (clickOnTab(SmokeCommonVariables.EnvironmentVariable, SmokeCommonVariables.ModeVariable, TabName.DRMTab)) {
			if (isSelected(driver, getGlobalRadioBtnInViewMode(10), "Global Radio Button")) {
				appLog.info("Global Radio Button Already Selected");
				if (clickOnEditIcon()) {
					if (verifyUserSelectedAfterGivingAccess(fname + " " + lname, "GlobalEditMode", null, null)) {
						appLog.info("CRM User is selected " + fname + " " + lname);
					} else {
						appLog.info("CRM User is not selected: " + fname + " " + lname);
						WebElement ele = isDisplayed(driver,
								FindElement(driver, "(//span[text()='" + fname + " " + lname + "']/../..//input)[2]",
										"User Radio button", action.SCROLLANDBOOLEAN, 60),
								"visibility", 60, "User Radio button");
						if (ele != null) {
							if (click(driver, ele, fname + " " + lname + " radio Box", action.BOOLEAN)) {
								appLog.info("Clicked on CRM User radio button: " + fname + " " + lname);
							} else {
								appLog.error("Not able to click on CRM User " + fname + " " + lname
										+ "  Radio Button so cannot give admin user access");
								return false;
							}
							if (click(driver, getAdminUserTabSaveButton(30), "Save Button", action.SCROLLANDBOOLEAN)) {
								appLog.info("click on save button");
								if (getAdminUserSuccessMediumCloseButton(10) != null) {
									if (click(driver, getAdminUserSuccessMediumCloseButton(10),
											"Admin user Success close button", action.BOOLEAN)) {
										appLog.info("Clicked on Close button");
									} else {
										appLog.error("Not able to click on close button");
										return false;
									}
								} else if (getAdminUserSuccessSmallCloseButton(10) != null) {
									if (click(driver, getAdminUserSuccessSmallCloseButton(10),
											"Admin user Success close button", action.BOOLEAN)) {
										appLog.info("Clicked on Close button");
									} else {
										appLog.error("Not able to click on close button");
										return false;
									}
								}
							} else {
								appLog.error("Not able to click on save button cannot give admin user access to "
										+ fname + " " + lname + "");
								return false;
							}
						} else {
							appLog.error(fname + " " + lname
									+ " is not visible on admin user grid so cannot give admin user access");
							return false;
						}
					}
				} else {
					appLog.error("Not able to click on edit icon so cannot check user access: " + fname + " " + lname);
					return false;
				}
			} else {
				appLog.info("Global Radio Button Not Selected");
				appLog.info("Trying to Switch Global Setting");
				List<String> lst = getallDealNameFromDropDownList();
				int i = 0;
				for (String string : lst) {
					i++;
					switchToDefaultContent(driver);
					switchSetting("Global", string, fname, lname, "Admin Users", "No", null, null, "Yes");
					if (verifyUserSelectedAfterGivingAccess(fname + " " + lname, "Global", null, null)) {
						appLog.info("CRM User is selected " + fname + " " + lname);
						break;
					} else {
						appLog.info("CRM User 1 is not selected " + fname + " " + lname);
						continue;
					}
				}
			}
		} else {
			appLog.error("Deal room tab is not loaded so cannot check CRM User access: " + fname + " " + lname);
			return false;
		}
		return true;
	}

	/**
	 * 
	 * @description- This method is used to change settings from global to by deal and by deal to global.
	 */
	public void switchSetting(String SettingType, String DealName, String userFirstName, String userLastName,
			String pagename, String switchManageApprovalAndWatermarkingSetting, String typeOfManageApprovalSetting,
			String typeOfWatermarkingSetting, String globalUseraccess,String CheckLinks) {
		SoftAssert sa = new SoftAssert();
		try{
		clickOnDRMTab();
		ThreadSleep(2000);
		if(!switchToFrame(driver, 20, getDealRoomManagerFrame(20))){
			throw new Exception();
		}
		ThreadSleep(2000);
		click(driver, getAdminUserTab(120), "Admin User Tab", action.THROWEXCEPTION);
		}catch(Throwable e){
		switchToDefaultContent(driver);
		clickOnTab(SmokeCommonVariables.EnvironmentVariable,SmokeCommonVariables.ModeVariable, TabName.DRMTab);
		ThreadSleep(2000);
		clickOnDRMTab();
		ThreadSleep(2000);
		switchToFrame(driver, 120, getDealRoomManagerFrame(120));
		click(driver, getAdminUserTab(120), "Admin User Tab", action.THROWEXCEPTION);
		}	
		if (SettingType.equalsIgnoreCase("Global")) {
			ThreadSleep(2000);
			if (!selectDealFromList(DealName, pagename)) {
				appLog.info("Deal Is not selected from select deal auto complete textbox");
				sa.assertTrue(false, "Deal Is not selected from select deal auto complete textbox");
			} else {
				appLog.info("Deal Is selected from select deal auto complete textbox");
			}
			ThreadSleep(4000);
			click(driver, getEditIconOnDRMTab(120), "Edit icon on DRM tab", action.THROWEXCEPTION);
			ThreadSleep(2000);
			click(driver, getGlobalRadioButton(120), "Global Radio Button", action.THROWEXCEPTION);
			ThreadSleep(10000);
			if (globalUseraccess.equalsIgnoreCase("Yes")) {
				WebElement adminusercheckbox = isDisplayed(driver,
						FindElement(driver,
								"//span[@id='grid_AdminUsers2']//span[text()='" + userFirstName + " " + userLastName
										+ "']/../..//input",
								"Users Name", action.THROWEXCEPTION, 60),
						"visibility", 60, "Internal User Check Box", action.THROWEXCEPTION);
				click(driver, adminusercheckbox, "Users Name", action.THROWEXCEPTION);
				// giveDealAccessToUser()
				appLog.info("Admin user checkbox is clicked successfully");
			} else {
				appLog.info("There is no need to provide user access when setting is global");
			}
			click(driver, getAdminUserTabSaveButton(120), "Save Button", action.SCROLLANDTHROWEXCEPTION);
			click(driver, getByDealSaveErrorMessageYesBtn(120), "Switch Setting Error Message Yes Button",
					action.SCROLLANDTHROWEXCEPTION);
			if (switchManageApprovalAndWatermarkingSetting.equalsIgnoreCase("Yes")) {
				changeManageApprovalSettingOnSuccessPopup(typeOfManageApprovalSetting);
				changeWatermarkingSettingOnSuccessPopup(typeOfWatermarkingSetting);
			}
			click(driver, getSuccessLargeSaveButton(120), "Save Button", action.SCROLLANDTHROWEXCEPTION);

		} else if (SettingType.equalsIgnoreCase("By Deal")) {
			ThreadSleep(4000);
			click(driver, getEditIconOnDRMTab(120), "Edit icon on DRM tab", action.THROWEXCEPTION);
			ThreadSleep(3000);
			click(driver, getByDealRadioButton(120), "By deal radio button", action.THROWEXCEPTION);
			sendKeys(driver, getDealSelectionListEditMode(120), DealName, "Deal Select TextBox",
					action.SCROLLANDTHROWEXCEPTION);
			ThreadSleep(5000);
			click(driver, FindElement(driver, "//ul[contains(@class,'ui-autocomplete')]//a[text()='"+DealName+"']", "Deal List", action.THROWEXCEPTION, 20),
					DealName + " Deal", action.BOOLEAN);
			appLog.info(DealName + " Deal is present in list.");
			// if(!selectDealFromList(DealName,pagename)){
			// appLog.info("Deal Is not selected from select deal auto complete
			// textbox");
			// sa.assertTrue(false,"Deal Is not selected from select deal auto
			// complete textbox");
			// } else{
			// appLog.info("Deal Is selected from select deal auto complete
			// textbox");
			// }
			ThreadSleep(5000);
			WebElement adminusercheckbox = isDisplayed(driver,
					FindElement(driver,
							"//span[@id='grid_AdminUsers2']//span[text()='" + userFirstName + " " + userLastName
									+ "']/../..//input",
							"Users Name", action.THROWEXCEPTION, 60),
					"visibility", 60, "Internal User Check Box", action.THROWEXCEPTION);
			click(driver, adminusercheckbox, "Users Name", action.THROWEXCEPTION);
			appLog.info("Admin user checkbox is clicked successfully");
			click(driver, getAdminUserTabSaveButton(120), "Save Button", action.SCROLLANDTHROWEXCEPTION);
			click(driver, getByDealSaveErrorMessageYesBtn(120), "Switch Setting Error Message Yes Button",
					action.SCROLLANDTHROWEXCEPTION);
			if (switchManageApprovalAndWatermarkingSetting.equalsIgnoreCase("Yes")) {
				changeManageApprovalSettingOnSuccessPopup(typeOfManageApprovalSetting);
				changeWatermarkingSettingOnSuccessPopup(typeOfWatermarkingSetting);
			}
			
			if(CheckLinks.equalsIgnoreCase("Yes")){
			if(click(driver,getSwitchSettingPopupManageApprovalLink(60), "Manage Approval Link", action.SCROLLANDBOOLEAN))	{
				String parentID = switchOnWindow(driver);
				if (parentID != null) {
					appLog.info("Successfully switched to new window");
					switchToFrame(driver, 30, getDealRoomManagerFrame(60));	
					String manageApprovalText=getDRMTabsLandingTextMsg(60).getText().trim();
					if(manageApprovalText.equalsIgnoreCase("Manage Approvals")){
						appLog.info("Manage Approval Page is displaying");
					}else{
						appLog.error("Manage Approval Page is not displaying");
						sa.assertTrue(false, "Manage Approval Page is not displaying");
					}
					driver.close();
					driver.switchTo().window(parentID);
					switchToFrame(driver, 30, getDealRoomManagerFrame(60));	
				} else {
					appLog.error("Not able to switch to new window");
					sa.assertTrue(false, "Not able to switch to new window");
				}
			}else{
				appLog.info("Not able to click on manage Approval Link");
				sa.assertTrue(false, "Not able to click on manage Approval Link");
			}
			if(click(driver,getSwitchSettingPopupWatermarkingLink(60), "Watermarking Link", action.SCROLLANDBOOLEAN))	{
				String parentID = switchOnWindow(driver);
				if (parentID != null) {
					appLog.info("Successfully switched to new window");
					switchToFrame(driver, 30, getDealRoomManagerFrame(60));
					String WatermarkingText=getDRMTabsLandingTextMsg(60).getText().trim();
					if(WatermarkingText.equalsIgnoreCase("Watermarking")){
						appLog.info("Watermarking Page is displaying");
					}else{
						appLog.error("Watermarking Page is not displaying");
						sa.assertTrue(false, "WatermarkingPage is not displaying");
					}
					driver.close();
					driver.switchTo().window(parentID);
					switchToFrame(driver, 30, getDealRoomManagerFrame(60));	
				} else {
					appLog.error("Not able to switch to new window");
					sa.assertTrue(false, "Not able to switch to new window");
				}
			}else{
				appLog.info("Not able to click on Watermarking Link");
				sa.assertTrue(false, "Not able to click on Watermarking Link");
			}			
			}else{
				appLog.info("There is no need to check manage Approvals and Watermarking links");
			}
		click(driver, getSuccessLargeSaveButton(120), "Save Button", action.SCROLLANDTHROWEXCEPTION);
		}
		sa.assertAll();

	}
	
	/**
	 * 
	 * @param excelPath
	 * @param userFirstName
	 * @param userLastName
	 * @param typeOfUser
	 * @param checkErrorMessage
	 * @param landingPage
	 */
	public void UserRegistrationAndErrorMsgCheck(String excelPath, String userFirstName, String userLastName, String typeOfUser,
			String checkErrorMessage, String landingPage) {
		SoftAssert sa = new SoftAssert();
		WebElement ele=null;
		clickOnDRMTab();
		switchToFrame(driver, 60, getDealRoomManagerFrame(60));

		if (typeOfUser.equalsIgnoreCase("SuperAdmin")) {
			click(driver, getNavatarSalesTeamLink(60), "Navatar Sales Team Link.", action.THROWEXCEPTION);

			if (!verifyNavatarSalesTeamLinkFunctionality("NavatarSalesTeamLink")) {
				sa.assertTrue(false, "Verification of navatar sales team link is unsuccessfull.");
			} else {
				appLog.info("Verification of the Navatar Sales Team Link is successfull.");
			}
			click(driver, getStartButton(60), "Start Button", action.THROWEXCEPTION);
		}
		if (checkErrorMessage.equalsIgnoreCase("Yes")) {
			getFirstName(60).clear();
			appLog.info("First Name Field is Cleared");
			clickonRegisterButton();
			String firstNameErrorMsg = getFirstNameErrorMsg(60).getText().trim();
			sa.assertTrue(firstNameErrorMsg.contains(DRMPageErrorMessage.CRMUserNamewhileRegistration),
					"First Name Error Message is not matched.");
			appLog.info("First Name error message is verified successfully.");
			sendKeys(driver, getFirstName(60), ExcelUtils.readData(excelPath, "User", 1, 1), "First Name", action.THROWEXCEPTION);
			getLastName(60).clear();
			appLog.info("Last Name Field is cleard.");
			clickonRegisterButton();
			String lastNameErrorMsg = getLastNameErrorMsg(60).getText().trim();
			sa.assertTrue(lastNameErrorMsg.contains(DRMPageErrorMessage.CRMUserNamewhileRegistration),
					"Last Name Error Message is not matched.");
			appLog.info("Last Name error message is verified successfully.");
			// sendKeys(driver,getLastName(60), ExcelUtils.readData(excelPath, "User", 1,
			// 1), "Last Name", action.THROWEXCEPTION);
			getFirstName(60).clear();
			checkFirstAndLastName(ExcelUtils.readData(excelPath, "User", 1, 7), ExcelUtils.readData(excelPath, "User", 1, 8));
//			if (!click(driver, getRegisterButton(60), "Register Button", action.BOOLEAN)) {
//				appLog.info("Register Button successfully clicked.");
//			}
			clickUsingCssSelectorPath(driver, CssPath.cssPathForRegisterButton, "Register Button");
			String errorMsg = switchToAlertAndGetMessage(driver, 60, action.GETTEXT);
			sa.assertTrue(errorMsg.contains(DRMPageErrorMessage.specialCharacterErrorMsg),
					"Special Character Message is not verified.");
			switchToAlertAndAcceptOrDecline(driver, 60, action.ACCEPT);
			checkFirstAndLastName(ExcelUtils.readData(excelPath, "User", 1, 9), ExcelUtils.readData(excelPath, "User", 1, 10));
//			if (!click(driver, getRegisterButton(60), "Register Button", action.BOOLEAN)) {
//				appLog.info("Register Button clicked successfully.");
//			}
			
			
			clickUsingCssSelectorPath(driver, CssPath.cssPathForRegisterButton, "Register Button");
			String maxCharErrorMsg = switchToAlertAndGetMessage(driver, 60, action.GETTEXT);
			sa.assertTrue(maxCharErrorMsg.contains(DRMPageErrorMessage.maxCharacterErrorMsg),
					"Max Character Message is not verified.");
			switchToAlertAndAcceptOrDecline(driver, 60, action.ACCEPT);
			isDisplayed(driver, getFirstName(60), "visibility", 60, "First Name").clear();
			isDisplayed(driver, getLastName(60), "visibility", 60, "First Name").clear();
			checkFirstAndLastName(userFirstName, userLastName);
		}
		clickonRegisterButton();
		driver.switchTo().defaultContent();
		String allowaccess = isDisplayed(driver, getAllowAccessLabelText(60), "visibility", 60, "Allow Access Text")
				.getText();
		sa.assertTrue(allowaccess.contains(DRMPageErrorMessage.allowAccessMsg), DRMPageErrorMessage.allowAccessMsg+" Text is not matched.");
		if (checkErrorMessage.equalsIgnoreCase("Yes")) {
			click(driver, getDenyButton(60), "Deny Button", action.THROWEXCEPTION);
			ele =  isDisplayed(driver, getDeniedAccessLabeltext(20), "visibility", 20,
					"Remote Access denied Text.");
			if (ele!=null) {
				String accessdeniedtext =ele.getText().trim();	
				sa.assertTrue(accessdeniedtext.contains(DRMPageErrorMessage.loginAuthenticationErrorMsg),DRMPageErrorMessage.loginAuthenticationErrorMsg);
			} else {
				appLog.info(DRMPageErrorMessage.loginAuthenticationErrorMsg+" is not visible ");
				sa.assertTrue(false,DRMPageErrorMessage.loginAuthenticationErrorMsg+" is not visible ");
			}
			
			
			driver.navigate().back();
		}
		click(driver, getAllowButton(60), "Allow Button", action.THROWEXCEPTION);
		switchToFrame(driver, 60, getDealRoomManagerFrame(60));
		String registrationMsg = isDisplayed(driver, getRegistrationsuccessfulText(60), "visibility", 60,
				"Registration Successful Text").getText().trim();
		sa.assertTrue(registrationMsg.contains(DRMPageErrorMessage.registrationSuccessfulErrorMsg),
				"Registration Successful Text is not matched.");
		click(driver, getRegistrationSuccessfulPopUpCloseButton(60), "Registration close button",
				action.THROWEXCEPTION);
		if (typeOfUser.equalsIgnoreCase("SuperAdmin")) {
			appLog.info("SuperAdmin Registration is successfully.");
			String adminUserText = isDisplayed(driver, getAdminUserLabelText(60), "visibility", 60,
					"Admin User label text").getText().trim();
			sa.assertTrue(adminUserText.contains("Admin Users"), "Admin User label is not matached");
			appLog.info("Super Admin Default landing page is verified successfully.");
		}
		if (typeOfUser.equalsIgnoreCase("CRMUser")) {
			appLog.info("CRM User Registration is successfully.");
			String adminUserText = isDisplayed(driver, getDRMTabsLandingTextMsg(60), "visibility", 60,
					"Admin User label text").getText().trim();
			sa.assertTrue(adminUserText.contains(landingPage), "CRM User label is not matached");
			appLog.info("CRM User Default landing page is verified successfully.");
		}

		driver.switchTo().defaultContent();
		sa.assertAll();
	}
	
	/**
	 * 
	 * @param ContactFirstName
	 * @param ContactLastNAme
	 * @return true/false
	 */
	public boolean verifyContactInContactAccessTabInDRM(String ContactFirstName, String ContactLastNAme) {
		
		if (!click(driver, getContactAccessTab(120), "Contact Access Tab", action.SCROLLANDBOOLEAN)) {
			appLog.info("Not Able to Click On Contact Access Tab");
			return false;
		}
		ThreadSleep(2000);
		sendKeys(driver, getContactAccessSearchBox(60), ContactFirstName + " " + ContactLastNAme,
				"Contact Access Search Box", action.SCROLLANDBOOLEAN);
		if (!click(driver, getSearchIcon().get(0), "Search Icon On Contact Access Popup", action.SCROLLANDBOOLEAN)) {
			appLog.info("Not Able to click on search Icon On Contact Access Popup");
			return false;
		}
		WebElement removeLink = FindElement(driver,
				"//a[text()='" + ContactFirstName + " " + ContactLastNAme + "']/../..//span//a[text()='Remove']",
				"Remove link", action.SCROLLANDBOOLEAN, 20);
		if (isDisplayed(driver, removeLink, "visibility", 20, "Remove Link") != null) {
			return true;
		}
		return false;
	}

	// Version 2 Changes
	/**
	 * 
	 * @param ele
	 * @return true/false
	 */
	public int labelColumnIndex(WebElement ele){
		
		if(ele!=null){
		String attributeValue = ele.getAttribute("id");
		appLog.info("getIntegerFromString(attributeValue).get(0) : "+getIntegerFromString(attributeValue).get(0));
		return getIntegerFromString(attributeValue).get(0);
		}else{
			return 5;	
		}		
	}
	
	/**
	 * @return true/false the firmNameInContactAccessTabGrid
	 */
	public List<WebElement> getFirmNameInContactAccessTabGrid(int timeout) {
		List<WebElement> eleList = new ArrayList<WebElement>();
		int i =labelColumnIndex(getFirmLabelContactAccessTab(timeout));
		appLog.info("Column Label Index : "+i);
		eleList=FindElements(driver, "//span[contains(@id,'grid_ContactAccess-cell-"+i+"')]", "Firm Name List");
		return eleList;
		
	}
	
	/**
	 * @return true/false the getcontactAccessFirmName
	 */
	public List<WebElement> getcontactAccessFirmName() {
		List<WebElement> eleList = new ArrayList<WebElement>();
		int i =labelColumnIndex(getExternalAdminLabelContactAccessTab(30))+3;
		appLog.info("Column Label Index : "+i);
		eleList=FindElements(driver, "//span[contains(@id,'grid_ContactAccess-rows')]/span//span["+i+"]//label", "Firm Name List");
		return eleList;
		
	}
	
	/**
	 * 
	 * @param excelPath
	 * @param sheetName
	 * @return true/false
	 */
	public static List<List<String>> folderStructureContentOrdering(String excelPath, String sheetName) {
		int i = 2;
		List<List<String>> depthOfFolder = new ArrayList<List<String>>();
		List<String> folders = new ArrayList<String>();
		while (true) {
			String value = ExcelUtils.readData(excelPath, sheetName, i, 0);
			if (value != null) {
				int totalValues = ExcelUtils.getLastColumn(excelPath, sheetName, i);
				System.err.println(totalValues);
				for (int j = 1; j < totalValues; j++) {
					String path = ExcelUtils.readData(excelPath, sheetName, i, j);
					folders.add(path);
				}
			} else {
				break;
			}
			depthOfFolder.add(folders);
			folders = new ArrayList<String>();
			i++;
		}
		return depthOfFolder;
	}
	
	/**
	 * 
	 * @param moveFolder
	 * @param excelPath
	 * @param sheetName
	 * @return true/false
	 */
	public static List<String> moveFrom(String moveFolder, String excelPath, String sheetName){
		List<List<String>> depthOfFolder = folderStructureContentOrdering(excelPath, sheetName);
		List<String> listOfMovedFolders = new ArrayList<String>();
		for(int i = 0; i < depthOfFolder.size(); i++){
			if(depthOfFolder.get(i).contains(moveFolder.split("/")[0])){
				for(int j = 0; j < depthOfFolder.get(i).size(); j++){
					String[] folders = depthOfFolder.get(i).get(j).split(",");
					List<String> listOfFolders = new ArrayList<String>();
					for (String folder : folders) {
						listOfFolders.add(folder);
					}
					for(int k = 0; k < listOfFolders.size(); k++){
						if(listOfFolders.get(k).contains(moveFolder)){
							listOfMovedFolders.add(listOfFolders.get(k));
							listOfFolders.remove(k);
							depthOfFolder.get(i).set(j, CommonLib.createStringOutOfList(listOfFolders));
							ExcelUtils.writeDataInExcel(CommonLib.createStringOutOfList(listOfFolders), sheetName, i+2, j+1);
						} else {
							if(j == depthOfFolder.get(i).size()-1){
//								System.err.println("folder is not present in the folder structure in excel");
							}
						}
					}
				}
				break;
			} else {
				System.err.println("Not Found");
			}
		}
		return listOfMovedFolders;
	}
	
	
	/**
	 * 
	 * @param excelPath
	 * @param folderTemplateName
	 * @param description
	 * @param pageName
	 * @param folderTemplateOrderType
	 * @return true/false
	 */
	public boolean createFolderTemplate(String excelPath, String folderTemplateName, String description, String pageName, FolderTemplateOrderType folderTemplateOrderType) {
		boolean flag;
		ThreadSleep(5000);
		if (click(driver, getAddFolderTemplateButton(60), "Add Folder Template Button", action.BOOLEAN)) {
			if (!sendKeys(driver, getFolderTemplateNameTextBox(60), folderTemplateName, "Folder template text box.",
					action.BOOLEAN)) {
				exit("Folder Template name text box is not visible Skipping this test case.");
				return false;
			}
			if (!sendKeys(driver, getFolderTemplateDescription(60), description, "Description Text Box",
					action.BOOLEAN)) {
				appLog.info("Not able to enter description.");
				flag = false;
			}
			ThreadSleep(2000);
			if (!folderTemplateOrderType.toString().equals(FolderTemplateOrderType.Alphabetical.toString())) {
				appLog.info("Inside ORDER tYPE");
				if (!selectVisibleTextFromDropDown(driver, getFolderTemplateOrderType(10), "Order Type : "+folderTemplateOrderType.toString(), folderTemplateOrderType.toString())) {
					appLog.info("Not able to Select Order Type : "+folderTemplateOrderType.toString());
					flag = false;
				}else{
					appLog.info("Selected Order Type : "+folderTemplateOrderType.toString());
				}
			}else{
				appLog.info("NOT INSIDE FOLDER TEMPLATE ORDER TYPE");
			}
			
			ThreadSleep(20000);
			Map<String, String> s = folderStructureInExcel(excelPath);
			Set<String> paths = s.keySet();
			Iterator<String> i = paths.iterator();
			i = paths.iterator();
			while (i.hasNext()) {
				String string = i.next();
				if (string.isEmpty())
					continue;
				createFolderStructure(string, s.get(string), description, pageName);
			}
			if (click(driver, getFolderTemplatePageSaveButton(60), "Folder Template Save Button",
					action.SCROLLANDBOOLEAN)) {
				if (click(driver, getConfirmSaveYesButton(60), "Confirm Save Yes Button", action.BOOLEAN)) {
					if (click(driver, getGoBackLink(60), "Go back link", action.SCROLLANDBOOLEAN)) {
						if (FindElement(driver, "//a[@title='" + folderTemplateName + "']", "Template Name",
								action.BOOLEAN, 60) != null) {
							appLog.info(folderTemplateName + " :Template has been created successfully.");
							return true;
						}
					}
				}
				return false;
			} else {
				return false;
			}

		} else {
			appLog.info(
					"Add Folder Template button is not visible. Cannot create folder template. Skipping this testcase");
			exit("Add Folder Button is not visible so cannot continue with this testcase.");
			return false;
		}
	}
	
	/**
	 * 
	 * @param folderTemplateOrderType
	 * @param timeOut
	 * @return true/false
	 */
	public WebElement getRadioButtonOrderTypeonContentOrdering(FolderTemplateOrderType folderTemplateOrderType,int timeOut){
		String xpath = "//span[text()='"+folderTemplateOrderType.toString()+"']/preceding-sibling::input";
		List<WebElement> ele = FindElements(driver, xpath, folderTemplateOrderType.toString());
		
		if (!ele.isEmpty() && ele.size()>1) {
			return ele.get(1);
		}
		
		return null;
		
	}
	
	/**
	 * 
	 * @param excelPath
	 * @param sheetName
	 * @param folderTemplateName
	 * @param description
	 * @param pageName
	 * @param folderTemplateOrderType
	 * @return true/false
	 */
	public boolean createFolderTemplate(String excelPath,String sheetName, String folderTemplateName, String description, String pageName, FolderTemplateOrderType folderTemplateOrderType) {
		boolean flag;
		ThreadSleep(5000);
		if (click(driver, getAddFolderTemplateButton(60), "Add Folder Template Button", action.BOOLEAN)) {
			if (!sendKeys(driver, getFolderTemplateNameTextBox(60), folderTemplateName, "Folder template text box.",
					action.BOOLEAN)) {
				exit("Folder Template name text box is not visible Skipping this test case.");
				return false;
			}
			if (!sendKeys(driver, getFolderTemplateDescription(60), description, "Description Text Box",
					action.BOOLEAN)) {
				appLog.info("Not able to enter description.");
				flag = false;
			}
			ThreadSleep(2000);
			if (!folderTemplateOrderType.toString().equals(FolderTemplateOrderType.Alphabetical.toString())) {
				appLog.info("Inside ORDER tYPE");
				if (!selectVisibleTextFromDropDown(driver, getFolderTemplateOrderType(10), "Order Type : "+folderTemplateOrderType.toString(), folderTemplateOrderType.toString())) {
					appLog.info("Not able to Select Order Type : "+folderTemplateOrderType.toString());
					flag = false;
				}else{
					appLog.info("Selected Order Type : "+folderTemplateOrderType.toString());
				}
			}else{
				appLog.info("NOT INSIDE FOLDER TEMPLATE ORDER TYPE");
			}
			
			ThreadSleep(20000);
			Map<String, String> s = folderStructureInExcel(excelPath,sheetName);
			Set<String> paths = s.keySet();
			Iterator<String> i = paths.iterator();
			i = paths.iterator();
			while (i.hasNext()) {
				String string = i.next();
				if (string.isEmpty())
					continue;
				createFolderStructure(string, s.get(string), description, pageName);
			}
			if (click(driver, getFolderTemplatePageSaveButton(60), "Folder Template Save Button",
					action.SCROLLANDBOOLEAN)) {
				if (click(driver, getConfirmSaveYesButton(60), "Confirm Save Yes Button", action.BOOLEAN)) {
					if (click(driver, getGoBackLink(60), "Go back link", action.SCROLLANDBOOLEAN)) {
						if (FindElement(driver, "//a[@title='" + folderTemplateName + "']", "Template Name",
								action.BOOLEAN, 60) != null) {
							appLog.info(folderTemplateName + " :Template has been created successfully.");
							return true;
						}
					}
				}
				return false;
			} else {
				return false;
			}

		} else {
			appLog.info(
					"Add Folder Template button is not visible. Cannot create folder template. Skipping this testcase");
			exit("Add Folder Button is not visible so cannot continue with this testcase.");
			return false;
		}
	}
	
	/**
	 * 
	 * @param excelPath
	 * @param sheetName
	 * @return true/false
	 */
	public Map<String, String> folderStructureInExcel(String excelPath,String sheetName) {
		int i = 2;
		Map<String, String> struct = new LinkedHashMap<String, String>();
		while (true) {
			String value = ExcelUtils.readData(excelPath, sheetName, i, 0);
			if (value != null) {
				int totalValues = ExcelUtils.getLastColumn(excelPath, sheetName, i);
				for (int j = 1; j < totalValues; j++) {
					String path = ExcelUtils.readData(excelPath, sheetName, i, j);
					String[] paths = path.split(",");
					for (int k = 0; k < paths.length; k++) {
						struct.put(paths[k], ExcelUtils.readData(excelPath, sheetName, i, 0));
					}
				}
			} else {
				break;
			}
			i++;
		}
		return struct;
	}
	
	/**
	 * 
	 * @param excelPath
	 * @param folderTemplateName
	 * @param description
	 * @param pageName
	 * @param folderTemplateOrderType
	 * @param sheetName
	 * @return true/false
	 */
	public boolean createFolderTemplate(String excelPath, String folderTemplateName, String description, String pageName, FolderTemplateOrderType folderTemplateOrderType,String sheetName) {
		boolean flag;
		ThreadSleep(5000);
		if (click(driver, getAddFolderTemplateButton(60), "Add Folder Template Button", action.BOOLEAN)) {
			if (!sendKeys(driver, getFolderTemplateNameTextBox(60), folderTemplateName, "Folder template text box.",
					action.BOOLEAN)) {
				exit("Folder Template name text box is not visible Skipping this test case.");
				return false;
			}
			if (!sendKeys(driver, getFolderTemplateDescription(60), description, "Description Text Box",
					action.BOOLEAN)) {
				appLog.info("Not able to enter description.");
				flag = false;
			}
			ThreadSleep(2000);
			if (!folderTemplateOrderType.toString().equals(FolderTemplateOrderType.Alphabetical.toString())) {
				appLog.info("Inside ORDER tYPE");
				if (!selectVisibleTextFromDropDown(driver, getFolderTemplateOrderType(10), "Order Type : "+folderTemplateOrderType.toString(), folderTemplateOrderType.toString())) {
					appLog.info("Not able to Select Order Type : "+folderTemplateOrderType.toString());
					flag = false;
				}else{
					appLog.info("Selected Order Type : "+folderTemplateOrderType.toString());
				}
			}else{
				appLog.info("NOT INSIDE FOLDER TEMPLATE ORDER TYPE");
			}
			
			ThreadSleep(20000);
			Map<String, String> s = folderStructureInExcel(excelPath, sheetName);
			Set<String> paths = s.keySet();
			Iterator<String> i = paths.iterator();
			i = paths.iterator();
			while (i.hasNext()) {
				String string = i.next();
				if (string.isEmpty())
					continue;
				createFolderStructure(string, s.get(string), description, pageName);
			}
			if (click(driver, getFolderTemplatePageSaveButton(60), "Folder Template Save Button",
					action.SCROLLANDBOOLEAN)) {
				if (click(driver, getConfirmSaveYesButton(60), "Confirm Save Yes Button", action.BOOLEAN)) {
					if (click(driver, getGoBackLink(60), "Go back link", action.SCROLLANDBOOLEAN)) {
						if (FindElement(driver, "//a[@title='" + folderTemplateName + "']", "Template Name",
								action.BOOLEAN, 60) != null) {
							appLog.info(folderTemplateName + " :Template has been created successfully.");
							return true;
						}
					}
				}
				return false;
			} else {
				return false;
			}

		} else {
			appLog.info(
					"Add Folder Template button is not visible. Cannot create folder template. Skipping this testcase");
			exit("Add Folder Button is not visible so cannot continue with this testcase.");
			return false;
		}
	}
	
	/**
	 * @author ANKIT JAISWAL
	 * @param SettingType
	 * @param DealName
	 * @param userFirstName
	 * @param userLastName
	 * @param pagename
	 * @param switchManageApprovalAndWatermarkingSetting
	 * @param typeOfManageApprovalSetting
	 * @param typeOfWatermarkingSetting
	 * @param globalUseraccess
	 * @return true/false
	 */
	public boolean ChangeDealAccessInByDeal(String SettingType, String DealName, String userFirstName, String userLastName,
			String pagename, String switchManageApprovalAndWatermarkingSetting, String typeOfManageApprovalSetting,
			String typeOfWatermarkingSetting, String globalUseraccess) {
		SoftAssert sa = new SoftAssert();
		Boolean flag = false;
		ThreadSleep(2000);
		switchToFrame(driver, 120, getDealRoomManagerFrame(120));
		ThreadSleep(4000);
		if(click(driver, getAdminUserTab(120), "Admin User Tab", action.BOOLEAN)) {
			appLog.info("Clicked on Admin User Tab");
			ThreadSleep(2000);
			if(selectDealFromList(DealName, "Admin Users")) {
				appLog.info("enter value in text box : "+DealName);
				ThreadSleep(2000);
//				WebElement ele=FindElement(driver, "//ul[contains(@class,'ui-autocomplete')]//a[text()='"+DealName+"']", "Deal List", action.SCROLLANDBOOLEAN, 20);
//				if(clickUsingJavaScript(driver,ele,DealName+" deal Name text")) {
//					appLog.info("Clicked on deal Name "+DealName);
//					
//				}else {
//					appLog.error("Not able to select deal name from drop down "+DealName+" so cannot switch setttings from global to by deal");
//					return false;
//				}
			}else {
				appLog.error("Not able to enter deal name in text box "+DealName+" so cannot switch settings to global to by deal");
				return false;
			}
			if(clickUsingJavaScript(driver, getEditIconOnDRMTab(120), "Edit icon on DRM tab")) {
				if(clickUsingJavaScript(driver, getByDealRadioButton(120), "By deal radio button")) {
					appLog.info("clicked on by deal radio button");
					ThreadSleep(5000);
					WebElement adminusercheckbox = isDisplayed(driver,
							FindElement(driver,
									"//span[@id='grid_AdminUsers2']//span[text()='" + userFirstName + " " + userLastName
									+ "']/../..//input",
									"Users Name", action.THROWEXCEPTION, 60),"visibility", 60, "Internal User Check Box", action.THROWEXCEPTION);
					if(clickUsingJavaScript(driver, adminusercheckbox, "user check box ")) {
						appLog.info("clicked on user name check box "+userFirstName+" "+userLastName);
						if(clickUsingJavaScript(driver, getAdminUserTabSaveButton(120), "Save Button")) {
							appLog.info("clicked on admin user tab save button");
							ThreadSleep(2000);
							if(clickUsingJavaScript(driver, getAdminUserSuccessSmallCloseButton(120), "Switch Setting Error Message Yes Button")) {
								appLog.info("clicked on Close button");
								flag = true;
							}else {
								appLog.error("Not able to click on Close button so cannot close the popUp");
							}
						}else {
							appLog.info("Not able to click on admin user tab save button so cannot switch settings from global to by deal");
						}
					}else {
						appLog.error("Not able to select user "+userFirstName+" "+userLastName+" so cannot switch setting from global to by deal");
					}
				}else {
					appLog.error("Not able to select by deal radio button so cannot switch setting to global to by deal");
				}
			}else {
				appLog.error("Not able to click on edit  icon so cannot switch swtting to global to by deal");
			}

		}else {
			appLog.error("Not able to click on admin user tab so cannot switch settings");
			sa.assertTrue(false, "Not able to click on admin user tab so cannot switch settings");
		}
		switchToDefaultContent(driver);
		return flag;

	}
	
	
}

