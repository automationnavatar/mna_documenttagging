package com.navatar.scripts;


import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.navatar.generic.BaseLib;
import com.navatar.generic.ExcelUtils;
import com.navatar.generic.EnumConstants.action;
import com.navatar.generic.EnumConstants.excelLabel;
import com.navatar.pageObjects.LoginPageBusinessLayer;
import static com.navatar.generic.CommonVariables.*;
import static com.navatar.generic.CommonLib.*;
import static com.navatar.generic.ExcelUtils.*;
public class SmokeTestCases extends BaseLib {
	String passwordResetLink = null;
	
	
	@Parameters({ "environment", "mode" })
	@Test
	public void MnASmokeTc001_1_createCRMUserAndResetPassword(String environment, String mode) {
		LoginPageBusinessLayer lp = new LoginPageBusinessLayer(driver);
		String[] userName=readAllDataForAColumn(ExcelUtils.OrgStatusExcelSheet, "OrgCredential", 0, false).split(",");
		String[] password=readAllDataForAColumn(ExcelUtils.OrgStatusExcelSheet, "OrgCredential", 1, false).split(",");
		String updatedpassword=null;
		for(int i=0; i<userName.length; i++) {
			if(lp.checkOrgStatus(userName[i], password[i])) {
				appLog.info("Org Status is checked for user : "+userName[i]);
				ThreadSleep(3000);
				String orgStatus=ExcelUtils.readData(ExcelUtils.OrgStatusExcelSheet,"OrgCredential",excelLabel.UserName,userName[i], excelLabel.OrgStatus);
				if(orgStatus.equalsIgnoreCase("Login Sucessful")) {
					updatedpassword=ExcelUtils.readData(ExcelUtils.OrgStatusExcelSheet,"OrgCredential",excelLabel.UserName,userName[i], excelLabel.UpdatePassword);
					if(!updatedpassword.isEmpty() || updatedpassword!=null) {
						if(click(driver, lp.getSalesForceLightingIcon(30), "login icon", action.BOOLEAN)) {
							appLog.info("clicked on Login Icon");
							if(click(driver, lp.getSettings_icon(30), "settings link", action.SCROLLANDBOOLEAN)) {
								appLog.info("clicked on settings link");
								ThreadSleep(3000);
								if(click(driver, lp.getChangePasswordLink(30), "change password link", action.SCROLLANDBOOLEAN)) {
									appLog.info("clicked on change password link ");
									if(switchToFrame(driver, 60, lp.getChangePasswordIframe(60))) {
										appLog.info("switch in change password frame ");
										
										if(sendKeys(driver, lp.getCurrentPassword(30), password[i], "current password text box ", action.SCROLLANDBOOLEAN)) {
											appLog.info("Enter current value : "+password[i]);
											if(sendKeys(driver, lp.getChangePasswordNewPassword(30), updatedpassword, "new password text box ", action.SCROLLANDBOOLEAN)) {
												appLog.info("Enter new password value : "+updatedpassword);
												
												if(sendKeys(driver, lp.getChangepasswordconfirmPassword(30), updatedpassword, "new password text box ", action.SCROLLANDBOOLEAN)) {
													appLog.info("Enter confirm new password  value : "+updatedpassword);
													
													if(click(driver, lp.getChangePasswordSaveButton(30), "save button", action.SCROLLANDBOOLEAN)) {
														appLog.info("clicked on save button");
														ThreadSleep(3000);
														if(lp.getChangePasswordSuccessfulMsg(30)!=null) {
															String msg = lp.getChangePasswordSuccessfulMsg(30).getText().trim();
															appLog.info("verify Password Change Message : "+msg);
															
															ExcelUtils.writeData(ExcelUtils.OrgStatusExcelSheet,"Login Successful and Password has been Updated successful","OrgCredential", excelLabel.UserName,userName[i],
																	excelLabel.OrgStatus);
															
														}else {
															appLog.error("Password Change Error Message is not visible so cannot verify Update Password Message "+userName[i]);
															sa.assertTrue(false, "Password Change Error Message is not visible so cannot verify Update Password Message "+userName[i]);
														}
														
														
													}else {
														appLog.error("Not able to click on update password save button  : "+userName[i]+" so cannot update password");
														sa.assertTrue(false, "Not able to click on update password save button  : "+userName[i]+" so cannot update password");
													}
													
													
													
												}else {
													appLog.error("Not able to pass enter confirm new  password  : "+userName[i]+" so cannot update password");
													sa.assertTrue(false, "Not able to pass enter confirm new password  : "+userName[i]+" so cannot update password");
												}
												
												
												
											}else {
												appLog.error("Not able to pass enter new password  : "+userName[i]+" so cannot update password");
												sa.assertTrue(false, "Not able to pass enter new password  : "+userName[i]+" so cannot update password");
											}
											
											
											
											
										}else {
											appLog.error("Not able to pass enter current password  : "+userName[i]+" so cannot update password");
											sa.assertTrue(false, "Not able to pass enter current password  : "+userName[i]+" so cannot update password");
										}
										
										
										
									}else {
										appLog.error("Not able to click switch on change password Iframe : "+userName[i]+" so cannot update password");
										sa.assertTrue(false, "Not able to click switch on change password Iframe : "+userName[i]+" so cannot update password");
									}
								}else {
									appLog.error("Not able to click on change password link so cannot update password "+userName[i]);
									sa.assertTrue(false, "Not able to click on change password link so cannot update password "+userName[i]);
								}
								
								
							}else {
								appLog.error("Not able to click on settings link so cannot update password "+userName[i]);
								sa.assertTrue(false, "Not able to click on settings link so cannot update password "+userName[i]);
							}
							
							
						}else {
							appLog.error("Not able to click on login icon so cannot update password "+userName[i]);
							sa.assertTrue(false, "Not able to click on login icon so cannot update password "+userName[i]);
						}
					}
				}
			}else {
				appLog.error("Not able to check Org Status for User : "+userName[i]);
				sa.assertTrue(false, "Not able to check Org Status for User : "+userName[i]);
			}
			if(i!=userName.length-1) {
				driver.close();
				config(browserToLaunch);
				 lp = new LoginPageBusinessLayer(driver);
			}
		}
		sa.assertAll();
	}
	
	
	

	
	

}
